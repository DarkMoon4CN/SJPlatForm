﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class PowerModel
    {
        public int ID { get; set; }
        public Nullable<int> RoleID { get; set; }
        public string Power_ID { get; set; }
        public string Power_Name { get; set; }
        public string ShowName { get; set; }
        public string Fid { get; set; }
        public string Path { get; set; }
        public Nullable<int> Sort { get; set; }
        public List<PowerModel> ChildPowerList { get; set; }
    }
}

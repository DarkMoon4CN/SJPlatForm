﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class DocumentAllInfosModel
    {
        public DocumentInfos DocumentInfo { get; set; }
        public List<DocumentAttachs> DocumentAttach { get; set; }
        public string[] InceptDeps { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class InfosCommInfoModel: InfosComm
    {
        public string FromName { set; get; }
        public string ToName { set; get; }
    }
}

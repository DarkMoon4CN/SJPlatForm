﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChuanYe.Models.Model
{
    public enum RecordType
    {
        火警录入=0,
        区县回复=1,
        火灾首报=2,
        误报申请=3,
        误报审批=4,
        火灾续报 =5,
        申请响应=6,
        市局询问=7,
        区县回答=8,
        启动响应=9,
        接收响应=10,
        成立前指=11,
        接收前指=12,
        市局调集扑救力量=13,
        调集提示=14,
        调集回复=15,
        火灾报灭=16,
        火灾终报=17,
        记录文档=18,
        定位火点=19,
        逐级响应=20,
        首报未按时操作=21,
        续报未按时操作=22,
        终报未按时操作=23,
        火灾转场=24
    }

    public enum FireAttachType 
    {
        Default=0,//默认附加
        Identification=1,//鉴定
        Case = 2,//立案
    }
}
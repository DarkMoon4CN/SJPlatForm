﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class MailSendModel
    {
        public int ID { set; get; }
        public Nullable<int> From { set; get; }
        public string ToName { set; get; }
        public string Title { set; get; }
        public Nullable<DateTime> PublishTime { set; get; }
        public Nullable<bool> IsView { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class View_SmsPhone
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public Nullable<int> Division { get; set; }
        public Nullable<double> DepSort { get; set; }
        public Nullable<double> UserSort { get; set; }
        public Nullable<double> PositionSort { get; set; }
        public Nullable<int>  PositionID { get; set; }
        public string Phone { get; set; }
        public Nullable<int> DepID { get; set; }
    }

    public class View_MailUsers
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public Nullable<double> Sort { get; set; }
        public Nullable<int> Division { get; set; }
        public Nullable<int> DepID { get; set; }

        public string Phone { get; set; }

        public bool? IsManager { get; set; }

    }
}

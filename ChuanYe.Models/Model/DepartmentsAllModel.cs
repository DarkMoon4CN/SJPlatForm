﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class DepartmentsAllModel
    {
        public int ID { get; set; }
        public Nullable<int> FID { get; set; }
        public string DepName { get; set; }

        public double? Sort { get; set; }

        public List<DepartmentsAllModel> ChildList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class DocumentInfosModel
    {
        public int ID { get; set; }
        public Nullable<int> publishDep { get; set; }
        public string PublishCode { get; set; }
        public string PublishPerson { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Nullable<int> InceptDep { get; set; }
        public string InceptPerson { get; set; }
        public string DocumentType { get; set; }
        public string InceptTel { get; set; }
        public string FileTitle { get; set; }
        public Nullable<bool> IsView { get; set; }
        public Nullable<System.DateTime> PublishDate { get; set; }
        public Nullable<System.DateTime> InceptDate { get; set; }
        public string InceptDepName { get; set; }
        public string PublishDepName { get; set; }
        public string InceptDepConvene { get; set; }
        public Nullable<int> IsSMS { get; set; }
        public string IsOverTime { get; set; }
        public int OverTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class UsersInfosSearchModel:UsersInfos
    {
        public string RoleName { get; set; }
        public string DepName { get; set; }
        public string PositionName { get; set; }
    }
    public class UsersInfosSearchResult
    {
        public List<UsersInfosSearchModel> UsersList { get; set; }
        public int totalRows { get; set; }
    }
}

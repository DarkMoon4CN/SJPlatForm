﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class LoginUserModel
    {
        public int ID { get; set; }
        public string UserCode { get; set; }
        public string PassWord { get; set; }
        public string UserName { get; set; }
        public Nullable<int> DepID { get; set; }
        public Nullable<int> RoleID { get; set; }
        public string Personal { get; set; }
        public Nullable<int> PositionID { get; set; }
        public string DepName { get; set; }
        public string RoleName { get; set; }
        public string AES_KEY { get; set; }
        public List<PowerModel> Powers { get; set; }
        public bool IsLogin { get; set; }
        public bool IsManager { get; set; }

        public string Address { get; set; }

        public bool? IsDisable { get; set; }

        public int? Division { get; set; }

    }

    public class LoginPower
    {
        public string Power_ID { get; set; }
        public string Power_Name { get; set; }
        public string Fid { get; set; }
        public string Path { get; set; }
        public Nullable<int> Sort { get; set; }
        public List<LoginPower> ChildPowerList { get; set; }
    }
    
}

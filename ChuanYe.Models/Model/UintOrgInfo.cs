﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class UintOrgInfo
    {
        public int UnitID { get; set; }

        public string UnitName { get; set; }

        public int U_OrgID { get; set; }
        public string O_AreaID { get; set; }
    }
}

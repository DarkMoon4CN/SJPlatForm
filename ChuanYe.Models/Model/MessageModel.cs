﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class MessageModel
    {
        public int Type { set; get;}
        public string Message { set; get; }
        public int Count { set; get; }

        public List<MessageAutomatic> Automatics { get; set; }

    }

    public class MessageAutomatic
    {
        /// <summary>
        /// 新消息兼容 消息ID
        /// </summary>
        public int M_ID { get; set; }

        /// <summary>
        /// 新消息兼容 消息根据业务附带的参数
        /// </summary>
        public string M_SendCustomData { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string M_SendContent { get; set; }


        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime? M_SendTime { get; set; }

    }


}

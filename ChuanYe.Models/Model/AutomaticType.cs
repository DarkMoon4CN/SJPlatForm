﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChuanYe.Models.Model
{

    /// <summary>
    /// M_Automatic消息类型
    /// </summary>
    public enum AutomaticType
    {
        接收火灾 = 0,
        火灾首报 = 1,
        火灾续报 = 2,
        火情询问 = 3,
        申请响应 = 4,
        接收响应 = 5,
        成立前指 = 6,
        调集其他区县扑火队 = 7,
        区县报灭 = 8,
        区县终报 = 9,
        误报申请 = 10,
        火灾报告 = 11,
        接收火灾倒计时提示 = 12,
        首报倒计时提示 = 13,
        续报倒计时提示 = 14,
        终报倒计时提示 = 15,
        调集区县回复 = 16,
        首报未作提示 = 17,
        续报未作提示 = 18,
        终报未作提示 = 19,
        火灾转场出 = 20,
        火灾转场入 = 21,
        定位火点 = 22,
        逐级响应 = 23,
        区县回复 = 24,


    }
}
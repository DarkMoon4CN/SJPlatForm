﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class OverTimeModel
    {
        public int ID { set; get; }
        public DateTime endTime { set; get; }
        public int OverTime { set; get; }
    }
}

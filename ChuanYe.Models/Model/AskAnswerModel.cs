﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models.Model
{
    public class AskAnswerModel
    {
        public int CFA_ID { get; set; }

        public int? F_FireID { get; set; }

        public string CFA_AskContent { get; set; }

        public DateTime? CFA_HandleTime { get; set; }

        public int Cou_ID { get; set; }

        public string  Cou_AnswerContent { get; set; }

        public string Cou_Answerer { get; set; }

        public DateTime? Cou_HandleTime { get; set; }

        public bool? Cou_CityRead { get; set; }

    }
}

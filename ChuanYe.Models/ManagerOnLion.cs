﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models
{
    public class ManagerOnLion
    {

        public int UserID { get; set; }

        public string UserName { get; set; }

        public double? Sort { get; set; }

        public int OnLionCount { get; set; }

        public string Phone { get; set; }
    }

    public class UsersOnLion
    {
        public string ConnectionId { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
    }

    public class FireConnection
    {
        public string ConnectionId { get; set; }
        public int FireID { get; set; }
    }
}

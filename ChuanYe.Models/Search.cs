﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models
{
    public class SmsSearch
    {
        public string Receiver { get; set; }
        public System.DateTime SendDate { get; set; }
    }

    public class SafetyInfoStatisticsSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
    }

    public class DistrictSafetyInfoStatisticsSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
    }

    public class SafetyInfoForMonthSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
    }

    public class SafetySms_SendInfoSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public string SmsInfo { get; set; }
    }

    public class NewsInfsSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DptID { get; set; }
        public int IsChecked { get; set; }
    }

    public class NewsInfsPublicSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
        public int NewsType { get; set; }
    }


    public class NewsInfsPublicGroupSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
        public List<int> NewsTypes { get; set; }
    }



    public class NewsInfsStatisticsSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepID { get; set; }
    }

    public class NewsInfsStatisticsByDepIdSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
    }

    public class NewsInfsGetListByDepIdSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
        public int NewsType { get; set; }
    }

    public class NewsTempListSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
        public int DocType { get; set; }
        public Nullable<bool> IsChecked { get; set; }
    }

    public class NewsInfsCountyStatisticsSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int DepId { get; set; }
    }

    public class ReceiveMailSearch
    {
        public int UserID { get; set; }
        public Nullable<bool> IsView { get; set; }

        public string Keyword { get; set; }
    }

    public class PublicMailSearch
    {
        public int UserID { get; set; }
        public Nullable<bool> IsView { get; set; }

        public string Keyword { get; set; }
    }

    public class ScheduleSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int UserID { get; set; }
    }

    public class MailSendSearch
    {
        public int UserID { get; set; }
        public Nullable<bool> IsView { get; set; }
        public string Keyword { get; set; }
    }

    public class DocumentInfosSendSearch
    {
        public int DepID { get; set; }
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public int ToDepID { get; set; }
        public Nullable<bool> IsView { get; set; }
        public string Title { get; set; }
        public int OverTime { get; set; }
    }

    public class DocumentInfosReceiveSearch
    {
        public int DepID { get; set; }
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public string Title { get; set; }
        public int OverTime { get; set; }
    }

    public class DocumentInfosWaitReceiveSearch
    {
        public int DepID { get; set; }
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public string Title { get; set; }
        public int OverTime { get; set; }
    }

    public class DocumentInfosReceivedSearch
    {
        public int DepID { get; set; }
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
        public string Title { get; set; }
    }
    public class DocumentInfosGetByBatchSearch
    {
        public DateTime PublishDate { get; set; }
        public string PublishPerson { get; set; }
        public int ID { get; set; }
    }
    public class PowerSearch
    {

    }
    public class DepartmentsSearch
    {

    }
    public class UsersInfosSearch
    {
        public int RoleID { get; set; }
        public string UserName { get; set; }
        public int Section { get; set; }
        
        public string Phone { get; set; }
    }
    public class DocOperationLogSearch
    {
        public DateTime stateTime { get; set; }
        public DateTime endTime { get; set; }
    }

    public class OutsideUserInfoSearch
    {
          public string UserName { get; set; }
          public string UserPhone { get; set; }
    }
}

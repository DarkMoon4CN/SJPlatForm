﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models
{
    public class LoginInfo
    {
        public int UserID { get; set; }
        public int Name { get; set; }
        public int Powers { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ChuanYe.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RolePower
    {
        public int RP_ID { get; set; }
        public Nullable<int> R_ID { get; set; }
        public string P_ID { get; set; }
        public Nullable<System.DateTime> RP_TransDatetime { get; set; }
        public string RP_TransUser { get; set; }
        public string RP_TransIP { get; set; }
    }
}

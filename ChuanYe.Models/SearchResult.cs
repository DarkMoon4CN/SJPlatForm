﻿using ChuanYe.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models
{
    public class Sms_SendInfoResult
    {
       public List<Sms_SendInfo> Sms_SendInfoList { get; set; }
       public int totalRows { get; set; }
    }

    public class SMS_ModelResult
    {
        public List<SMS_Model> SMS_ModelList { get; set; }
        public int totalRows { get; set; }
    }

    public class SafetyInfoResult
    {
        public List<SP_SafetyInfoCityList_Result> SafetyInfoList { get; set; }
        public int totalRows { get; set; }
    }

    public class SafetySms_SendInfoResult
    {
        public List<SP_SafetySms_SendInfoList_Result> SafetySms_SendInfoList { get; set; }
        public int totalRows { get; set; }
    }

    public class NewsInfsResult
    {
        public List<View_NewsInfs> NewsInfsList { get; set; }
        public int totalRows { get; set; }
    }

    public class NewsInfsStatisticsResult
    {
        public List<SP_NewsInfsStatistics_Result> NewsInfsList { get; set; }
        public int totalRows { get; set; }
    }

    public class NewsInfsGetListByDepIdResult
    {
        public List<SP_NewsInfsGetListByDepId_Result> NewsInfsList { get; set; }
        public int totalRows { get; set; }
    }
    

    public class GetAllSmsPhoneResult
    {
        public int ID { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string UserName { get; set; }
        public Nullable<double> Sort { get; set; }
        public List<ChuanYe.Models.Model.View_SmsPhone> children { get; set; }
    }

    public class GetAllMailResult
    {
        public int ID { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string UserName { get; set; }
        public Nullable<double> Sort { get; set; }
        public List<ChuanYe.Models.Model.View_MailUsers> children { get; set; }
    }

    public class NewsTempListResult
    {
        public List<SP_NewsTempList_Result> NewsTempList { get; set; }
        public int totalRows { get; set; }
    }

    public class ReceiveMailListResult
    {
        public List<ReceiveMailModel> NewsTempList { get; set; }
        public int totalRows { get; set; }
    }

    public class PublicMailResult
    {
        public List<MailPublicModel> PublicMailList { get; set; }
        public int totalRows { get; set; }
    }

    public class MailSendResult
    {
        public List<MailSendModel> MailSendModelList { get; set; }
        public int totalRows { get; set; }
    }

    public class InfosCommInfoResult
    {
        public InfosCommInfoModel InfosCommInfo { get; set; }
        public List<InfosCommAttachs> InfosCommAttachsList { get; set; }
    }

    public class ScheduleGetMonthListResult
    {
        public int S_ID { get; set; }
        public string S_Title { get; set; }
        public Nullable<DateTime> S_Date { get; set; }

        public bool? S_AutomaticState { get; set; }
        public int  M_ID{ get; set; }
    }

    public class DocumentInfosSendResult
    {
        public List<DocumentInfosModel> DocumentInfosList { get; set; }
        public int totalRows { get; set; }
    }

    public class DocumentInfosReceiveResult
    {
        public List<DocumentInfosModel> DocumentInfosList { get; set; }
        public int totalRows { get; set; }
    }

    public class DocumentInfosWaitReceiveResult
    {
        public List<DocumentInfos> DocumentInfosList { get; set; }
        public int totalRows { get; set; }
    }

    public class DocumentInfosReceivedResult
    {
        public List<DocumentInfos> DocumentInfosList { get; set; }
        public int totalRows { get; set; }
    }

    public class PowerResult
    {
        public List<PowerInfoModel> PowerInfoModelList { get; set; }
        public int totalRows { get; set; }
    }

    public class MemberShip_RolesResult
    {
        public MemberShip_Roles MemberShip_Role { get; set; }
        public List<RolePower> RolePowerList { get; set; }
    }

    public class MemberShip_RolesSearchResult
    {
        public List<MemberShip_Roles> RoleList { get; set; }
        public int totalRows { get; set; }
    }

    public class DocOperationLogResult
    {
        public List<DocOperationLog> DocOperationLogList { get; set; }
        public int totalRows { get; set; }
    }

    public class DepartmentsSearchResult
    {
        public List<Departments> DepartmentsList { get; set; }
        public int totalRows { get; set; }
    }

    public class NewsTempOneResult
    {
        public NewsTemp NewsTemp { get; set; }
        public List<NewsAttachs> NewsAttachsList { get; set; }
    }
    public class NewsInfsOneResult
    {
        public NewsInfsModel NewsInfs { get; set; }
        public List<NewsAttachs> NewsAttachsList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models
{
    public class ReceiveMailModel
    {
        public int ID { get; set; }
        public Nullable<int> From { get; set; }
        public Nullable<int> To { get; set; }
        public string FromName { get; set; }
        public string ToName { get; set; }
        public string Title { get; set; }
        public Nullable<System.DateTime> PublishTime { get; set; }
        public Nullable<bool> IsView { get; set; }
        public Nullable<int> FSState { get; set; }
        public Nullable<int> GGState { get; set; }
        public Nullable<int> JSState { get; set; }
    }
}

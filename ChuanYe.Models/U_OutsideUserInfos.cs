//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ChuanYe.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class U_OutsideUserInfos
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string UserPhone { get; set; }
        public Nullable<bool> IsSafeSMS { get; set; }
        public Nullable<bool> IsFireSMS { get; set; }
        public Nullable<bool> IsResponSMS { get; set; }
        public Nullable<bool> IsDisable { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string DepName { get; set; }
        public Nullable<int> PositionID { get; set; }
        public string PositionName { get; set; }
        public int Sort { get; set; }
    }
}

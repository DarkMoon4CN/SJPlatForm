//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ChuanYe.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class W_Warehouse
    {
        public int WH_ID { get; set; }
        public string WH_Name { get; set; }
        public string WH_Spell { get; set; }
        public string WH_Address { get; set; }
        public string WH_Phone { get; set; }
        public string WH_Capacity { get; set; }
        public string WH_Photo { get; set; }
        public Nullable<int> Entity_ID { get; set; }
        public string Entity_Name { get; set; }
        public Nullable<System.DateTime> WH_TransDatetime { get; set; }
        public Nullable<decimal> WH_Longitude { get; set; }
        public Nullable<decimal> WH_Latitude { get; set; }
    }
}

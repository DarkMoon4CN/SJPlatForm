//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ChuanYe.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class F_CountyFireAnswer
    {
        public int Cou_ID { get; set; }
        public int F_FireID { get; set; }
        public int CFA_ID { get; set; }
        public string Cou_Answerer { get; set; }
        public string Cou_AnswerContent { get; set; }
        public System.DateTime Cou_HandleTime { get; set; }
        public System.DateTime Cou_TransDatetime { get; set; }
        public string Cou_TransUser { get; set; }
        public string Cou_TransIP { get; set; }
        public Nullable<bool> Cou_CityRead { get; set; }
    }
}

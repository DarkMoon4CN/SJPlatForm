﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Models
{
    public class GridPager
    {
        // Properties
        public string order { get; set; }

        public int page { get; set; }

        public int rows { get; set; }

        public string sort { get; set; }

        public int totalPages
        {
            get
            {
                return (int)Math.Ceiling((double)(((float)this.totalRows) / ((float)this.rows)));
            }
        }

        public int totalRows { get; set; }
    }


    public class GridPager2
    {
        public int DataId { get; set; }
        public string Keyword { get; set; }

        public int PageIndex { get; set; }

        public int StartRow { get; set; }
         
        public int Sort { get; set; }

        public int PageSize{get;set;}

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((double)(((float)this.TotalRows) / ((float)this.PageSize)));
            }
        }
        public int TotalRows { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }
    }

    public class BlackboxModel
    {
        /// <summary>
        /// 戳
        /// </summary>
        public string blackbox { set; get; }

        /// <summary>
        /// 公钥
        /// </summary>
        public string key { set; get; }
    }
}

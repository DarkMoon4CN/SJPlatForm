//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ChuanYe.Models
{
    using System;
    
    public partial class SP_NewsTempList_Result
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string PublishCode { get; set; }
        public string PublishPerson { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<System.DateTime> PublishDate { get; set; }
        public Nullable<int> DocType { get; set; }
        public Nullable<bool> IsChecked { get; set; }
        public Nullable<int> CopyId { get; set; }
        public Nullable<int> NewsAttachId { get; set; }
        public string FilePath { get; set; }
    }
}

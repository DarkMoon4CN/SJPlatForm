//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ChuanYe.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class F_OrgInfo
    {
        public int O_ID { get; set; }
        public string O_OrgName { get; set; }
        public string O_OrgPhone { get; set; }
        public string O_AreaName { get; set; }
        public string O_UpOrg { get; set; }
        public string O_Remark { get; set; }
        public string O_AreaID { get; set; }
        public System.DateTime O_TransDatetime { get; set; }
        public string O_TransUser { get; set; }
        public string O_TransIP { get; set; }
    }
}

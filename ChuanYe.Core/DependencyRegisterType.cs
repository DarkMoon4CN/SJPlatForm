﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using ChuanYe.BLL;
using ChuanYe.DAL;
using ChuanYe.IBLL;
using ChuanYe.IDAL;

namespace ChuanYe.Core
{
    public class DependencyRegisterType
    {
        //系统注入
        public static void Container_Sys(ref UnityContainer container)
        {
            //用户
            container.RegisterType<IUsersInfosBLL, UsersInfosBLL>();
            container.RegisterType<IUsersInfosDAL, UsersInfosDAL>();

            //短信
            container.RegisterType<ISms_SendInfoBLL, Sms_SendInfoBLL>();
            container.RegisterType<ISms_SendInfoDAL, Sms_SendInfoDAL>();

            //平安报送
            container.RegisterType<ISafetyInfoBLL, SafetyInfoBLL>();
            container.RegisterType<ISafetyInfoDAL, SafetyInfoDAL>();

            //信息管理
            container.RegisterType<INewsBLL, NewsBLL>();
            container.RegisterType<INewsDAL, NewsDAL>();

            //邮箱管理
            container.RegisterType<IMailBLL, MailBLL>();
            container.RegisterType<IMailDAL, MailDAL>();

            //公文管理
            container.RegisterType<IDocumentInfosBLL, DocumentInfosBLL>();
            container.RegisterType<IDocumentInfosDAL, DocumentInfosDAL>();


            //火灾处置
            container.RegisterType<IFireInfoBLL, FireInfoBLL>();
            container.RegisterType<IFireInfoDAL, FireInfoDAL>();

            //城市表
            container.RegisterType<ILinchpinBLL, LinchpinBLL>();
            container.RegisterType<ILinchpinDAL, LinchpinDAL>();

            //指挥部
            container.RegisterType<IMemberInfoBLL, MemberInfoBLL>();
            container.RegisterType<IMemberInfoDAL, MemberInfoDAL>();

            //火灾统计
            container.RegisterType<IFireStatisticsBLL, FireStatisticsBLL>();
            container.RegisterType<IFireStatisticsDAL, FireStatisticsDAL>();



            //物资管理
            container.RegisterType<IGoodsManageBLL, GoodsManageBLL>();
            container.RegisterType<IGoodsManageDAL, GoodsManageDAL>();



            //办公消息
            container.RegisterType<IAutomaticBLL, AutomaticBLL>();
            container.RegisterType<IAutomaticDAL, AutomaticDAL>();



            //轮播
            container.RegisterType<ICarouselFigureBLL, CarouselFigureBLL>();
            container.RegisterType<ICarouselFigureDAL, CarouselFigureDAL>();


        }
    }
}

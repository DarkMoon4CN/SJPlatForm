﻿using ChuanYe.IBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using ChuanYe.IDAL;

namespace ChuanYe.BLL
{
    public class CarouselFigureBLL : ICarouselFigureBLL
    {
        [Dependency]
        public ICarouselFigureDAL dal { get; set; }

        public int AddCarouselFigure(CarouselFigure info)
        {
            return dal.AddCarouselFigure(info);
        }

        public List<CarouselFigure> GetCarouselFigureAll(int searchType = 0)
        {
            return dal.GetCarouselFigureAll(searchType);
        }


        public int UpdateCarouselFigure(CarouselFigure info)
        {
            return dal.UpdateCarouselFigure(info);
        }
    }
}

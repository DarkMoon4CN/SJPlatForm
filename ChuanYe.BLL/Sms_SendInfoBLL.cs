﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;
using ChuanYe.IBLL;

namespace ChuanYe.BLL
{
    public class Sms_SendInfoBLL:ISms_SendInfoBLL
    {
        [Dependency]
        public ISms_SendInfoDAL s_DAL { get; set; }

        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateSms_SendInfo(Sms_SendInfo model)
        {
            return s_DAL.CreateSms_SendInfo(model);
        }

        public List<ChuanYe.Models.Model.View_SmsPhone> GetSmsPhone()
        {
            return s_DAL.GetSmsPhone();
        }

        /// <summary>
        /// 获取所有部门
        /// </summary>
        /// <returns></returns>
        public List<Departments> GetAllDepartments()
        {
            return s_DAL.GetAllDepartments();
        }

        public Sms_SendInfoResult GetSms_SendInfo(Sms_SendInfo search, GridPager Pager)
        {
            return s_DAL.GetSms_SendInfo(search, Pager);
        }

        public int Sms_SendInfoDelete(int ID)
        {
            return s_DAL.Sms_SendInfoDelete(ID);
        }

        public List<SMS_Model> GetSMS_Model()
        {
            return s_DAL.GetSMS_Model();
        }

        public bool CreateSMS_Model(SMS_Model model)
        {
            return s_DAL.CreateSMS_Model(model);
        }
        
        public SMS_Model GetSMS_ModelByID(int ID)
        {
            return s_DAL.GetSMS_ModelByID(ID);
        }

        public int SMS_ModelDelete(int ID)
        {
            return s_DAL.SMS_ModelDelete(ID);
        }

        public int SMS_ModelUpdate(SMS_Model model)
        {
            return s_DAL.SMS_ModelUpdate(model);
        }

        public SMS_ModelResult GetSMS_Model(string keyword, int? sms_mtype, GridPager Pager)
        {
            return s_DAL.GetSMS_Model(keyword,sms_mtype, Pager);
        }

        public List<SMS_Model> GetSMS_Model(int sms_mtype)
        {
            return s_DAL.GetSMS_Model(sms_mtype);
        }
    }
}

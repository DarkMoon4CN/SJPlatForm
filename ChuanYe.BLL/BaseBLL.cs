﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;

namespace ChuanYe.BLL
{
    public class BaseBLL : IDisposable
    {
        protected SJZHPlatFormEntities db = new SJZHPlatFormEntities();

        public void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
            }
        }
    }
}

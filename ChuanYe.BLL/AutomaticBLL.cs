﻿using ChuanYe.IBLL;
using ChuanYe.IDAL;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.BLL
{
    public class AutomaticBLL : IAutomaticBLL
    {

        [Dependency]
        public IAutomaticDAL dal { get; set; }

        public int AddAutomatic(Automatic info)
        {
           return   dal.AddAutomatic(info);
        }

        public List<Automatic> GetAutomatic(string receiveArea, int receiveUserID, string receiveUserName, int isReplay = 0)
        {
           return  dal.GetAutomatic(receiveArea, receiveUserID, receiveUserName,isReplay);
        }

        public int UpdateAutomaticReplay(int mid, int isReplay)
        {
            return dal.UpdateAutomaticReplay(mid, isReplay);
        }

        public int UpdateAutomaticReplay(int receiveUserID, DateTime? sendTime, int type, int isReplay = 0)
        {
            return dal.UpdateAutomaticReplay(receiveUserID, sendTime, type, isReplay);
        }

        public int DeleteAutomatic(string customData)
        {
            return dal.DeleteAutomatic(customData);
        }

        public List<Automatic> GetAutomatic(List<string> customDatas)
        {
            return dal.GetAutomatic(customDatas);
        }
    }
}

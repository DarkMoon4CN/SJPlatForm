﻿using ChuanYe.IBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;
using System.Data;
using ChuanYe.Models.Model;

namespace ChuanYe.BLL
{
    public class FireInfoBLL : IFireInfoBLL
    {
        [Dependency]
        public IFireInfoDAL dal { get; set; }


        [Dependency]
        public IUsersInfosBLL userDal { get; set; }



        public F_FireInfo FireInfoDetail(int fireID)
        {
            return dal.FireInfoDetail(fireID);
        }

        public List<F_FireInfo> ListByPager(string[] addressCountys, int fireSate, GridPager2 pager)
        {
            if (pager != null)
            {
                var result = dal.ListByPager(addressCountys, fireSate, pager);
                return result;
            }
            return null;
        }

        public List<M_Record> MRListByFireID(int fireID, GridPager2 pager, int searchLevel = 1)
        {
            if (pager != null)
            {
                var result = dal.MRListByFireID(fireID, pager, searchLevel);
                return result;
            }
            return null;
        }

        public List<M_Record> MRListByFireIDs(List<int> fireIDs, int searchLevel = 1)
        {
            return dal.MRListByFireIDs(fireIDs,searchLevel);
        }

        public int Add(F_FireInfo info)
        {
            return dal.Add(info);
        }

        public int AddRecord(M_Record info)
        {
            return dal.AddRecord(info);
        }

        public List<M_Record> CKRecordTypeByTypes(int fireID, List<int> types)
        {
            return dal.CKRecordTypeByTypes(fireID, types);
        }

        public int AddCityFireAsk(F_CityFireAsk info)
        {
            return  dal.AddCityFireAsk(info);
        }

        public F_CityFireAsk CityFireAskDetail(int cfa_ID)
        {
            return dal.CityFireAskDetail(cfa_ID);
        }

        public int AddCountyFireAnswer(F_CountyFireAnswer info)
        {
            return dal.AddCountyFireAnswer(info);
        }


        public F_FireFirstReport FireFirstReportDetail(int fireID)
        {
            return dal.FireFirstReportDetail(fireID);
        }

        public int AddFireFirstReport(F_FireFirstReport info)
        {
            return dal.AddFireFirstReport(info);
        }

        public int AddFireContinueReport(F_FireContinueReport info)
        {
            return dal.AddFireContinueReport(info);
        }

        public F_FireContinueReport FireContinueReportDetail(int fireID)
        {
            return dal.FireContinueReportDetail(fireID);
        }

        public List<F_FireContinueReport> GetFireContinueReport(int fireID)
        {
            return dal.GetFireContinueReport(fireID);
        }


        public M_Record DetailMRecordByFireID(int fireID, int mrType, int searchLevel = 1)
        {
            return dal.DetailMRecordByFireID(fireID, mrType, searchLevel);
        }

        public int AddCatalog(Catalog info)
        {
            return dal.AddCatalog(info);
        }

        public Catalog FireCatalogDetail(int fireID)
        {
            return dal.FireCatalogDetail(fireID);
        }

        public List<Catalog> GetFireCatalog(int fireID)
        {
            return dal.GetFireCatalog(fireID);
        }



        public int AddFireAutomatic(M_Automatic info)
        {
           return  dal.AddFireAutomatic(info);
        }

        public List<M_Automatic> GetFireAutomatic(int fireID, List<int> mTypes)
        {
            return dal.GetFireAutomatic(fireID, mTypes);
        }

        public List<M_Automatic> GetFireAutomatic(List<int> fireIDs, List<int> mTypes)
        {
            return dal.GetFireAutomatic(fireIDs, mTypes);
        }


        public KeyValuePair<int, string> AddFireAutomaticForInit(F_FireInfo info)
        {
            M_Automatic initA = new M_Automatic();
            string initAString = info.F_FireName + "，特此报告。";
            initA.M_Content = initAString;
            initA.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initA.M_AssembleID = 0;
            initA.M_From = 0;
            initA.M_To = 1;
            initA.M_Type = (int)AutomaticType.接收火灾;
            initA.F_FireID = info.F_FireID;
            initA.M_Receive = "接收人";
            initA.M_UserID = -1;
            initA.M_HandleTime = DateTime.Now;
            initA.M_IsFull = 1;
            initA.M_Account = null;
            initA.M_NUserID = Convert.ToInt32(info.F_TransUser);
            initA.M_NHandleTime = initA.M_HandleTime;
            initA.M_NIsReplay = 1;
            initA.M_IsReplay = 1;
            initA.M_Intime = initA.M_HandleTime;
            initA.M_DateTime = initA.M_HandleTime;
            initA.M_User_ID = initA.M_NUserID;
            initA.M_IP = info.F_TransIP;
            int maID = dal.AddFireAutomatic(initA);
            string maContent = initAString;


            M_Automatic initB = new M_Automatic();
            string firstTime = info.F_TransDatetime.Value.AddMinutes(5).ToString("yyyy年MM月dd日HH时mm分");
            string initBString = string.Format("您还未对{0}进行首报，请在{1}前进行首报", info.F_FireName, firstTime);
            initB.M_Content = initBString;
            initB.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initB.M_AssembleID = 0;
            initB.M_From = 1;
            initB.M_To = 0;
            initB.M_Type = (int)AutomaticType.首报倒计时提示;
            initB.F_FireID = info.F_FireID;
            initB.M_Receive = "接收人";
            initB.M_UserID = Convert.ToInt32(info.F_TransUser);
            initB.M_HandleTime = DateTime.Now;
            initB.M_IsFull = 1;
            initB.M_Account = null;
            initB.M_NUserID = -1;
            initB.M_NHandleTime = initA.M_HandleTime;
            initB.M_NIsReplay = 1;
            initB.M_IsReplay = 0;
            initB.M_Intime = info.F_TransDatetime.Value.AddMinutes(5);
            initB.M_DateTime = initA.M_HandleTime;
            initB.M_User_ID = initA.M_NUserID;
            initB.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initB);
            M_Automatic initC = new M_Automatic();


            string depStr = string.Empty;
            #region 获取出部门领导及电话 排除重复Address筛选规则
            //DepName=昌平
            var departments = userDal.DepartmentsInfo(info.F_Address);
            var screenRule = departments.Where(p => p.Manager.StartsWith(p.DepName) 
                                || info.F_FireName.StartsWith(p.DepName)).FirstOrDefault();
            if (screenRule == null)
            {
                screenRule = departments.FirstOrDefault();
            }
            
            if (screenRule != null)
            {
                depStr = "请速与{0}森防指负责人: {1}【{2}】进行联系";
                depStr = string.Format(screenRule.DepName, screenRule.Leader, screenRule.LeaderTel);
            }
            #endregion

            string initCString = string.Format("{0}没有在{1}前进行首报。", info.F_FireName, firstTime) + depStr;
            initC.M_Content = initCString;
            initC.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initC.M_AssembleID = 0;
            initC.M_From = 0;
            initC.M_To = 1;
            initC.M_Type = (int)AutomaticType.首报未作提示;
            initC.F_FireID = info.F_FireID;
            initC.M_Receive = "接收人";
            initC.M_UserID = Convert.ToInt32(info.F_TransUser) ;
            initC.M_HandleTime = DateTime.Now;
            initC.M_IsFull = 1;
            initC.M_Account = null;
            initC.M_NUserID = -1;
            initC.M_NHandleTime = initA.M_HandleTime;
            initC.M_NIsReplay = 1;
            initC.M_IsReplay = 1;
            initC.M_Intime = info.F_TransDatetime.Value.AddMinutes(5);
            initC.M_DateTime = initA.M_HandleTime;
            initC.M_User_ID = initA.M_NUserID;
            initC.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initC);

            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;
        }

        public int UpdateFireAutoMatic(int fireID, List<int> mTypes, int replay = 0)
        {
            return dal.UpdateFireAutoMatic(fireID, mTypes, replay);
        }

        public KeyValuePair<int, string> AddFireAutomaticForFirst(F_FireFirstReport firstInfo, F_FireInfo info)
        {
          
            //移除录入时的 首报未作提示=17
            if (info.F_TransDatetime.Value.AddMinutes(10)>firstInfo.R_TransDatetime)
            {
                DeleteFireAutoMatic(info.F_FireID, (int)AutomaticType.首报未作提示);
            }
            M_Automatic initA = new M_Automatic();
            string initAString =string.Format("{0}于{1}完成首报",info.F_FireName,firstInfo.R_TransDatetime);
            initA.M_Content = initAString;
            initA.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initA.M_AssembleID = 0;
            initA.M_From = 0;
            initA.M_To = 1;
            initA.M_Type = (int)AutomaticType.火灾首报;
            initA.F_FireID = info.F_FireID;
            initA.M_Receive = "接收人";
            initA.M_UserID = Convert.ToInt32(firstInfo.R_TransUser);
            initA.M_HandleTime = DateTime.Now;
            initA.M_IsFull = 1;
            initA.M_Account = null;
            initA.M_NUserID = -1;
            initA.M_NHandleTime = initA.M_HandleTime;
            initA.M_NIsReplay = 1;
            initA.M_IsReplay = 1;
            initA.M_Intime = initA.M_HandleTime;
            initA.M_DateTime = initA.M_HandleTime;
            initA.M_User_ID = initA.M_UserID;
            initA.M_IP = info.F_TransIP;
            int maID= dal.AddFireAutomatic(initA);
            string maContent = initAString;

            M_Automatic initB = new M_Automatic();
            string continueTime = firstInfo.R_TransDatetime.Value.AddMinutes(30).ToString("yyyy年MM月dd日HH时mm分");
            string initBString = string.Format("您还未对{0}进行续报，请在{1}(30分钟内)进行续报", info.F_FireName, continueTime);
            initB.M_Content = initBString;
            initB.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initB.M_AssembleID = 0;
            initB.M_From = 0;
            initB.M_To = 1;
            initB.M_Type = (int)AutomaticType.续报倒计时提示;
            initB.F_FireID = info.F_FireID;
            initB.M_Receive = "接收人";
            initB.M_UserID = Convert.ToInt32(firstInfo.R_TransUser);
            initB.M_HandleTime = DateTime.Now;
            initB.M_IsFull = 1;
            initB.M_Account = null;
            initB.M_NUserID = -1;
            initB.M_NHandleTime = initA.M_HandleTime;
            initB.M_NIsReplay = 1;
            initB.M_IsReplay = 0;
            initB.M_Intime = firstInfo.R_TransDatetime.Value.AddMinutes(30);
            initB.M_DateTime = initA.M_HandleTime;
            initB.M_User_ID = initA.M_UserID;
            initB.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initB);


            M_Automatic initC = new M_Automatic();
            string initCString = string.Format("{0}没有在{1}前进行续报",info.F_FireName, continueTime);
            initC.M_Content = initCString;
            initC.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initC.M_AssembleID = 0;
            initC.M_From = 0;
            initC.M_To = 1;
            initC.M_Type = (int)AutomaticType.续报未作提示;
            initC.F_FireID = info.F_FireID;
            initC.M_Receive = "接收人";
            initC.M_UserID = Convert.ToInt32(firstInfo.R_TransUser);
            initC.M_HandleTime = DateTime.Now;
            initC.M_IsFull = 1;
            initC.M_Account = null;
            initC.M_NUserID = -1;
            initC.M_NHandleTime = initA.M_HandleTime;
            initC.M_NIsReplay = 1;
            initC.M_IsReplay = 0;
            initC.M_Intime = firstInfo.R_TransDatetime.Value.AddMinutes(30);
            initC.M_DateTime = initA.M_HandleTime;
            initC.M_User_ID = initA.M_UserID;
            initC.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initC);

            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;
        }


        public int DeleteFireAutoMatic(int fireID, int mType)
        {
            return dal.DeleteFireAutoMatic(fireID, mType);
        }

        public KeyValuePair<int,string> AddFireAutomaticForContinue(F_FireContinueReport continueInfo,bool isMany=false)
        {
            F_FireFirstReport firstInfo = dal.FireFirstReportDetail(continueInfo.F_FireID.Value);
            F_FireInfo info = dal.FireInfoDetail(continueInfo.F_FireID.Value);
            //移除首报时录入 续报未作提示=18
            if (firstInfo.R_TransDatetime.Value.AddMinutes(30) > continueInfo.C_TransDatetime)
            {
                //多次写入时忽略
                if (isMany == false)
                {
                    DeleteFireAutoMatic(continueInfo.F_FireID.Value, (int)AutomaticType.续报未作提示);
                }
            }
            M_Automatic initA = new M_Automatic();
            string initAString = string.Format("{0}于{1}完成续报", info.F_FireName, continueInfo.C_TransDatetime);
            initA.M_Content = initAString;
            initA.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initA.M_AssembleID = 0;
            initA.M_From = 0;
            initA.M_To = 1;
            initA.M_Type = (int)AutomaticType.火灾续报;
            initA.F_FireID = info.F_FireID;
            initA.M_Receive = "接收人";
            initA.M_UserID = Convert.ToInt32(continueInfo.C_TransUser);
            initA.M_HandleTime = DateTime.Now;
            initA.M_IsFull = 1;
            initA.M_Account = null;
            initA.M_NUserID = -1;
            initA.M_NHandleTime = initA.M_HandleTime;
            initA.M_NIsReplay = 1;
            initA.M_IsReplay = 1;
            initA.M_Intime = initA.M_HandleTime;
            initA.M_DateTime = initA.M_HandleTime;
            initA.M_User_ID = initA.M_UserID;
            initA.M_IP = info.F_TransIP;

            int maID = dal.AddFireAutomatic(initA);
            string maContent = initAString;
            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;

        }


        public KeyValuePair<int, string> AddFireAutomaticForCatalog(Catalog catalogInfo)
        {
            F_FireInfo info = dal.FireInfoDetail(catalogInfo.F_FireID.Value);
            M_Automatic ma = new M_Automatic();
            string maString =string.Format("{0}于{1}申请误报，请尽快处理。申请人：{2},申请理由：{3}",
                                            info.F_FireName,catalogInfo.A_TransDatetime,catalogInfo.A_Applicant,catalogInfo.A_FalseReason);
            ma.M_Content = maString;
            ma.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            ma.M_AssembleID = 0;
            ma.M_From = 0;
            ma.M_To = 1;
            ma.M_Type = (int)AutomaticType.误报申请;
            ma.F_FireID = info.F_FireID;
            ma.M_Receive = "接收人";
            ma.M_UserID = -1;
            ma.M_HandleTime = DateTime.Now;
            ma.M_IsFull = 1;
            ma.M_Account = null;
            ma.M_NUserID = Convert.ToInt32(info.F_TransUser);
            ma.M_NHandleTime = ma.M_HandleTime;
            ma.M_NIsReplay = 1;
            ma.M_IsReplay = 1;
            ma.M_Intime = ma.M_HandleTime;
            ma.M_DateTime = ma.M_HandleTime;
            ma.M_User_ID = ma.M_NUserID;
            ma.M_IP = info.F_TransIP;
            int maID=AddFireAutomatic(ma);
            string maContent = maString;
            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;
        }

        public int AddApplyRespond(F_ApplyRespond info)
        {
            return dal.AddApplyRespond(info);
        }

        public F_ApplyRespond FireApplyRespondDetail(int fireID)
        {
            return dal.FireApplyRespondDetail(fireID);
        }


        public KeyValuePair<int, string> AddFireAutomaticForApplyRespond(F_ApplyRespond applyInfo,int level=3)
        {

            F_FireInfo info = dal.FireInfoDetail(applyInfo.F_FireID.Value);
            M_Automatic ma = new M_Automatic();
            string maString = string.Format("{0}于已{1}申请启动{2}级响应，请尽快处理",
                                       info.F_FireName, applyInfo.AR_TransDatetime, GetLevelString(level));
            ma.M_Content = maString;
            ma.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            ma.M_AssembleID = 0;
            ma.M_From = 0;
            ma.M_To = 1;
            ma.M_Type = (int)AutomaticType.申请响应;
            ma.F_FireID = info.F_FireID;
            ma.M_Receive = "接收人";
            ma.M_UserID = -1;
            ma.M_HandleTime = DateTime.Now;
            ma.M_IsFull = 1;
            ma.M_Account = null;
            ma.M_NUserID = Convert.ToInt32(info.F_TransUser);
            ma.M_NHandleTime = ma.M_HandleTime;
            ma.M_NIsReplay = 1;
            ma.M_IsReplay = 1;
            ma.M_Intime = ma.M_HandleTime;
            ma.M_DateTime = ma.M_HandleTime;
            ma.M_User_ID = ma.M_NUserID;
            ma.M_IP = info.F_TransIP;

            int maID = AddFireAutomatic(ma);
            string maContent = maString;
            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;

        }
        public int AddReportOutFire(F_ReportOutFire info)
        {
            return dal.AddReportOutFire(info);
        }

        public KeyValuePair<int, string> AddFireAutomaticForReportOutFire(F_ReportOutFire reportOutInfo)
        {
            F_FireInfo info = dal.FireInfoDetail(reportOutInfo.F_FireID.Value);
            M_Automatic initA = new M_Automatic();
            string initAString = string.Format("{0}火灾已报灭。", info.F_FireName);
            initA.M_Content = initAString;
            initA.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initA.M_AssembleID = 0;
            initA.M_From = 0;
            initA.M_To = 1;
            initA.M_Type = (int)AutomaticType.区县报灭;
            initA.F_FireID = info.F_FireID;
            initA.M_Receive = "接收人";
            initA.M_UserID = Convert.ToInt32(reportOutInfo.FR_TransUser);
            initA.M_HandleTime = DateTime.Now;
            initA.M_IsFull = 1;
            initA.M_Account = null;
            initA.M_NUserID = -1;
            initA.M_NHandleTime = initA.M_HandleTime;
            initA.M_NIsReplay = 1;
            initA.M_IsReplay = 1;
            initA.M_Intime = initA.M_HandleTime;
            initA.M_DateTime = initA.M_HandleTime;
            initA.M_User_ID = initA.M_UserID;
            initA.M_IP = info.F_TransIP;
            int maID =dal.AddFireAutomatic(initA);


            M_Automatic initB = new M_Automatic();
            string endTime = reportOutInfo.FR_TransDatetime.Value.AddMinutes(30).ToString("yyyy年MM月dd日HH时mm分");
            string initBString = string.Format("您还未对{0}进行续]终报，请在{1}(2小时内)进行续报", info.F_FireName, endTime);
            initB.M_Content = initBString;
            initB.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initB.M_AssembleID = 0;
            initB.M_From = 1;
            initB.M_To = 0;
            initB.M_Type = (int)AutomaticType.终报倒计时提示;
            initB.F_FireID = info.F_FireID;
            initB.M_Receive = "接收人";
            initB.M_UserID = Convert.ToInt32(reportOutInfo.FR_TransUser);
            initB.M_HandleTime = DateTime.Now;
            initB.M_IsFull = 1;
            initB.M_Account = null;
            initB.M_NUserID = -1;
            initB.M_NHandleTime = initA.M_HandleTime;
            initB.M_NIsReplay = 1;
            initB.M_IsReplay = 0;
            initB.M_Intime = reportOutInfo.FR_TransDatetime.Value.AddMinutes(120);
            initB.M_DateTime = initA.M_HandleTime;
            initB.M_User_ID = initA.M_UserID;
            initB.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initB);

            M_Automatic initC = new M_Automatic();
            string initCString = string.Format("{0}没有在{1}前进行终报", info.F_FireName, endTime);
            initC.M_Content = initCString;
            initC.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initC.M_AssembleID = 0;
            initC.M_From = 0;
            initC.M_To = 1;
            initC.M_Type = (int)AutomaticType.终报未作提示;
            initC.F_FireID = info.F_FireID;
            initC.M_Receive = "接收人";
            initC.M_UserID = Convert.ToInt32(reportOutInfo.FR_TransUser);
            initC.M_HandleTime = DateTime.Now;
            initC.M_IsFull = 1;
            initC.M_Account = null;
            initC.M_NUserID = -1;
            initC.M_NHandleTime = initA.M_HandleTime;
            initC.M_NIsReplay = 1;
            initC.M_IsReplay = 0;
            initC.M_Intime = reportOutInfo.FR_TransDatetime.Value.AddMinutes(120);
            initC.M_DateTime = initA.M_HandleTime;
            initC.M_User_ID = initA.M_UserID;
            initC.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initC);
            string maContent = initAString;
            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;


        }

        public F_ReportOutFire FireReportOutDetail(int fireID)
        {
            return dal.ReportOutFireDetail(fireID);
        }

        public int AddFireFinalReport(F_FireFinalReport info)
        {
            return dal.AddFireFinalReport(info);
        }


        public KeyValuePair<int, string> AddFireAutomaticForFinalReport(F_FireFinalReport finalReportInfo, F_FireInfo fireInfo)
        {
            M_Automatic initA = new M_Automatic();
            string initAString = string.Format("{0}于{1}完成终报", fireInfo.F_FireName, finalReportInfo.FFR_TransDatetime);
            initA.M_Content = initAString;
            initA.M_DistrictID = Convert.ToInt32(fireInfo.F_AddressCounty);
            initA.M_AssembleID = 0;
            initA.M_From = 0;
            initA.M_To = 1;
            initA.M_Type = (int)AutomaticType.区县终报;
            initA.F_FireID = fireInfo.F_FireID;
            initA.M_Receive = "接收人";
            initA.M_UserID = Convert.ToInt32(finalReportInfo.FFR_TransUser);
            initA.M_HandleTime = DateTime.Now;
            initA.M_IsFull = 1;
            initA.M_Account = null;
            initA.M_NUserID = -1;
            initA.M_NHandleTime = initA.M_HandleTime;
            initA.M_NIsReplay = 1;
            initA.M_IsReplay = 1;
            initA.M_Intime = initA.M_HandleTime;
            initA.M_DateTime = initA.M_HandleTime;
            initA.M_User_ID = initA.M_UserID;
            initA.M_IP = finalReportInfo.FFR_TransIP;
            int maID=dal.AddFireAutomatic(initA);
            string maContent = initAString;
            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;
        }



        public F_FireFinalReport FireFinalReportDetail(int fireID)
        {
            return dal.FireFinalReportDetail(fireID);
        }

        public int UpdateFireInfoState(int fireID, int isState)
        {
            return dal.UpdateFireInfoState(fireID, isState);
        }


        public KeyValuePair<int, string> AddFireAutomaticForTransField(F_FireInfo info)
        {
            M_Automatic initA = new M_Automatic();

            string happenDatetime = info.F_HappenDatetime.Value.ToString("yyyy年MM月dd日HH时mm分");
            string initAString = string.Format( "请接收{0}{1}火灾并尽快在接警10分钟内进行首报。",info.F_Address, happenDatetime);
            initA.M_Content = initAString;
            initA.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initA.M_AssembleID = 0;
            initA.M_From = 1;
            initA.M_To = 0;
            initA.M_Type = (int)AutomaticType.接收火灾;
            initA.F_FireID = info.F_FireID;
            initA.M_Receive = "接收人";
            initA.M_UserID = -1;
            initA.M_HandleTime = DateTime.Now;
            initA.M_IsFull = 1;
            initA.M_Account = null;
            initA.M_NUserID = Convert.ToInt32(info.F_TransUser);
            initA.M_NHandleTime = initA.M_HandleTime;
            initA.M_NIsReplay = 1;
            initA.M_IsReplay = 1;
            initA.M_Intime = initA.M_HandleTime;
            initA.M_DateTime = initA.M_HandleTime;
            initA.M_User_ID = initA.M_NUserID;
            initA.M_IP = info.F_TransIP;
            int maID = dal.AddFireAutomatic(initA);

            M_Automatic initB = new M_Automatic();
            string firstTime = info.F_TransDatetime.Value.AddMinutes(10).ToString("yyyy年MM月dd日HH时mm分");
            string initBString = string.Format("您还未对{0}进行首报，请在{1}前进行首报", info.F_FireName, firstTime);
            initB.M_Content = initBString;
            initB.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initB.M_AssembleID = 0;
            initB.M_From = 1;
            initB.M_To = 0;
            initB.M_Type = (int)AutomaticType.首报倒计时提示;
            initB.F_FireID = info.F_FireID;
            initB.M_Receive = "接收人";
            initB.M_UserID = Convert.ToInt32(info.F_TransUser);
            initB.M_HandleTime = DateTime.Now;
            initB.M_IsFull = 1;
            initB.M_Account = null;
            initB.M_NUserID = -1;
            initB.M_NHandleTime = initA.M_HandleTime;
            initB.M_NIsReplay = 1;
            initB.M_IsReplay = 1;
            initB.M_Intime = info.F_TransDatetime.Value.AddMinutes(10);
            initB.M_DateTime = initA.M_HandleTime;
            initB.M_User_ID = initA.M_NUserID;
            initB.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initB);

            M_Automatic initC = new M_Automatic();


            string initCString = string.Format("{0}没有在{1}前进行首报", info.F_FireName, firstTime);
            initC.M_Content = initCString;
            initC.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
            initC.M_AssembleID = 0;
            initC.M_From = 0;
            initC.M_To = 1;
            initC.M_Type = (int)AutomaticType.首报未作提示;
            initC.F_FireID = info.F_FireID;
            initC.M_Receive = "接收人";
            initC.M_UserID = Convert.ToInt32(info.F_TransUser);
            initC.M_HandleTime = DateTime.Now;
            initC.M_IsFull = 1;
            initC.M_Account = null;
            initC.M_NUserID = -1;
            initC.M_NHandleTime = initA.M_HandleTime;
            initC.M_NIsReplay = 1;
            initC.M_IsReplay = 1;
            initC.M_Intime = info.F_TransDatetime.Value.AddMinutes(10);
            initC.M_DateTime = initA.M_HandleTime;
            initC.M_User_ID = initA.M_NUserID;
            initC.M_IP = info.F_TransIP;
            dal.AddFireAutomatic(initC);

            string maContent = initAString;
            KeyValuePair<int, string> result = new KeyValuePair<int, string>(maID, maContent);
            return result;

        }



        public int AddFireTransField(F_FireTransField info)
        {
            return dal.AddFireTransField(info);
        }

        public F_FireTransField FireTransFieldDetail(int fireID)
        {
            return dal.FireTransFieldDetail(fireID);
        }



        public int AddFireSite(F_FireSite info)
        {
            return dal.AddFireSite(info);
        }

        public int AddFireDocumentation(F_FireDocumentation info)
        {
            return dal.AddFireDocumentation(info);
        }



        public List<F_FireDocumentation> GetFireDocumentation(int fireID)
        {
            return dal.GetFireDocumentation(fireID);
        }




        public F_FireSite FireSiteDetail(int fireID)
        {
            return dal.FireSiteDetail(fireID);
        }



        public List<F_FireSite> GetFireSiteInfoByFireID(int fireID)
        {
            return dal.GetFireSiteInfoByFireID(fireID);
        }


        public int UpdateFireSite(F_FireSite info)
        {
            return dal.UpdateFireSite(info);
        }



        public int AddFrontCommand(F_FrontCommand info)
        {
            return dal.AddFrontCommand(info);
        }

        public int UpdateFrontCommand(F_FrontCommand info)
        {
            return dal.UpdateFrontCommand(info);
        }
        public F_FrontCommand FrontCommandDetail(int fireID)
        {
            return dal.FrontCommandDetail(fireID);
        }
        public List<F_FrontCommand> GetFrontCommand(int fireID)
        {
            return dal.GetFrontCommand(fireID);
        }


        public F_StepResponse StepResponseDetail(int fireID, int responseGrade=0)
        {
            return  dal.StepResponseDetail(fireID,responseGrade);
        }

        public int AddStepResponse(F_StepResponse info)
        {
            return dal.AddStepResponse(info);
        }

        public int AddStepResponseTempPeople(List<F_StepResponseTempPeople> infos) 
        {
            return dal.AddStepResponseTempPeople(infos);
        }

        public List<F_StepResponseTempPeople> GetStepResponseTempPeople(int fireID, int srID) 
        {
            return dal.GetStepResponseTempPeople(fireID,srID);
        }


        public int AddFrontCommandTempPeople(List<F_FrontCommandTempPeople> infos)
        {
            return dal.AddFrontCommandTempPeople(infos);
        }

        public List<F_FrontCommandTempPeople> GetFrontCommandTempPeople(int fireID, int fcID)
        {
            return dal.GetFrontCommandTempPeople(fireID, fcID);
        }



        public List<F_StartCondition> GetStartCondition(int responseGrade)
        {
            return dal.GetStartCondition(responseGrade);
        }

        public int AddFightingForce(F_FightingForce info)
        {
            return dal.AddFightingForce(info);
        }

        public List<F_FightingForce> GetFightingForce(int fireID)
        {
            return dal.GetFightingForce(fireID);
        }

        public int AddResponse(F_Response info)
        {
            return dal.AddResponse(info);
        }

        public List<F_Response> GetResponse(int fireID)
        {
            return dal.GetResponse(fireID);
        }

        public List<AskAnswerModel> FireAskAndAnswer(int fireID)
        {
            return dal.FireAskAndAnswer(fireID);
        }
        public int UpdateFireDocumentation(F_FireDocumentation info)
        {
            return dal.UpdateFireDocumentation(info);
        }

        public int UpdateFireAutoMaticToReplay(int autoMaticId, int fireID, List<int> mTypes, int replay = 0)
        {
            return dal.UpdateFireAutoMaticToReplay(autoMaticId, fireID, mTypes, replay);
        }
        public int UpdateFireAutoMaticToNReplay(int autoMaticId, int fireID, List<int> mTypes, int replay = 0)
        {
            return dal.UpdateFireAutoMaticToReplay(autoMaticId, fireID, mTypes, replay);
        }


        public List<M_Automatic> GetAutomatic(int fireID, int isCityManager)
        {
            return dal.GetAutomatic(fireID, isCityManager);
        }

        public M_Automatic AutoMaticDetail(int autoMaticId)
        {
            return dal.AutoMaticDetail(autoMaticId);
        }
        public int AddFireForceSupport(F_FireForceSupport info)
        {
            return dal.AddFireForceSupport(info);
        }

        public int AddFireSituation(F_FireSituation info)
        {
            return dal.AddFireSituation(info);
        }

        public int UpdateFireSituation(F_FireSituation info)
        {
            return dal.UpdateFireSituation(info);
        }


        public List<M_Automatic> GetAutomatic(int addressCounty, List<int> messageType, int isCityManager)
        {
            return dal.GetAutomatic(addressCounty, messageType, isCityManager);
        }


        public int UpdateCatalogToApproval(Catalog info)
        {
            return dal.UpdateCatalogToApproval(info);
        }


        public int UpdateAnswerRead(int couID, bool cityRead = true)
        {
            return dal.UpdateAnswerRead(couID, cityRead);
        }


        public List<F_FireFinalReport> GetFireFinalReport(List<int> fireIDs)
        {
            return dal.GetFireFinalReport(fireIDs);
        }

        public F_FireInfo FireInfoDetail(string phone)
        {
            return dal.FireInfoDetail(phone);
        }


        public int AddFireAttachs(F_FireAttachs info)
        {
            return dal.AddFireAttachs(info);
        }
        public int AddFireAttachs(List<F_FireAttachs> infos)
        {
            return dal.AddFireAttachs(infos);
        }


        public List<F_FireAttachs> GetFireAttachs(int fireID)
        {
            return dal.GetFireAttachs(fireID);
        }

        public List<F_CountyFireAnswer> ExistCountyFireAnswer(int cfa_ID)
        {
            return dal.ExistCountyFireAnswer(cfa_ID);
        }
        public int UpdateCountyFireAnswer(F_CountyFireAnswer info)
        {
            return dal.UpdateCountyFireAnswer(info);
        }

        public List<F_FireInfo> GetFireInfo(string addressCounty, int fireState) {
            return dal.GetFireInfo(addressCounty, fireState);
        }

        public List<F_FireAttachs> GetFireAttachs(List<int> fireIDs) {
            return dal.GetFireAttachs(fireIDs);
        }


        public void RemoveFireAttachs(int fireAttachsID) 
        {
            dal.RemoveFireAttachs(fireAttachsID);
        }

        public List<F_FireInfo> GetFireInfoFromForce(string addressCounty, int fireState)
        {
            return dal.GetFireInfoFromForce(addressCounty, fireState);
        }

        public List<F_FireInfo> GetFireInfoFromForceByPager(string addressCounty, int fireState, GridPager2 pager)
        {
            return dal.GetFireInfoFromForceByPager(addressCounty, fireState,pager);
        }

        public List<dynamic> GetFireForceSupportByPager(int addressCounty, GridPager2 pager)
        {
            return dal.GetFireForceSupportByPager(addressCounty, pager);
        }


        public List<dynamic> GetFireReporterInfo(string phone)
        {
            return dal.GetFireReporterInfo(phone);
        }



        public int AddFireBlCons(F_BlCons info)
        {
            return dal.AddFireBlCons(info);
        }

        public List<F_BlCons> GetFireBlCons(int fireID)
        {
            return dal.GetFireBlCons(fireID);
        }

        public int UpdateFireFinalReport(F_FireFinalReport info)
        {
            return dal.UpdateFireFinalReport(info);
        }

        public int UpdateFireBlCons(F_BlCons info)
        {
           return dal.UpdateFireBlCons(info);
        }
        public int DeleteFireBlCons(int id)
        {
            return dal.DeleteFireBlCons(id);
        }



        public int AddFireRecordPlot(F_FireRecordPlot info)
        {
            return dal.AddFireRecordPlot(info);
        }

        public List<F_FireRecordPlot> GetFireRecordPlot(int mrID, int fireID, int mrType)
        {
            return dal.GetFireRecordPlot(mrID, fireID, mrType);
        }



        public List<F_FightingForce> GetFightingForce(List<int?> fireIDs) 
        {
            return dal.GetFightingForce(fireIDs);
        }





        public int AddAlarmRecord(F_AlarmRecord info)
        {
            return dal.AddAlarmRecord(info);
        }

        public void ChangeFireAlarmRecordState(int fireID, int arID)
        {
            dal.ChangeFireAlarmRecordState(fireID, arID);
        }


        public int AddAlarmRecordConfig(F_AlarmRecordConfig info) 
        {
            return dal.AddAlarmRecordConfig(info);
        }

        public List<F_AlarmRecordConfig> GetAlarmRecordConfig(DateTime nowDateTime, string locaID, int unitID)
        {
            return dal.GetAlarmRecordConfig(nowDateTime,locaID,unitID);
        }

        public F_AlarmRecord GetAlarmRecord(int arID)
        {
            return dal.GetAlarmRecord(arID);
        }

        private string GetLevelString(int level)
        {
            string text = string.Empty;
            switch (level)
            {
                case 1:text = "一";break;
                case 2:text = "二";break;
                default: text = "三"; break;
            }
            return text;
        }

        
       
    }
}

﻿using ChuanYe.IBLL;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.BLL
{
    public class FireStatisticsBLL: IFireStatisticsBLL
    {
        [Dependency]
        public IFireStatisticsDAL dal { get; set; }

        public dynamic CountyStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.CountyStatistics(startTime,endTime);
        }

        public dynamic TimeStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.TimeStatistics(startTime, endTime);
        }

        public dynamic FireWhyStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.FireWhyStatistics(startTime, endTime);
        }

        public dynamic AlarmTypeStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.AlarmTypeStatistics(startTime, endTime);
        }

        public dynamic FireTypeStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.FireTypeStatistics(startTime, endTime);
        }

        public dynamic GareaStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.GareaStatistics(startTime,endTime);
        }

        public dynamic LareaStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.LareaStatistics(startTime, endTime);
        }

        public dynamic NumStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.NumStatistics(startTime, endTime);
        }
        public dynamic LossStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.LossStatistics(startTime, endTime);
        }

        public dynamic SummaryStatistics(DateTime startTime, DateTime endTime)
        {
            return dal.SummaryStatistics(startTime, endTime);
        }
    }
}

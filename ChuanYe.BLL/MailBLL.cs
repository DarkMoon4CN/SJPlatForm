﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;
using ChuanYe.Models;
using ChuanYe.IBLL;
using ChuanYe.Models.Model;

namespace ChuanYe.BLL
{
    public class MailBLL: IMailBLL
    {
        [Dependency]
        public IMailDAL s_DAL { get; set; }

        /// <summary>
        /// 获取全部邮箱用户
        /// </summary>
        /// <returns></returns>
        public List<Models.Model.View_MailUsers> GetAllMailUsers()
        {
            return s_DAL.GetAllMailUsers();
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateInfosComm(InfosComm model, List<InfosCommAttachs> InfosCommAttachsModel)
        {
            return s_DAL.CreateInfosComm(model, InfosCommAttachsModel);
        }

        /// <summary>
        /// 保存附件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateInfosCommAttachs(InfosCommAttachs model)
        {
            return s_DAL.CreateInfosCommAttachs(model);
        }

        /// <summary>
        /// 接收的邮件
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public ReceiveMailListResult ReceiveMailGetList(ReceiveMailSearch Search, GridPager Pager)
        {
            return s_DAL.ReceiveMailGetList(Search, Pager);
        }
        /// <summary>
        /// 邮件详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public InfosCommInfoModel InfosCommGet(int ID)
        {
            return s_DAL.InfosCommGet(ID);
        }
        /// <summary>
        /// 邮件附件列表
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<InfosCommAttachs> InfosCommAttachsGet(int ID)
        {
            return s_DAL.InfosCommAttachsGet(ID);
        }
        /// <summary>
        /// 删除邮件
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int InfosCommDelete(int ID)
        {
            return s_DAL.InfosCommDelete(ID);
        }
        /// <summary>
        /// 改变已读状态
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int InfosCommUpdate(int ID)
        {
            return s_DAL.InfosCommUpdate(ID);
        }
        /// <summary>
        /// 批量更改已读状态
        /// </summary>
        /// <param name="IDs"></param>
        /// <returns></returns>
        public int InfosCommUpdateByIDs(int[] IDs)
        {
            return s_DAL.InfosCommUpdateByIDs(IDs);
        }


        /// <summary>
        /// 公共邮箱列表
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public PublicMailResult PublicMailGetList(PublicMailSearch Search, GridPager Pager)
        {
            return s_DAL.PublicMailGetList(Search, Pager);
        }
        /// <summary>
        /// 已发邮件列表
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public MailSendResult MailSendGetList(MailSendSearch Search, GridPager Pager)
        {
            return s_DAL.MailSendGetList(Search, Pager);
        }
        /// <summary>
        /// 获取用户接收到的邮件的附件总大小
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public decimal GetAllInfosCommAttachsSize(int UserID)
        {
            return s_DAL.GetAllInfosCommAttachsSize(UserID);
        }
        /// <summary>
        /// 添加日程
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int ScheduleCreate(Schedule model)
        {
            return s_DAL.ScheduleCreate(model);
        }
        /// <summary>
        /// 获取日程
        /// </summary>
        /// <param name="S_ID"></param>
        /// <returns></returns>
        public Schedule ScheduleGet(int S_ID)
        {
            return s_DAL.ScheduleGet(S_ID);
        }
        /// <summary>
        /// 获取某个月的日程信息
        /// </summary>
        /// <param name="Search"></param>
        /// <returns></returns>
        public List<ScheduleGetMonthListResult> ScheduleGetMonthList(ScheduleSearch Search)
        {
            return s_DAL.ScheduleGetMonthList(Search);
        }
        /// <summary>
        /// 获取某天的日程信息
        /// </summary>
        /// <param name="Search"></param>
        /// <returns></returns>
        public List<ScheduleGetMonthListResult> ScheduleGetDayList(ScheduleSearch Search)
        {
            return s_DAL.ScheduleGetDayList(Search);
        }
        /// <summary>
        /// 修改日程信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int ScheduleUpdate(Schedule model)
        {
            return s_DAL.ScheduleUpdate(model);
        }
        /// <summary>
        /// 删除日程信息
        /// </summary>
        /// <param name="S_ID"></param>
        /// <returns></returns>
        public int ScheduleDelete(int S_ID)
        {
            return s_DAL.ScheduleDelete(S_ID);
        }

        public int ScheduleAutomaticStateUpdate(Schedule model)
        {
            return s_DAL.ScheduleAutomaticStateUpdate(model);
        }
    }
}

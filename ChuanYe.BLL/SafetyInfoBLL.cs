﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IDAL;
using ChuanYe.IBLL;
using Microsoft.Practices.Unity;
using ChuanYe.Models.Model;

namespace ChuanYe.BLL
{
    public class SafetyInfoBLL: ISafetyInfoBLL
    {
        [Dependency]
        public ISafetyInfoDAL s_DAL { get; set; }

        public List<View_SmsPhone> GetLeader()
         {
            return s_DAL.GetLeader();
        }

        public bool CreateSafetyInfo(SafetyInfo model)
        {
            return s_DAL.CreateSafetyInfo(model);
        }

        public SafetySms_SendInfoResult GetLastSafetyInfo(SafetySms_SendInfoSearch search, GridPager pager)
        {
            return s_DAL.GetLastSafetyInfo(search, pager);
        }

        public SafetyInfoResult GetSafetyInfoListByDay(SafetyInfo search, GridPager Pager)
        {
            return s_DAL.GetSafetyInfoListByDay(search, Pager);
        }

        public List<SP_SafetyInfoStatistics_Result> GetSafetyInfoStatistics(SafetyInfoStatisticsSearch search)
        {
            return s_DAL.GetSafetyInfoStatistics(search);
        }
        
        public SP_DistrictSafetyInfoStatistics_Result GetDistrictSafetyInfoStatistics(DistrictSafetyInfoStatisticsSearch search)
        {
            return s_DAL.GetDistrictSafetyInfoStatistics(search);
        }

        public List<SafetyInfo> GetSafetyInfoForMonth(SafetyInfoForMonthSearch search)
        {
            return s_DAL.GetSafetyInfoForMonth(search);
        }
        public SafetyInfo GetSafetyInfoByID(int ID)
        {
            return s_DAL.GetSafetyInfoByID(ID);
        }

        public int SafetyInfoUpdate(SafetyInfo model)
        {
            return s_DAL.SafetyInfoUpdate(model);
        }

        /// <summary>
        /// 获取本区县今日是否已报送平安
        /// </summary>
        /// <param name="DepId"></param>
        /// <returns></returns>
        public int IsSendToday(int DepId)
        {
            return s_DAL.IsSendToday(DepId);
        }

        public int IsSendToday(int DepId, DateTime? date = null)
        {
            return s_DAL.IsSendToday(DepId, date);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IBLL;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;

namespace ChuanYe.BLL
{
    public class NewsBLL:INewsBLL
    {
        [Dependency]
        public INewsDAL s_DAL { get; set; }


        /// <summary>
        /// 保存临时新闻信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool NewsTempCreate(NewsTemp model, NewsAttachs AttachModel)
        {
            return s_DAL.NewsTempCreate(model, AttachModel);
        }
        /// <summary>
        /// 修改临时新闻信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool NewsTempUpdate(NewsTemp model)
        {
            return s_DAL.NewsTempUpdate(model);
        }
        /// <summary>
        /// 保存新闻信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool NewsInfsCreate(NewsInfs model)
        {
            return s_DAL.NewsInfsCreate(model);
        }
        /// <summary>
        /// 修改新闻
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int NewsInfsUpdate(NewsInfs model)
        {
            return s_DAL.NewsInfsUpdate(model);
        }
        /// <summary>
        /// 发布新闻
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int NewsInfsPublic(NewsInfs model)
        {
            return s_DAL.NewsInfsPublic(model);
        }

        /// <summary>
        /// 根据ID获取单条临时新闻数据
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public NewsTempOneResult NewsTempGetByID(int ID)
        {
            return s_DAL.NewsTempGetByID(ID);
        }

        /// <summary>
        /// 根据ID获取单条已发布新闻数据
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public NewsInfs NewsInfsGetByID(int ID)
        {
            return s_DAL.NewsInfsGetByID(ID);
        }

        /// <summary>
        /// 根据新闻ID获取附件列表
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<NewsAttachs> NewsAttachsGetByNewsID(int ID)
        {
            return s_DAL.NewsAttachsGetByNewsID(ID);
        }

        /// <summary>
        /// 删除新闻
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int NewsTempDelete(int ID)
        {
            return s_DAL.NewsTempDelete(ID);
        }
        /// <summary>
        /// 删除新闻
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int NewsInfsDelete(int ID)
        {
            return s_DAL.NewsInfsDelete(ID);
        }

        /// <summary>
        /// 保存新闻附件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int NewsAttachsCreate(NewsAttachs model)
        {
            return s_DAL.NewsAttachsCreate(model);
        }

        /// <summary>
        /// 保存新闻阅读状态表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool NewsReadStateCreate(NewsReadState model)
        {
            return s_DAL.NewsReadStateCreate(model);
        }

        /// <summary>
        /// 分页查询临时新闻列表
        /// </summary>
        /// <param name="search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public NewsTempListResult NewsTempGetList(NewsTempListSearch search, GridPager Pager)
        {
            return s_DAL.NewsTempGetList(search, Pager);
        }
        /// <summary>
        /// 分页查询新闻列表
        /// </summary>
        /// <param name="search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public NewsInfsResult NewsInfsGetList(NewsInfsSearch search, GridPager Pager)
        {
            return s_DAL.NewsInfsGetList(search, Pager);
        }
        /// <summary>
        /// 获取已发布新闻列表
        /// </summary>
        /// <param name="search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public NewsInfsResult NewsInfsPublicGetList(NewsInfsPublicSearch search, GridPager Pager)
        {
            return s_DAL.NewsInfsPublicGetList(search, Pager);
        }
        /// <summary>
        /// 获取新闻信息统计
        /// </summary>
        /// <param name="search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public NewsInfsStatisticsResult NewsInfsStatisticsGet(NewsInfsStatisticsSearch search, GridPager Pager)
        {
            return s_DAL.NewsInfsStatisticsGet(search, Pager);
        }
        /// <summary>
        /// 获取部门各信息类型数量统计
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public SP_NewsInfsStatisticsByDepId_Result NewsInfsStatisticsByDepIdGet(NewsInfsStatisticsByDepIdSearch search)
        {
            return s_DAL.NewsInfsStatisticsByDepIdGet(search);
        }
        /// <summary>
        /// 获取部门各信息类型信息列表
        /// </summary>
        /// <param name="search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public NewsInfsGetListByDepIdResult NewsInfsGetListByDepId(NewsInfsGetListByDepIdSearch search, GridPager Pager)
        {
            return s_DAL.NewsInfsGetListByDepId(search, Pager);
        }
        /// <summary>
        /// 区县获取新闻信息统计
        /// </summary>
        /// <param name="search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public SP_NewsInfsCountyStatistics_Result NewsInfsCountyStatisticsGet(NewsInfsCountyStatisticsSearch search)
        {
            return s_DAL.NewsInfsCountyStatisticsGet(search);
        }

        public  List<dynamic> NewsInfsPublicGetListForGroup(NewsInfsPublicGroupSearch search, GridPager Pager)
        {
            return s_DAL.NewsInfsPublicGetListForGroup( search,  Pager);
        }

        public NewsInfsResult NewsInfsPublicGetList2(NewsInfsPublicSearch search, GridPager Pager)
        {
            return s_DAL.NewsInfsPublicGetList2(search, Pager);
        }

        public List<View_NewsTemp> NewTempGetList(View_NewsTemp info, GridPager2 pager)
        {
            return s_DAL.NewTempGetList(info, pager);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.IBLL;
using ChuanYe.Models;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;

namespace ChuanYe.BLL
{
    public class UserInfosBLL: BaseBLL,IUserInfosBLL
    {

        //[Dependency]
        //public UsersInfos m_Rep { get; set; }
        ///// <summary>
        ///// 登录
        ///// </summary>
        ///// <param name="username"></param>
        ///// <param name="pwd"></param>
        ///// <returns></returns>
        //public UsersInfos Login(string username, string pwd)
        //{
        //    return m_Rep.Login(username, pwd);
        //}
        ///// <summary>
        ///// 修改密码
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="newpwd"></param>
        ///// <returns></returns>
        //public int EdtPwd(int userid, string OldPassword, string Password)
        //{
        //    UsersInfos user = m_Rep.GetByID(userid);
        //    if (user!=null&&user.Password == OldPassword)
        //    {
        //        user.Password = Password;
        //        int IsEdt = m_Rep.Edit(user);
        //        return IsEdt;
        //    }
        //    return 2;
        //}
        ///// <summary>
        ///// 昵称是否存在
        ///// </summary>
        ///// <param name="nick"></param>
        ///// <returns></returns>
        //public bool IsExistNick(string nick)
        //{
        //    if (db.UserInfos.SingleOrDefault(a => a.UserName == nick&& a.IsDel==false) != null)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        ///// <summary>
        ///// 修改
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //public int Edit(UserInfos model)
        //{
        //    int IsEdt = m_Rep.Edit(model);
        //    return IsEdt;
        //}
        ///// <summary>
        ///// 获取用户列表
        ///// </summary>
        ///// <param name="pager"></param>
        ///// <param name="queryStr"></param>
        ///// <returns></returns>
        //public List<View_GetUserList> GetList(ref GridPager pager, string queryStr)
        //{
        //    List<View_GetUserList> sd = new List<View_GetUserList>();
        //    return sd;
        //}
        ///// <summary>
        ///// 创建用户
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //public bool Create(UserInfos model)
        //{
        //    bool IsCreate = m_Rep.Create(model);
        //    return IsCreate;
        //}
        ///// <summary>
        ///// 获取单个用户
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //public UserInfos GetByID(int id)
        //{
        //    return m_Rep.GetByID(id);
        //}
        ///// <summary>
        ///// 获取所有模块权限
        ///// </summary>
        ///// <returns></returns>
        //public List<Power> GetAllPower()
        //{
        //    IQueryable<Power> list = db.Power.AsQueryable();
        //    List<Power> PowerList = list.ToList();
        //    PowerList = (from a in PowerList
        //                 orderby a.Sort
        //                 select new Power
        //                 {
        //                     PowerID = a.PowerID,
        //                     PowerName = a.PowerName,
        //                     Code = a.Code,
        //                     Sort = a.Sort,
        //                     Url = a.Url
        //                 }).ToList();
        //    return PowerList;
        //}
        ///// <summary>
        ///// 是否存在该用户
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //public bool IsExist(int id)
        //{
        //    if (db.UserInfos.SingleOrDefault(a => a.UserID == id && a.IsDel == false) != null)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        ///// <summary>
        ///// 获取所有模块权限
        ///// </summary>
        ///// <returns></returns>
        //public List<Role> GetAllRole()
        //{
        //    IQueryable<Role> list = db.Role.AsQueryable();
        //    List<Role> RoleList = list.ToList();
        //    return RoleList;
        //}
    }
}

﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.IDAL;
using ChuanYe.IBLL;
using ChuanYe.Models;
using ChuanYe.Models.Model;

namespace ChuanYe.BLL
{
    public class UsersInfosBLL: IUsersInfosBLL
    {
        [Dependency]
        public IUsersInfosDAL  s_DAL { get; set; }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public LoginUserModel LoginIn(UsersInfos model)
        {
            return s_DAL.LoginIn(model);
        }
        /// <summary>
        /// 登录有户信息
        /// </summary>
        /// <returns></returns>
        public LoginUserModel LoginUserInfos(int ID)
        {
            return s_DAL.LoginUserInfos(ID);
        }

        public bool IsIDRight(int ID)
        {
            return s_DAL.IsIDRight(ID);
        }

        #region 模块管理
        
        /// <summary>
        /// 添加模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int PowerCreate(Power model)
        {
            return s_DAL.PowerCreate(model);
        }
        /// <summary>
        /// 删除模块
        /// </summary>
        /// <param name="ID">模块ID</param>
        /// <returns></returns>
        public int PowerDelete(int ID)
        {
            return s_DAL.PowerDelete(ID);
        }
        /// <summary>
        /// 模块详情
        /// </summary>
        /// <param name="ID">模块ID</param>
        /// <returns></returns>
        public Power PowerInfo(int ID)
        {
            return s_DAL.PowerInfo(ID);
        }
        /// <summary>
        /// 修改模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int PowerUpdate(Power model)
        {
            return s_DAL.PowerUpdate(model);
        }
        /// <summary>
        /// 查询模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PowerResult PowerGetList(PowerSearch Search, GridPager Pager)
        {
            return s_DAL.PowerGetList(Search, Pager);
        }
        /// <summary>
        /// 查询全部模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<Power> PowerGetAll(PowerSearch Search, GridPager Pager)
        {
            return s_DAL.PowerGetAll(Search, Pager);
        }
        #endregion

        #region 职务管理
        /// <summary>
        /// 是否已有相同的职务名称
        /// </summary>
        /// <param name="PositionName"></param>
        /// <returns></returns>
        public int PositionIsHave(string PositionName)
        {
            return s_DAL.PositionIsHave(PositionName);
        }

        public int PositionIsHave(string positionName, int id = 0)
        {
            return s_DAL.PositionIsHave(positionName, id);
        }


        /// <summary>
        /// 添加职务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int PositionCreate(Position model)
        {
            return s_DAL.PositionCreate(model);
        }
        /// <summary>
        /// 删除职务
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int PositionDelete(int ID)
        {
            return s_DAL.PositionDelete(ID);
        }
        /// <summary>
        /// 职务详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Position PositionInfo(int ID)
        {
            return s_DAL.PositionInfo(ID);
        }
        /// <summary>
        /// 修改职务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int PositionUpdate(Position model)
        {
            return s_DAL.PositionUpdate(model);
        }
        /// <summary>
        /// 获取全部职务
        /// </summary>
        /// <returns></returns>
        public List<Position> PositionGetAll()
        {
            return s_DAL.PositionGetAll();
        }
        #endregion

        #region 角色管理
        /// <summary>
        /// 获取权限树 此接口标注为 表结构与数据不可变动，返回数据顺序不可变动，疑问由吴精明 解释
        /// </summary>
        /// <returns></returns>
        public List<PowerModel> PowerGetAllTree()
        {
            return s_DAL.PowerGetAllTree();
        }
        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="model"></param>
        /// <param name="RolePowerList"></param>
        /// <returns></returns>
        public int RolesCreate(MemberShip_Roles model, List<RolePower> RolePowerList)
        {
            return s_DAL.RolesCreate(model, RolePowerList);
        }
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int RolesDelete(int ID)
        {
            return s_DAL.RolesDelete(ID);
        }
        /// <summary>
        /// 角色详情 此接口标注为 表结构与数据不可变动，返回数据顺序不可变动，疑问由吴精明 解释
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public MemberShip_RolesResult RolesInfo(int ID)
        {
            return s_DAL.RolesInfo(ID);
        }
        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="model"></param>
        /// <param name="RolePowerList"></param>
        /// <returns></returns>
        public int RolesUpdate(MemberShip_Roles model, List<RolePower> RolePowerList)
        {
            return s_DAL.RolesUpdate(model, RolePowerList);
        }
        /// <summary>
        /// 获取全部角色
        /// </summary>
        /// <returns></returns>
        public List<MemberShip_Roles> MemberShip_RolesGetAll()
        {
            return s_DAL.MemberShip_RolesGetAll();
        }
        /// <summary>
        /// 分页查询角色
        /// </summary>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public MemberShip_RolesSearchResult MemberShip_RolesSearch(GridPager Pager)
        {
            return s_DAL.MemberShip_RolesSearch(Pager);
        }

        public List<UsersInfos> GetUserInfos(List<int> ids)
        {
            return s_DAL.GetUserInfos(ids);
        }

        #endregion

        #region 机构管理
        /// <summary>
        /// 添加机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int DepartmentsCreate(Departments model)
        {
            return s_DAL.DepartmentsCreate(model);
        }
        /// <summary>
        /// 删除机构
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int DepartmentsDelete(int ID)
        {
            return s_DAL.DepartmentsDelete(ID);
        }
        /// <summary>
        /// 机构详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Departments DepartmentsInfo(int ID)
        {
            return s_DAL.DepartmentsInfo(ID);
        }

        public List<Departments> DepartmentsInfo(string address)
        {
            return s_DAL.DepartmentsInfo(address);
        }






        /// <summary>
        /// 修改机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int DepartmentsUpdate(Departments model)
        {
            return s_DAL.DepartmentsUpdate(model);
        }
        /// <summary>
        /// 机构查询
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public DepartmentsSearchResult DepartmentsSearch(DepartmentsSearch Search, GridPager Pager)
        {
            return s_DAL.DepartmentsSearch(Search, Pager);
        }
        /// <summary>
        /// 获取部门下拉框
        /// </summary>
        /// <returns></returns>
        public List<DepartmentsAllModel> DepartmentsGetAll()
        {
            return s_DAL.DepartmentsGetAll();
        }
        #endregion

        #region 用户管理
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int UsersInfosCreate(UsersInfos model)
        {
            return s_DAL.UsersInfosCreate(model);
        }
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int UsersInfosDelete(int ID)
        {
            return s_DAL.UsersInfosDelete(ID);
        }
        /// <summary>
        /// 用户详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public UsersInfos UsersInfosInfo(int ID)
        {
            return s_DAL.UsersInfosInfo(ID);
        }
        /// <summary>
        /// 用户修改
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int UsersInfosUpdate(UsersInfos model)
        {
            return s_DAL.UsersInfosUpdate(model);
        }
        /// <summary>
        /// 用户查询
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        public UsersInfosSearchResult UsersInfosSelete(UsersInfosSearch Search, GridPager Pager)
        {
            return s_DAL.UsersInfosSelete(Search, Pager);
        }

        public UsersInfosSearchResult UsersInfosSelete2(UsersInfosSearch Search, GridPager Pager)
        {
            return s_DAL.UsersInfosSelete2(Search, Pager);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int ChangePW(UsersInfos model)
        {
            return s_DAL.ChangePW(model);
        }

        public int UsersInfosUpdateDisable(int userid, bool isDisable)
        {
            return s_DAL.UsersInfosUpdateDisable(userid,isDisable);
        }

        public UsersInfos UsersInfosInfo(string userCode)
        {
            return s_DAL.UsersInfosInfo(userCode);
        }

        public List<UsersInfos> UsersInfosByUserCode(string userCode)
        {
            return s_DAL.UsersInfosByUserCode(userCode);
        }
        #endregion

        #region 日志管理
        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int DocOperationLogCreate(DocOperationLog model)
        {
            return s_DAL.DocOperationLogCreate(model);
        }
        /// <summary>
        /// 日志查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DocOperationLogResult DocOperationLogSearch(DocOperationLogSearch Search, GridPager Pager)
        {
            return s_DAL.DocOperationLogSearch(Search, Pager);
        }
        #endregion



        #region 安全登录秘钥

        public List<U_AppAuthor> GetAppAuthor(int appId=0)
        {
            return s_DAL.GetAppAuthor(appId);
        }
        #endregion



        #region 短信组管理


        public int AddOutsideUserInfos(U_OutsideUserInfos info) {
            return s_DAL.AddOutsideUserInfos(info);
        }
        public List<U_OutsideUserInfos> OutsideUserInfosSelect(OutsideUserInfoSearch Search, GridPager Pager, ref int totalRows)
        {
            return s_DAL.OutsideUserInfosSelect(Search, Pager, ref totalRows);
        }

        public int OutsideUserInfosDelete(int id)
        {
            return s_DAL.OutsideUserInfosDelete(id);
        }
        public int OutsideUserInfosUpdate(U_OutsideUserInfos info)
        {
            return s_DAL.OutsideUserInfosUpdate(info);
        }

        public U_OutsideUserInfos OutsideUserInfosDetail(int id)
        {
            return s_DAL.OutsideUserInfosDetail(id);
        }


        public List<U_OutsideUserInfos> OutsideUserInfosSelect(int selectType=1)
        {
            return s_DAL.OutsideUserInfosSelect(selectType);
        }

        #endregion


        public LoginUserModel Login(string userCode)
        {
            return s_DAL.Login(userCode);
        }
        public List<PowerModel> GetPower(int roleID)
        {
            return s_DAL.GetPower(roleID);
        }


        /// <summary>
        /// 更新浏览总人数
        /// </summary>
        /// <returns></returns>
        public void UpdateBrowsingTotal() {
             s_DAL.UpdateBrowsingTotal();
        }


        public List<Position> PositionByPage(string positionName, GridPager pager)
        {
           return  s_DAL.PositionByPage(positionName,pager);
        }

        public List<UsersInfos> UsersInfosByPhone(string phone){
            return s_DAL.UsersInfosByPhone(phone);
        }

        public int UsersInfosCount(int positionID, bool isOfficial)
        {
            return s_DAL.UsersInfosCount(positionID, isOfficial);
        }

        public LoginUserCache GetLoginUserCache(int userID)
        {
            return s_DAL.GetLoginUserCache(userID);
        }

        public void AddOrUpdateLoginUserCache(LoginUserCache info)
        {
             s_DAL.AddOrUpdateLoginUserCache(info);
        }

    }
}

﻿using ChuanYe.IBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;

namespace ChuanYe.BLL
{
    public class GoodsManageBLL : IGoodsManageBLL
    {

        [Dependency]
        public IGoodsManageDAL dal { get; set; }

        public List<W_GoodsInfo> GetGoodsInfo(W_GoodsInfo info)
        {
            return  dal.GetGoodsInfo(info);
        }

        public List<W_GoodsInfo> GetGoodsInfoByPage(W_GoodsInfo info, GridPager2 pager)
        {
            return dal.GetGoodsInfoByPage(info,pager);
        }

        public List<W_Warehouse> GetWarehouse(W_Warehouse info)
        {
            return dal.GetWarehouse(info);
        }

        public List<W_Warehouse> GetWarehouseByPage(W_Warehouse info, GridPager2 pager)
        {
            return dal.GetWarehouseByPage(info,pager);
        }
    }
}

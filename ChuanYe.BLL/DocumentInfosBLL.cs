﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IDAL;
using ChuanYe.IBLL;
using Microsoft.Practices.Unity;
using ChuanYe.Models.Model;

namespace ChuanYe.BLL
{
    public class DocumentInfosBLL: IDocumentInfosBLL
    {
        [Dependency]
        public IDocumentInfosDAL s_DAL { get; set; }

        /// <summary>
        /// 获取全部部门
        /// </summary>
        /// <returns></returns>
        public List<Departments> DepartmentsIsSend()
        {
            return s_DAL.DepartmentsIsSend();
        }

        public List<Departments> DepartmentsManagerCode()
        {
            return s_DAL.DepartmentsManagerCode();
        }


        /// <summary>
        /// 发送公文
        /// </summary>
        /// <param name="DInfo">公文信息</param>
        /// <param name="DAttachs">附件信息</param>
        /// <param name="DSms">短信信息</param>
        /// <returns></returns>
        public int DocumentInfosCreate(DocumentInfos DInfo, List<DocumentAttachs> DAttachs, Sms_SendInfo DSms)
        {
            return s_DAL.DocumentInfosCreate(DInfo, DAttachs, DSms);
        }
        /// <summary>
        /// 已发送公文
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        public DocumentInfosSendResult DocumentInfosGetSend(DocumentInfosSendSearch search, GridPager Pager)
        {
            DocumentInfosSendResult result= s_DAL.DocumentInfosGetSend(search, Pager);
            foreach (DocumentInfosModel model in result.DocumentInfosList)
            {
                if (model.IsView == false && Convert.ToDecimal(model.IsOverTime) >= 0)
                {
                    OverTimeModel otModel = new OverTimeModel();
                    otModel.ID = model.ID;
                    otModel.OverTime = search.OverTime;
                    otModel.endTime = DateTime.Now;
                    model.IsOverTime = s_DAL.DocumentInfosIsOverTime(otModel);
                }
            }
            return result;
        }
        /// <summary>
        /// 市局公文查询
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        public DocumentInfosReceiveResult DocumentInfosReceive(DocumentInfosReceiveSearch search, GridPager Pager)
        {
            return s_DAL.DocumentInfosReceive(search, Pager);
        }
        /// <summary>
        /// 获取公文查询公文详情
        /// </summary>
        /// <param name="model">查询条件</param>
        /// <returns></returns>
        public DocumentAllInfosModel DocumentInfosGetByBatch(DocumentInfosGetByBatchSearch model)
        {
            return s_DAL.DocumentInfosGetByBatch(model);
        }
        /// <summary>
        /// 获取公文详情
        /// </summary>
        /// <param name="ID">公文ID</param>
        /// <returns></returns>
        public DocumentAllInfosModel DocumentInfosGetByID(int ID)
        {
            return s_DAL.DocumentInfosGetByID(ID);
        }
        /// <summary>
        /// 删除公文
        /// </summary>
        /// <param name="ID">公文ID</param>
        /// <returns></returns>
        public int DocumentInfosDelete(int ID)
        {
            return s_DAL.DocumentInfosDelete(ID);
        }
        /// <summary>
        /// 删除附件
        /// </summary>
        /// <param name="ID">附件ID</param>
        /// <returns></returns>
        public int DocumentAttachsDelete(int ID)
        {
            return s_DAL.DocumentAttachsDelete(ID);
        }

        /// <summary>
        /// 接收公文
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string DocumentInfosReceive(DocumentInfosModel model)
        {
            OverTimeModel otModel = new OverTimeModel();
            otModel.ID = model.ID;
            otModel.OverTime = model.OverTime;
            otModel.endTime = Convert.ToDateTime(model.InceptDate);
            int result= s_DAL.DocumentInfosReceive(model);
            if (result > 0)
            {
                return s_DAL.DocumentInfosIsOverTime(otModel);
            }
            else
            {
                return "失败";
            }
        }
        /// <summary>
        /// 区县公文接收
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        public DocumentInfosWaitReceiveResult DocumentInfosWaitReceive(DocumentInfosWaitReceiveSearch search, GridPager Pager)
        {
            DocumentInfosWaitReceiveResult result= s_DAL.DocumentInfosWaitReceive(search, Pager);
            foreach (DocumentInfos model in result.DocumentInfosList)
            {
                if (Convert.ToDecimal(model.IsOverTime) >= 0)
                {
                    OverTimeModel otModel = new OverTimeModel();
                    otModel.ID = model.ID;
                    otModel.OverTime = search.OverTime;
                    otModel.endTime = DateTime.Now;
                    model.IsOverTime = s_DAL.DocumentInfosIsOverTime(otModel);
                }
            }
            return result;
        }
        /// <summary>
        /// 区县已收公文
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        public DocumentInfosReceivedResult DocumentInfosReceived(DocumentInfosReceivedSearch search, GridPager Pager)
        {
            return s_DAL.DocumentInfosReceived(search, Pager);
        }


        public int GetUnReadDocument(int depId)
        {
            return s_DAL.GetUnReadDocument(depId);
        }
        public DocumentInfosReceivedResult GetUnReceivedDocument(int depId)
        {
            return s_DAL.GetUnReceivedDocument(depId);
        }

    }
}

﻿using ChuanYe.IBLL;
using ChuanYe.IDAL;
using ChuanYe.Models;
using ChuanYe.Models.Model;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.BLL
{
    public  class MemberInfoBLL: IMemberInfoBLL
    {
        [Dependency]
        public IMemberInfoDAL dal { get; set; }

        public List<F_MemberInfo> GetMemberInfo(int orgID)
        {
            return dal.GetMemberInfo(orgID);
        }

        public List<F_OrgInfo> GetOrgInfo()
        {
            return dal.GetOrgInfo();
        }

        public List<F_MemberInfo> GetMemberInfo(List<int> mIDs)
        {
            return dal.GetMemberInfo(mIDs);
        }

        public List<F_UnitType> GetUnitType(int unitTypeID = 0)
        {
            return dal.GetUnitType(unitTypeID);
        }

        public int AddUnitType(F_UnitType info)
        {
            return dal.AddUnitType(info);
        }

        public int UpdateUnitType(F_UnitType info)
        {
            return dal.UpdateUnitType(info);
        }

        public List<F_Unit> GetUnit(int uintID, GridPager2 pager)
        {
            return dal.GetUnit(uintID,pager);
        }

        public List<F_Unit> GetUnitByUnitTypeID(int unitTypeID, int uhID, int responseLevel, GridPager2 pager)
        {
            return dal.GetUnitByUnitTypeID(unitTypeID,uhID,responseLevel ,pager);
        }


        public int AddUnit(F_Unit info)
        {
            return dal.AddUnit(info);
        }

        public int UpdateUnit(F_Unit info)
        {
            return dal.UpdateUnit(info);
        }

        public List<UintOrgInfo> UnitJoinOrgInfo(List<int> unitIDs)
        {
            return dal.UnitJoinOrgInfo(unitIDs);
        }


        public List<DirectorysTb> GetDirectorysTb(int parentID)
        {
            return dal.GetDirectorysTb(parentID);
        }

        public List<LinkCorresponding> GetLinkCorresponding()
        {
           return dal.GetLinkCorresponding();
        }



        public int AddGradeTemplate(OneGradeTemplate info)
        {
            return dal.AddGradeTemplate(info);
        }

        public List<DirectorysTb> DirectorysTbByPager(int parentID,string name, GridPager2 pager)
        {
            return dal.DirectorysTbByPager(parentID, name,pager);
        }

        public int AddDirectorysTb(DirectorysTb info)
        {
            return dal.AddDirectorysTb(info);
        }

        public int UpdateDirectorysTb(DirectorysTb info)
        {
            return dal.UpdateDirectorysTb(info);
        }
        public int DeleteDirectorysTb(int id)
        {
            return dal.DeleteDirectorysTb(id);
        }

        public DirectorysTb DirectorysTbDetail(int id)
        {
            return dal.DirectorysTbDetail(id);
        }

        public List<F_UnitHeadquarter> UintHeadquarterByPager(int parentID, string uhName, int level, string uhLeader,int uhLinchpinID, GridPager2 pager)
        {
            return dal.UintHeadquarterByPager(parentID,uhName,level, uhLeader,uhLinchpinID,pager);
        }

        public List<F_UnitMember> UnitMemberByPager(int unitID, string phone, string name, GridPager2 pager)
        {
               return dal.UnitMemberByPager(unitID, phone, name,  pager);
        }

        public int AddUintHeadquarter(F_UnitHeadquarter info)
        {
            return dal.AddUintHeadquarter(info);
        }

        public int UpDateUintHeadquarter(F_UnitHeadquarter info)
        {
            return dal.UpDateUintHeadquarter(info);
        }

        public int DeleteUintHeadquarter(int uhID)
        {
            return dal.DeleteUintHeadquarter(uhID);
        }

        public List<LinkCorresponding> LinkCorrespondingByPager(LinkCorresponding info, GridPager2 pager)
        {
            return dal.LinkCorrespondingByPager(info,pager);
        }

        public int AddLinkCorresponding(LinkCorresponding info)
        {
            return dal.AddLinkCorresponding(info);
        }

        public int UpDateLinkCorresponding(LinkCorresponding info)
        {
            return dal.UpDateLinkCorresponding(info);
        }

        public int DeleteLinkCorresponding(int id)
        {
            return dal.DeleteLinkCorresponding(id);
        }




        public List<OneGradeTemplate> GetGradeTemplate()
        {
            return dal.GetGradeTemplate();
        }

        public List<OneGradeTemplate> GradeTemplateByPager(OneGradeTemplate info, GridPager2 pager)
        {
            return dal.GradeTemplateByPager(info,pager);
        }

        public int UpDateGradeTemplate(OneGradeTemplate info)
        {
            return dal.UpDateGradeTemplate(info);
        }

        public int DeleteGradeTemplate(int id)
        {
            return dal.DeleteGradeTemplate(id);
        }

        public int AddOtherGradeTemplate(ThreeAndTwoGradeTemplate info)
        {
            return dal.AddOtherGradeTemplate(info);
        }


        public List<ThreeAndTwoGradeTemplate> GetOtherGradeTemplate()
        {
            return dal.GetOtherGradeTemplate();
        }

         


        public int DeleteOtherGradeTemplate(int id)
        {
            return dal.DeleteOtherGradeTemplate(id);
        }

        public int UpDateOtherGradeTemplate(ThreeAndTwoGradeTemplate info)
        {
            return dal.UpDateOtherGradeTemplate(info);
        }

        public List<dynamic> OtherGradeTemplateByPager(ThreeAndTwoGradeTemplate info, GridPager2 pager)
        {
            return dal.OtherGradeTemplateByPager(info,pager);
        }

        public List<F_Unit> UnitByPager(F_Unit info, GridPager2 pager)
        {
            return dal.UnitByPager(info, pager);
        }

        public int DeleteUnit(int uid)
        {
            return dal.DeleteUnit(uid);
        }

        public List<F_UnitMember> UnitMemberByPager(F_UnitMember info, GridPager2 pager)
        {
            return dal.UnitMemberByPager(info,pager);
        }

        public int AddUnitMember(F_UnitMember info)
        {
            return dal.AddUnitMember(info);
        }

        public int UpdateUnitMember(F_UnitMember info)
        {
            return dal.UpdateUnitMember(info);
        }

        public int DeleteUnitMember(int umID)
        {
            return dal.DeleteUnitMember(umID);
        }

        public List<F_UnitHeadquarterMember> UintHeadquarterMemberByPager(F_UnitHeadquarterMember info, GridPager2 pager)
        {
            return dal.UintHeadquarterMemberByPager(info, pager);
        }

        public int AddUintHeadquarterMember(F_UnitHeadquarterMember info)
        {
            return dal.AddUintHeadquarterMember(info);
        }

        public List<F_UnitHeadquarterMember> GetUnitHeadquarterMember(List<int> ids)
        {
            return dal.GetUnitHeadquarterMember(ids);
        }

        public List<F_UnitHeadquarterMember> GetUnitHeadquarterMemberByDepIDs(List<int> depIDs)
        {
            return dal.GetUnitHeadquarterMemberByDepIDs(depIDs);
        }

        public int UpdateUintHeadquarterMember(F_UnitHeadquarterMember info)
        {
            return dal.UpdateUintHeadquarterMember(info);
        }

        public int DeleteUintHeadquarterMember(int uhmID)
        {
            return dal.DeleteUintHeadquarterMember(uhmID);
        }

        public List<F_Unit> GetUnitByName(string unitName)
        {
            return dal.GetUnitByName(unitName);
        }



        public List<F_UnitHeadquarterDepartment> GetUnitHeadquarterDepartment(string uhdName)
        {
            return dal.GetUnitHeadquarterDepartment(uhdName);
        }

        public List<F_UnitHeadquarterDepartment> GetUnitHeadquarterDepartment(List<int> uhdIds)
        {
            return dal.GetUnitHeadquarterDepartment(uhdIds);
        }

        public int AddUnitHeadquarterDepartment(F_UnitHeadquarterDepartment info)
        {
            return dal.AddUnitHeadquarterDepartment(info);
        }

        public int UpdateUnitHeadquarterDepartment(F_UnitHeadquarterDepartment info)
        {
            return dal.UpdateUnitHeadquarterDepartment(info);
        }

        public List<F_UnitHeadquarterDepartment> UnitHeadquarterDepartmentByPager(F_UnitHeadquarterDepartment info, GridPager2 pager)
        {
            return dal.UnitHeadquarterDepartmentByPager(info,pager);
        }

        public int DeleteUnitHeadquarterDepartment(int uhdID)
        {
            return dal.DeleteUnitHeadquarterDepartment(uhdID);
        }

        public List<DirectorysTb> GetDirectorysTb(int parentID, string name)
        {
            return dal.GetDirectorysTb(parentID, name);
        }


        public List<F_UnitHeadquarter> GetUnitHeadquarter(string uhName)
        {
            return dal.GetUnitHeadquarter(uhName);
        }

        public List<F_UnitHeadquarterMember> GetUnitHeadquarterMember(string uhmLeader)
        {
            return dal.GetUnitHeadquarterMember(uhmLeader);
        }


        public List<F_UnitHeadquarter> GetUnitHeadquarter(int uhID)
        {
            return dal.GetUnitHeadquarter(uhID);
        }


        public List<ThreeAndTwoGradeTemplate> GetOtherGradeTemplate(int sGradeID, long linchpinIID)
        {
            return dal.GetOtherGradeTemplate(sGradeID,linchpinIID);
        }
    }
}

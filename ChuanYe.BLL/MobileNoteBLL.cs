﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.IBLL;
using ChuanYe.IDAL;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using System.Data.Entity;

namespace ChuanYe.BLL
{
    public class MobileNoteBLL : BaseBLL, IMobileNoteBLL
    {

        [Dependency]
        public IMobileNoteDAL m_Rep { get; set; }
        /// <summary>
        /// 获取获取待发送短信数据
        /// </summary>
        /// <returns></returns>
        public IEnumerator<View_MobileNote_MobileNoteDetail> GetMobileNoteDetailList(int MaxCounte)
        {
            return m_Rep.GetMobileNoteDetailList(MaxCounte).GetEnumerator();
        }

        /// <summary>
        /// 发送成功修改状态
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int MobileNoteDetailUpdateForSuccess(int id)
        {
            MobileNoteDetail model= db.MobileNoteDetail.SingleOrDefault(a => a.ID==id);
            model.state = 1;
            db.MobileNoteDetail.Attach(model);
            db.Entry(model).State = EntityState.Modified;
            return db.SaveChanges();
        }
        /// <summary>
        /// 备份原始数据 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int MobileNoteDetailSendAdd(MobileNoteDetailSend model)
        {
            db.MobileNoteDetailSend.Add(model);
            return db.SaveChanges();
        }
        /// <summary>
        /// 发送失败修改发送次数
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sCount"></param>
        /// <returns></returns>
        public int MobileNoteDetailUpdateForsCount(int id, int sCount)
        {
            MobileNoteDetail model = db.MobileNoteDetail.SingleOrDefault(a => a.ID == id);
            model.SendCount = sCount;
            db.MobileNoteDetail.Attach(model);
            db.Entry(model).State = EntityState.Modified;
            return db.SaveChanges();
        }
        /// <summary>
        /// 每24小时进行一次数据性能维护,清除成功数据
        /// </summary>
        /// <returns></returns>
        public int DeleteMobileNoteDetailSuccess()
        {
            IQueryable<MobileNoteDetail> list = from f in db.MobileNoteDetail
                                                where f.state == 1
                                                select f;
            db.MobileNoteDetail.RemoveRange(list);
            return db.SaveChanges();
        }
    }
}

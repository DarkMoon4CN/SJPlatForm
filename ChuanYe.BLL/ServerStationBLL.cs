﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IBLL;
using ChuanYe.IDAL;
using Microsoft.Practices.Unity;
using System.Data.Entity;

namespace ChuanYe.BLL
{
  
    public class ServerStationBLL
    {
        /// <summary>
        /// 获取全部的站点信息
        /// </summary>
        /// <returns></returns>
        public static List<Station> GetStationAll()
        {
            using (DXMeteorologicalEntities db = new DXMeteorologicalEntities())
            {
                IQueryable<Station> list = db.Station.AsQueryable();
                List<Station> RoleList = list.ToList();
                return RoleList;
            }
        }
        /// <summary>
        /// 根据IP获取站点信息
        /// </summary>
        /// <param name="SIP"></param>
        /// <returns></returns>
        public static Station GetStationBySIP(string SIP)
        {
            using (DXMeteorologicalEntities db = new DXMeteorologicalEntities())
            {
                Station model = db.Station.SingleOrDefault(a => a.StationIP == SIP);
                return model;
            }
        }
        /// <summary>
        /// 根据IP获取气象信息
        /// </summary>
        /// <param name="SIP"></param>
        /// <returns></returns>
        public static List<View_StationWeather> GetStationWeatherBySIP(string SIP)
        {
            using (DXMeteorologicalEntities db = new DXMeteorologicalEntities())
            {
                IQueryable<View_StationWeather> list = db.View_StationWeather.AsQueryable();
                List<View_StationWeather> RoleList = list.ToList();
                return RoleList;
            }
        }
        /// <summary>
        /// 增加气象信息
        /// </summary>
        /// <param name="WInfoModeln"></param>
        /// <returns></returns>
        public static bool StationWeatherAdd(StationWeather WInfoModeln)
        {
            using (DXMeteorologicalEntities db = new DXMeteorologicalEntities())
            {
                db.StationWeather.Add(WInfoModeln);
                return db.SaveChanges() > 0 ? true : false;
            }
        }
        /// <summary>
        /// 增加历史气象信息
        /// </summary>
        /// <param name="WInfoModeln"></param>
        /// <returns></returns>
        public static bool StationWeatherHistoryAdd(StationWeatherHistory WInfoModeln)
        {
            using (DXMeteorologicalEntities db = new DXMeteorologicalEntities())
            {
                db.StationWeatherHistory.Add(WInfoModeln);
                return db.SaveChanges() > 0 ? true : false;
            }
        }
        /// <summary>
        /// 增加气象信息
        /// </summary>
        /// <param name="WInfoModeln"></param>
        /// <returns></returns>
        public static int StationWeatherUpdate(StationWeather WInfoModeln)
        {
            using (DXMeteorologicalEntities db = new DXMeteorologicalEntities())
            {
                db.StationWeather.Attach(WInfoModeln);
                db.Entry(WInfoModeln).State = EntityState.Modified;
                return db.SaveChanges();
            }
        }
    }
}

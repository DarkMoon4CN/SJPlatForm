﻿using ChuanYe.IBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using ChuanYe.IDAL;

namespace ChuanYe.BLL
{
    public class LinchpinBLL : ILinchpinBLL
    {
        [Dependency]
        public ILinchpinDAL dal { get; set; }

        public Linchpin GetLinchpin(int linchpinID)
        {
            return dal.GetLinchpin(linchpinID);   
        }

        public List<Linchpin> GetSubLinchpin(int linchpinID)
        {
            return dal.GetSubLinchpin(linchpinID);
        }

        public List<Linchpin> GetLinchpin(List<long> linchpinIDs)
        {
            return dal.GetLinchpin(linchpinIDs);
        }
    }
}

﻿using ChuanYe.IDAL;
using ChuanYe.Models;
using ChuanYe.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.DAL
{
    public class GoodsManageDAL : IGoodsManageDAL
    {

        readonly SJZHPlatFormEntities ef = new SJZHPlatFormEntities();


        public List<W_Warehouse> GetWarehouseByPage(W_Warehouse info, GridPager2 pager)
        {

            var where = PredicateExtensionses.True<W_Warehouse>();

            if (info.WH_ID>0)
            {
                where = where.And(p => p.WH_ID==info.WH_ID);
            }
            if (!string.IsNullOrEmpty(info.WH_Name))
            {
                where = where.And(p => p.WH_Name.Contains(info.WH_Name));
            }

            if (!string.IsNullOrEmpty(info.WH_Address))
            {
                where = where.And(p => p.WH_Address.Contains(info.WH_Address));
            }

            if (!string.IsNullOrEmpty(info.WH_Phone))
            {
                where = where.And(p => p.WH_Address.Contains(info.WH_Phone));
            }

            if (!string.IsNullOrEmpty(info.WH_Phone))
            {
                where = where.And(p => p.WH_Address.Contains(info.WH_Phone));
            }

            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }

            if (pager.StartTime != null)
            {
                where = where.And(p => p.WH_TransDatetime >= pager.StartTime);
                if (pager.EndTime != null)
                {
                    where = where.And(p => p.WH_TransDatetime <= pager.EndTime);
                }
            }

            var list = ef.W_Warehouse.Where(where).OrderByDescending(o => o.WH_TransDatetime).Skip(pager.StartRow)
                                               .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.W_Warehouse.Where(where).Count();
            return list;
        }


        public List<W_Warehouse> GetWarehouse(W_Warehouse info)
        {
            var where = PredicateExtensionses.True<W_Warehouse>();

            if (info.WH_ID > 0)
            {
                where = where.And(p => p.WH_ID == info.WH_ID);
            }
            if (!string.IsNullOrEmpty(info.WH_Name))
            {
                where = where.And(p => p.WH_Name.Contains(info.WH_Name));
            }

            if (!string.IsNullOrEmpty(info.WH_Address))
            {
                where = where.And(p => p.WH_Address.Contains(info.WH_Address));
            }

            if (!string.IsNullOrEmpty(info.WH_Phone))
            {
                where = where.And(p => p.WH_Address.Contains(info.WH_Phone));
            }

            if (!string.IsNullOrEmpty(info.WH_Phone))
            {
                where = where.And(p => p.WH_Address.Contains(info.WH_Phone));
            }

          
            var list = ef.W_Warehouse.Where(where).OrderByDescending(o => o.WH_TransDatetime).ToList();
            return list;
        }



        public List<W_GoodsInfo> GetGoodsInfoByPage(W_GoodsInfo info, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<W_GoodsInfo>();

            if (info.G_ID >0)
            {
                where = where.And(p => p.WH_ID == info.G_ID);
            }

            if (!string.IsNullOrEmpty(info.G_NO))
            {
                where = where.And(p => p.G_NO == info.G_NO);
            }

            if (info.WH_ID >0)
            {
                where = where.And(p => p.WH_ID==info.WH_ID);
            }
            else if(!string.IsNullOrEmpty(info.WH_Name))
            {
                where = where.And(p => p.WH_Name.Contains(info.WH_Name));
            }

            if (pager.StartTime != null)
            {
                where = where.And(p => p.G_TransDatetime >= pager.StartTime);
                if (pager.EndTime != null)
                {
                    where = where.And(p => p.G_TransDatetime <= pager.EndTime);
                }
            }

            var list = ef.W_GoodsInfo.Where(where).OrderByDescending(o => o.G_TransDatetime).Skip(pager.StartRow)
                                               .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.W_GoodsInfo.Where(where).Count();
            return list;
        }

        public List<W_GoodsInfo> GetGoodsInfo(W_GoodsInfo info)
        {
            var where = PredicateExtensionses.True<W_GoodsInfo>();

            if (info.G_ID > 0)
            {
                where = where.And(p => p.WH_ID == info.G_ID);
            }

            if (!string.IsNullOrEmpty(info.G_NO))
            {
                where = where.And(p => p.G_NO == info.G_NO);
            }

            if (info.WH_ID > 0)
            {
                where = where.And(p => p.WH_ID == info.WH_ID);
            }
            else if (!string.IsNullOrEmpty(info.WH_Name))
            {
                where = where.And(p => p.WH_Name.Contains(info.WH_Name));
            }
            var list = ef.W_GoodsInfo.Where(where).OrderByDescending(o => o.G_TransDatetime).ToList();
            return list;
        }

    }
}

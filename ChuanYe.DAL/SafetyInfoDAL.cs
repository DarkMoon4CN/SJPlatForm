﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IDAL;
using System.Data.Entity.Core.Objects;
using ChuanYe.Models.Model;

namespace ChuanYe.DAL
{
    public class SafetyInfoDAL: ISafetyInfoDAL
    {
        /// <summary>
        /// 获取电话簿列表
        /// </summary>
        /// <returns></returns>
        public List<View_SmsPhone> GetLeader()
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                //IQueryable<View_SmsPhone> list = db.View_SmsPhone.Where(m=>m.IsLeader == true).OrderBy(m=>m.Sort).AsQueryable();
                return db.UsersInfos
                    .Where(m => m.IsSafeDelivery == true)
                    .Select(m => new View_SmsPhone
                    {
                        UserID = m.ID,
                        Phone = m.Phone,
                        UserName = m.UserName,
                        UserSort = m.Sort
                    }).OrderBy(m => m.UserSort).ToList();
            }
        }
        /// <summary>
        /// 保存报送信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateSafetyInfo(SafetyInfo model)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                db.SafetyInfo.Add(model);
                return db.SaveChanges() > 0 ? true : false;
            }
        }
        /// <summary>
        /// 获取报送短信信息
        /// </summary>
        /// <param name="search">搜索条件</param>
        /// <param name="pager">分页信息</param>
        /// <returns></returns>
        public SafetySms_SendInfoResult GetLastSafetyInfo(SafetySms_SendInfoSearch search, GridPager pager)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var TotalCountParameter = new ObjectParameter("TotalCount", typeof(int));
                List<SP_SafetySms_SendInfoList_Result> result = db.SP_SafetySms_SendInfoList(search.stateTime,search.endTime, search.SmsInfo, pager.rows, pager.page, TotalCountParameter).ToList();
                SafetySms_SendInfoResult resultInfo = new SafetySms_SendInfoResult();
                resultInfo.SafetySms_SendInfoList = result;
                resultInfo.totalRows = Convert.ToInt32(TotalCountParameter.Value);
                return resultInfo;
            }
        }
        /// <summary>
        /// 获取某天平安报送记录
        /// </summary>
        /// <param name="search">搜索条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        public SafetyInfoResult GetSafetyInfoListByDay(SafetyInfo search, GridPager Pager)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                if (search.ReportDate != null)
                {
                    startDate = Convert.ToDateTime(Convert.ToDateTime(search.ReportDate).ToShortDateString() + " 00:00:00");
                    endDate = Convert.ToDateTime(Convert.ToDateTime(search.ReportDate).ToShortDateString() + " 23:59:59");
                }
                var TotalCountParameter = new ObjectParameter("TotalCount", typeof(int));

                List<SP_SafetyInfoCityList_Result> List = db.SP_SafetyInfoCityList(startDate, endDate, Pager.rows, Pager.page, TotalCountParameter).ToList();
                SafetyInfoResult result = new SafetyInfoResult();
                result.SafetyInfoList = List;
                result.totalRows = Convert.ToInt32(TotalCountParameter.Value);
                return result;
            }
        }
        /// <summary>
        /// 市局获取报送统计列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public List<SP_SafetyInfoStatistics_Result> GetSafetyInfoStatistics(SafetyInfoStatisticsSearch search)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                return db.SP_SafetyInfoStatistics(search.stateTime, search.endTime).ToList();
            }
        }
        /// <summary>
        /// 区县获取报送统计列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public SP_DistrictSafetyInfoStatistics_Result GetDistrictSafetyInfoStatistics(DistrictSafetyInfoStatisticsSearch search)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var linq = db.SP_DistrictSafetyInfoStatistics(search.stateTime, search.endTime, search.DepId).ToList();
                if (linq.Count() == 0)
                {
                    SP_DistrictSafetyInfoStatistics_Result model = new SP_DistrictSafetyInfoStatistics_Result();
                    model.allCount = 0;
                    model.safeIsCount = 0;
                    model.safeNotCount = 0;
                    model.safepercent = 0;
                    return model;
                }
                return linq[0];
            }
        }
        /// <summary>
        /// 区县逐月查询报送记录
        /// </summary>
        /// <param name="DepId"></param>
        /// <returns></returns>
        public List<SafetyInfo> GetSafetyInfoForMonth(SafetyInfoForMonthSearch search)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                //DateTime stateDate = DateTime.Now.AddDays(1 - DateTime.Now.Day);
                List<SafetyInfo> resuit = db.SafetyInfo.Where(m => m.DepId == search.DepId && m.ReportDate > search.stateTime& m.ReportDate < search.endTime).ToList();
                return resuit;
            }
        }
        /// <summary>
        /// 获取某条平安报送信息
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public SafetyInfo GetSafetyInfoByID(int ID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                SafetyInfo resuit = db.SafetyInfo.FirstOrDefault(m => m.ID == ID);
                return resuit;
            }
        }
        /// <summary>
        /// 添加，修改平安报送备注
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int SafetyInfoUpdate(SafetyInfo model)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var Info = db.SafetyInfo.FirstOrDefault(m => m.ID == model.ID);
                if (Info != null)
                {
                    Info.Remarks = model.Remarks;
                }
                int i = db.SaveChanges();
                return i;
            }
        }
        /// <summary>
        /// 获取本区县今日是否已报送平安
        /// </summary>
        /// <param name="DepId"></param>
        /// <returns></returns>
        public int IsSendToday(int DepId)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                DateTime startDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
                DateTime endDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");
                var Info = db.SafetyInfo.FirstOrDefault(m => m.DepId == DepId & m.ReportDate >= startDate & m.ReportDate <= endDate);
                if (Info != null)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int IsSendToday(int DepId, DateTime? date=null)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                DateTime startDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
                DateTime endDate = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");
                if (date != null)
                {
                    startDate = Convert.ToDateTime(date.Value.ToShortDateString() + " 00:00:00");
                    endDate = Convert.ToDateTime(date.Value.ToShortDateString() + " 00:00:00");
                }
                var Info = db.SafetyInfo.FirstOrDefault(m => m.DepId == DepId & m.ReportDate >= startDate & m.ReportDate <= endDate);
                if (Info != null)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}

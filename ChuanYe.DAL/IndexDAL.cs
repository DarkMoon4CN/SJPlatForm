﻿using ChuanYe.Models;
using ChuanYe.Models.Model;
using ChuanYe.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.DAL
{
    public class IndexDAL
    {
        public static List<MessageModel> GetMessage(int ID, int DepID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                List<MessageModel> result = new List<MessageModel>();

                //邮件
                MessageModel model1 = new MessageModel();
                model1.Type = 1;
                model1.Count = db.InfosComm.Where(m => m.To == ID && m.IsView == false).Count();

                result.Add(model1);

                //公文
                MessageModel model2 = new MessageModel();
                model2.Type = 2;
                model2.Count = db.DocumentInfos
                    .Where(m => m.InceptDep == DepID && m.IsView == false)
                    .Count();
                result.Add(model2);

                //日程
                MessageModel model3 = new MessageModel();
                DateTime now = (DateTime)DateTime.Now.Date;
                DateTime nowInt = (DateTime)DateTime.Now.Date;
                List<Schedule> linq = db.Schedule
                    .Where(m => m.S_TransUser==ID && m.S_Date >= now && 
                    (m.S_TsDate== now || EntityFunctions.DiffDays(DateTime.Now, m.S_TsDate) <= m.S_Int))
                    .ToList();
                model3.Type = 3;
                model3.Count = linq.Count();
                result.Add(model3);

                //2018/09/20 日程提醒 更新
                var automatics = GetAutomatic(ID, 4, 0);
                result.Add(new MessageModel() {
                    Automatics = automatics,
                    Count = automatics.Count,
                    Message = string.Empty,
                    Type=4,
                });


                //2018/12/10 公文超过20分钟提醒
                MessageModel model5 = new MessageModel();
                DateTime expireTime = DateTime.Now.AddMinutes(-20);
                var query = db.DocumentInfos.Where(m => m.InceptDep == DepID && m.IsView == false && m.PublishDate > expireTime);
                int count5 = query.Count();
                model5.Type = 5;
                model5.Message = string.Format("您当前有[{0}]公文未处理", count5);
                model5.Count = count5;
                result.Add(model5);
                return result;
            }  

        }
        /// <summary>
        /// 获取未读邮件信息
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="DepID"></param>
        /// <returns></returns>
        public static MessageModel GetMailMessage(int ID, int DepID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                //邮件
                MessageModel model1 = new MessageModel();
                model1.Type = 1;
                model1.Count = db.InfosComm.Where(m => m.To == ID && m.IsView == false).Count();
                    //+ db.InfosComm.
                    //    Join(
                    //    db.UsersInfos,
                    //    I => I.From,
                    //    U => U.ID,
                    //    (I, U) => new MailPublicModel
                    //    {
                    //        ID = I.ID,
                    //        To = I.To,
                    //        FromName = U.UserName,
                    //        FSState = I.FSState,
                    //        GGState = I.GGState,
                    //        IsView = I.IsView,
                    //        JSState = I.JSState,
                    //        PublishTime = I.PublishTime,
                    //        Title = I.Title
                    //    })
                    //    .DefaultIfEmpty()
                    //    .Where(m => m.To == db.UsersInfos.FirstOrDefault(
                    //    B => B.Section == db.UsersInfos.FirstOrDefault(D => D.ID == ID).Section &&
                    //    B.Division == 1).ID &&
                    //    m.IsView == false
                    //    )
                    //    .OrderByDescending(P => P.PublishTime).Count();
                return model1;
            }
        }


        /// <summary>
        /// 获取未读邮件信息2 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="DepID"></param>
        /// <returns></returns>
        public static MessageModel GetMailMessage2(int ID, int DepID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                MessageModel model = new MessageModel();
                var rst = db.InfosComm.Join(
                    db.UsersInfos,
                    A => A.From,
                    B => B.ID,
                    (A, B) => new ReceiveMailModel
                    {
                        ID = A.ID,
                        FromName = B.UserName,
                        Title = A.Title,
                        PublishTime = A.PublishTime,
                        IsView = A.IsView,
                        To = A.To
                    }
                    ).Where(M => M.To == ID &&
                    M.IsView == false);
                int count = rst.Count();
                model.Type = 1;
                model.Count = count;
                return model;
            }
        }




        /// <summary>
        /// 获取公文提醒
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="DepID"></param>
        /// <returns></returns>
        public static MessageModel GetDocumentMessage(int ID, int DepID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                //公文
                MessageModel model = new MessageModel();
                model.Type = 2;
                model.Count = db.DocumentInfos
                    .Where(m => m.InceptDep == DepID && m.IsView == false)
                    .Count();
                return model;
            }
        }
        /// <summary>
        /// 获取日志提醒
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="DepID"></param>
        /// <returns></returns>
        public static MessageModel GetScheduleMessage(int ID, int DepID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                //日程
                MessageModel model = new MessageModel();
                DateTime now = (DateTime)DateTime.Now.Date;
                DateTime nowInt = (DateTime)DateTime.Now.Date;
                List<Schedule> linq = db.Schedule
                    .Where(m => m.S_TransUser == ID && m.S_Date >= now &&
                    (m.S_TsDate == now || EntityFunctions.DiffDays(DateTime.Now, m.S_TsDate) <= m.S_Int))
                    .ToList();
                model.Type = 3;
                model.Count = linq.Count();
                return model;
            }
        }
        /// <summary>
        /// 获取浏览量
        /// </summary>
        /// <returns></returns>
        public static int GetBrowsingTotal()
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                return db.BrowsingTotal.FirstOrDefault().Total;
            }
        }

        /// <summary>
        /// 获取在线值守列表
        /// </summary>
        /// <returns></returns>
        public static List<ManagerOnLion> GetManager()
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                return db.UsersInfos.Where(m => m.IsManager == true)
                    .Select(m => new ManagerOnLion
                    {
                        UserID=m.ID,
                        UserName = m.UserName
                    }).ToList();
            }
        }

        /// <summary>
        ///  Sort大于0  IsManager等于 true
        /// </summary>
        /// <returns></returns>
        public static List<ManagerOnLion> GetManager2()
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                return db.UsersInfos.Where(m => m.IsManager == true && m.Sort > 0).OrderBy(o => o.Sort)
                    .Select(m => new ManagerOnLion
                    {
                        UserID = m.ID,
                        UserName = m.UserName,
                        Sort = m.Sort,
                        Phone=m.Phone,
                    }).ToList();
            }
        }

        /// <summary>
        /// 接收未读日程信息 Type消息类型 0邮件，1.未读邮件，2.公文提醒 3.日志提醒 4.日程提醒
        /// </summary>
        /// <param name="receiveUserID"></param>
        /// <param name="type"></param>
        /// <param name="isReplay"></param>
        /// <returns></returns>
        public static List<MessageAutomatic> GetAutomatic(int receiveUserID,int  type ,int isReplay = 0)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var where = PredicateExtensionses.True<Automatic>();
                if (receiveUserID > 0)
                {
                    DateTime startTime= Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
                    DateTime endTime = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");
                    where = where.And(p => p.M_ReceiveUserID == receiveUserID && p.M_Type==type);
                    where = where.And(p =>p.M_SendTime  >= startTime);
                    where = where.And(p => p.M_SendTime < endTime);
                    where = where.And(p =>p.M_IsReplay==0);
                    var list= db.Automatic.Where(where).Select(s=>new  MessageAutomatic() {
                         M_ID=s.M_ID,
                         M_SendContent=s.M_SendContent,
                         M_SendTime=s.M_SendTime,
                         M_SendCustomData=s.M_SendCustomData,
                    }).ToList();
                    return list;
                }
                return null;
            }
       
        }

    }
}

﻿using ChuanYe.Models;
using ChuanYe.Utils;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.IDAL;
namespace ChuanYe.DAL
{
    public class AutomaticDAL: IAutomaticDAL
    {
        readonly SJZHPlatFormEntities ef = new SJZHPlatFormEntities();

        public int AddAutomatic(Automatic info)
        {
            ef.Automatic.Add(info);
            ef.SaveChanges();
            return info.M_ID;
        }

        public List<Automatic> GetAutomatic(string receiveArea, int receiveUserID, string receiveUserName, int isReplay=0)
        {
            var where = PredicateExtensionses.True<Automatic>();
            if (!string.IsNullOrEmpty(receiveArea))
            {
                where = where.And(p=>p.M_ReceiveArea== receiveArea);
            }

            if (!string.IsNullOrEmpty(receiveUserName))
            {
                where = where.And(p => p.M_ReceiveUserName == receiveUserName);
            }

            if (receiveUserID>0)
            {
                where = where.And(p => p.M_ReceiveUserID == receiveUserID);
            }

            return ef.Automatic.Where(where).ToList();
        }

        public int UpdateAutomaticReplay(int mid,int isReplay)
        {
            var where = PredicateExtensionses.True<Automatic>();
            where = where.And(p => p.M_ID == mid);
            return ef.Automatic.Where(where).Update(u => new Automatic { M_IsReplay = isReplay });
        }

        public int UpdateAutomaticReplay(int receiveUserID,DateTime? sendTime,int type,int isReplay=0)
        {
            var where = PredicateExtensionses.True<Automatic>();
            if (receiveUserID > 0 && sendTime != null && type > 0)
            {
                where = where.And(p => p.M_ReceiveUserID == receiveUserID);
                where = where.And(p => p.M_SendTime == sendTime);
                where = where.And(p => p.M_Type == type);
            }
            return ef.Automatic.Where(where).Update(u => new Automatic { M_IsReplay = isReplay });
        }

        public List<Automatic> GetAutomatic(List<string> customDatas)
        {
            var where = PredicateExtensionses.True<Automatic>();
            where = where.And(p => customDatas.Contains(p.M_SendCustomData));
            var query = ef.Automatic.Where(p => customDatas.Contains(p.M_SendCustomData));
            //System.Diagnostics.Debug.WriteLine(query);
            return query.ToList();
        }



        public int DeleteAutomatic(string customData)
        {
            return ef.Automatic.Where(p => p.M_SendCustomData == customData).Delete();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.IDAL;
using System.Data.Entity;

namespace ChuanYe.DAL
{
    public class Sms_SendInfoDAL: ISms_SendInfoDAL
    {
        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateSms_SendInfo(Sms_SendInfo model)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                db.Sms_SendInfo.Add(model);
                return db.SaveChanges() > 0 ? true : false;
            }
        }
        /// <summary>
        /// 获取通讯录
        /// </summary>
        /// <returns></returns>
        public List<ChuanYe.Models.Model.View_SmsPhone> GetSmsPhone()
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                //IQueryable<View_SmsPhone> list = db.View_SmsPhone.OrderBy(m=>m.Sort).AsQueryable();
                //return list.ToList();
                return db.UsersInfos
                    .Join(
                    db.Departments,
                    A => A.Section,
                    B => B.ID,
                    (A, B) => new ChuanYe.Models.Model.View_SmsPhone
                    {
                        DepID = A.Section,
                        Phone = A.Phone,
                        Division = A.Division,
                        DepSort = B.Sort,
                        UserSort = A.Sort,
                        PositionSort = null,
                        PositionID = A.PositionID,
                        UserID = A.ID,
                        UserName = A.UserName
                    }
                    )
                    .Join(
                    db.Position,
                    C => C.PositionID,
                    D => D.ID,
                    (C, D) => new ChuanYe.Models.Model.View_SmsPhone
                    {
                        DepID = C.DepID,
                        Phone = C.Phone,
                        Division = C.Division,
                        DepSort = C.DepSort,
                        UserSort = C.UserSort,
                        PositionSort = D.Sort,
                        PositionID = C.PositionID,
                        UserID = C.UserID,
                        UserName = C.UserName
                    }
                    )
                    .Where(m => m.Division == 0)
                    .OrderBy(m => m.DepSort)
                    .ThenBy(m => m.PositionSort)
                    .ThenBy(m => m.UserSort)
                    .ToList();
            }
        }
        /// <summary>
        /// 获取所有部门
        /// </summary>
        /// <returns></returns>
        public List<Departments> GetAllDepartments()
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                IQueryable<Departments> list = db.Departments.OrderBy(m => m.Sort).AsQueryable();
                return list.ToList();
            }
        }
        /// <summary>
        /// 获取短信列表
        /// </summary>
        /// <param name="search">搜索条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        public Sms_SendInfoResult GetSms_SendInfo(Sms_SendInfo search, GridPager Pager)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                Sms_SendInfoResult result = new Sms_SendInfoResult();
                if (search.SendDate == null)
                {
                    result.Sms_SendInfoList = db.Sms_SendInfo.Where(
                        m => (search.Receiver == "" || m.Receiver.Contains(search.Receiver)) &&
                        (search.SmsInfo == "" || m.SmsInfo.Contains(search.SmsInfo)) &&
                        (search.IsAchieved == null || m.IsAchieved == search.IsAchieved) &&
                        m.IsSafety==null
                        ).OrderByDescending(m => m.SendDate).Skip(Pager.rows * (Pager.page - 1)).Take(Pager.rows).ToList();
                    result.totalRows = db.Sms_SendInfo.Where(
                        m => (search.Receiver == "" || m.Receiver.Contains(search.Receiver)) &&
                        (search.SmsInfo == "" || m.SmsInfo.Contains(search.SmsInfo))&&
                        (search.IsAchieved == null || m.IsAchieved == search.IsAchieved) &&
                        m.IsSafety == null
                        ).Count();
                }
                else {
                    DateTime endTime = Convert.ToDateTime(Convert.ToDateTime(search.SendDate).ToShortDateString() + " 23:59:59");
                    result.Sms_SendInfoList = db.Sms_SendInfo.Where(
                        m => (search.Receiver == "" || m.Receiver.Contains(search.Receiver)) &&
                        (m.SendDate >= search.SendDate) &&
                        (m.SendDate <= endTime) &&
                        (search.SmsInfo == "" || m.SmsInfo.Contains(search.SmsInfo)) &&
                        (search.IsAchieved == null || m.IsAchieved == search.IsAchieved) &&
                        m.IsSafety == null
                        ).OrderByDescending(m => m.SendDate).Skip(Pager.rows * (Pager.page - 1)).Take(Pager.rows).ToList();
                    result.totalRows = db.Sms_SendInfo.Where(
                        m => (search.Receiver == "" || 
                        m.Receiver.Contains(search.Receiver)) &&
                        (search.SmsInfo == "" || m.SmsInfo.Contains(search.SmsInfo)) &&
                        (search.IsAchieved == null || m.IsAchieved == search.IsAchieved) &&
                        (m.SendDate >= search.SendDate) &&
                        (m.SendDate <= endTime) &&
                        m.IsSafety == null)
                        .Count();
                }
                return result;
            }
        }
        /// <summary>
        /// 删除短信
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int Sms_SendInfoDelete(int ID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var DelSms_SendInfo = db.Sms_SendInfo.FirstOrDefault(m => m.ID == ID);
                if (DelSms_SendInfo != null)
                {
                    db.Sms_SendInfo.Remove(DelSms_SendInfo);
                }
                int i = db.SaveChanges();
                return i;
            }
        }
        /// <summary>
        /// 获取全部短信模板
        /// </summary>
        /// <returns></returns>
        public List<SMS_Model> GetSMS_Model()
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                IQueryable<SMS_Model> list = db.SMS_Model.OrderByDescending(m=>m.SMS_MID).AsQueryable();
                return list.ToList();
            }
        }
        /// <summary>
        /// 创建短信模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateSMS_Model(SMS_Model model)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                db.SMS_Model.Add(model);
                return db.SaveChanges() > 0 ? true : false;
            }
        }
        /// <summary>
        /// 删除短信模板
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public int SMS_ModelDelete(int ID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var Info = db.SMS_Model.FirstOrDefault(m => m.SMS_MID == ID);
                if (Info != null)
                {
                    db.SMS_Model.Remove(Info);
                }
                int i = db.SaveChanges();
                return i;
            }
        }

        /// <summary>
        ///根据ID获取短信模板
        /// </summary>
        /// <param name="ID">主键ID</param>
        /// <returns></returns>
        public SMS_Model GetSMS_ModelByID(int ID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                return db.SMS_Model.FirstOrDefault(m => m.SMS_MID == ID);
            }
        }
        /// <summary>
        /// 修改短信模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int SMS_ModelUpdate(SMS_Model model)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var info = db.SMS_Model.FirstOrDefault(m => m.SMS_MID == model.SMS_MID);
                if (info != null)
                {
                    info.SMS_Mcont = model.SMS_Mcont;
                    info.SMS_MType = model.SMS_MType;
                    //Info.SMS_Mtitle = model.SMS_Mtitle;
                    db.SMS_Model.Add(info);
                    db.Entry<SMS_Model>(info).State = EntityState.Modified;
                   return db.SaveChanges();
                }
                return 0;
            }
        }


        public SMS_ModelResult GetSMS_Model(string keyword,int? sms_mtype, GridPager Pager)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var linq = from a in db.SMS_Model select a;
                if (!string.IsNullOrEmpty(keyword))
                {
                    linq = linq.Where(p => p.SMS_Mcont.Contains(keyword));
                }
                if (sms_mtype.HasValue)
                {
                    linq = linq.Where(p => p.SMS_MType== sms_mtype.Value);
                }
                SMS_ModelResult result = new SMS_ModelResult();
                result.totalRows = linq.Count();
                result.SMS_ModelList= linq.OrderByDescending(m => m.SMS_MID)
                    .Skip(Pager.rows * (Pager.page - 1))
                    .Take(Pager.rows)
                    .ToList();
                return result;
            }
        }


        public List<SMS_Model> GetSMS_Model(int  sms_mtype)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                return db.SMS_Model.Where(m => m.SMS_MType == sms_mtype).ToList();
            }
        }




    }
}

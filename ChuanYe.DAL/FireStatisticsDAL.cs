﻿using ChuanYe.IDAL;
using ChuanYe.Models;
using ChuanYe.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.DAL
{
    public class FireStatisticsDAL: IFireStatisticsDAL
    {
        readonly SJZHPlatFormEntities ef = new SJZHPlatFormEntities();

        #region 单项
        public dynamic CountyStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();

            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
           
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
           
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.F_AddressCounty ,g.LinchpinName })
                         .Select(s => new { s.Key.F_AddressCounty,s.Key.LinchpinName, Count = s.Count() }).ToList();
            return result;
        }

        public dynamic TimeStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();

            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);

            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);

            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.FFR_TransDatetime.Hour })
                .Select(s => new { s.Key.Hour, Count = s.Count() }).ToList();
            return result;
        }

        public dynamic FireWhyStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result=ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.FFR_WhyID })
                .Select(s => new { s.Key.FFR_WhyID, Count = s.Count() }).ToList();
            return result;
        }

        public dynamic AlarmTypeStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.FFR_Type })
                .Select(s => new { s.Key.FFR_Type, Count = s.Count() }).ToList();
            return result;
        }

        public dynamic FireTypeStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.FFR_FireTypeID })
                .Select(s => new { s.Key.FFR_FireTypeID, Count = s.Count() })
                .ToList();
            return result;
        }


        #endregion

        #region 汇总
        public dynamic GareaStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.F_AddressCounty, g.LinchpinName })
                           .Select(s => 
                                     new { s.Key.F_AddressCounty, s.Key.LinchpinName,
                                           Count = s.Sum(sum=>  (sum.FFR_Gunit== "公顷"? sum.FFR_Garea*100000:sum.FFR_Garea)) })
                           .ToList();
            return result;
         }

        public dynamic LareaStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.F_AddressCounty, g.LinchpinName })
                           .Select(s =>
                                     new {
                                         s.Key.F_AddressCounty,
                                         s.Key.LinchpinName,
                                         Count = s.Sum(sum => (sum.FFR_Lunit == "公顷" ? sum.FFR_Larea * 100000 : sum.FFR_Larea))
                                     })
                           .ToList();
            return result;
        }

        public  dynamic NumStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.F_AddressCounty, g.LinchpinName })
                           .Select(s =>
                                     new {
                                         s.Key.F_AddressCounty,
                                         s.Key.LinchpinName,
                                         Count = s.Sum(sum => (sum.FFR_Num))
                                     })
                           .ToList();
            return result;
        }


        public dynamic DieStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.F_AddressCounty, g.LinchpinName })
                           .Select(s =>
                                     new {
                                         s.Key.F_AddressCounty,
                                         s.Key.LinchpinName,
                                         Count = s.Sum(sum => (sum.FFR_Die))
                                     })
                           .ToList();
            return result;
        }

        public dynamic LossStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.F_AddressCounty, g.LinchpinName })
                           .Select(s =>
                                     new {
                                         s.Key.F_AddressCounty,
                                         s.Key.LinchpinName,
                                         Count = s.Sum(sum => (sum.FFR_Loss))
                                     })
                           .ToList();
            return result;
        }


        public dynamic SummaryStatistics(DateTime startTime, DateTime endTime)
        {
            var where = PredicateExtensionses.True<View_FinalReportStatistics>();
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, startTime) <= 0);
            where = where.And(p => DbFunctions.DiffDays(p.FFR_TransDatetime, endTime) >= 0);
            var result = ef.View_FinalReportStatistics.Where(where).GroupBy(g => new { g.F_AddressCounty, g.LinchpinName })
                           .Select(s =>
                                     new {
                                         s.Key.F_AddressCounty,
                                         s.Key.LinchpinName,
                                         Garea = s.Sum(sum => (sum.FFR_Gunit == "公顷" ? sum.FFR_Garea * 100000 : sum.FFR_Garea)),
                                         Larea = s.Sum(sum => (sum.FFR_Lunit == "公顷" ? sum.FFR_Larea * 100000 : sum.FFR_Larea)),
                                         Num = s.Sum(sum => (sum.FFR_Num)),
                                         Die = s.Sum(sum => (sum.FFR_Die)),
                                         Loss = s.Sum(sum => (sum.FFR_Loss))
                                     })
                           .ToList();
            return result;
        }

        #endregion



    }
}

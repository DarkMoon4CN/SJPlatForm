﻿using ChuanYe.IDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;

namespace ChuanYe.DAL
{
    public class LinchpinDAL : ILinchpinDAL
    {

        readonly SJZHPlatFormEntities ef = new SJZHPlatFormEntities();

        public Linchpin GetLinchpin(int linchpinID)
        {
            return ef.Linchpin.Where(p => p.LinchpinIID == linchpinID).FirstOrDefault();
        }

        public List<Linchpin> GetSubLinchpin(int linchpinID)
        {
            return ef.Linchpin.Where(p => p.ParentID == linchpinID).ToList();  
        }


        public List<Linchpin> GetLinchpin(List<long> linchpinIDs)
        {
            return ef.Linchpin.Where(p => linchpinIDs.Contains(p.LinchpinIID)).OrderBy(o=>o.LinchpinIID).ToList();
        }



    }
}

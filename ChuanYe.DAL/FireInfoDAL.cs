﻿using ChuanYe.IDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.Utils;
using EntityFramework.Extensions;
using ChuanYe.Models.Model;
using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace ChuanYe.DAL
{
    public class FireInfoDAL : IFireInfoDAL
    {
        readonly SJZHPlatFormEntities ef = new SJZHPlatFormEntities();

        public int Add(F_FireInfo info)
        {
            ef.F_FireInfo.Add(info);
            ef.SaveChanges();
            return info.F_FireID;
        }

        public int AddRecord(M_Record info)
        {
            ef.M_Record.Add(info);
            ef.SaveChanges();
            return info.MR_ID;
        }

        public List<F_FireInfo> ListByPager(string[] addressCountys, int fireSate, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_FireInfo>();

            where = where.And(p => p.F_FireID > pager.DataId);
            if (addressCountys != null && addressCountys.Length > 0)
            {
                where = where.And(p => addressCountys.Contains(p.F_AddressCounty));
            }
            if (!string.IsNullOrEmpty(pager.Keyword))
            {
                where = where.And(p => p.F_FireName.Contains(pager.Keyword) || p.F_Address.Contains(pager.Keyword));
            }
            if (pager.StartTime != null)
            {
                where = where.And(p => p.F_HappenDatetime >= pager.StartTime);
                if (pager.EndTime != null)
                {
                    where = where.And(p => p.F_HappenDatetime <= pager.EndTime);
                }
            }
            where = where.And(p => p.F_IsState == fireSate);

            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }

            var list = ef.F_FireInfo.Where(where).OrderByDescending(o => o.F_AlarmTime)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_FireInfo.Where(where).Count();
            return list;
        }


        public List<M_Record> MRListByFireID(int fireID, GridPager2 pager, int searchLevel = 1)
        {
            var where = PredicateExtensionses.True<M_Record>();
            where = where.And(p => p.MR_ID > pager.DataId);
            where = where.And(p => p.F_FireID == fireID);
            if (searchLevel == 1)
            {
                //where = where.And(p => p.MR_Type <= 20 && p.MR_Type!= 18);
                where = where.And(p => p.MR_Type != 18);//区县隐藏文档
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }

            var list = ef.M_Record.Where(where).OrderByDescending(o => o.MR_AddTime).ThenBy(t => t.MR_HandleTime).Skip(pager.StartRow)
                                               .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.M_Record.Where(where).Count();
            return list;
        }

        public List<M_Record> MRListByFireIDs(List<int> fireIDs, int searchLevel = 1)
        {
            var where = PredicateExtensionses.True<M_Record>();
            where = where.And(p => fireIDs.Contains(p.F_FireID.Value));
            if (searchLevel == 1)
            {
                where = where.And(p => p.MR_Type <= 20 && p.MR_Type != 18);
            }
            var result = ef.M_Record.Where(where).GroupBy(g => new { g.F_FireID })
                .Select(s => new { MR_ID = s.Max(m => m.MR_ID) })
                .Join(ef.M_Record,
                    E => E.MR_ID,
                    F => F.MR_ID, (E, F) => new
                    {
                        F_FireID = F.F_FireID,
                        MR_ID = F.MR_ID,
                        MR_Type = F.MR_Type,
                    }).OrderByDescending(o => o.MR_ID).ToList();
            List<M_Record> list = new List<M_Record>();
            foreach (var item in result)
            {
                M_Record mr = new M_Record();
                mr.MR_ID = item.MR_ID;
                mr.F_FireID = item.F_FireID;
                mr.MR_Type = item.MR_Type;
                list.Add(mr);
            }
            return list;
        }


        public List<M_Record> CKRecordTypeByTypes(int fireID, List<int> types)
        {
            List<int> fireIDs = new List<int>() { fireID };
            var list = MRListByFireIDs(fireIDs);
            var result = list.Where(p => types.Contains(p.MR_Type.Value)).ToList();
            return result;
        }

        public int AddCityFireAsk(F_CityFireAsk info)
        {
            ef.F_CityFireAsk.Add(info);
            ef.SaveChanges();
            return info.CFA_ID;
        }

        public F_CityFireAsk CityFireAskDetail(int cfa_ID)
        {
            return ef.F_CityFireAsk.Where(p => p.CFA_ID == cfa_ID).FirstOrDefault();
        }

        public int AddCountyFireAnswer(F_CountyFireAnswer info)
        {
            ef.F_CountyFireAnswer.Add(info);
            ef.SaveChanges();
            return info.Cou_ID;
        }

        public List<F_CountyFireAnswer> ExistCountyFireAnswer(int cfa_ID)
        {
            return ef.F_CountyFireAnswer.Where(p => p.CFA_ID == cfa_ID).ToList();
        }

        public int UpdateCountyFireAnswer(F_CountyFireAnswer info)
        {
            var entity = ef.F_CountyFireAnswer.Where(p => p.Cou_ID == info.Cou_ID).FirstOrDefault();
            entity.Cou_HandleTime = info.Cou_HandleTime;
            entity.Cou_TransDatetime = info.Cou_TransDatetime;
            entity.Cou_AnswerContent = info.Cou_AnswerContent;
            ef.SaveChanges();
            return entity.Cou_ID;
        }

        public int AddFireFirstReport(F_FireFirstReport info)
        {
            var entity = ef.F_FireFirstReport.Where(p => p.F_FireID == info.F_FireID).FirstOrDefault();
            if (entity == null)
            {
                ef.F_FireFirstReport.Add(info);
                ef.SaveChanges();
                return info.R_ID;
            }
            else
            {
                return entity.R_ID;
            }

        }

        public F_FireFirstReport FireFirstReportDetail(int fireID)
        {
            return ef.F_FireFirstReport.Where(p => p.F_FireID == fireID).FirstOrDefault();
        }

        public int AddFireContinueReport(F_FireContinueReport info)
        {
            ef.F_FireContinueReport.Add(info);
            ef.SaveChanges();
            return info.C_ID;
        }

        public F_FireContinueReport FireContinueReportDetail(int fireID)
        {
            return ef.F_FireContinueReport.Where(p => p.F_FireID == fireID).OrderBy(o => o.C_TransDatetime).FirstOrDefault();
        }
        public List<F_FireContinueReport> GetFireContinueReport(int fireID)
        {
            return ef.F_FireContinueReport.Where(p => p.F_FireID == fireID).OrderBy(o => o.C_TransDatetime).ToList();
        }



        public M_Record DetailMRecordByFireID(int fireID, int mrType, int searchLevel = 1)
        {
            var where = PredicateExtensionses.True<M_Record>();
            where = where.And(p => p.F_FireID == fireID && p.MR_Type == mrType);
            if (searchLevel == 1)
            {
                where = where.And(p => p.MR_Type <= 20);
            }
            return ef.M_Record.Where(where).FirstOrDefault();
        }

        public int AddCatalog(Catalog info)
        {
            ef.Catalog.Add(info);
            ef.SaveChanges();
            return info.A_ID;
        }

        public Catalog FireCatalogDetail(int fireID)
        {
            return ef.Catalog.Where(p => p.F_FireID == fireID).OrderByDescending(o => o.A_TransDatetime).FirstOrDefault();
        }

        public List<Catalog> GetFireCatalog(int fireID)
        {
            return ef.Catalog.Where(p => p.F_FireID == fireID).OrderByDescending(o => o.A_TransDatetime).ToList();
        }


        public int AddFireAutomatic(M_Automatic info)
        {
            ef.M_Automatic.Add(info);
            ef.SaveChanges();
            return info.M_ID;
        }

        public List<M_Automatic> GetFireAutomatic(int fireID, List<int> mTypes)
        {
            var where = PredicateExtensionses.True<M_Automatic>();
            if (fireID > 0)
            {
                where = where.And(p => p.F_FireID == fireID);
            }
            if (mTypes != null && mTypes.Count > 0)
            {
                where = where.And(p => mTypes.Contains(p.M_Type.Value));
            }

            return ef.M_Automatic.Where(where).ToList();
        }

        public List<M_Automatic> GetFireAutomatic(List<int> fireIDs, List<int> mTypes)
        {
            var where = PredicateExtensionses.True<M_Automatic>();
            if (fireIDs != null && fireIDs.Count > 0)
            {
                where = where.And(p => fireIDs.Contains(p.F_FireID.Value));
            }
            if (mTypes != null && mTypes.Count > 0)
            {
                where = where.And(p => mTypes.Contains(p.M_Type.Value));
            }
            return ef.M_Automatic.Where(where).ToList();
        }


        public int UpdateFireAutoMatic(int fireID, List<int> mTypes, int replay = 0)
        {
            var where = PredicateExtensionses.True<M_Automatic>();
            where = where.And(p => p.F_FireID == fireID);
            if (mTypes != null && mTypes.Count > 0)
            {
                where = where.And(p => mTypes.Contains(p.M_Type.Value));
            }
            return ef.M_Automatic.Where(where).Update(u => new M_Automatic { M_IsReplay = 0 });
        }
        public int DeleteFireAutoMatic(int fireID, int mType)
        {
            var where = PredicateExtensionses.True<M_Automatic>();

            if (fireID > 0 && mType > 0)
            {
                where = where.And(p => p.F_FireID == fireID);
                where = where.And(p => p.M_Type.Value == mType);
            }
            return ef.M_Automatic.Where(where).Delete();
        }

        public int AddApplyRespond(F_ApplyRespond info)
        {
            ef.F_ApplyRespond.Add(info);
            ef.SaveChanges();
            return info.AR_ID;
        }

        public F_FireInfo FireInfoDetail(int fireID)
        {
            var where = PredicateExtensionses.True<F_FireInfo>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FireInfo.Where(where).FirstOrDefault();
            return result;
        }

        public F_ApplyRespond FireApplyRespondDetail(int fireID)
        {
            var where = PredicateExtensionses.True<F_ApplyRespond>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_ApplyRespond.Where(where).FirstOrDefault();
            return result;
        }


        public int AddReportOutFire(F_ReportOutFire info)
        {
            ef.F_ReportOutFire.Add(info);
            ef.SaveChanges();
            return info.Res_ID;
        }

        public F_ReportOutFire ReportOutFireDetail(int fireID)
        {
            var where = PredicateExtensionses.True<F_ReportOutFire>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_ReportOutFire.Where(where).FirstOrDefault();
            return result;
        }

        public int AddFireFinalReport(F_FireFinalReport info)
        {
            ef.F_FireFinalReport.Add(info);
            ef.SaveChanges();
            return info.FFR_ID;
        }


        public F_FireFinalReport FireFinalReportDetail(int fireID)
        {
            var where = PredicateExtensionses.True<F_FireFinalReport>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FireFinalReport.Where(where).FirstOrDefault();
            return result;
        }

        public List<F_FireFinalReport> GetFireFinalReport(List<int> fireIDs) {
            var where = PredicateExtensionses.True<F_FireFinalReport>();
            where = where.And(p => fireIDs.Contains(p.F_FireID));
            var result = ef.F_FireFinalReport.Where(where).ToList();
            return result;
        }


        public int UpdateFireInfoState(int fireID, int isState)
        {
            var where = PredicateExtensionses.True<F_FireInfo>();
            where = where.And(p => p.F_FireID == fireID);
            return ef.F_FireInfo.Where(where).Update(u => new F_FireInfo { F_IsState = 1 });
        }


        public int AddFireTransField(F_FireTransField info)
        {
            ef.F_FireTransField.Add(info);
            ef.SaveChanges();
            return info.FT_ID;
        }

        public F_FireTransField FireTransFieldDetail(int fireID)
        {
            var where = PredicateExtensionses.True<F_FireTransField>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FireTransField.Where(where).FirstOrDefault();
            return result;
        }


        public int AddFireSite(F_FireSite info)
        {
            ef.F_FireSite.Add(info);
            ef.SaveChanges();
            return info.FS_ID;
        }


        public F_FireSite FireSiteDetail(int fireID)
        {
            var where = PredicateExtensionses.True<F_FireSite>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FireSite.Where(where).FirstOrDefault();
            return result;
        }

        public List<F_FireSite> GetFireSiteInfoByFireID(int fireID)
        {
            var where = PredicateExtensionses.True<F_FireSite>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FireSite.Where(where).ToList();
            return result;
        }


        public int UpdateFireSite(F_FireSite info)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var entity = db.F_FireSite.Where(p => p.FS_ID == info.FS_ID).FirstOrDefault();
                if (entity != null)
                {
                    entity.F_FireID = info.F_FireID;
                    entity.FS_Longitude = info.FS_Longitude;
                    entity.FS_Latitude = info.FS_Latitude;
                    entity.FS_Cont = info.FS_Cont;
                    entity.FS_TransDatetime = info.FS_TransDatetime;
                    entity.FS_HandleTime = info.FS_HandleTime;
                    entity.FS_TransUser = info.FS_TransUser;
                    entity.FS_TransIP = info.FS_TransIP;
                    db.F_FireSite.Add(entity);
                    db.Entry<F_FireSite>(entity).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    db.F_FireSite.Add(info);
                    db.Entry<F_FireSite>(info).State = EntityState.Added;
                    db.SaveChanges();
                }
                return entity.FS_ID;
            }
        }



        public int AddFireDocumentation(F_FireDocumentation info)
        {
            ef.F_FireDocumentation.Add(info);
            ef.SaveChanges();
            return info.FD_ID;
        }
        public int UpdateFireDocumentation(F_FireDocumentation info)
        {
            var where = PredicateExtensionses.True<F_FireDocumentation>();
            where = where.And(p => p.FD_ID == info.FD_ID);
            return ef.F_FireDocumentation.Where(where).Update(u => new F_FireDocumentation {
                FD_MessageContent = info.FD_MessageContent,
                FD_MessageTime = info.FD_MessageTime,
                FD_TransDatetime = info.FD_TransDatetime,
                FD_TransUser = info.FD_TransUser,
                FD_TransIP = info.FD_TransIP,
            });

        }


        public List<F_FireDocumentation> GetFireDocumentation(int fireID)
        {
            var where = PredicateExtensionses.True<F_FireDocumentation>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FireDocumentation.Where(where).ToList();
            return result;
        }

        public int AddFrontCommand(F_FrontCommand info)
        {
            ef.F_FrontCommand.Add(info);
            ef.SaveChanges();
            return info.FC_ID;
        }

        public int UpdateFrontCommand(F_FrontCommand info)
        {
            var where = PredicateExtensionses.True<F_FrontCommand>();
            where = where.And(p => p.FC_ID == info.FC_ID && p.F_FireID == info.F_FireID);
            return ef.F_FrontCommand.Where(where)
                     .Update(u => new F_FrontCommand {
                         FC_CommanderName = info.FC_CommanderName,
                         FC_Tel = info.FC_Tel,
                         FC_LocaleLongitude = info.FC_LocaleLongitude,
                         FC_LocaleLatitude = info.FC_LocaleLatitude,
                         FC_Member = info.FC_Member,
                         FC_HandleTime = info.FC_HandleTime,
                         FC_TransDatetime = info.FC_TransDatetime,
                         FC_TransUser = info.FC_TransUser,
                         FC_TransIP = info.FC_TransIP,
                         FC_Address = info.FC_Address
                     });
        }

        public F_FrontCommand FrontCommandDetail(int fireID)
        {
            var where = PredicateExtensionses.True<F_FrontCommand>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FrontCommand.Where(where).OrderByDescending(o => o.FC_ID).FirstOrDefault();
            return result;
        }

        public List<F_FrontCommand> GetFrontCommand(int fireID)
        {
            var where = PredicateExtensionses.True<F_FrontCommand>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FrontCommand.Where(where).OrderByDescending(o => o.FC_ID).ToList();
            return result;
        }

        public F_StepResponse StepResponseDetail(int fireID, int responseGrade)
        {
            var where = PredicateExtensionses.True<F_StepResponse>();
            where = where.And(p => p.F_FireID == fireID);
            if (responseGrade != 0)
            {
                where = where.And(p => p.SR_ResponseGrade == responseGrade);
            }
            var result = ef.F_StepResponse.Where(where).OrderByDescending(o => o.SR_ID).FirstOrDefault();
            return result;
        }

        public int AddStepResponse(F_StepResponse info)
        {
            ef.F_StepResponse.Add(info);
            ef.SaveChanges();
            return info.SR_ID;
        }

        public int AddStepResponseTempPeople(List<F_StepResponseTempPeople> infos)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                foreach (var item in infos)
                {
                    ef.F_StepResponseTempPeople.Add(item);
                    ef.Entry<F_StepResponseTempPeople>(item).State = EntityState.Added;

                }
                ef.SaveChanges();
            }
            return 1;
        }



        public List<F_StepResponseTempPeople> GetStepResponseTempPeople(int fireID, int srID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var linq = from a in ef.F_StepResponseTempPeople select a;
                if (fireID > 0)
                {
                    linq = linq.Where(p => p.F_FireID == fireID);
                }
                if (srID > 0)
                {
                    linq = linq.Where(p => p.SR_ID == srID);
                }
                return linq.OrderBy(o => o.SR_ID).ToList();
            }
        }



        public int AddFrontCommandTempPeople(List<F_FrontCommandTempPeople> infos)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                foreach (var item in infos)
                {
                    ef.F_FrontCommandTempPeople.Add(item);
                    ef.Entry<F_FrontCommandTempPeople>(item).State = EntityState.Added;

                }
                ef.SaveChanges();
            }
            return 1;
        }


        public List<F_FrontCommandTempPeople> GetFrontCommandTempPeople(int fireID, int fcID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var linq = from a in ef.F_FrontCommandTempPeople select a;
                if (fireID > 0)
                {
                    linq = linq.Where(p => p.F_FireID == fireID);
                }
                if (fcID > 0)
                {
                    linq = linq.Where(p => p.FC_ID == fcID);
                }
                return linq.OrderBy(o => o.FC_ID).ToList();
            }
        }










        public List<F_StartCondition> GetStartCondition(int responseGrade)
        {
            var where = PredicateExtensionses.True<F_StartCondition>();
            where = where.And(p => p.SC_Level == responseGrade);
            var result = ef.F_StartCondition.Where(where).OrderBy(o => o.SC_Sort).ToList();
            return result;
        }


        public int AddFightingForce(F_FightingForce info)
        {
            ef.F_FightingForce.Add(info);
            ef.SaveChanges();
            return info.FF_ID;
        }

        public List<F_FightingForce> GetFightingForce(int fireID)
        {
            var where = PredicateExtensionses.True<F_FightingForce>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FightingForce.Where(where).ToList();
            return result;
        }


        public int AddResponse(F_Response info)
        {
            ef.F_Response.Add(info);
            ef.SaveChanges();
            return info.Res_ID;
        }

        public List<F_Response> GetResponse(int fireID)
        {
            var where = PredicateExtensionses.True<F_Response>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_Response.Where(where).ToList();
            return result;
        }


        public List<AskAnswerModel> FireAskAndAnswer(int fireID)
        {
            var result = ef.F_CityFireAsk.Where(p => p.F_FireID == fireID)
                .Select(s => new { s.CFA_ID, s.F_FireID, s.CFA_AskContent, s.CFA_HandleTime })
                .GroupJoin(ef.F_CountyFireAnswer,
                    E => E.CFA_ID,
                    F => F.CFA_ID, (E, F) => new
                    {
                        E, F
                    }).Select(s => new AskAnswerModel
                    {
                        CFA_ID = s.E.CFA_ID,
                        F_FireID = s.E.F_FireID,
                        CFA_AskContent = s.E.CFA_AskContent,
                        CFA_HandleTime = s.E.CFA_HandleTime,
                        Cou_ID = s.F.FirstOrDefault() != null ? s.F.FirstOrDefault().Cou_ID : 0,
                        Cou_AnswerContent = s.F.FirstOrDefault() != null ? s.F.FirstOrDefault().Cou_AnswerContent : null,
                        Cou_Answerer = s.F.FirstOrDefault() != null ? s.F.FirstOrDefault().Cou_Answerer : null,
                        Cou_HandleTime = s.F.FirstOrDefault() == null ? new System.DateTime?() : s.F.FirstOrDefault().Cou_HandleTime,
                        Cou_CityRead = s.F.FirstOrDefault() != null ? s.F.FirstOrDefault().Cou_CityRead : false,
                    }).OrderBy(o => o.CFA_ID).ToList();
            return result;
        }

        public int UpdateAnswerRead(int couID, bool cityRead)
        {
            if (couID > 0)
            {
                return ef.F_CountyFireAnswer.Where(p => p.Cou_ID == couID).Update(u => new F_CountyFireAnswer { Cou_CityRead = cityRead });
            }
            return 0;
        }


        public int UpdateFireAutoMaticToReplay(int autoMaticId, int fireID, List<int> mTypes, int replay = 0)
        {
            var where = PredicateExtensionses.True<M_Automatic>();
            if (mTypes != null && mTypes.Count > 0)
            {
                where = where.And(p => mTypes.Contains(p.M_Type.Value));
            }
            if (autoMaticId != 0)
            {
                where = where.And(p => p.M_ID == autoMaticId);
            }
            if (fireID != 0)
            {
                where = where.And(p => p.F_FireID == fireID);
            }
            return ef.M_Automatic.Where(where).Update(u => new M_Automatic { M_IsReplay = 0 });
        }

        public int UpdateFireAutoMaticToNReplay(int autoMaticId, int fireID, List<int> mTypes, int replay = 0)
        {
            var where = PredicateExtensionses.True<M_Automatic>();
            if (mTypes != null && mTypes.Count > 0)
            {
                where = where.And(p => mTypes.Contains(p.M_Type.Value));
            }
            if (autoMaticId != 0)
            {
                where = where.And(p => p.M_ID == autoMaticId);
            }
            if (fireID != 0)
            {
                where = where.And(p => p.F_FireID == fireID);
            }
            return ef.M_Automatic.Where(where).Update(u => new M_Automatic { M_NIsReplay = 0 });
        }


        public List<M_Automatic> GetAutomatic(int fireID, int isCityManager)
        {
            var where = PredicateExtensionses.True<M_Automatic>();
            where = where.And(p => p.F_FireID == fireID);
            if (isCityManager == 1)
            {
                where = where.And(p => p.M_To == 0 && p.M_From == 1);
            }
            else {
                where = where.And(p => p.M_To == 1 && p.M_From == 0);
            }
            where = where.And(p => p.M_IsReplay == 1);
            return ef.M_Automatic.Where(where).ToList();
        }

        public List<M_Automatic> GetAutomatic(int addressCounty, List<int> messageType, int isCityManager)
        {
            var where = PredicateExtensionses.True<M_Automatic>();
            List<int> hidTypes = new List<int>() { 12, 13, 14, 15, 17 };
            if (messageType != null && messageType.Count > 0)
            {
                where = where.And(p => messageType.Contains(p.M_Type.Value));
            }
            if (addressCounty > 0)
            {
                where = where.And(p => p.M_DistrictID == addressCounty);
            }

            if (isCityManager == 1)
            {
                where = where.And(p => p.M_To == 0 && p.M_From == 1);
            }
            else
            {
                where = where.And(p => p.M_To == 1 && p.M_From == 0);
            }
            where = where.And(p => p.M_IsReplay == 1);
            where = where.And(p => !hidTypes.Contains(p.M_Type.Value));
            return ef.M_Automatic.Where(where).ToList();
        }



        public M_Automatic AutoMaticDetail(int autoMaticId)
        {
            return ef.M_Automatic.Where(p => p.M_ID == autoMaticId).FirstOrDefault();
        }


        public int AddFireForceSupport(F_FireForceSupport info)
        {
            ef.F_FireForceSupport.Add(info);
            ef.SaveChanges();
            return info.FFS_ID;
        }

        public int AddFireSituation(F_FireSituation info)
        {
            ef.F_FireSituation.Add(info);
            ef.SaveChanges();
            return info.FS_ID;
        }

        public int UpdateFireSituation(F_FireSituation info)
        {
            var where = PredicateExtensionses.True<F_FireSituation>();
            where = where.And(p => p.F_FireID == info.F_FireID && p.F_FireState == info.F_FireState);
            return ef.F_FireSituation.Where(where)
                     .Update(u => new F_FireSituation
                     {
                         F_FireID = info.F_FireID,
                         F_FireState = info.F_FireState,
                         FS_FilePath = info.FS_FilePath,
                         FS_TransTime = info.FS_TransTime
                     });
        }


        public int UpdateCatalogToApproval(Catalog info)
        {
            var where = PredicateExtensionses.True<Catalog>();
            where = where.And(p => p.F_FireID == info.F_FireID && string.IsNullOrEmpty(p.A_Reason) && p.A_ReaTime == null);
            return ef.Catalog.Where(where)
                     .Update(u => new Catalog
                     {
                         F_FireID = info.F_FireID,
                         A_Results = info.A_Results,
                         A_Reason = info.A_Reason,
                         A_ReaTime = info.A_ReaTime,
                     });
        }


        public F_FireInfo FireInfoDetail(string phone)
        {
            var where = PredicateExtensionses.True<F_FireInfo>();
            where = where.And(p => p.F_ReporterPhone == phone);
            var result = ef.F_FireInfo.Where(where).OrderByDescending(o => o.F_AlarmTime).FirstOrDefault();
            return result;
        }


        public int AddFireAttachs(F_FireAttachs info)
        {
            ef.F_FireAttachs.Add(info);
            ef.SaveChanges();
            return info.ID;
        }
        public int AddFireAttachs(List<F_FireAttachs> infos)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                foreach (var item in infos)
                {
                    ef.F_FireAttachs.Add(item);
                    ef.Entry<F_FireAttachs>(item).State = EntityState.Added;
                }
                ef.SaveChanges();
            }
            return 1;
        }



        public List<F_FireAttachs> GetFireAttachs(int fireID)
        {
            var where = PredicateExtensionses.True<F_FireAttachs>();
            where = where.And(p => p.F_FireID == fireID);
            var result = ef.F_FireAttachs.Where(where).OrderByDescending(o => o.AddTime).ToList();
            return result;
        }

        public List<F_FireAttachs> GetFireAttachs(List<int> fireIDs)
        {
            var where = PredicateExtensionses.True<F_FireAttachs>();
            where = where.And(p => fireIDs.Contains(p.F_FireID));
            var result = ef.F_FireAttachs.Where(where).OrderByDescending(o => o.AddTime).ToList();
            return result;
        }

        public void RemoveFireAttachs(int fireAttachsID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var exist = db.F_FireAttachs.Where(m => m.ID == fireAttachsID).FirstOrDefault();
                if (exist != null)
                {
                    ef.F_FireAttachs.Add(exist);
                    ef.Entry<F_FireAttachs>(exist).State = EntityState.Deleted;
                    ef.SaveChanges();
                }
            }
        }

        public List<F_FireInfo> GetFireInfo(string addressCounty, int fireState)
        {
            var where = PredicateExtensionses.True<F_FireInfo>();
            if (!string.IsNullOrEmpty(addressCounty))
            {
                where = where.And(p => p.F_AddressCounty == addressCounty);
            }
            where = where.And(p => p.F_IsState == fireState);
            return ef.F_FireInfo.Where(where).ToList();
        }


        public List<F_FireInfo> GetFireInfoFromForce(string addressCounty, int fireState)
        {
            if (addressCounty != null)
            {
                var where = PredicateExtensionses.True<F_FireInfo>();
                var temp = ef.F_FightingForce.Where(p => p.FF_UH_LinchpinID.Value.ToString() == addressCounty).ToList();
                var fireIDs = temp.Select(s => s.F_FireID).ToList();
                where = where.And(p => fireIDs.Contains(p.F_FireID) && p.F_IsState == fireState);
                var result = ef.F_FireInfo.Where(where).ToList();
                return result;
            }
            return new List<F_FireInfo>();
        }

        public List<F_FireInfo> GetFireInfoFromForceByPager(string addressCounty, int fireState, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_FireInfo>();
            if (!string.IsNullOrEmpty(pager.Keyword))
            {
                where = where.And(p => p.F_FireName.Contains(pager.Keyword) || p.F_Address.Contains(pager.Keyword));
            }

            if (pager.StartTime != null)
            {
                where = where.And(p => p.F_HappenDatetime >= pager.StartTime);
                if (pager.EndTime != null)
                {
                    where = where.And(p => p.F_HappenDatetime <= pager.EndTime);
                }
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }

            var temp = ef.F_FightingForce.Where(p => p.FF_UH_LinchpinID.Value.ToString() == addressCounty).ToList();

            var fireIDs = temp.Select(s => s.F_FireID).ToList();
            where = where.And(p => fireIDs.Contains(p.F_FireID) && p.F_IsState == fireState);
            var result = ef.F_FireInfo.Where(where).OrderByDescending(o => o.F_AlarmTime)
                    .Skip(pager.StartRow)
                    .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_FireInfo.Where(where).Count();
            return result;
        }


        public List<dynamic> GetFireForceSupportByPager(int addressCounty, GridPager2 pager)
        {
            var linq = from a in ef.F_FireForceSupport
                       join b in ef.F_FireInfo on a.F_FireID equals b.F_FireID
                       into c
                       let d = c.FirstOrDefault()
                       join e in ef.Linchpin on a.FFS_FromCounty equals e.LinchpinIID
                       into f
                       let g = f.FirstOrDefault()
                       join h in ef.Linchpin on a.FFS_ToCounty equals h.LinchpinIID
                       into i
                       let j = i.FirstOrDefault()
                       select new
                       {
                           a.FFS_ID,
                           a.F_FireID,
                           d.F_FireName,
                           FromCountyID = g.LinchpinIID,
                           FromCountyName = g.LinchpinName,
                           ToCountyID = j.LinchpinIID,
                           ToCountyName = j.LinchpinName,
                           a.FFS_TransDatetime,
                           a.FFS_Content,
                       };
            if (addressCounty > 0)
            {
                linq = linq.Where(p => p.FromCountyID == addressCounty);
            }
            if (!string.IsNullOrEmpty(pager.Keyword))
            {
                linq = linq.Where(p => p.F_FireName.Contains(pager.Keyword)
                                   || p.FromCountyName.Contains(pager.Keyword)
                                   || p.ToCountyName.Contains(pager.Keyword));
            }

            if (pager.StartTime != null)
            {
                linq = linq.Where(p => p.FFS_TransDatetime >= pager.StartTime);
                if (pager.EndTime != null)
                {
                    linq = linq.Where(p => p.FFS_TransDatetime <= pager.EndTime);
                }
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            pager.TotalRows = linq.Count();
            var result = linq.OrderByDescending(o => o.FFS_TransDatetime).Skip(pager.StartRow).Take(pager.PageSize).ToList<dynamic>();
            return result;
        }


        public List<dynamic> GetFireReporterInfo(string phone)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                var where = PredicateExtensionses.True<F_FireInfo>();
                where = where.And(p => p.F_ReporterPhone == phone);

                //每组获取最新一条
                var linq = from a in ef.F_FireInfo
                           where a.F_ReporterPhone == phone
                           group a by new { a.F_ReporterUnit } into temp
                           let time = temp.Max(a => a.F_AlarmTime)
                           from row in temp
                           where row.F_AlarmTime == time
                           select row;
                var data = linq.Select(s => new { s.F_ReporterUnit, s.F_ReporterName, s.F_ReporterWorknum }).ToList<dynamic>();
                return data;
            }
            return new List<dynamic>();
        }


        public int AddFireBlCons(F_BlCons info)
        {
            ef.F_BlCons.Add(info);
            ef.SaveChanges();
            return info.id;
        }


        public int UpdateFireBlCons(F_BlCons info)
        {
            var where = PredicateExtensionses.True<F_BlCons>();
            where = where.And(p => p.id == info.id);
            return ef.F_BlCons.Where(where)
                     .Update(u => new F_BlCons
                     {
                         F_fireid = info.F_fireid,
                         Contens = info.Contens,
                     });
        }
        public int DeleteFireBlCons(int id)
        {
            if (id > 0)
            {
                var where = PredicateExtensionses.True<F_BlCons>();
                where = where.And(p => p.id == id);
                return ef.F_BlCons.Where(where).Delete();
            }
            return -1;
        }



        public List<F_BlCons> GetFireBlCons(int fireID)
        {
            var where = PredicateExtensionses.True<F_BlCons>();
            where = where.And(p => p.F_fireid == fireID);
            var result = ef.F_BlCons.Where(where).OrderBy(o => o.CreateTime).ToList();
            return result;
        }

        public int UpdateFireFinalReport(F_FireFinalReport info)
        {
            var where = PredicateExtensionses.True<F_FireFinalReport>();
            where = where.And(p => p.FFR_ID == info.FFR_ID);
            return ef.F_FireFinalReport.Where(where)
                     .Update(u => new F_FireFinalReport
                     {
                         F_FireID = info.F_FireID,
                         FFR_Address = info.FFR_Address,
                         FFR_Jdate = info.FFR_Jdate,
                         FFR_Type = info.FFR_Type,
                         FFR_Cont = info.FFR_Cont,
                         FFR_Fh = info.FFR_Fh,
                         FFR_FhPeople = info.FFR_FhPeople,
                         FFR_Com = info.FFR_Com,
                         FFR_ComPeople = info.FFR_ComPeople,
                         FFR_Ranks = info.FFR_Ranks,
                         FFR_Mas = info.FFR_Mas,
                         FFR_Leader = info.FFR_Leader,
                         FFR_Ptime = info.FFR_Ptime,
                         FFR_Zf = info.FFR_Zf,
                         FFR_Zpeople = info.FFR_Zpeople,
                         FFR_BZf = info.FFR_BZf,
                         FFR_LocalP = info.FFR_LocalP,
                         FFR_Weather = info.FFR_Weather,
                         FFR_Wind = info.FFR_Wind,
                         FFR_Direc = info.FFR_Direc,
                         FFR_Temp = info.FFR_Temp,
                         FFR_Damp = info.FFR_Damp,
                         FFR_Garea = info.FFR_Garea,
                         FFR_Gunit = info.FFR_Gunit,
                         FFR_Larea = info.FFR_Larea,
                         FFR_Lunit = info.FFR_Lunit,
                         FFR_Trees = info.FFR_Trees,
                         FFR_Age = info.FFR_Age,
                         FFR_Num = info.FFR_Num,
                         FFR_Die = info.FFR_Die,
                         FFR_Why = info.FFR_Why,
                         FFR_WhyID = info.FFR_WhyID,
                         FFR_Loss = info.FFR_Loss,
                         FFR_Other = info.FFR_Other,
                         FFR_Reporter = info.FFR_Reporter,
                         FFR_ReporterTime = info.FFR_ReporterTime,
                         FFR_FireTypeID = info.FFR_FireTypeID,
                         FFR_FireType = info.FFR_FireType,
                         FFR_TransUser = info.FFR_TransUser,
                         FFR_TransDatetime = info.FFR_TransDatetime,
                         FFR_TransIP = info.FFR_TransIP,
                     });
        }


        public List<ManagerOnLion> GetManager2()
        {
            return IndexDAL.GetManager2();
        }


        public int AddFireRecordPlot(F_FireRecordPlot info)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var exist = ef.F_FireRecordPlot.Where(p => p.F_FireID == info.F_FireID && p.MR_Type == info.MR_Type).FirstOrDefault();
                if (exist != null)
                {
                    ef.F_FireRecordPlot.Add(exist);
                    ef.Entry<F_FireRecordPlot>(exist).State = EntityState.Deleted;
                    ef.SaveChanges();
                }

                ef.F_FireRecordPlot.Add(info);
                ef.Entry<F_FireRecordPlot>(info).State = EntityState.Added;
                ef.SaveChanges();
            }
            return info.FRP_ID;
        }


        public List<F_FireRecordPlot> GetFireRecordPlot(int mrID, int fireID, int mrType)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var linq = from a in db.F_FireRecordPlot select a;
                if (mrID > 0)
                {
                    linq = linq.Where(p=>p.MR_ID==mrID);
                }

                if (fireID > 0)
                {
                    linq = linq.Where(p => p.F_FireID == fireID);
                }

                if (mrType > 0)
                {
                    linq = linq.Where(p => p.MR_Type == mrType);
                }
                return linq.ToList();

            }
        }

        public List<F_FightingForce> GetFightingForce(List<int?> fireIDs)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var linq = from a in db.F_FightingForce select a;
                if (fireIDs!=null && fireIDs.Count > 0)
                {
                    linq = linq.Where(p => fireIDs.Contains(p.F_FireID));
                }
                return linq.ToList();

            }
        }

        public int AddAlarmRecord(F_AlarmRecord info)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                db.F_AlarmRecord.Add(info);
                db.Entry<F_AlarmRecord>(info).State = EntityState.Added;
                db.SaveChanges();
                return info.AR_ID;
            }
        }

        public F_AlarmRecord GetAlarmRecord(int arID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                return db.F_AlarmRecord.Where(p => p.AR_ID == arID).FirstOrDefault();
            }
        }


        public void  ChangeFireAlarmRecordState(int fireID, int arID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var exist = db.F_AlarmRecord.Where(p => p.AR_ID == arID).FirstOrDefault();
                if (exist != null && exist.AuditState==0)
                {
                    exist.FireID = fireID;
                    exist.AuditState = 1;
                    db.F_AlarmRecord.Add(exist);
                    db.Entry<F_AlarmRecord>(exist).State = EntityState.Modified;
                    db.SaveChanges();
                }
               
            }
        }

        public int AddAlarmRecordConfig(F_AlarmRecordConfig info)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {

                List<F_AlarmRecordConfig> exists= new List<F_AlarmRecordConfig>();
                if (!string.IsNullOrEmpty(info.LocaID) && info.UnitID == 0)
                {
                    exists = db.F_AlarmRecordConfig.Where(p => p.LocaID == info.LocaID).ToList();
                }
                else if (!string.IsNullOrEmpty(info.LocaID) && info.UnitID > 0)
                {
                    exists = db.F_AlarmRecordConfig.Where(p => p.LocaID == info.LocaID &&  p.UnitID==info.UnitID).ToList();
                }

                foreach (var item in exists)
                {
                    db.F_AlarmRecordConfig.Add(item);
                    db.Entry<F_AlarmRecordConfig>(item).State = EntityState.Deleted;
                    db.SaveChanges();
                }

                db.F_AlarmRecordConfig.Add(info);
                db.Entry<F_AlarmRecordConfig>(info).State = EntityState.Added;
                db.SaveChanges();
                return info.ARC_ID;
            }
        }

        public List<F_AlarmRecordConfig> GetAlarmRecordConfig(DateTime nowDateTime,string locaID,int unitID)
        {
            using (SJZHPlatFormEntities db = new SJZHPlatFormEntities())
            {
                var linq = from a in db.F_AlarmRecordConfig select a;
                if (!string.IsNullOrEmpty(locaID))
                {
                    linq = linq.Where(p => p.LocaID==locaID);
                }
                if (unitID>0)
                {
                    linq = linq.Where(p => p.UnitID == unitID);
                }
                linq = linq.Where(p=> SqlFunctions.DateDiff("minute", p.StartTime,nowDateTime) <= p.Value);
                return linq.ToList();
            }
        }

    }
}

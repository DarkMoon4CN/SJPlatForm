﻿using ChuanYe.IDAL;
using ChuanYe.Models;
using ChuanYe.Utils;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.DAL
{
    public class CarouselFigureDAL: ICarouselFigureDAL
    {
        readonly SJZHPlatFormEntities ef = new SJZHPlatFormEntities();
        public int AddCarouselFigure(CarouselFigure info)
        {
            ef.CarouselFigure.Add(info);
            ef.SaveChanges();
            return info.CF_ID;
        }


        public int UpdateCarouselFigure(CarouselFigure info)
        {
            var where = PredicateExtensionses.True<CarouselFigure>();
            where = where.And(p => p.CF_ID == info.CF_ID);
            return ef.CarouselFigure.Where(where).Update(u => new CarouselFigure
            {
                CF_Name = info.CF_Name,
                CF_CreateTime =info.CF_CreateTime,
                CF_ImgPath=info.CF_ImgPath,
                CF_IsEnable=info.CF_IsEnable,
            });
        }

        public List<CarouselFigure> GetCarouselFigureAll(int searchType=0)
        {
            var where = PredicateExtensionses.True<CarouselFigure>();
            if (searchType == 1)
            {
                where = where.And(p=>p.CF_IsEnable==true);
            }
            var result = ef.CarouselFigure.Where(where).OrderBy(o=>o.CF_ID).ToList();
            return result;
        }


    }

}

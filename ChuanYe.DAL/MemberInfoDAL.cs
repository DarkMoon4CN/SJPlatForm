﻿using ChuanYe.IDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.Utils;
using EntityFramework.Extensions;
using ChuanYe.Models.Model;

namespace ChuanYe.DAL
{
    public class MemberInfoDAL : IMemberInfoDAL
    {
        readonly SJZHPlatFormEntities ef = new SJZHPlatFormEntities();
        public List<F_MemberInfo> GetMemberInfo(int orgID)
        {
            var where = PredicateExtensionses.True<F_MemberInfo>();
            if (orgID > 0)
            {
                where = where.And(p => p.M_OrgID == orgID.ToString());
            }
            return ef.F_MemberInfo.Where(where).ToList();
        }

        public List<F_OrgInfo> GetOrgInfo()
        {
            return ef.F_OrgInfo.Where(p => p.O_ID > 0).ToList();
        }

        public List<F_MemberInfo> GetMemberInfo(List<int> mIDs)
        {
            var where = PredicateExtensionses.True<F_MemberInfo>();
            where = where.And(p => mIDs.Contains(p.M_ID));
            return ef.F_MemberInfo.Where(where).ToList();
        }

        public List<F_UnitType> GetUnitType(int unitTypeID = 0)
        {
            var where = PredicateExtensionses.True<F_UnitType>();
            if (unitTypeID != 0)
            {
                where = where.And(p => p.UT_ID == unitTypeID);
            }
            return ef.F_UnitType.Where(where).ToList();
        }

        public int AddUnitType(F_UnitType info)
        {
            ef.F_UnitType.Add(info);
            ef.SaveChanges();
            return info.UT_ID;
        }

        public int UpdateUnitType(F_UnitType info)
        {
            var where = PredicateExtensionses.True<F_UnitType>();
            where = where.And(p => p.UT_ID == info.UT_ID);
            return ef.F_UnitType.Where(where).Update(u => new F_UnitType {
                UT_TypeName = info.UT_TypeName,
                UT_MainLeader = info.UT_MainLeader,
                UT_OtherLeader = info.UT_OtherLeader,
                UT_Remark = info.UT_Remark,
                UT_TransDatetime = info.UT_TransDatetime,
                UT_TransIP = info.UT_TransIP,
                UT_TransUser = info.UT_TransUser,
            });
        }

        public List<F_Unit> GetUnit(int uintID, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_Unit>();
            if (uintID != 0)
            {
                where = where.And(p => p.U_ID == uintID);
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var result= ef.F_Unit.Where(where).OrderByDescending(o => o.U_TransDatetime).Skip(pager.StartRow)
                                               .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_Unit.Where(where).Count();
            return result;
        }

        public List<F_Unit> GetUnitByName(string unitName)
        {
            return ef.F_Unit.Where(p => p.U_UnitName == unitName).ToList();
        }


        public List<F_Unit> GetUnitByUnitTypeID(int unitTypeID,int uhID,int responseLevel, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_Unit>();
            if (unitTypeID != 0)
            {
                where = where.And(p => p.U_UnitType == unitTypeID.ToString());
            }
            if (uhID > 0)
            {
                where = where.And(p => p.UH_ID == uhID);
            }

            if (responseLevel > 0)
            {
                where = where.And(p => p.U_ResponseLevel == responseLevel);
            }


            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var result = ef.F_Unit.Where(where).OrderByDescending(o => o.U_TransDatetime).Skip(pager.StartRow)
                                               .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_Unit.Where(where).Count();
            return result;
        }

        public int AddUnit(F_Unit info)
        {
            ef.F_Unit.Add(info);
            ef.SaveChanges();
            return info.U_ID;
        }
        public int UpdateUnit(F_Unit info)
        {
            var where = PredicateExtensionses.True<F_Unit>();
            where = where.And(p => p.U_ID == info.U_ID);
            return ef.F_Unit.Where(where).Update(u => new F_Unit
            {
                U_UnitName = info.U_UnitName,
                U_UnitType = info.U_UnitType,
                U_OrgID = info.U_OrgID,
                U_Phone = info.U_Phone,
                U_Tel=info.U_Tel,
                U_Address = info.U_Address,
                U_Fax = info.U_Fax,
                U_ResponseLevel = info.U_ResponseLevel,
                U_SeatLongitude = info.U_SeatLongitude,
                U_SeatLatitude = info.U_SeatLatitude,
                U_Remark = info.U_Remark,
                U_TransDatetime = info.U_TransDatetime,
                U_Email = info.U_Email,
                U_Total = info.U_Total,
                U_TransIP = info.U_TransIP,
                U_TransUser = info.U_TransUser,
                U_Emergency = info.U_Emergency,
                U_Captain=info.U_Captain,
                UH_ID=info.UH_ID,
                UH_Name=info.UH_Name,
            });
        }


        public List<F_Unit> UnitByPager(F_Unit info, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_Unit>();
            if (info != null)
            {
                if (!string.IsNullOrEmpty(info.U_UnitType))
                {
                    where = where.And(p => p.U_UnitType == info.U_UnitType);
                }
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.F_Unit.Where(where).OrderByDescending(o => o.U_ID)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_Unit.Where(where).Count();
            return list;
        }

        public int DeleteUnit(int uid)
        {
            return ef.F_Unit.Where(p => p.U_ID == uid).Delete();
        }
        public List<UintOrgInfo> UnitJoinOrgInfo(List<int> unitIDs)
        {
            var temp = ef.F_Unit.Where(p => unitIDs.Contains(p.U_ID)).Select(s =>
                         new { U_ID = s.U_ID, U_UnitName = s.U_UnitName, U_OrgID = s.U_OrgID }).ToList()
                         .Select(s => new UintOrgInfo { UnitID = s.U_ID, UnitName = s.U_UnitName, U_OrgID = Convert.ToInt32(s.U_OrgID) }).ToList();

            var result = temp.Join(ef.F_OrgInfo, E => E.U_OrgID, F => F.O_ID, (E, F) =>
                           new UintOrgInfo {
                               UnitID = E.UnitID,
                               UnitName = E.UnitName,
                               U_OrgID = F.O_ID,
                               O_AreaID = F.O_AreaID
                           }).ToList();
            return result;

        }

        public List<DirectorysTb> GetDirectorysTb(int parentID)
        {
            var where = PredicateExtensionses.True<DirectorysTb>();
            if (parentID > 0)
            {
                where = where.And(p => p.DParentID == parentID);
            }

            return ef.DirectorysTb.Where(where).ToList();
        }

        public List<DirectorysTb> DirectorysTbByPager(int parentID, string name, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<DirectorysTb>();
            if (parentID == -1)
            {
                where = where.And(p => p.DParentID != 0);
            }
            else
            {
                where = where.And(p => p.DParentID == parentID);
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            if (!string.IsNullOrEmpty(name))
            {
                where = where.And(p => p.DName.Contains(name));
            }
            var list = ef.DirectorysTb.Where(where).OrderByDescending(o => o.id)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.DirectorysTb.Where(where).Count();
            return list;
        }

        public List<DirectorysTb> GetDirectorysTb(int parentID, string name)
        {
            var where = PredicateExtensionses.True<DirectorysTb>();
            where = where.And(p => p.DParentID == parentID);
            where = where.And(p => p.DName==name);
            var list = ef.DirectorysTb.Where(where).ToList();
            return list;
        }


        public DirectorysTb DirectorysTbDetail(int id)
        {
            return ef.DirectorysTb.Where(p => p.id == id).FirstOrDefault();
        }

        public int AddDirectorysTb(DirectorysTb info)
        {
            ef.DirectorysTb.Add(info);
            ef.SaveChanges();
            return info.id;
        }

        public int UpdateDirectorysTb(DirectorysTb info)
        {
            var where = PredicateExtensionses.True<DirectorysTb>();
            where = where.And(p => p.id == info.id);
            return ef.DirectorysTb.Where(where)
                     .Update(u => new DirectorysTb {  DName = info.DName, DParentID=info.DParentID });
        }

        public int DeleteDirectorysTb(int id)
        {
            return ef.DirectorysTb.Where(p => p.id == id).Delete();
        }

        public List<LinkCorresponding> GetLinkCorresponding()
        {
            return ef.LinkCorresponding.ToList();
        }

        public List<F_UnitHeadquarter> UintHeadquarterByPager(int parentID,string uhName,int level, string uhLeader, int uhLinchpinID, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_UnitHeadquarter>();
            if (parentID > -1)
            {
                where = where.And(p => p.UH_ParentId == parentID);
            }
            if (level > 0)
            {
                where = where.And(p=>p.UH_Level==level);
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            if (!string.IsNullOrEmpty(uhName))
            {
                where = where.And(p => p.UH_Name.Contains(uhName));
            }
            if (!string.IsNullOrEmpty(uhLeader))
            {
                where = where.And(p => p.UH_Name.Contains(uhLeader));
            }
            if (uhLinchpinID > 0)
            {
                where = where.And(p => p.UH_LinchpinID==uhLinchpinID);
            }
            
            var list = ef.F_UnitHeadquarter.Where(where).OrderBy(o => o.UH_ParentId)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_UnitHeadquarter.Where(where).Count();
            return list;
        }


        public List<F_UnitHeadquarter> GetUnitHeadquarter(string uhName)
        {
            return ef.F_UnitHeadquarter.Where(p => p.UH_Name == uhName).ToList();
        }

        public List<F_UnitHeadquarter> GetUnitHeadquarter(int uhID)
        {
            return ef.F_UnitHeadquarter.Where(p => p.UH_ID == uhID).ToList();
        }


        public int AddUintHeadquarter(F_UnitHeadquarter info)
        {
            ef.F_UnitHeadquarter.Add(info);
            ef.SaveChanges();
            return info.UH_ID;
        }

        public int UpDateUintHeadquarter(F_UnitHeadquarter info)
        {
            var where = PredicateExtensionses.True<F_UnitHeadquarter>();
            where = where.And(p => p.UH_ID == info.UH_ID);
            return ef.F_UnitHeadquarter.Where(where)
                     .Update(u => new F_UnitHeadquarter
                     { UH_Leader = info.UH_Leader,  UH_Name = info.UH_Name,
                       UH_Phone=info.UH_Phone, UH_TransDatetime=info.UH_TransDatetime,
                       UH_ParentId=info.UH_ParentId, UH_Sort=info.UH_Sort,
                       UH_Remarks=info.UH_Remarks, UH_TransIP=info.UH_TransIP,
                       UH_TransUser=info.UH_TransUser,
                       UH_LinchpinID=info.UH_LinchpinID,
                     });
        }

        public int DeleteUintHeadquarter(int uhID)
        {
            return ef.F_UnitHeadquarter.Where(p => p.UH_ID == uhID).Delete();
        }

        public List<F_UnitMember> UnitMemberByPager(int unitID, string phone, string name, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_UnitMember>();
            if (!string.IsNullOrEmpty(name))
            {
                where = where.And(p => p.UM_MemberName.Contains(name));
            }
            if (!string.IsNullOrEmpty(phone))
            {
                where = where.And(p => p.UM_Phone.Contains(phone));
            }
            if (unitID > 0)
            {
                string uid = unitID.ToString();
                where = where.And(p =>p.UM_Unit== uid);
            }
           
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.F_UnitMember.Where(where).OrderByDescending(o => o.UM_ID)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_UnitMember.Where(where).Count();
            return list;
        }

    




        public List<LinkCorresponding> LinkCorrespondingByPager(LinkCorresponding info, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<LinkCorresponding>();
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.LinkCorresponding.Where(where).OrderByDescending(o => o.ID)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.LinkCorresponding.Where(where).Count();
            return list;
        }

        public int AddLinkCorresponding(LinkCorresponding info)
        {
            ef.LinkCorresponding.Add(info);
            ef.SaveChanges();
            return info.ID;
        }

        public int UpDateLinkCorresponding(LinkCorresponding info)
        {
            var where = PredicateExtensionses.True<LinkCorresponding>();
            where = where.And(p => p.ID == info.ID);
            return ef.LinkCorresponding.Where(where)
                     .Update(u => new LinkCorresponding
                     {
                           ContactMen=info.ContactMen,
                           DepartMent=info.DepartMent,
                           ContactPhone=info.ContactPhone,
                           Duty=info.Duty,
                           MobilePhone=info.MobilePhone,
                     });
        }
        public int DeleteLinkCorresponding(int id)
        {
            return ef.LinkCorresponding.Where(p => p.ID == id).Delete();
        }

        public List<OneGradeTemplate> GetGradeTemplate()
        {
            return ef.OneGradeTemplate.ToList();
        }

        public List<OneGradeTemplate> GradeTemplateByPager(OneGradeTemplate info,GridPager2 pager)
        {
            var where = PredicateExtensionses.True<OneGradeTemplate>();
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.OneGradeTemplate.Where(where).OrderByDescending(o => o.ID)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.OneGradeTemplate.Where(where).Count();
            return list;
        }

        public int AddGradeTemplate(OneGradeTemplate info)
        {
            ef.OneGradeTemplate.Add(info);
            ef.SaveChanges();
            return info.ID;
        }

        public int UpDateGradeTemplate(OneGradeTemplate info)
        {
            var where = PredicateExtensionses.True<OneGradeTemplate>();
            where = where.And(p => p.ID == info.ID);
            return ef.OneGradeTemplate.Where(where)
                     .Update(u => new OneGradeTemplate
                     {
                         Leader=info.Leader,
                         MobilePhone=info.MobilePhone,
                         Num=info.Num,
                         Phone=info.Phone,
                         pUnitID=info.pUnitID,
                         pUnitName=info.pUnitName,
                     });
        }

        public int DeleteGradeTemplate(int id)
        {
            return ef.OneGradeTemplate.Where(p => p.ID == id).Delete();
        }

        public List<ThreeAndTwoGradeTemplate> GetOtherGradeTemplate()
        {
            return ef.ThreeAndTwoGradeTemplate.ToList();
        }

        public List<ThreeAndTwoGradeTemplate> GetOtherGradeTemplate(int sGradeID, long linchpinIID)
        {
            return ef.ThreeAndTwoGradeTemplate.Where(p=>p.sGradeID==sGradeID && p.LinchpinIID== linchpinIID).ToList();
        }


        public int AddOtherGradeTemplate(ThreeAndTwoGradeTemplate info)
        {
            ef.ThreeAndTwoGradeTemplate.Add(info);
            ef.SaveChanges();
            return info.ID;
        }

        public List<dynamic>  OtherGradeTemplateByPager(ThreeAndTwoGradeTemplate info,GridPager2 pager)
        {
            var where = PredicateExtensionses.True<ThreeAndTwoGradeTemplate>();
            if (info != null)
            {
                if (info.sGradeID > 0)
                {
                    where = where.And(p=>p.sGradeID==info.sGradeID);
                }

                if (info.LinchpinIID > 0)
                {
                    where = where.And(p => p.LinchpinIID == info.LinchpinIID);
                }
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.ThreeAndTwoGradeTemplate.Where(where).OrderByDescending(o => o.ID)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();

            var list2 = new List<dynamic>();
            //Unit 扑火队
            foreach (var item in list)
            {
                if (!string.IsNullOrEmpty(item.FireResourceListID))
                {
                    try
                    {
                        var unitIDs = item.FireResourceListID.Split('|').Select<string, int>(s => Convert.ToInt32(s)).Distinct().ToList();
                        var units=ef.F_Unit.Where(p => unitIDs.Contains(p.U_ID)).Select(s=>new { 
                                 s.U_ID,s.UH_Name,s.U_Captain,s.U_Phone,s.U_Tel
                        }).ToList();
                        var data = new {
                            ID = item.ID,
                            sGradeID=item.sGradeID,
                            LinchpinIID =item.LinchpinIID,
                            LinchpinIName=item.LinchpinIName,
                            FireResourceList =item.FireResourceList,
                            FireResourceListID=item.FireResourceListID,
                            Units=units
                        };
                        list2.Add(data);
                    }
                    catch (Exception)
                    { 
                    }
                }
            }
            pager.TotalRows = ef.ThreeAndTwoGradeTemplate.Where(where).Count();
            return list2;
        }

        public int UpDateOtherGradeTemplate(ThreeAndTwoGradeTemplate info)
        {
            var where = PredicateExtensionses.True<ThreeAndTwoGradeTemplate>();
            where = where.And(p => p.ID == info.ID);
            return ef.ThreeAndTwoGradeTemplate.Where(where)
                     .Update(u => new ThreeAndTwoGradeTemplate
                     {
                         FireResourceList=info.FireResourceList,
                         LinchpinIID=info.LinchpinIID,
                         LinchpinIName=info.LinchpinIName,
                         FireResourceListID=info.FireResourceListID,
                         sGradeID=info.sGradeID
                     });
        }

        public int DeleteOtherGradeTemplate(int id)
        {
            return ef.ThreeAndTwoGradeTemplate.Where(p => p.ID == id).Delete();
        }



        

        public List<F_UnitMember> UnitMemberByPager(F_UnitMember info, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_UnitMember>();
            if (info != null)
            {
                
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.F_UnitMember.Where(where).OrderByDescending(o => o.UM_ID)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_UnitMember.Where(where).Count();
            return list;
        }

        public int AddUnitMember(F_UnitMember info)
        {
            ef.F_UnitMember.Add(info);
            ef.SaveChanges();
            return info.UM_ID;
        }

        public int UpdateUnitMember(F_UnitMember info)
        {
            var where = PredicateExtensionses.True<F_UnitMember>();
            where = where.And(p => p.UM_ID == info.UM_ID);
            return ef.F_UnitMember.Where(where)
                     .Update(u => new F_UnitMember
                     {
                             UM_MemberName=info.UM_MemberName,
                             UM_MemberSex=info.UM_MemberSex,
                             UM_Birthday=info.UM_Birthday,
                             UM_Nation=info.UM_Nation,
                             UM_Unit=info.UM_Unit,
                             pUnitName = info.pUnitName,
                             UM_Post=info.UM_Post,
                             UM_Phone=info.UM_Phone,
                             UM_Pic=info.UM_Pic,
                             UM_Address = info.UM_Address,
                             UM_Email=info.UM_Email,
                             UM_Remark=info.UM_Remark,
                             UM_TransDatetime=info.UM_TransDatetime,
                             UM_TransIP=info.UM_TransIP,
                             UM_TransUser=info.UM_TransUser,
                             UM_Tel=info.UM_Tel,
                     });

        }

        public int DeleteUnitMember(int umID)
        {
            return ef.F_UnitMember.Where(p => p.UM_ID == umID).Delete();
        }



        #region  F_UintHeadquarterMember

        public List<F_UnitHeadquarterMember> UintHeadquarterMemberByPager(F_UnitHeadquarterMember info, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_UnitHeadquarterMember>();
            if (info != null)
            {
                if (!string.IsNullOrEmpty(info.UHM_Leader))
                {
                    where = where.And(p => p.UHM_Leader.Contains(info.UHM_Leader));
                }
                if (info.UH_ID > 0)
                {
                    where = where.And(p => p.UH_ID==info.UH_ID);
                }
            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.F_UnitHeadquarterMember.Where(where).OrderByDescending(o=>o.UHM_TransTime)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_UnitHeadquarterMember.Where(where).Count();
            return list;
        }

        public List<F_UnitHeadquarterMember> GetUnitHeadquarterMember(List<int> ids)
        {
           return ef.F_UnitHeadquarterMember.Where(p => ids.Contains(p.UHM_ID)).ToList();
        }

        public List<F_UnitHeadquarterMember> GetUnitHeadquarterMemberByDepIDs(List<int> depIDs)
        {
            return ef.F_UnitHeadquarterMember.Where(p => depIDs.Contains(p.UHM_DepID)).ToList();
        }




        public List<F_UnitHeadquarterMember> GetUnitHeadquarterMember(string uhmLeader)
        {
            return ef.F_UnitHeadquarterMember.Where(p => p.UHM_Leader==uhmLeader).ToList();
        }


        public int AddUintHeadquarterMember(F_UnitHeadquarterMember info)
        {
            ef.F_UnitHeadquarterMember.Add(info);
            ef.SaveChanges();
            return info.UHM_ID;
        }
        public int UpdateUintHeadquarterMember(F_UnitHeadquarterMember info)
        {
            var where = PredicateExtensionses.True<F_UnitHeadquarterMember>();
            where = where.And(p => p.UHM_ID == info.UHM_ID);
            return ef.F_UnitHeadquarterMember.Where(where)
                     .Update(u => new F_UnitHeadquarterMember
                     {
                         UHM_Leader = info.UHM_Leader,
                         UHM_DepID = info.UHM_DepID,
                         UHM_DepName = info.UHM_DepName,
                         UHM_Phone = info.UHM_Phone,
                         UHM_Tel = info.UHM_Tel,
                         UHM_LiaisonName=info.UHM_LiaisonName,
                         UHM_LiaisonPhone = info.UHM_LiaisonPhone,
                         UHM_LiaisonTel = info.UHM_LiaisonTel,
                         UH_ID = info.UH_ID,
                         UH_Name = info.UH_Name,
                         UHM_TransTime = info.UHM_TransTime,
                         UHM_TransUser = info.UHM_TransUser,
                         UHM_TransIP = info.UHM_TransIP,
                         UHM_LiaisonPost=info.UHM_LiaisonPost,
                         UHM_Post= info.UHM_Post,
                     });
        }

        public int DeleteUintHeadquarterMember(int uhmID)
        {
            return ef.F_UnitHeadquarterMember.Where(p => p.UHM_ID == uhmID).Delete();
        }

        #endregion



        public int AddUnitHeadquarterDepartment(F_UnitHeadquarterDepartment info)
        {
            ef.F_UnitHeadquarterDepartment.Add(info);
            ef.SaveChanges();
            return info.UHD_ID;
        }

        public int UpdateUnitHeadquarterDepartment(F_UnitHeadquarterDepartment info)
        {
            var where = PredicateExtensionses.True<F_UnitHeadquarterDepartment>();
            where = where.And(p => p.UHD_ID == info.UHD_ID);
            return ef.F_UnitHeadquarterDepartment.Where(where)
                     .Update(u => new F_UnitHeadquarterDepartment
                     {
                          UHD_Name=info.UHD_Name,
                          UHD_Remarks=info.UHD_Remarks,
                          UHD_Sort=info.UHD_Sort,
                          UHD_TransDatetime=info.UHD_TransDatetime,
                          UHD_TransIP=info.UHD_TransIP,
                          UHD_TransUser=info.UHD_TransUser
                     });
        }


        public List<F_UnitHeadquarterDepartment> GetUnitHeadquarterDepartment(string uhdName)
        {
            return ef.F_UnitHeadquarterDepartment.Where(p => p.UHD_Name == uhdName).ToList();
        }

        public List<F_UnitHeadquarterDepartment> GetUnitHeadquarterDepartment(List<int> uhdIds)
        {
            return ef.F_UnitHeadquarterDepartment.Where(p =>uhdIds.Contains(p.UHD_ID)).ToList();
        }



        public List<F_UnitHeadquarterDepartment> UnitHeadquarterDepartmentByPager(F_UnitHeadquarterDepartment info, GridPager2 pager)
        {
            var where = PredicateExtensionses.True<F_UnitHeadquarterDepartment>();
            if (info != null)
            {

            }
            if (pager.StartRow == 0)
            {
                pager.StartRow = pager.PageSize * (pager.PageIndex - 1);
            }
            var list = ef.F_UnitHeadquarterDepartment.Where(where).OrderByDescending(o => o.UHD_Sort).ThenByDescending(t=>t.UHD_ID)
                     .Skip(pager.StartRow)
                     .Take(pager.PageSize).ToList();
            pager.TotalRows = ef.F_UnitHeadquarterDepartment.Where(where).Count();
            return list;
        }

        public int DeleteUnitHeadquarterDepartment(int uhdID)
        {
            return ef.F_UnitHeadquarterDepartment.Where(p => p.UHD_ID == uhdID).Delete();
        }
    }
}

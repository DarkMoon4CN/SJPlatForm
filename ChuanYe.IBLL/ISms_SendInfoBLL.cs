﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;

namespace ChuanYe.IBLL
{
    public interface ISms_SendInfoBLL
    {
        bool CreateSms_SendInfo(Sms_SendInfo model);
        List<ChuanYe.Models.Model.View_SmsPhone> GetSmsPhone();
        List<Departments> GetAllDepartments();
        Sms_SendInfoResult GetSms_SendInfo(Sms_SendInfo search, GridPager Pager);
        int Sms_SendInfoDelete(int ID);
        List<SMS_Model> GetSMS_Model();
        bool CreateSMS_Model(SMS_Model model);
        SMS_Model GetSMS_ModelByID(int ID);
        int SMS_ModelDelete(int ID);
        int SMS_ModelUpdate(SMS_Model model);
        SMS_ModelResult GetSMS_Model(string keyword, int? sms_mtype, GridPager Pager);


        List<SMS_Model> GetSMS_Model(int sms_mtype);
    }
}

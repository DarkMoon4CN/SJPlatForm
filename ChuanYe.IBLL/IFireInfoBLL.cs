﻿using ChuanYe.Models;
using ChuanYe.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IBLL
{
    public interface IFireInfoBLL
    {
        /// <summary>
        /// 火灾列表
        /// </summary>
        /// <param name="addressCountys">区县集合</param>
        /// <param name="fireSate">状态</param>
        /// <param name="pager">分页属性</param>
        /// <returns></returns>
        List<F_FireInfo> ListByPager(string[] addressCountys, int fireSate, GridPager2 pager);

        /// <summary>
        ///  根据火灾信息ID 获取当前正在操作的流程 
        /// </summary>
        /// <param name="fireIDs">火灾ID</param>
        /// <param name="searchLevel">查询级别 1.区县(隐藏所有首报等未及时操作的) </param>
        /// <returns></returns>
        List<M_Record> MRListByFireIDs(List<int> fireIDs, int searchLevel = 1);

        /// <summary>
        ///  获取火灾详情
        /// </summary>
        /// <param name="fireID">火灾ID</param>
        /// <returns></returns>
        F_FireInfo FireInfoDetail(int fireID);

        /// <summary>
        /// 根据火灾信息ID 获取此ID下所有操作流程
        /// </summary>
        /// <param name="fireID">火灾ID</param>
        ///  <param name="pager">分页类</param>
        /// <param name="searchLevel">查询级别 1.区县(隐藏所有首报等未及时操作,市局记录文档等) </param>
        /// <returns></returns>
        List<M_Record> MRListByFireID(int fireID, GridPager2 pager, int searchLevel = 1);

        /// <summary>
        /// 新增火灾信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int Add(F_FireInfo info);

        /// <summary>
        /// 增加火灾处置记录
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddRecord(M_Record info);

        /// <summary>
        /// 根据火灾ID 验证当前流程参数
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        List<M_Record> CKRecordTypeByTypes(int fireID, List<int> types);



        /// <summary>
        /// 增加市局询问信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddCityFireAsk(F_CityFireAsk info);



        /// <summary>
        /// 查找市局询问信息
        /// </summary>
        /// <param name="cfa_ID"></param>
        /// <returns></returns>
        F_CityFireAsk CityFireAskDetail(int cfa_ID);

        /// <summary>
        /// 增加区县回答信息
        /// </summary>
        int AddCountyFireAnswer(F_CountyFireAnswer info);


        /// <summary>
        /// 增加首报信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireFirstReport(F_FireFirstReport info);

        /// <summary>
        /// 首报信息详情
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
       F_FireFirstReport FireFirstReportDetail(int fireID);



        /// <summary>
        /// 增加续报信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireContinueReport(F_FireContinueReport info);


        /// <summary>
        /// 续报信息详情
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        F_FireContinueReport FireContinueReportDetail(int fireID);


        /// <summary>
        /// 续报集合
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_FireContinueReport> GetFireContinueReport(int fireID);


        /// <summary>
        /// 根据火灾信息ID和 MR_Type获取火灾处置信息
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="searchLevel"></param>
        /// <returns></returns>
        M_Record DetailMRecordByFireID(int fireID, int mrType, int searchLevel = 1);



        /// <summary>
        /// 增加误报信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddCatalog(Catalog info);


        /// <summary>
        /// 误报信息详情
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        Catalog FireCatalogDetail(int fireID);


        /// <summary>
        /// 误报信息列表
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<Catalog> GetFireCatalog(int fireID);



        /// <summary>
        /// 增加火灾消息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireAutomatic(M_Automatic info);


        /// <summary>
        ///  根据火灾ID和消息类型 获取消息信息
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="mTypes"></param>
        /// <returns></returns>
        List<M_Automatic> GetFireAutomatic(int fireID, List<int> mTypes);


        /// <summary>
        ///  根据火灾ID和消息类型 获取消息信息
        /// </summary>
        /// <param name="fireIDs"></param>
        /// <param name="mTypes"></param>
        List<M_Automatic> GetFireAutomatic(List<int> fireIDs, List<int> mTypes);

        /// <summary>
        /// 增加火灾时初始化消息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        KeyValuePair<int, string> AddFireAutomaticForInit(F_FireInfo info);


        /// <summary>
        /// 根据 火灾ID 消息类型 更改操作状态
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="mTypes"></param>
        /// <param name="replay"></param>
        /// <returns></returns>
        int UpdateFireAutoMatic(int fireID, List<int> mTypes, int replay = 0);


        /// <summary>
        /// 增加火灾时首报消息
        /// </summary>
        /// <param name="firstInfo"></param>
        /// <returns></returns>
        KeyValuePair<int,string> AddFireAutomaticForFirst(F_FireFirstReport firstInfo,F_FireInfo info);


        /// <summary>
        /// 根据火灾ID 消息类型 进行移除
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="mType"></param>
        /// <returns></returns>
        int DeleteFireAutoMatic(int fireID, int mType);

        /// <summary>
        /// 增加续报消息
        /// </summary>
        /// <param name="continueInfo"></param>
        /// <param name="isMany">是否多次续报</param>
        KeyValuePair<int, string> AddFireAutomaticForContinue(F_FireContinueReport continueInfo,bool isMany= false);


        /// <summary>
        /// 增加误报消息
        /// </summary>
        /// <param name="catalog"></param>
        KeyValuePair<int, string> AddFireAutomaticForCatalog(Catalog catalogInfo);


        /// <summary>
        ///  增加申请响应
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddApplyRespond(F_ApplyRespond info);

        /// <summary>
        /// 增加响应消息
        /// </summary>
        /// <param name="applyInfo"></param>
        /// <param name="level"></param>
        KeyValuePair<int, string> AddFireAutomaticForApplyRespond(F_ApplyRespond applyInfo, int level = 3);

        /// <summary>
        /// 获取申请响应信息
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        F_ApplyRespond FireApplyRespondDetail(int fireID);

        /// <summary>
        /// 增加报灭信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddReportOutFire(F_ReportOutFire info);

        /// <summary>
        /// 增加报灭消息
        /// </summary>
        /// <param name="reportOutInfo"></param>
        KeyValuePair<int, string>  AddFireAutomaticForReportOutFire(F_ReportOutFire reportOutInfo);


        /// <summary>
        /// 获取报灭信息
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        F_ReportOutFire FireReportOutDetail(int fireID);



        /// <summary>
        /// 增加火灾终报报告
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireFinalReport(F_FireFinalReport info);

        /// <summary>
        /// 增加火灾终报报告消息
        /// </summary>
        /// <param name="finalReportInfo"></param>
        /// <param name="fireInfo"></param>
        /// <returns></returns>
        KeyValuePair<int, string> AddFireAutomaticForFinalReport(F_FireFinalReport finalReportInfo,F_FireInfo fireInfo);

        /// <summary>
        ///  获取火灾终报报告
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        F_FireFinalReport FireFinalReportDetail(int fireID);



        /// <summary>
        /// 更新火灾信息状态
        /// </summary>
        /// <returns></returns>
        int UpdateFireInfoState(int fireID, int isState);

        /// <summary>
        /// 添加火灾消息（转场）
        /// </summary>
        /// <param name="info"></param>
        KeyValuePair<int, string> AddFireAutomaticForTransField(F_FireInfo info);

        /// <summary>
        /// 增加火灾转场信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireTransField(F_FireTransField info);


        /// <summary>
        /// 获取火灾转场信息
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        F_FireTransField FireTransFieldDetail(int fireID);


        /// <summary>
        /// 增加定位火点
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireSite(F_FireSite info);


        /// <summary>
        /// 增加纪录文档
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireDocumentation(F_FireDocumentation info);



        /// <summary>
        ///  根据火灾ID,获取火灾记录文档
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_FireDocumentation> GetFireDocumentation(int fireID);



        /// <summary>
        /// 获取定位火点
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        F_FireSite FireSiteDetail(int fireID);

        /// <summary>
        /// 根据火灾ID 获取多个定位火点
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_FireSite> GetFireSiteInfoByFireID(int fireID);

        /// <summary>
        /// 更新定位火点
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateFireSite(F_FireSite info);


        /// <summary>
        /// 添加成立前指
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFrontCommand(F_FrontCommand info);


        /// <summary>
        /// 更新成立前指
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateFrontCommand(F_FrontCommand info);

        /// <summary>
        /// 获取前指
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        F_FrontCommand FrontCommandDetail(int fireID);


        /// <summary>
        /// 获取前指
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_FrontCommand> GetFrontCommand(int fireID);


        /// <summary>
        /// 获取逐级响应
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="responseGrade"></param>
        /// <returns></returns>
        F_StepResponse StepResponseDetail(int fireID, int responseGrade=0);

        /// <summary>
        /// 增加逐级响应
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddStepResponse(F_StepResponse info);



        /// <summary>
        /// 增加响应 临时人员
        /// </summary>
        /// <param name="infos"></param>
        /// <returns></returns>
        int AddStepResponseTempPeople(List<F_StepResponseTempPeople> infos);


        /// <summary>
        /// 获取响应 临时人员
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="srID"></param>
        /// <returns></returns>
        List<F_StepResponseTempPeople> GetStepResponseTempPeople(int fireID,int srID);

        /// <summary>
        /// 根据响应级别获取启动条件
        /// </summary>
        /// <param name="responseGrade"></param>
        /// <returns></returns>
        List<F_StartCondition> GetStartCondition(int responseGrade);


        /// <summary>
        /// 增加成立前指 临时人员
        /// </summary>
        /// <param name="infos"></param>
        /// <returns></returns>
        int AddFrontCommandTempPeople(List<F_FrontCommandTempPeople> infos);


        /// <summary>
        /// 获取成立前指 临时人员
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="srID"></param>
        /// <returns></returns>
        List<F_FrontCommandTempPeople> GetFrontCommandTempPeople(int fireID, int fcID);




        /// <summary>
        /// 增加扑救力量
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFightingForce(F_FightingForce info);


        /// <summary>
        /// 获取扑救力量
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_FightingForce> GetFightingForce(int fireID);

        /// <summary>
        /// 增加联动响应
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddResponse(F_Response info);

        /// <summary>
        ///  获取联动响应
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_Response> GetResponse(int fireID);



        /// <summary>
        /// 获取火灾询问与回答
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<AskAnswerModel> FireAskAndAnswer(int fireID);

        /// <summary>
        ///  修改文档
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateFireDocumentation(F_FireDocumentation info);


        /// <summary>
        /// 修改消息信息状态 Replay
        /// </summary>
        /// <param name="autoMaticId"></param>
        /// <param name="fireID"></param>
        /// <param name="mTypes"></param>
        /// <param name="replay"></param>
        /// <returns></returns>

        int UpdateFireAutoMaticToReplay(int autoMaticId, int fireID, List<int> mTypes, int replay = 0);

        /// <summary>
        /// 修改消息信息状态 NReplay
        /// </summary>
        /// <param name="autoMaticId"></param>
        /// <param name="fireID"></param>
        /// <param name="mTypes"></param>
        /// <param name="replay"></param>
        /// <returns></returns>
        int UpdateFireAutoMaticToNReplay(int autoMaticId, int fireID, List<int> mTypes, int replay = 0);




        /// <summary>
        /// 获取消息
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="isCityManager"></param>
        /// <returns></returns>

        List<M_Automatic> GetAutomatic(int fireID, int isCityManager);

        /// <summary>
        /// 获取消息
        /// </summary>
        /// <param name="autoMaticId"></param>
        /// <returns></returns>
        M_Automatic AutoMaticDetail(int autoMaticId);


        /// <summary>
        /// 火灾支援信息表
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireForceSupport(F_FireForceSupport info);


        /// <summary>
        /// 增加态势图
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireSituation(F_FireSituation info);
        /// <summary>
        ///  编辑态势图
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateFireSituation(F_FireSituation info);



        /// <summary>
        ///  市局全局获取未读消息 
        /// </summary>
        /// <param name="addressCounty">地区ID</param>
        /// <param name="messageType">消息类型</param>
        /// <param name="isCityManager">是否是市局</param>
        /// <returns></returns>

        List<M_Automatic> GetAutomatic(int addressCounty, List<int> messageType, int isCityManager);

        /// <summary>
        /// 更新或审批误报
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateCatalogToApproval(Catalog info);

        /// <summary>
        /// 市局更改回复状态
        /// </summary>
        /// <param name="couID"></param>
        /// <param name="cityRead"></param>
        /// <returns></returns>
        int UpdateAnswerRead(int couID, bool cityRead=true);


        /// <summary>
        /// 获取终报
        /// </summary>
        /// <param name="fireIDs"></param>
        /// <returns></returns>
        List<F_FireFinalReport> GetFireFinalReport(List<int> fireIDs);


        /// <summary>
        /// 获取火灾详情
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        F_FireInfo FireInfoDetail(string phone);


        /// <summary>
        /// 增加火灾附件
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireAttachs(F_FireAttachs info);


        /// <summary>
        /// 增加火灾附件
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireAttachs(List<F_FireAttachs> infos);

        /// <summary>
        /// 火灾附件
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_FireAttachs> GetFireAttachs(int fireID);



        /// <summary>
        /// 查询区县回答根据市局询问Id
        /// </summary>
        /// <param name="cfa_ID"></param>
        /// <returns></returns>
        List<F_CountyFireAnswer> ExistCountyFireAnswer(int cfa_ID);

        /// <summary>
        /// 更新区县回答表
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateCountyFireAnswer(F_CountyFireAnswer info);

        /// <summary>
        /// 获取大区未完成火灾列表
        /// </summary>
        /// <param name="addressCounty"></param>
        /// <param name="fireState"></param>
        /// <returns></returns>
        List<F_FireInfo> GetFireInfo(string addressCounty,int fireState);


        /// <summary>
        /// 火灾附件
        /// </summary>
        /// <param name="fireIDs"></param>
        /// <returns></returns>
        List<F_FireAttachs> GetFireAttachs(List<int> fireIDs);

        /// <summary>
        /// 删除附件
        /// </summary>
        /// <param name="fireAttachsID"></param>

        void RemoveFireAttachs(int fireAttachsID);


        /// <summary>
        /// 根据扑救力量中的大区获取火灾
        /// </summary>
        /// <param name="addressCounty"></param>
        /// <param name="fireState"></param>
        /// <returns></returns>
        List<F_FireInfo> GetFireInfoFromForce(string addressCounty, int fireState);

        /// <summary>
        /// 根据扑救力量中的大区获取火灾 分页
        /// </summary>
        /// <param name="addressCounty"></param>
        /// <param name="fireState"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<F_FireInfo> GetFireInfoFromForceByPager(string addressCounty, int fireState, GridPager2 pager);


        /// <summary>
        ///  获取支援火灾信息
        /// </summary>
        /// <param name="addressCounty"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<dynamic> GetFireForceSupportByPager(int addressCounty, GridPager2 pager);


        /// <summary>
        ///  根据输入的手机号返回类似相关数据
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        List<dynamic> GetFireReporterInfo(string phone);


        /// <summary>
        /// 添加火灾接警内容
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireBlCons(F_BlCons info);

        /// <summary>
        /// 获取火灾接警列表
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_BlCons> GetFireBlCons(int fireID);

        /// <summary>
        /// 更新火灾终报
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateFireFinalReport(F_FireFinalReport info);

        /// <summary>
        /// 更新火灾接警信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateFireBlCons(F_BlCons info);

        /// <summary>
        /// 删除火灾接警信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteFireBlCons(int id);


        /// <summary>
        /// 增加 火灾处置进度标绘
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddFireRecordPlot(F_FireRecordPlot info);

        /// <summary>
        /// 获取 火灾处置进度标绘
        /// </summary>
        /// <param name="mrID"></param>
        /// <param name="fireID"></param>
        /// <param name="mrType"></param>
        /// <returns></returns>
        List<F_FireRecordPlot> GetFireRecordPlot(int mrID, int fireID, int mrType);


        /// <summary>
        /// 获取扑救力量
        /// </summary>
        /// <param name="fireID"></param>
        /// <returns></returns>
        List<F_FightingForce> GetFightingForce(List<int?> fireIDs);



        /// <summary>
        /// 告警记录
        /// </summary>
        /// <param name="info"></param>
        int AddAlarmRecord(F_AlarmRecord info);


        /// <summary>
        /// 更改告警记录状态
        /// </summary>
        /// <param name="fireID"></param>
        /// <param name="arID"></param>
        void ChangeFireAlarmRecordState(int fireID, int arID);


        /// <summary>
        /// 增加告警记录配置
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddAlarmRecordConfig(F_AlarmRecordConfig info);

        /// <summary>
        /// 获取有效配置
        /// </summary>
        /// <param name="nowDateTime"></param>
        /// <returns></returns>
        List<F_AlarmRecordConfig> GetAlarmRecordConfig(DateTime nowDateTime, string locaID, int unitID);


        /// <summary>
        /// 获取告警记录
        /// </summary>
        /// <param name="arID"></param>
        /// <returns></returns>
        F_AlarmRecord GetAlarmRecord(int arID);
    }
}

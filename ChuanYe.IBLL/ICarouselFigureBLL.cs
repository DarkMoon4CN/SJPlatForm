﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IBLL
{
    public interface ICarouselFigureBLL
    {
        int AddCarouselFigure(CarouselFigure info);
        List<CarouselFigure> GetCarouselFigureAll(int searchType = 0);

        int UpdateCarouselFigure(CarouselFigure info);
    }
}

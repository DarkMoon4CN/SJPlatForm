﻿using ChuanYe.Models;
using ChuanYe.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IBLL
{
    public interface IDocumentInfosBLL
    {
        /// <summary>
        /// 获取全部部门
        /// </summary>
        /// <returns></returns>
        List<Departments> DepartmentsIsSend();

        /// <summary>
        /// 查找MangerCode 不为空
        /// </summary>
        /// <returns></returns>
        List<Departments> DepartmentsManagerCode();

        /// <summary>
        /// 发送公文
        /// </summary>
        /// <param name="DInfo">公文信息</param>
        /// <param name="DAttachs">附件信息</param>
        /// <param name="DSms">短信信息</param>
        /// <returns></returns>
        int DocumentInfosCreate(DocumentInfos DInfo, List<DocumentAttachs> DAttachs, Sms_SendInfo DSms);
        /// <summary>
        /// 已发送公文
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        DocumentInfosSendResult DocumentInfosGetSend(DocumentInfosSendSearch search, GridPager Pager);
        /// <summary>
        /// 市局公文查询
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        DocumentInfosReceiveResult DocumentInfosReceive(DocumentInfosReceiveSearch search, GridPager Pager);
        /// <summary>
        /// 获取公文查询公文详情
        /// </summary>
        /// <param name="model">查询条件</param>
        /// <returns></returns>
        DocumentAllInfosModel DocumentInfosGetByBatch(DocumentInfosGetByBatchSearch model);
        /// <summary>
        /// 获取公文详情
        /// </summary>
        /// <param name="ID">公文ID</param>
        /// <returns></returns>
        DocumentAllInfosModel DocumentInfosGetByID(int ID);
        /// <summary>
        /// 删除公文
        /// </summary>
        /// <param name="ID">公文ID</param>
        /// <returns></returns>
        int DocumentInfosDelete(int ID);
        /// <summary>
        /// 删除附件
        /// </summary>
        /// <param name="ID">附件ID</param>
        /// <returns></returns>
        int DocumentAttachsDelete(int ID);
        /// <summary>
        /// 接收公文
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        string DocumentInfosReceive(DocumentInfosModel model);
        /// <summary>
        /// 区县公文接收
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        DocumentInfosWaitReceiveResult DocumentInfosWaitReceive(DocumentInfosWaitReceiveSearch search, GridPager Pager);
        /// <summary>
        /// 区县已收公文
        /// </summary>
        /// <param name="search">查询条件</param>
        /// <param name="Pager">分页信息</param>
        /// <returns></returns>
        DocumentInfosReceivedResult DocumentInfosReceived(DocumentInfosReceivedSearch search, GridPager Pager);



        int GetUnReadDocument(int depId);
        DocumentInfosReceivedResult GetUnReceivedDocument(int depId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;

namespace ChuanYe.IBLL
{
    public interface IStationBLL
    {
        /// <summary>
        /// 获取全部的站点信息
        /// </summary>
        /// <returns></returns>
        List<Station> GetStationAll();
        /// <summary>
        /// 根据IP获取站点信息
        /// </summary>
        /// <returns></returns>
        Station GetStationBySIP(string SIP);
        /// <summary>
        /// 根据IP获取气象信息
        /// </summary>
        /// <param name="SIP"></param>
        /// <returns></returns>
        List<View_StationWeather> GetStationWeatherBySIP(string SIP);
        /// <summary>
        /// 增加气象信息
        /// </summary>
        /// <param name="WInfoModeln"></param>
        /// <returns></returns>
        bool StationWeatherAdd(StationWeather WInfoModeln);
        /// <summary>
        /// 增加历史气象信息
        /// </summary>
        /// <param name="WInfoModeln"></param>
        /// <returns></returns>
        bool StationWeatherHistoryAdd(StationWeatherHistory WInfoModeln);
        /// <summary>
        /// 增加气象信息
        /// </summary>
        /// <param name="WInfoModeln"></param>
        /// <returns></returns>
        int StationWeatherUpdate(StationWeather WInfoModeln);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;


namespace ChuanYe.IBLL
{
    public interface IUserInfosBLL
    {
        ///// <summary>
        ///// 登录
        ///// </summary>
        ///// <param name="username"></param>
        ///// <param name="pwd"></param>
        ///// <returns></returns>
        //UsersInfos Login(string username, string pwd);
        ///// <summary>
        ///// 修改密码
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="newpwd"></param>
        ///// <returns></returns>
        //int EdtPwd(int userid, string OldPassword, string Password);
        ///// <summary>
        ///// 昵称是否存在
        ///// </summary>
        ///// <param name="nick"></param>
        ///// <returns></returns>
        //bool IsExistNick(string nick);
        ///// <summary>
        ///// 修改
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //int Edit(UsersInfos model);
        ///// <summary>
        ///// 获取用户列表
        ///// </summary>
        ///// <param name="pager"></param>
        ///// <param name="queryStr"></param>
        ///// <returns></returns>
        //List<UsersInfos> GetList(ref GridPager pager, string queryStr);
        ///// <summary>
        ///// 创建用户
        ///// </summary>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //bool Create(UsersInfos model);
        ///// <summary>
        ///// 获取单个用户
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //UsersInfos GetByID(int id);
        ///// <summary>
        ///// 获取所有模块权限
        ///// </summary>
        ///// <returns></returns>
        ////List<Power> GetAllPower();
        ///// <summary>
        ///// 是否存在该用户
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //bool IsExist(int id);
        ///// <summary>
        ///// 获取所有模块权限
        ///// </summary>
        ///// <returns></returns>
        ////List<Role> GetAllRole();
    }
}

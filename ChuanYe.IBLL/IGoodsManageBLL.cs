﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IBLL
{
    public interface IGoodsManageBLL
    {
        /// <summary>
        ///  物资仓库分页
        /// </summary>
        /// <param name="info"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<W_Warehouse> GetWarehouseByPage(W_Warehouse info, GridPager2 pager);

        /// <summary>
        /// 物资管理列表
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        List<W_Warehouse> GetWarehouse(W_Warehouse info);



        /// <summary>
        /// 物资列表 分页
        /// </summary>
        /// <param name="info"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<W_GoodsInfo> GetGoodsInfoByPage(W_GoodsInfo info, GridPager2 pager);

        /// <summary>
        /// 物资列表
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        List<W_GoodsInfo> GetGoodsInfo(W_GoodsInfo info);
    }
}

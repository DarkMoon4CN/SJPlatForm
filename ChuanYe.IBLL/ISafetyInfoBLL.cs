﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.Models.Model;

namespace ChuanYe.IBLL
{
    public interface ISafetyInfoBLL
    {
        List<View_SmsPhone> GetLeader();
        bool CreateSafetyInfo(SafetyInfo model);
        SafetySms_SendInfoResult GetLastSafetyInfo(SafetySms_SendInfoSearch search, GridPager pager);
        SafetyInfoResult GetSafetyInfoListByDay(SafetyInfo search, GridPager Pager);
        List<SP_SafetyInfoStatistics_Result> GetSafetyInfoStatistics(SafetyInfoStatisticsSearch search);
        SP_DistrictSafetyInfoStatistics_Result GetDistrictSafetyInfoStatistics(DistrictSafetyInfoStatisticsSearch search);
        List<SafetyInfo> GetSafetyInfoForMonth(SafetyInfoForMonthSearch search);
        SafetyInfo GetSafetyInfoByID(int ID);
        int SafetyInfoUpdate(SafetyInfo model);
        int IsSendToday(int DepId);

        int IsSendToday(int DepId, DateTime? date = null);
    }
}

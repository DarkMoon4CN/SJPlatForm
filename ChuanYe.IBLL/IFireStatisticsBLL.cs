﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IBLL
{
    public interface IFireStatisticsBLL
    {
        dynamic CountyStatistics(DateTime startTime, DateTime endTime);
        dynamic TimeStatistics(DateTime startTime, DateTime endTime);
        dynamic FireWhyStatistics(DateTime startTime, DateTime endTime);
        dynamic AlarmTypeStatistics(DateTime startTime, DateTime endTime);
        dynamic FireTypeStatistics(DateTime startTime, DateTime endTime);
        dynamic GareaStatistics(DateTime startTime, DateTime endTime);

        dynamic LareaStatistics(DateTime startTime, DateTime endTime);

        dynamic NumStatistics(DateTime startTime, DateTime endTime);
        dynamic LossStatistics(DateTime startTime, DateTime endTime);

        dynamic SummaryStatistics(DateTime startTime, DateTime endTime);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.Models.Model;

namespace ChuanYe.IBLL
{
    public interface IMailBLL
    {
        List<Models.Model.View_MailUsers> GetAllMailUsers();
        bool CreateInfosComm(InfosComm model, List<InfosCommAttachs> InfosCommAttachsModel);
        bool CreateInfosCommAttachs(InfosCommAttachs model);
        ReceiveMailListResult ReceiveMailGetList(ReceiveMailSearch Search, GridPager Pager);
        InfosCommInfoModel InfosCommGet(int ID);
        List<InfosCommAttachs> InfosCommAttachsGet(int ID);
        int InfosCommDelete(int ID);
        int InfosCommUpdate(int ID);
        PublicMailResult PublicMailGetList(PublicMailSearch Search, GridPager Pager);
        MailSendResult MailSendGetList(MailSendSearch Search, GridPager Pager);
        decimal GetAllInfosCommAttachsSize(int UserID);
        int ScheduleCreate(Schedule model);
        Schedule ScheduleGet(int S_ID);
        List<ScheduleGetMonthListResult> ScheduleGetMonthList(ScheduleSearch Search);
        List<ScheduleGetMonthListResult> ScheduleGetDayList(ScheduleSearch Search);
        int ScheduleUpdate(Schedule model);
        int ScheduleDelete(int S_ID);

        int InfosCommUpdateByIDs(int[] IDs);


        int ScheduleAutomaticStateUpdate(Schedule model);
    }
}

﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IBLL
{
    public interface INewsBLL
    {
        bool NewsTempCreate(NewsTemp model, NewsAttachs AttachModel);
        bool NewsTempUpdate(NewsTemp model);
        bool NewsInfsCreate(NewsInfs model);
        int NewsInfsUpdate(NewsInfs model);
        int NewsInfsPublic(NewsInfs model);
        NewsTempOneResult NewsTempGetByID(int ID);
        NewsInfs NewsInfsGetByID(int ID);
        List<NewsAttachs> NewsAttachsGetByNewsID(int ID);
        int NewsTempDelete(int ID);
        int NewsInfsDelete(int ID);
        int NewsAttachsCreate(NewsAttachs model);
        bool NewsReadStateCreate(NewsReadState model);
        NewsTempListResult NewsTempGetList(NewsTempListSearch search, GridPager Pager);
        NewsInfsResult NewsInfsGetList(NewsInfsSearch search, GridPager Pager);
        NewsInfsResult NewsInfsPublicGetList(NewsInfsPublicSearch search, GridPager Pager);

        NewsInfsStatisticsResult NewsInfsStatisticsGet(NewsInfsStatisticsSearch search, GridPager Pager);
        SP_NewsInfsStatisticsByDepId_Result NewsInfsStatisticsByDepIdGet(NewsInfsStatisticsByDepIdSearch search);
        NewsInfsGetListByDepIdResult NewsInfsGetListByDepId(NewsInfsGetListByDepIdSearch search, GridPager Pager);
        SP_NewsInfsCountyStatistics_Result NewsInfsCountyStatisticsGet(NewsInfsCountyStatisticsSearch search);

        List<dynamic> NewsInfsPublicGetListForGroup(NewsInfsPublicGroupSearch search, GridPager Pager);
        NewsInfsResult NewsInfsPublicGetList2(NewsInfsPublicSearch search, GridPager Pager);


        List<View_NewsTemp> NewTempGetList(View_NewsTemp info, GridPager2 pager);
    }
}

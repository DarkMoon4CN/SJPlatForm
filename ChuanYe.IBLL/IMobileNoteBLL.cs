﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;

namespace ChuanYe.IBLL
{
    public interface IMobileNoteBLL
    {
        /// <summary>
        /// 获取获取待发送短信数据
        /// </summary>
        /// <returns></returns>
        IEnumerator<View_MobileNote_MobileNoteDetail> GetMobileNoteDetailList(int MaxCounte);
        /// <summary>
        /// 发送成功修改状态
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int MobileNoteDetailUpdateForSuccess(int id);
        /// <summary>
        /// 备份原始数据 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int MobileNoteDetailSendAdd(MobileNoteDetailSend model);
        /// <summary>
        /// 发送失败修改发送次数
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int MobileNoteDetailUpdateForsCount(int id,int sCount);
        /// <summary>
        /// 每24小时进行一次数据性能维护,清除成功数据
        /// </summary>
        /// <returns></returns>
        int DeleteMobileNoteDetailSuccess();
    }
}

﻿using ChuanYe.Commom;
using ChuanYe.IBLL;
using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data;
using ChuanYe.Utils;
using SJPlatForm.FireDisposal.Web.Models;
using Microsoft.Practices.Unity;
using SJPlatForm.FireDisposal.Web.Factory;
using System.Web.Http.Cors;

namespace SJPlatForm.FireDisposal.Web.Controllers
{
    /// <summary>
    /// 火灾处置
    /// </summary>
    [EnableCors("*", "*", "*")]
    public class FireInfoController : BaseHubController<FireDisposalChatHub>
    {
        /// <summary>
        /// 业务依赖块
        /// </summary>
        [Dependency]
        public IFireInfoBLL bll { get; set; }

        /// <summary>
        /// 其他业务块
        /// </summary>
        [Dependency]
        public ILinchpinBLL linchpinBll { get; set; }

        #region 火灾查询及增加相关


        /// <summary>
        /// 获取火灾tips 列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, int> GetFireInfoByPager(FireInfoReq obj)
        {
            ResultDto<dynamic, int> result = new ResultDto<dynamic, int>();
            List<F_FireInfo> list = null;
            try
            {
                Log.Debug("FireInfoController",obj.SerializeJSON());
                GridPager2 pager = Pickout.GetGridPager2(obj);
                list= bll.ListByPager(obj.AddressCountys, obj.FireSate, pager);

                List<int> fireIDs = list.Select(s => s.F_FireID).ToList();
                var mrList=bll.MRListByFireIDs(fireIDs);

                var data = from info in list
                           join mr in mrList
                           on info.F_FireID equals mr.F_FireID
                           select new { info.F_FireID,info.F_Address,info.F_FireName,info.F_AddressCounty,MR_Message=Pickout.GetMRecordTypeText(mr.MR_Type.Value)};
                result.FirstParam = data;
                result.SecondParam = pager.TotalRows;
                result.Status = 1;
                result.Message = "Requst Is Success";
            }catch(Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireInfoByPager Server Error:" +ex.Message;
                Log.Error("FireInfoController GetFireInfoByPager", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 根据火灾ID 获取火灾详情页中的 火灾数据，处置数据
        /// </summary>
        /// <param name="obj">obj.FireID</param>
        /// <returns>Data:FireInfo  Second:M_Record Value:M_Record->RowCount</returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic, dynamic, dynamic> GetFireInfoByFireID(FireDetailReq obj)
        {
            ResultDto<dynamic,dynamic, dynamic, dynamic> result = new ResultDto<dynamic, dynamic, dynamic, dynamic>();
            try
            {
                if (obj != null && obj.FireID>0)
                {
                    F_FireInfo fireDetail = bll.DetailByFireID(obj.FireID);
                    if (fireDetail != null)
                    {
                        //获取火灾记录分页
                        GridPager2 pager = Pickout.GetGridPager2(obj);
                        List<M_Record> mrList = bll.MRListByFireID(fireDetail.F_FireID, pager);

                        //获取页面权限
                        List<int> selectedType = new List<int>() {0,3,10};
                        var temp=bll.CKRecordTypeByTypes(obj.FireID, selectedType);
                        var typeResult = selectedType.Select(s => new { MR_Type = s,
                                          Auth = (temp.Where(p=>p.MR_Type==s).FirstOrDefault()!=null ? 1:0 )});
                        result.FirstParam =fireDetail;
                        result.SecondParam =mrList;
                        result.ThirdParam =pager.TotalRows;
                        result.FourthParam = typeResult;
                        result.Status = 1;
                        result.Message = "Requst Is Success";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireInfoByFireID Server Error";
                Log.Error("FireInfoController GetFireInfoByFireID", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  增加火灾信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireInfo(AddFireInfoReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj !=null &&(!string.IsNullOrEmpty(obj.FireName)
                    && !string.IsNullOrEmpty(obj.AlarmContent)
                    && obj.HappenDatetime != null
                    && obj.AlarmTime != null))
                {
                    F_FireInfo info = new F_FireInfo();
                    info.F_FireName = obj.FireName;

                    info.F_HappenDatetime = obj.HappenDatetime;
                    info.F_TransDatetime = DateTime.Now;

                    info.F_AlarmTime = obj.AlarmTime;
                    info.F_AlarmContent = obj.AlarmContent;
                    info.F_Alarm = obj.Alarm;

                    info.F_AddressCounty = obj.AddressCounty;
                    info.F_Address = obj.Address;
                    info.F_AddressTown = obj.AddressTown;
                    info.F_AddressVillage = obj.AddressVillage;


                    info.F_ReporterName = obj.ReporterName;
                    info.F_ReporterUnit = obj.ReporterUnit;
                    info.F_ReporterWorknum = obj.ReporterWorknum;
                    info.F_ReporterPhone = obj.ReporterPhone;

                    info.F_TransUser = obj.TransUser;
                    info.F_TransIP = CheckRequest.GetWebClientIp();
                    info.F_TransIP = info.F_TransIP == null ? "0.0.0.0" : info.F_TransIP;
                    info.F_IsChange = 0;
                    info.F_IsState = 0;

                    int fireID=bll.Add(info);
                    if (fireID != 0)
                    {
                        M_Record mr = new M_Record();
                        mr.F_FireID = fireID;
                        mr.MR_HandleTime = info.F_HappenDatetime;
                        mr.MR_Type = 0;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        string happenDatetime = info.F_HappenDatetime.Value.ToString("yyyy年MM月dd日HH时mm分");
                        string mrContent = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), happenDatetime,info.F_Alarm,info.F_Address,info.F_AlarmContent);
                        mr.MR_Content = mrContent;
                        mr.MR_AddTime = info.F_TransDatetime;
                        mr.MR_AddIP = info.F_TransIP;
                        mr.MR_Applicant = info.F_Alarm;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);
                    }

                    result.FirstParam = fireID;
                    result.Status = 1;
                    result.Message = "Requst Is Success";
              
                  
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
              
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFire Server Error";
                Log.Error("FireInfoController AddFire", ex.ToString());
            }
            return result;
        }

        #endregion


        public ResultDto<string> SendCityFireAsk(SendCityFireAskReq obj)
        {
            return null;
        }


        #region 区县等级联菜单相关

        /// <summary>
        /// 根据ID 获取城市信息
        /// </summary>
        /// <param name="obj">obj.LinchpinID</param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetLinchpin(dynamic obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                result.FirstParam=linchpinBll.GetLinchpin(obj.LinchpinID);
                result.Status = 1;
                result.Message = "Requst Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetLinchpin Server Error";
                Log.Error("FireInfoController GetLinchpin", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 根据ID 获取下级城市信息集合
        /// </summary>
        /// <param name="obj">obj.LinchpinID</param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetSubLinchpin(dynamic obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                result.FirstParam=linchpinBll.GetSubLinchpin(obj.LinchpinID);
                result.Status = 1;
                result.Message = "Requst Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetSubLinchpin Server Error";
                Log.Error("FireInfoController GetSubLinchpin", ex.ToString());
            }
            return result;
        }
        #endregion

    

    }
}
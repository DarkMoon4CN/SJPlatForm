﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.FireDisposal.Web.Controllers
{
    public class Pickout
    {
        public Pickout() { }

        public static GridPager2 GetGridPager2(dynamic entity)
        {
            GridPager2 pager = new GridPager2();
            if (entity != null)
            {
                pager.DataId = entity.DataId;
                pager.Keyword = entity.Keyword;
                pager.Sort = entity.Sort;
                pager.PageIndex = entity.PageIndex==0 ? 1 :entity.PageIndex;
                pager.PageSize = entity.PageSize==0 ? 10:entity.PageSize;
                pager.StartRow = entity.StartRow;
            }
            return pager;
        }
        public static string GetMRecordTypeText(int mrType, bool isCounty=true)
        {
            string msg = string.Empty;
            switch (mrType)
            {
                case 0:msg = isCounty==true?"首报倒计时":"火警录入";break;
                case 1: msg = "区县回复"; break;
                case 2: msg = "火灾首报"; break;
                case 3: msg = "误报申请"; break;
                case 4: msg = "误报审批"; break;
                case 5: msg = "火灾续报"; break;
                case 6: msg = "申请响应"; break;
                case 7: msg = "市局询问"; break;
                case 8: msg = "区县回答"; break;
                case 9: msg = "启动响应"; break;
                case 10: msg = "接收响应"; break;
                case 11: msg = "成立前指"; break;
                case 12: msg = "接收前指"; break;
                case 13: msg = "市局调集扑救力量"; break;
                case 14: msg = "调集提示"; break;
                case 15: msg = "调集回复"; break;
                case 16: msg = "火灾报灭"; break;
                case 17: msg = "火灾终报"; break;
                case 18: msg = "记录文档"; break;
                case 19: msg = "定位火点"; break;
                case 20: msg = "逐级响应"; break;
                case 21: msg = "首报未按时操作"; break;
                case 22: msg = "续报未按时操作"; break;
                case 23: msg = "终报未按时操作"; break;
            }
            return msg;
        }



        public static string GetFireInfoFormat(int mrType)
        {
            string content = string.Empty;
            switch (mrType)
            {
                case 0:
                    content= "{0}接{1}报警在 {2} 发生火灾，接报案内容：{3}";break;
        }
            return content;
        }
       
    }
}
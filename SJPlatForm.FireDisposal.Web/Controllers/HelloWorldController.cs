﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SJPlatForm.FireDisposal.Web.Controllers
{
    public class HelloWorldController : ApiController
    {

        [HttpGet]
        public string SayHello()
        {
            string message = "hello world ,Test connection successful,";
            message += "   server time: " + DateTime.Now.ToString();
            return message;
        }
    }
}

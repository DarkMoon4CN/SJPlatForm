﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Data;
using ChuanYe.DAL;
using ChuanYe.Models;
using System.Threading.Tasks;
using ChuanYe.Commom;
using ChuanYe.Utils;
using Microsoft.AspNet.SignalR.Hubs;
using SJPlatForm.FireDisposal.Web.Factory;

namespace SJPlatForm.FireDisposal.Web
{
    public class FireDisposalChatHub : Hub
    {
        protected readonly FireInfoDAL dal = new FireInfoDAL();
        protected readonly UsersInfosDAL userinfoDAL = new UsersInfosDAL();
        public void Test(string message)

        {
            Clients.All.sendMessage("1", message);
        }

        public void SendText(string type, int fid, int userid, string message)
        {
            Clients.All.sendMessage(fid, message);
        }

       
        public void SendCityFireAsk(int userid, int fireid, string message)
        {

            string userName = string.Empty;
            //当前连接的connectionId
            string connectionId = Context.ConnectionId;
            var existConnection = fireQorAConnection.Where(p => p.ConnectionId == connectionId).FirstOrDefault();
            if (existConnection == null)
            {
                fireQorAConnection.Add(new FireQorAConnection() { ConnectionId = connectionId, FireID = fireid });
            }
            else
            {
                existConnection.FireID = fireid;
            }
            try
            {
                //写入市局询问库
                F_CityFireAsk ask = new F_CityFireAsk();
                ask.CFA_TransUser = userid.ToString();
                ask.CFA_TransDatetime = DateTime.Now;
                ask.CFA_TransIP = CheckRequest.GetWebClientIp();
                ask.CFA_TransIP = ask.CFA_TransIP == null ? "0.0.0.0" : ask.CFA_TransIP;
                ask.CFA_HandleTime = ask.CFA_TransDatetime;
                ask.CFA_AskContent = message;
                ask.F_FireID = fireid;
                int askId = dal.AddCityFireAsk(ask);

                //写入火灾信息表
                M_Record record = new M_Record();
                record.F_FireID = fireid;
                record.MR_HandleTime = ask.CFA_TransDatetime;
                record.MR_Type = 7;//市局询问 类型
                record.MR_UserID = 0;
                record.MR_Content = ask.CFA_AskContent;
                record.CFA_ID = askId;
                record.MR_AddTime = record.MR_HandleTime;
                record.MR_AddUser = userid.ToInt();
                record.MR_AddIP = ask.CFA_TransIP;
                record.MR_Applicant = "0";
                int recordId = dal.AddRecord(record);
                if (askId > 0 && recordId > 0)
                {
                    userName = userinfoDAL.UsersInfosInfo(userid).UserName;
                }
            }
            catch (Exception ex)
            {
                Log.Error("FireDisposalChatHub ", ex.ToString());
            }


            List<string> sendClients = fireQorAConnection.Select(s => s.ConnectionId).ToList();
            Clients.Clients(sendClients).sendMessage(userName, message);
        }


        public void SendCountyFireAnswer(int userid, int fireid, int askid, string answername, string message)
        {
            string userName = string.Empty;
            string connectionId = Context.ConnectionId;
            var existConnection = fireQorAConnection.Where(p => p.ConnectionId == connectionId).FirstOrDefault();
            if (existConnection == null)
            {
                fireQorAConnection.Add(new FireQorAConnection() { ConnectionId = connectionId, FireID = fireid });
            }
            else
            {
                existConnection.FireID = fireid;
            }
            try
            {
                //判定是否回复过
                var existList=dal.ExistCountyFireAnswer(askid);
                int answerId = 0;
               
                //写入区县回复库
                F_CountyFireAnswer answer = new F_CountyFireAnswer();
                answer.F_FireID = fireid.ToInt();
                answer.CFA_ID = askid;
                answer.Cou_Answerer = answername;
                answer.Cou_AnswerContent = message;
                answer.Cou_TransDatetime = DateTime.Now;
                answer.Cou_HandleTime = answer.Cou_TransDatetime;
                answer.Cou_TransUser = userid.ToString();
                answer.Cou_TransIP = CheckRequest.GetWebClientIp() == null ? "0.0.0.0" : CheckRequest.GetWebClientIp();
                if (existList.Count == 0)
                {
                    answerId = dal.AddCountyFireAnswer(answer);
                }
                else
                {
                    var temp = existList.FirstOrDefault();
                    temp.Cou_AnswerContent = answer.Cou_AnswerContent;
                    temp.Cou_Answerer = answername;
                    temp.Cou_TransDatetime = answer.Cou_TransDatetime;
                    temp.Cou_HandleTime = answer.Cou_HandleTime;
                    temp.Cou_TransUser = answer.Cou_TransUser;
                    answerId= dal.UpdateCountyFireAnswer(temp);
                }
                if (existList.Count == 0)
                {
                    //写入火灾信息表
                    M_Record record = new M_Record();
                    record.F_FireID = fireid.ToInt();
                    record.MR_HandleTime = answer.Cou_TransDatetime;
                    record.MR_Type = 8;//区县回复 类型
                    record.MR_UserID = 0;
                    record.MR_Content = answer.Cou_AnswerContent;
                    record.CFA_ID = answer.CFA_ID;
                    record.MR_AddTime = answer.Cou_TransDatetime;
                    record.MR_AddUser = userid;
                    record.MR_AddIP = answer.Cou_TransIP;
                    record.MR_Applicant = answername;
                    int recordId=recordId = dal.AddRecord(record);
                }
                if (answerId > 0)
                {
                    userName = userinfoDAL.UsersInfosInfo(userid).UserName;
                }
            }
            catch (Exception ex)
            {
                Log.Error("FireDisposalChatHub ", ex.ToString());
            }
            List<string> sendClients = fireQorAConnection.Select(s => s.ConnectionId).ToList();
            Clients.Clients(sendClients).sendMessage(userName, message);
        }


        public override Task OnConnected()
        {
            string connectionId = Context.ConnectionId;
            var existConnection = fireQorAConnection.Where(p => p.ConnectionId == connectionId).FirstOrDefault();
            if (existConnection == null)
            {
                fireQorAConnection.Add(new FireQorAConnection() { ConnectionId = connectionId });
            }
            return base.OnConnected();
        }


        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            string connectionId = Context.ConnectionId;
            var removeConnection = fireQorAConnection.Where(p => p.ConnectionId == connectionId).FirstOrDefault();
            if (removeConnection != null)
            {
                fireQorAConnection.Remove(removeConnection);
            }
            return base.OnDisconnected(stopCalled);
        }
        public static List<FireQorAConnection> fireQorAConnection = new List<FireQorAConnection>();

    }
}




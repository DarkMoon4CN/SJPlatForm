﻿using ChuanYe.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace SJPlatForm.FireDisposal.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new Microsoft.Practices.Unity.UnityContainer();
            DependencyRegisterType.Container_Sys(ref container);
            config.DependencyResolver = new UnityResolver(container);
            
            config.EnableCors();

            // Web API 配置和服务
            config.MapHttpAttributeRoutes();

            // Web API 路由
            config.Routes.MapHttpRoute(
            name: "CustomApi",
            routeTemplate: "{controller}/{action}/{id}",
            defaults: new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
            name: "OtherApi",
            routeTemplate: "{controller}/{id}",
            defaults: new { id = RouteParameter.Optional });



            //----------------------------------------------------------
            config.Routes.MapHttpRoute(
            name: "HelloApi",
            routeTemplate: "{controller}/{action}/{id}",
            defaults: new { controller = "HelloWorld", action = "SayHello", id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            //默认返回 json
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("datatype", "json", "application/json"));
            //返回格式选择 datatype 可以替换为任何参数 
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("datatype", "xml", "application/xml"));
        }
    }
}

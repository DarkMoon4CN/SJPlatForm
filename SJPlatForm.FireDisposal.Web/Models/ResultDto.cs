﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.FireDisposal.Web.Models
{

    /// <summary>
    /// 返回数据体
    /// </summary>
    public class ResultDtoBase
    { 

        public ResultDtoBase() { }
        /// <summary>
        /// 1.表示成功，其他失败，自定义
        /// </summary>
        public int Status { get; set; }
        public string Message { get; set; }
    }


    /// <summary>
    /// 返回附带单个参数
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResultDto<T> : ResultDtoBase
    {
        public T FirstParam { get; set; }
    }

    /// <summary>
    /// 返回附带两个参数
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="V"></typeparam>
    public class ResultDto<T, T2> : ResultDtoBase
    {
        public T FirstParam { get; set; }
        public T2 SecondParam { get; set; }
    }

    public class ResultDto<T, T2, T3> : ResultDtoBase
    {
        public T FirstParam { get; set; }
        public T2 SecondParam { get; set; }
        
        public T3 ThirdParam { get; set; }
    }

    public class ResultDto<T, T2, T3,T4> : ResultDtoBase
    {
        public T FirstParam { get; set; }
        public T2 SecondParam { get; set; }
        public T3 ThirdParam { get; set; }
        public T4 FourthParam { get; set; }
    }

}
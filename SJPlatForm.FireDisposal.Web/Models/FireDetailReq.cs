﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.FireDisposal.Web.Models
{
    public class FireDetailReq: GridPager2
    {
        public int FireID { get; set; }
    }
}
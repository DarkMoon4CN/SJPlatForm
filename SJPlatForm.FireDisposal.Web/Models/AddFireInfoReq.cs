﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.FireDisposal.Web.Models
{
    public class AddFireInfoReq
    {
        public int FireID { get; set; }

        public string FireName { get; set; }
        public string ReporterName { get; set; }
        public string ReporterUnit { get; set; }
        public string ReporterWorknum { get; set; }
        public string ReporterPhone { get; set; }
        public string AddressCounty { get; set; }
        public string AddressTown { get; set; }
        public string AddressVillage { get; set; }
        public string Address { get; set; }
        public DateTime?  HappenDatetime { get; set; }
        public DateTime? AlarmTime { get; set; }
        public string Alarm { get; set; }
        public string AlarmContent { get; set; }
        public string TransUser { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.FireDisposal.Web.Models
{
    public class SendCityFireAskReq
    {
        int Userid { get; set; }
        int Fireid { get; set; }
        string Message { get; set; }
    }
}
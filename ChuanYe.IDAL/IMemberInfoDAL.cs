﻿using ChuanYe.Models;
using ChuanYe.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IDAL
{
    public interface IMemberInfoDAL
    {

        /// <summary>
        /// 获取所有防火指挥部，但O_UpOrg!=0
        /// </summary>
        /// <returns></returns>
        List<F_OrgInfo> GetOrgInfo();

        /// <summary>
        /// 根据防火指挥部获取其下的用户
        /// </summary>
        /// <param name="orgID"></param>
        /// <returns></returns>
        List<F_MemberInfo> GetMemberInfo(int orgID);


        /// <summary>
        /// 根据ID 集合获取用户
        /// </summary>
        /// <param name="mIDs"></param>
        /// <returns></returns>
        List<F_MemberInfo> GetMemberInfo(List<int> mIDs);


        /// <summary>
        ///  获取单位类型
        /// </summary>
        /// <param name="unitTypeID"></param>
        /// <returns></returns>
        List<F_UnitType> GetUnitType(int unitTypeID = 0);


        /// <summary>
        /// 增加单位类型
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddUnitType(F_UnitType info);

        /// <summary>
        /// 编辑单位类型
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateUnitType(F_UnitType info);

        /// <summary>
        /// 获取单位
        /// </summary>
        /// <param name="uintID"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<F_Unit> GetUnit(int uintID, GridPager2 pager);


        /// <summary>
        /// 获取单位
        /// </summary>
        /// <param name="unitTypeID"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<F_Unit> GetUnitByUnitTypeID(int unitTypeID, int uhID, int responseLevel, GridPager2 pager);

        /// <summary>
        /// 增加单位
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddUnit(F_Unit info);


        /// <summary>
        /// 编辑单位
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateUnit(F_Unit info);

        /// <summary>
        /// 获取单位 分页
        /// </summary>
        /// <param name="info"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<F_Unit> UnitByPager(F_Unit info, GridPager2 pager);

        /// <summary>
        /// 移除单位
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        int DeleteUnit(int uid);



        /// <summary>
        /// 联合查询 根据单位 获取单位所在区县ID
        /// </summary>
        /// <param name="unitIDs"></param>
        /// <returns></returns>
        List<UintOrgInfo> UnitJoinOrgInfo(List<int> unitIDs);

        /// <summary>
        /// 获取起火原因和报警方式
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        List<DirectorysTb> GetDirectorysTb(int parentID);

        /// <summary>
        /// 获取联动响应单位
        /// </summary>
        /// <returns></returns>

        List<LinkCorresponding> GetLinkCorresponding();


        /// <summary>
        /// 火灾原因等分页
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="name"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<DirectorysTb> DirectorysTbByPager(int parentID,string name,GridPager2 pager);

        /// <summary>
        /// 增加火灾原因等
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddDirectorysTb(DirectorysTb info);


        /// <summary>
        /// 更新火灾原因等
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateDirectorysTb(DirectorysTb info);

        /// <summary>
        /// 移除火灾原因等
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteDirectorysTb(int id);



        /// <summary>
        ///  单条 火灾原因等 数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DirectorysTb DirectorysTbDetail(int id);

        /// <summary>
        /// 森林防火指挥部
        /// </summary>
        /// <param name="parentID">上一级ID</param>
        /// <param name="uhName">指挥部名</param>
        ///  <param name="level">级别</param>
        /// <param name="uhLeader">指挥领导</param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<F_UnitHeadquarter> UintHeadquarterByPager(int parentID, string uhName, int level, string uhLeader,int uhLinchpinID, GridPager2 pager);



        /// <summary>
        /// 增加森林防火指挥部相关信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddUintHeadquarter(F_UnitHeadquarter info);

        /// <summary>
        /// 编辑森林防火指挥部相关信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpDateUintHeadquarter(F_UnitHeadquarter info);

        /// <summary>
        /// 移除森林防火指挥部相关信息
        /// </summary>
        /// <param name="uhID"></param>
        /// <returns></returns>
        int DeleteUintHeadquarter(int uhID);


        /// <summary>
        ///  森林防火指挥部成员
        /// </summary>
        /// <param name="unitID">单位Id</param>
        /// <param name="phone">成员电话</param>
        /// <param name="name">成员姓名</param>
        /// <param name="uhID">指挥部ID</param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<F_UnitMember> UnitMemberByPager(int unitID, string phone, string name,GridPager2 pager);



        #region 联动响应
        List<LinkCorresponding> LinkCorrespondingByPager(LinkCorresponding info, GridPager2 pager);
        int AddLinkCorresponding(LinkCorresponding info);

        int UpDateLinkCorresponding(LinkCorresponding info);

        int DeleteLinkCorresponding(int id);
        #endregion

        #region 一级响应
        List<OneGradeTemplate> GetGradeTemplate();
        List<OneGradeTemplate> GradeTemplateByPager(OneGradeTemplate info, GridPager2 pager);
        int AddGradeTemplate(OneGradeTemplate info);
        int UpDateGradeTemplate(OneGradeTemplate info);
        int DeleteGradeTemplate(int id);
        #endregion

        #region 2级与3级响应
        int DeleteOtherGradeTemplate(int id);
        int UpDateOtherGradeTemplate(ThreeAndTwoGradeTemplate info);
        List<dynamic> OtherGradeTemplateByPager(ThreeAndTwoGradeTemplate info, GridPager2 pager);

        List<ThreeAndTwoGradeTemplate> GetOtherGradeTemplate();

        int AddOtherGradeTemplate(ThreeAndTwoGradeTemplate info);
        #endregion

        #region 单位成员
        List<F_UnitMember> UnitMemberByPager(F_UnitMember info, GridPager2 pager);
        int AddUnitMember(F_UnitMember info);
        int UpdateUnitMember(F_UnitMember info);
        int DeleteUnitMember(int umID);

        List<F_Unit> GetUnitByName(string unitName);
        #endregion

        #region  F_UnitHeadquarterMember

        List<F_UnitHeadquarterMember> UintHeadquarterMemberByPager(F_UnitHeadquarterMember info, GridPager2 pager);

        int AddUintHeadquarterMember(F_UnitHeadquarterMember info);
       
        int UpdateUintHeadquarterMember(F_UnitHeadquarterMember info);

        List<F_UnitHeadquarterMember> GetUnitHeadquarterMember(List<int> ids);


        List<F_UnitHeadquarterMember> GetUnitHeadquarterMemberByDepIDs(List<int> depIDs);

        int DeleteUintHeadquarterMember(int uhmID);
        #endregion



        /// <summary>
        /// 指挥部单位 列表
        /// </summary>
        /// <param name="info"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        List<F_UnitHeadquarterDepartment> UnitHeadquarterDepartmentByPager(F_UnitHeadquarterDepartment info, GridPager2 pager);


        /// <summary>
        ///  编辑指挥部单位
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int UpdateUnitHeadquarterDepartment(F_UnitHeadquarterDepartment info);

        /// <summary>
        /// 增加指挥部单位
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddUnitHeadquarterDepartment(F_UnitHeadquarterDepartment info);


        /// <summary>
        /// 查询指挥部单位
        /// </summary>
        /// <param name="uhdName"></param>
        /// <returns></returns>
        List<F_UnitHeadquarterDepartment> GetUnitHeadquarterDepartment(string uhdName);

        /// <summary>
        /// 查询指挥部单位
        /// </summary>
        /// <param name="uhdIds"></param>
        /// <returns></returns>
        List<F_UnitHeadquarterDepartment> GetUnitHeadquarterDepartment(List<int> uhdIds);

        /// <summary>
        /// 移除指挥部单位
        /// </summary>
        /// <param name="uhdID"></param>
        /// <returns></returns>
        int DeleteUnitHeadquarterDepartment(int uhdID);


        /// <summary>
        ///  数据字典
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        List<DirectorysTb> GetDirectorysTb(int parentID, string name);

        /// <summary>
        /// 指挥部
        /// </summary>
        /// <param name="uhName"></param>
        /// <returns></returns>
        List<F_UnitHeadquarter> GetUnitHeadquarter(string uhName);

        /// <summary>
        /// 指挥部
        /// </summary>
        /// <param name="uhID"></param>
        /// <returns></returns>
        List<F_UnitHeadquarter> GetUnitHeadquarter(int uhID);

        /// <summary>
        /// 获取指挥部人员信息
        /// </summary>
        /// <param name="uhmLeader"></param>
        /// <returns></returns>
        List<F_UnitHeadquarterMember> GetUnitHeadquarterMember(string uhmLeader);

        /// <summary>
        /// 获取2,3级响应
        /// </summary>
        /// <param name="sGradeID"></param>
        /// <param name="linchpinIID"></param>
        /// <returns></returns>

        List<ThreeAndTwoGradeTemplate> GetOtherGradeTemplate(int sGradeID, long linchpinIID);
    }
}

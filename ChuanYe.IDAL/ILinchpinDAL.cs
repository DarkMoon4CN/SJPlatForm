﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IDAL
{
    public interface ILinchpinDAL
    {
        /// <summary>
        /// 根据ID 获取下级城市信息集合
        /// </summary>
        /// <param name="linchpinID"></param>
        /// <returns></returns>
        List<Linchpin> GetSubLinchpin(int linchpinID);

        // <summary>
        /// 根据ID 获取城市信息
        /// </summary>
        /// <param name="linchpinID"></param>
        /// <returns></returns>
        Linchpin GetLinchpin(int linchpinID);


        /// <summary>
        /// 获取多个城市信息
        /// </summary>
        /// <param name="linchpinIDs"></param>
        /// <returns></returns>
        List<Linchpin> GetLinchpin(List<long> linchpinIDs);
    }
}

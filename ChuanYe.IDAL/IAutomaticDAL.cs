﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IDAL
{
    public interface IAutomaticDAL
    {

        /// <summary>
        /// 增加消息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddAutomatic(Automatic info);


        /// <summary>
        /// 获取消息
        /// </summary>
        /// <param name="receiveArea"></param>
        /// <param name="receiveUserID"></param>
        /// <param name="receiveUserName"></param>
        /// <param name="isReplay"></param>
        /// <returns></returns>
        List<Automatic> GetAutomatic(string receiveArea, int receiveUserID, string receiveUserName, int isReplay = 0);

        /// <summary>
        /// 修改消息已读状态
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="isReplay"></param>
        /// <returns></returns>
        int UpdateAutomaticReplay(int mid, int isReplay);

        /// <summary>
        /// 修改消息已读状态
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="isReplay"></param>
        /// <returns></returns>
        int UpdateAutomaticReplay(int receiveUserID, DateTime? sendTime, int type, int isReplay = 0);

        /// <summary>
        /// 根据所属业务ID,移除消息
        /// </summary>
        /// <param name="customData"></param>
        /// <returns></returns>
        int DeleteAutomatic(string  customData);

        /// <summary>
        /// 根据接收用户 获取信息
        /// </summary>
        /// <param name="receiveUserIDs"></param>
        /// <returns></returns>
        List<Automatic> GetAutomatic(List<string> customDatas);


    }
}

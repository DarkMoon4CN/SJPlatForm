﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;

namespace ChuanYe.IDAL
{
    public interface IStationDAL
    {
        /// <summary>
        /// 根据IP获取站点信息
        /// </summary>
        /// <returns></returns>
        Station GetStationBySIP(string SIP);
    }
}

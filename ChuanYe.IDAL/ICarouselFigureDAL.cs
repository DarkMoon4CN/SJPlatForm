﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.IDAL
{
    public interface ICarouselFigureDAL
    {
        int AddCarouselFigure(CarouselFigure info);
        List<CarouselFigure> GetCarouselFigureAll(int searchType = 0);

        int UpdateCarouselFigure(CarouselFigure info);
    }
}

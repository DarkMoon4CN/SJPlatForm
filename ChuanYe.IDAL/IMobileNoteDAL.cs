﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;

namespace ChuanYe.IDAL
{
    public interface IMobileNoteDAL
    {
        /// <summary>
        /// 获取获取待发送短信数据
        /// </summary>
        /// <returns></returns>
        IQueryable<View_MobileNote_MobileNoteDetail> GetMobileNoteDetailList(int MaxCounte);
    }
}

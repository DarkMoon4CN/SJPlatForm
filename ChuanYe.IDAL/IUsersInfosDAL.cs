﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChuanYe.Models;
using ChuanYe.Models.Model;

namespace ChuanYe.IDAL
{
    public interface IUsersInfosDAL
    {
        #region 登录
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        LoginUserModel LoginIn(UsersInfos model);

        /// <summary>
        /// 登录有户信息
        /// </summary>
        /// <returns></returns>
        LoginUserModel LoginUserInfos(int ID);
        /// <summary>
        /// 根据ID判断是否存在
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool IsIDRight(int ID);
        #endregion

        #region 模块管理
        /// <summary>
        /// 添加模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int PowerCreate(Power model);
        /// <summary>
        /// 删除模块
        /// </summary>
        /// <param name="ID">模块ID</param>
        /// <returns></returns>
        int PowerDelete(int ID);
        /// <summary>
        /// 模块详情
        /// </summary>
        /// <param name="ID">模块ID</param>
        /// <returns></returns>
        Power PowerInfo(int ID);
        /// <summary>
        /// 修改模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int PowerUpdate(Power model);
        /// <summary>
        /// 查询模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PowerResult PowerGetList(PowerSearch Search, GridPager Pager);
        /// <summary>
        /// 查询全部模块
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<Power> PowerGetAll(PowerSearch Search, GridPager Pager);
        #endregion

        #region 职务管理
        /// <summary>
        /// 是否已有相同的职务名称
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int PositionIsHave(string PositionName);

        int PositionIsHave(string positionName, int id = 0);

        /// <summary>
        /// 添加职务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int PositionCreate(Position model);
        /// <summary>
        /// 删除职务
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        int PositionDelete(int ID);
        /// <summary>
        /// 职务详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Position PositionInfo(int ID);
        /// <summary>
        /// 修改职务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int PositionUpdate(Position model);
        /// <summary>
        /// 获取全部职务
        /// </summary>
        /// <returns></returns>
        List<Position> PositionGetAll();

        /// <summary>
        /// 获取职务分页
        /// </summary>
        /// <returns></returns>
        List<Position> PositionByPage(string positionName, GridPager pager);

        #endregion

        #region 角色管理
        /// <summary>
        /// 获取权限树
        /// </summary>
        /// <returns></returns>
        List<PowerModel> PowerGetAllTree();
        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="model"></param>
        /// <param name="RolePowerList"></param>
        /// <returns></returns>
        int RolesCreate(MemberShip_Roles model, List<RolePower> RolePowerList);
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        int RolesDelete(int ID);
        /// <summary>
        /// 角色详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        MemberShip_RolesResult RolesInfo(int ID);
        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="model"></param>
        /// <param name="RolePowerList"></param>
        /// <returns></returns>
        int RolesUpdate(MemberShip_Roles model, List<RolePower> RolePowerList);
        /// <summary>
        /// 获取全部角色
        /// </summary>
        /// <returns></returns>
        List<MemberShip_Roles> MemberShip_RolesGetAll();
        /// <summary>
        /// 分页查询角色
        /// </summary>
        /// <param name="Pager"></param>
        /// <returns></returns>
        MemberShip_RolesSearchResult MemberShip_RolesSearch(GridPager Pager);

    
        #endregion

        #region 机构管理
        /// <summary>
        /// 添加机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int DepartmentsCreate(Departments model);
        /// <summary>
        /// 删除机构
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        int DepartmentsDelete(int ID);
        /// <summary>
        /// 机构详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Departments DepartmentsInfo(int ID);



        List<Departments> DepartmentsInfo(string address);

        /// <summary>
        /// 修改机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int DepartmentsUpdate(Departments model);
        /// <summary>
        /// 机构查询
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        DepartmentsSearchResult DepartmentsSearch(DepartmentsSearch Search, GridPager Pager);
        /// <summary>
        /// 获取部门下拉框
        /// </summary>
        /// <returns></returns>
        List<DepartmentsAllModel> DepartmentsGetAll();
        #endregion

        #region 用户管理
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int UsersInfosCreate(UsersInfos model);
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        int UsersInfosDelete(int ID);
        /// <summary>
        /// 用户详情
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        UsersInfos UsersInfosInfo(int ID);
        /// <summary>
        /// 用户修改
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        int UsersInfosUpdate(UsersInfos model);
        /// <summary>
        /// 用户查询
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        UsersInfosSearchResult UsersInfosSelete(UsersInfosSearch Search, GridPager Pager);


        /// <summary>
        ///  查询多个用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        List<UsersInfos> GetUserInfos(List<int> ids);

        /// <summary>
        /// 用户查询
        /// </summary>
        /// <param name="Search"></param>
        /// <param name="Pager"></param>
        /// <returns></returns>
        UsersInfosSearchResult UsersInfosSelete2(UsersInfosSearch Search, GridPager Pager);


        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int ChangePW(UsersInfos model);



        /// <summary>
        /// 更改用户禁用状态
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="isDisable"></param>
        /// <returns></returns>
        int UsersInfosUpdateDisable(int userid, bool isDisable);

        /// <summary>
        ///  userCode 用户详情
        /// </summary>
        /// <param name="userCode"></param>
        /// <returns></returns>
        UsersInfos UsersInfosInfo(string userCode);

        List<UsersInfos> UsersInfosByUserCode(string userCode);

        int UsersInfosCount(int positionID, bool isOfficial);
        List<UsersInfos> UsersInfosByPhone(string phone);

        #endregion

        #region 日志管理
        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int DocOperationLogCreate(DocOperationLog model);
        /// <summary>
        /// 日志查询
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        DocOperationLogResult DocOperationLogSearch(DocOperationLogSearch Search, GridPager Pager);
        #endregion

        #region 安全登录秘钥
        /// <summary>
        /// 获取秘钥信息
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        List<U_AppAuthor> GetAppAuthor(int appId);
        #endregion



        #region 短信组管理 

        /// <summary>
        /// 写入短信组用户信息（外部用户）
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int AddOutsideUserInfos(U_OutsideUserInfos info);

        /// <summary>
        /// 查询短信组用户信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        List<U_OutsideUserInfos> OutsideUserInfosSelect(OutsideUserInfoSearch Search, GridPager Pager, ref int totalRows);

        /// <summary>
        ///  移除短信组用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int OutsideUserInfosDelete(int id);


        /// <summary>
        /// 更新短信组用户信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        int OutsideUserInfosUpdate(U_OutsideUserInfos info);

        /// <summary>
        /// 查看短信组单条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        U_OutsideUserInfos OutsideUserInfosDetail(int id);


        /// <summary>
        ///  1.安全短信 2.火灾短信 3.联动短信
        /// </summary>
        /// <param name="selectType"></param>
        /// <returns></returns>
        List<U_OutsideUserInfos> OutsideUserInfosSelect(int selectType);

        #endregion


        #region 迭代或重构代码
        /// <summary>
        ///  重构登录方法 2018-06-26 解决登录速度过慢
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        LoginUserModel Login(string userCode);

        /// <summary>
        /// 根据权限ID 获取所有子级权限
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        List<PowerModel> GetPower(int roleID);
        #endregion


        /// <summary>
        /// 更新浏览总人数
        /// </summary>
        /// <returns></returns>
        void UpdateBrowsingTotal();


        /// <summary>
        /// 获取用户登陆缓存记录
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        LoginUserCache GetLoginUserCache(int userID);

        /// <summary>
        /// 增加或编辑用户登陆缓存记录
        /// </summary>
        /// <param name="info"></param>
        void AddOrUpdateLoginUserCache(LoginUserCache info);

    }
}

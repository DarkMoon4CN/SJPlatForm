﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ChuanYe.Commom
{
    public static class ValueConvert
    {
        public static string MD5(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            Byte[] dateToHash = (new System.Text.UnicodeEncoding()).GetBytes(str);
            byte[] hashvalue = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(dateToHash);
            str = BitConverter.ToString(hashvalue).Trim();
            string str1 = System.Text.Encoding.Default.GetString(hashvalue);
            return str;
        }



        public static DateTime StateTimeConvert(this object stateTime)
        {
            return Convert.ToDateTime(Convert.ToDateTime(stateTime).ToShortDateString() + " 00:00:00");
        }

        public static DateTime EndTimeConvert(this object endTime)
        {
            return Convert.ToDateTime(Convert.ToDateTime(endTime).ToShortDateString() + " 23:59:59");
        }

        public static DateTime MonthFristDay(this object dateTime)
        {
            DateTime dt = Convert.ToDateTime(dateTime);
            return Convert.ToDateTime(dt.AddDays(1 - dt.Day).ToShortDateString() + " 00:00:00");
        }

        public static DateTime MonthLastDay(this object dateTime)
        {
            DateTime dt = Convert.ToDateTime(dateTime);
            return Convert.ToDateTime(dt.AddDays(1 - dt.Day).AddMonths(1).AddDays(-1).ToShortDateString() + " 23:59:59");
        }

        public static DateTime TimeStampToDateTime(this object timeSpan)
        {
            long jsTimeStamp = Convert.ToInt64(timeSpan);
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
            DateTime dt = startTime.AddMilliseconds(jsTimeStamp);
            return dt;
        }

        public static long DateTimeToTimeStamp(this object dateTime)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
            long timeStamp = (long)(Convert.ToDateTime(dateTime) - startTime).TotalMilliseconds; // 相差毫秒数
            return timeStamp;
        }
    }
}

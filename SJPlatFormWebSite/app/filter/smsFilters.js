//格式化球员位置
angular.module("App").filter("sendFilter",['$sce',function($sce) {
	return function(val) {
		var pos = "Unknown";
		switch (val) {
			case true:
				pos = "<span class=\"btn btnSend\">已发送</span>";
				break;
			case false:
				pos = "<span class=\"btn btnUnsent\">未发送</span>";
				break;
			default:
				break;
		}
		return $sce.trustAsHtml(pos);
    }
}]);


angular.module("App").filter("infoDocTypeFilter",['$sce',function($sce) {
    return function(val) {
        var pos = "Unknown";
        switch (val) {
            case 1:
                pos = "信息";
                break;
            case 2:
                pos = "公文";
                break;
            default:
                break;
        }
        return $sce.trustAsHtml(pos);
    }
}])
    .filter("infoNewsTypeFilter",['$sce',function($sce) {
        return function(val) {
            var pos = "Unknown";
            switch (val) {
                case 1:
                    pos = "经验交流";
                    break;
                case 2:
                    pos = "防火信息";
                    break;
                case 3:
                    pos = "通知通报";
                    break;
                case 4:
                    pos = "专项行动";
                    break;
                case 5:
                    pos = "队伍建设";
                    break;
                case 6:
                    pos = "保险";
                    break;
                case 7:
                    pos = "领导讲话";
                    break;
                default:
                    break;
            }
            return $sce.trustAsHtml(pos);
        }
    }])
    .filter("infoIsAchievedFilter",['$sce',function($sce) {
        return function(val) {
            var pos = "Unknown";
            switch (val) {
                case true:
                    pos = "已发布";
                    break;
                case false:
                    pos = "未发布";
                    break;
                default:
                    break;
            }
            return $sce.trustAsHtml(pos);
        }
    }])
    .filter("mailstatusFilter",['$sce',function($sce) {
        return function(val) {
            var pos = "Unknown";
            switch (val) {
                case true:
                    pos = "已读";
                    break;
                case false:
                    pos = "未读";
                    break;
                default:
                    break;
            }
            return $sce.trustAsHtml(pos);
        }
    }])
    .filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });
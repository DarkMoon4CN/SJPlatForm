'use strict';
//定义模块app,注入路由依赖
angular.module("App", [
    'ngAnimate',
    'ct.ui.router.extras',
    'ui.bootstrap',
    'treeControl',
    'ngSanitize',
    'ngCookies',
    'ct.ui.router.extras',
    'oc.lazyLoad',
    'angular-loading-bar',
    'textAngular',
    'angular-icheck',
    'LocalStorageModule',
    'ngImgCrop',
    'ngVerify'
])
    .service('appConfig', ['$rootScope', function ($rootScope) {
        var appConfig = {
            host: '172.16.2.68',
            port: '80',
            appId: 'ceshi',
            rows: 10,
            maxSize: 3
        };
        $rootScope.appConfig = appConfig;
        return appConfig;
    }])
    .service('settings', ['$rootScope', function ($rootScope) {
        var settings = {
            //
        };
        $rootScope.settings = settings;
        return settings;
    }])
    .config(['$stateProvider', '$locationProvider', '$urlRouterProvider', '$provide', '$compileProvider', '$httpProvider', 'cfpLoadingBarProvider','localStorageServiceProvider', function ($stateProvider, $locationProvider, $urlRouterProvider, $provide, $compileProvider, $httpProvider, cfpLoadingBarProvider,localStorageServiceProvider) {

        //cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
        //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Loading...</div>';
        $provide.decorator('taTranslations', function ($delegate) {
            $delegate.heading.tooltip = '字号';
            $delegate.p.tooltip = '段落';
            $delegate.pre.tooltip = '格式';
            $delegate.indent.tooltip = '缩进';
            $delegate.outdent.tooltip = '不缩进';
            $delegate.html.tooltip = '标签/纯文本';
            $delegate.justifyLeft.tooltip = '左对齐';
            $delegate.justifyCenter.tooltip = '居中';
            $delegate.justifyRight.tooltip = '右对齐';
            $delegate.bold.tooltip = '加粗';
            $delegate.italic.tooltip = '斜体';
            $delegate.underline.tooltip = '下划线';
            $delegate.insertLink.tooltip = '插入链接';
            $delegate.insertLink.dialogPrompt = "输入地址";
            $delegate.insertImage.tooltip = '插入图片链接';
            $delegate.insertImage.dialogPrompt = '输入图片地址';
            $delegate.insertVideo.tooltip = '插入视频';
            $delegate.insertVideo.dialogPrompt = '输入youtube视频地址';
            $delegate.clear.tooltip = '清除格式';
            $delegate.charcount.tooltip = '字符数';
            $delegate.wordcount.tooltip = '词数';
            $delegate.redo.tooltip = '重做';
            $delegate.undo.tooltip = '撤回';
            $delegate.ol.tooltip = '有序列表';
            $delegate.ul.tooltip = '无序列表';

            $delegate.editLink.reLinkButton.tooltip = '重新链接';
            $delegate.editLink.unLinkButton.tooltip = '取消链接';
            $delegate.editLink.targetToggle.buttontext = '新窗口打开';
            return $delegate;
        });
        $provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$rootScope', 'taSelection', 'taToolFunctions',
            function (taRegisterTool, taOptions, $rootScope, taSelection, taToolFunctions) { // $delegate is the taOptions we are decorating

                taOptions.toolbar = [
                    ['bold', 'italics', 'underline', 'h2', 'h4', 'h6'],
                    ['ul', 'ol', 'justifyLeft', 'justifyCenter', 'justifyRight', 'pre', 'indent', 'outdent'],
                    ['html'],
                    ['redo', 'undo', 'clear']
                ];


                //taOptions.toolbar[2].push('uploadImage');

                return taOptions;
            }
        ]);
        $provide.decorator('ngClickDirective', ['$delegate', '$timeout', function ($delegate, $timeout) {
            var original = $delegate[0].compile;
            var delay = 500;
            $delegate[0].compile = function (element, attrs, transclude) {

                var disabled = false;

                function onClick(evt) {
                    if (disabled) {
                        evt.preventDefault();
                        evt.stopImmediatePropagation();
                    } else {
                        disabled = true;
                        $timeout(function () {
                            disabled = false;
                        }, delay, false);
                    }
                }

                //   scope.$on('$destroy', function () { iElement.off('click', onClick); });
                element.on('click', onClick);

                return original(element, attrs, transclude);
            };
            return $delegate;
        }]);

        localStorageServiceProvider.setPrefix('App')
        //localStorageServiceProvider.setStorageType('sessionStorage');


        //请求拦截器和过滤器
        $locationProvider.html5Mode({
            enable: true,
            requireBase: false
        });
        $httpProvider.interceptors.push('httpInterceptor');


        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);

        /* 使用when来对一些不合法的路由进行重定向 */
        $urlRouterProvider.when('', '/404');


        $stateProvider
        //登录
            .state('login', {
                url: "/login",
                templateUrl: 'views/public/login.html',
                controller: "loginCtrl",
                data: {
                    pageTitle: '登录',
                    path : ''
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'Login',
                            files: [
                                "app/controller/loginController.js",
                                "app/service/loginService.js",
                                "app/service/cookieService.js"
                            ]
                        }
                        ]);
                    }]
                }
            })

            //-------------------短信//
            .state('sms', {
                abstract: true,
                url: "/sms",
                templateUrl: 'views/public/layout.html',
                data: {
                    pageTitle: '短信'
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'App',
                            files: [
                                "app/controller/smsController.js",
                                "app/controller/componentController.js",
                                "app/service/commonService.js"
                            ]
                        }
                        ]);
                    }],
                    menu : ['auth','cookie',function(auth,cookie){
                        return auth.subMenu("sms",cookie.getMenus());
                    }]
                }
            })
            .state('sms.add', {
                url: "/add",
                /*templateUrl: 'views/sms/add.html',
                controller: "smsAddCtrl",*/
                views: {
                    '':
                        {
                            controller: 'smsAddCtrl',
                            templateUrl: 'views/sms/add.html'
                        },
                    'tab':
                        {
                            controller: 'smsTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '短信添加'
                }
            })
            .state('sms.list', {
                url: "/list",
                /*templateUrl: 'views/sms/list.html',
                controller: "smsListCtrl",*/
                views: {
                    '':
                        {
                            controller: 'smsListCtrl',
                            templateUrl: 'views/sms/list.html'
                        },
                    'tab':
                        {
                            controller: 'smsTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '短信列表'
                }
            })
            .state('sms.template', {
                url: "/template",
                /*templateUrl: 'views/sms/smsTemplate.html',
                controller: "smsTemplateListCtrl",*/
                views: {
                    '':
                        {
                            controller: 'smsTemplateListCtrl',
                            templateUrl: 'views/sms/smsTemplate.html'
                        },
                    'tab':
                        {
                            controller: 'smsTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '短信模版'
                }
            })

            //-------------------平安报送//
            .state('safe', {
                abstract: true,
                url: "/safe",
                templateUrl: 'views/public/layout.html',
                data: {
                    pageTitle: '平安'
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'App',
                            files: [
                                "app/controller/safeController.js",
                                "app/service/safeService.js"

                            ]
                        }
                        ]);
                    }],
                    menu : ['auth','cookie',function(auth,cookie){
                        return auth.subMenu("safe",cookie.getMenus());
                    }]
                }
            })
            .state('safe.safetyinfo', {
                url: "/safetyinfo",
               /* templateUrl: 'views/safe/safetyinfo.html',
                controller: "safetyInfoCtrl",*/
                views: {
                    '':
                        {
                            controller: 'safetyInfoCtrl',
                            templateUrl: 'views/safe/safetyinfo.html'
                        },
                    'tab':
                        {
                            controller: 'safeTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '平安报送'
                }
            })
            .state('safe.history', {
                url: "/history",
                /*templateUrl: 'views/safe/history.html',
                controller: "smsHistoryCtrl",*/
                views: {
                    '':
                        {
                            controller: 'smsHistoryCtrl',
                            templateUrl: 'views/safe/history.html'
                        },
                    'tab':
                        {
                            controller: 'safeTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '历史短信'
                }
            })
            .state('safe.send', {
                url: "/send",
                /*templateUrl: 'views/safe/send.html',
                controller: "safeSendCtrl",*/
                views: {
                    '':
                        {
                            controller: 'safeSendCtrl',
                            templateUrl: 'views/safe/send.html'
                        },
                    'tab':
                        {
                            controller: 'safeTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '短信发送'
                }
            })
            .state('safe.statis', {
                url: "/statis",
                /*templateUrl: 'views/safe/statis.html',
                controller: "safeStatisCtrl",*/
                views: {
                    '':
                        {
                            controller: 'safeStatisCtrl',
                            templateUrl: 'views/safe/statis.html'
                        },
                    'tab':
                        {
                            controller: 'safeTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '报送统计'
                }
            })
            //-------------------信息管理//
            .state('info', {
                abstract: true,
                url: "/info",
                templateUrl: 'views/public/layout.html',
                data: {
                    pageTitle: '信息'
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'App',
                            files: [
                                "app/controller/infoController.js",
                                "app/service/infoService.js",
                                "app/filter/commonFilters.js",
                                "app/service/commonService.js",
                                "app/controller/componentController.js"
                            ]
                        }
                        ]);
                    }],
                    menu : ['auth','cookie',function(auth,cookie){
                        return auth.subMenu("info",cookie.getMenus());

                    }]
                }
            })
            .state('info.done', {
                url: "/done",
               // templateUrl: 'views/info/done.html',
               // controller: "doneInfoCtrl",
                views: {
                    '':
                        {
                            controller: 'doneInfoCtrl',
                            templateUrl: 'views/info/done.html'
                        },
                    'tab':
                        {
                            controller: 'infoTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '已发布新闻列表'
                }
            })
            .state('info.statis', {
                url: "/statis",
                /*templateUrl: 'views/info/statis.html',
                controller: "statisInfoCtrl",*/
                views: {
                    '':
                        {
                            controller: 'statisInfoCtrl',
                            templateUrl: 'views/info/statis.html'
                        },
                    'tab':
                        {
                            controller: 'infoTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '新闻统计'
                }
            })
            .state('info.release', {
                url: "/release",
                /*templateUrl: 'views/info/release.html',
                controller: "releaseInfoCtrl",*/
                views: {
                    '':
                        {
                            controller: 'releaseInfoCtrl',
                            templateUrl: 'views/info/release.html'
                        },
                    'tab':
                        {
                            controller: 'infoTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '发布新闻'
                }
            })
            .state('info.upload', {
                url: "/upload",
                /*templateUrl: 'views/info/upload.html',
                controller: "PreviewInfoCtrl",*/
                views: {
                    '':
                        {
                            controller: 'PreviewInfoCtrl',
                            templateUrl: 'views/info/upload.html'
                        },
                    'tab':
                        {
                            controller: 'infoTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '信息上传'
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'ngFileUpload',
                            files: [
                                "static/js/ng-file-upload/dist/ng-file-upload-shim.min.js",
                                "static/js/ng-file-upload/dist/ng-file-upload.min.js"
                            ]
                        }
                        ]);
                    }]
                }
            })
            .state('info.prevsend', {
                url: "/prevsend",
                /*templateUrl: 'views/info/preview.html',
                controller: "prevSendInfoCtrl",*/
                views: {
                    '':
                        {
                            controller: 'prevSendInfoCtrl',
                            templateUrl: 'views/info/preview.html'
                        },
                    'tab':
                        {
                            controller: 'infoTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                data: {
                    pageTitle: '信息预览发布'
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'ngFileUpload',
                            files: [
                                "static/js/ng-file-upload/dist/ng-file-upload-shim.min.js",
                                "static/js/ng-file-upload/dist/ng-file-upload.min.js"
                            ]
                        }
                        ]);
                    }]
                }
            })
            .state('info.view', {
                url: "/{ID:[0-9]{1,8}}",   // '/result/:id/:number',//需要传的参数的键名
                templateUrl: 'views/info/view.html',
                controller: "contentInfoCtrl",
                data: {
                    pageTitle: '查看内容'
                }
            })
            .state('info.send', {
                url: "/send/{ID:[0-9]{1,8}}",   // '/result/:id/:number',//需要传的参数的键名
                templateUrl: 'views/info/presend.html',
                controller: "SendInfoCtrl",
                data: {
                    pageTitle: '发送新闻'
                }
            })
            .state('info.detailed', {
                url: '/detailed/{ID:[0-9]{1,8}}',   ///contacts/{contactId:[0-9]{1,8}}"
                templateUrl: 'views/info/detailed.html',
                controller: "detailedInfoCtrl",
                data: {
                    pageTitle: '类别信息统计'
                }
            })

            //-----------邮件管理
            .state('mail', {
                abstract: true,
                url: "/mail",
                templateUrl: 'views/public/layout.html',
                data: {
                    pageTitle: '邮件',
                    path : ''
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'App',
                            files: [
                                "app/controller/mailController.js",
                                "app/service/mailService.js",
                                "app/service/commonService.js",
                                "app/filter/commonFilters.js",
                                "app/controller/componentController.js"
                            ]
                        }
                        ]);
                    }],
                    menu : ['auth','cookie',function(auth,cookie){
                         return auth.subMenu("mail",cookie.getMenus());

                    }]
                }
            })
            .state('mail.send', {
                url: "/send",
                views: {
                    '':
                        {
                            controller: 'sendMailCtrl',
                            templateUrl: 'views/mail/send.html'
                        },
                    'tab':
                        {
                            controller: 'mailTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                deepStateRedirect: true,
                sticky: true,
                data: {
                    pageTitle: '发送邮件',
                    path : 'mail.send'
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'ngFileUpload',
                            files: [
                                "static/js/ng-file-upload/dist/ng-file-upload-shim.min.js",
                                "static/js/ng-file-upload/dist/ng-file-upload.min.js"
                            ]
                        }
                        ]);
                    }]
                }
            })
            .state('mail.receive', {
                url: "/receive",
                views: {
                    '':
                        {
                            controller: 'receiveMailCtrl',
                            templateUrl: 'views/mail/receive.html'
                        },
                    'tab':
                        {
                            controller: 'mailTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                deepStateRedirect: true,
                sticky: true,
                data: {
                    pageTitle: '接收邮件',
                    path : 'mail.receive'
                }
            })
            .state('mail.public', {
                url: "/public",
                views: {
                    '':
                        {
                            controller: 'publicMailCtrl',
                            templateUrl: 'views/mail/public.html'
                        },
                    'tab':
                        {
                            controller: 'mailTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                deepStateRedirect: true,
                sticky: true,
                data: {
                    pageTitle: '公共邮件',
                    path : ''
                }
            })
            .state('mail.completed', {
                url: "/completed",
                views: {
                    '':
                        {
                            controller: 'completedMailCtrl',
                            templateUrl: 'views/mail/completed.html'
                        },
                    'tab':
                        {
                            controller: 'mailTabCtrl',
                            templateUrl: 'views/public/tab.html'
                        }
                },
                deepStateRedirect: true,
                sticky: true,
                data: {
                    pageTitle: '已发送邮件',
                    path : 'mail.completed'
                }
            })
            .state('mail.cha', {
                url: "/{ID:[0-9]{1,8}}",
                templateUrl: 'views/mail/cha.html',
                controller: "chaMailCtrl",
                data: {
                    pageTitle: '查看邮件详情',
                    path : 'mail.cha'
                }
            })
            .state('mail.ceshi', {
                url: "/ceshi",
                templateUrl: 'views/mail/ceshi.html',
                controller: "ceshiCtrl",
                data: {
                    pageTitle: 'ceshi',
                    path : 'mail.ceshi'
                }
            })


    }])


    .run(['$rootScope', 'settings', '$state', '$window', '$timeout','$log', 'common','auth', function ($rootScope, settings, $state, $window, $timeout, $log,common,auth) {
        $rootScope.$state = $state;
        //设置
        //路由开始切换
        /**
         * args[0]: 事件
         * args[1]: 要切换的路由
         * args[2]: 第一次进入该方法,没有当前路由,为undefined
         */
        $rootScope.$on('$stateChangeSuccess', function (event, next, current) {
            if (next.name!="login"&&!auth.isAccessUrl(next.name)) {
                alert("您没有权限访问当前页面！");
            }
        });


        //当$location.path发生变化或者$location.url发生变化时触发
        $rootScope.$on('$locationChangeStart', function (event, msg) {
           // console.log(event);
        });

        //当且仅当path或url变化成功后触发
        $rootScope.$on('$locationChangeSuccess', function (event, msg) {
          // console.log(event);
        });

    }])



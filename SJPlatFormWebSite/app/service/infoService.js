angular.module("App")
    .service("infoService",['apiService',function(apiService) {

        //获取所属部门
        this.getDeptlist = function(params, config) {
            var api = "/actionapi/Sms/GetAllDepartments";
            return apiService.apiPost(api, params, config);
        };

        //获取发布新闻列表
        this.getReleaseList = function(params, config) {
            var api = "/actionapi/News/NewsTempGetList";
            return apiService.apiPost(api, params, config);
        };

        //获取已发布新闻列表
        this.getDoneList = function(params, config) {
            var api = "/actionapi/News/NewsInfsPublicGetList";
            return apiService.apiPost(api, params, config);
        };

        //获取信息统计
        this.getStaticList = function(params, config) {
            var api = "/actionapi/News/NewsInfsStatisticsGet";
            return apiService.apiPost(api, params, config);
        };

        //获取部门信息统计
        this.getDeptstaticList = function(params, config) {
            var api = "/actionapi/News/NewsInfsGetListByDepId";
            return apiService.apiPost(api, params, config);
        };

        //获取所有部门统计
        this.getDeptstaticCount = function(params, config) {
            var api = "/actionapi/News/NewsInfsStatisticsByDepIdGet";
            return apiService.apiPost(api, params, config);
        };

        //获取文章详情
        this.getArticleContent = function(params, config) {
            var api = "/actionapi/News/NewsTempGetByID";
            return apiService.apiPost(api, params, config);
        };

        //发布新闻附件
        this.sendNews = function(params, config) {
            var api = "/actionapi/News/NewsTempCreate";
            return apiService.apiPost(api, params, config);
        };



    }])



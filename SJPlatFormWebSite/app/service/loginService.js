angular.module("App")
    .service("loginService",['apiService','cookie',function(apiService,cookie) {

        //登录
        this.loginIn = function(params, config) {
            var api = "/actionapi/Login/LoginIn";
            return apiService.apiPost(api, params, config);
        };



        //保存用户id
        this.setUserid = function (userid) {
            cookie.setUserid(userid);
        }

        //保存时间戳
        this.setTimestamp = function (timestamp) {
            cookie.setTimestamp(timestamp);
        }

        //生成token
        this.generateToken = function (userid,timestamp) {
            return cookie.jm(userid,timestamp);
        }

        //销毁token
        this.removeUser = function () {
            cookie.removeUser();
        }

    }])
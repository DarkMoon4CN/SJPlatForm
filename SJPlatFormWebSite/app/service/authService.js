'use strict';
angular.module('App')
    .service('auth', ['apiService','cookie',
        function (apiService,cookie) {

            var urlPermissions = cookie.getMenus() || {};

            function convertState(state) {
                return state.replace(".", "\\\.").replace("*", ".*");
            }

            //判断权限
            this.isAccessUrl = function(url) {
                var urlPermissions = [];
                var menus = cookie.getMenus() || {};
                if(!angular.equals({}, menus)){
                    angular.forEach(menus, function (item,index) {
                        angular.forEach(item.ChildPowerList, function (subitem,index) {
                            urlPermissions.push(subitem.Path);
                        });
                    });
                }
                if(urlPermissions.indexOf(url) > -1 ){
                       return true;
                }
                return false;
            }

            //设置一级菜单
            this.topMenu = function (data) {
                var menus = [];
                var topmenu = {};
                var keepGoing = true;
                angular.forEach(data, function (item) {
                    angular.forEach(item.ChildPowerList, function (subitem,index) {
                        if(index == 0){
                            topmenu = {
                                itemSort : item.Sort,
                                itemName : item.ShowName,
                                itemPath : subitem.Path
                            }
                            menus.push(topmenu);
                            keepGoing = false;
                        }
                    });

                });
                cookie.setTopMenu(menus);

            }


            this.subMenu = function (menuUrl,data) {
                var keepGoing = true;
                var submenus = {};
                angular.forEach(data, function (item) {
                    if(keepGoing){
                        if(item.Path == menuUrl){
                            submenus =  item.ChildPowerList;
                            keepGoing = false;
                        }
                    }

                });
                console.log(submenus);

                return submenus;
            }


        }]);
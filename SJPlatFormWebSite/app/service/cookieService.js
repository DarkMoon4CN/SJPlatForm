'use strict';
angular.module("App")
    .service("cookie", ['$rootScope', '$cookies', 'utils', 'localStorageService', function ($rootScope, $cookies, utils, localStorageService) {
        var userKey = "appUser";
        this.expires = function () {
            var q_today = new Date();
            var q_expiresValue = new Date(q_today);
            return q_expiresValue.setMinutes(q_today.getSeconds() + 43200);
        }

        this.getObj = function (name) {
            return $cookies.getObject(name);
        };
        this.removeObj = function (name) {
            $cookies.remove(name);
        };
        this.setObj = function (name, obj) {
            $cookies.putObject(name, obj);
        };

        //保存用户id
        this.setUserid = function (userid) {
            this.setObj('userid', userid);
        }

        //保存用户id
        this.setUserName = function (username) {
            this.setObj('username', username);
        }

        //保存时间轴
        this.setTimestamp = function (timestamp) {
            this.setObj('timestamp', timestamp);
        }

        //生成jm
        this.jm = function (userid, timestamp) {
            var jmxx = utils.jm([userid, timestamp, 4]);
            this.setObj(userKey, jmxx);
            return true;
        }

        //获取token
        this.getjm = function () {
            return this.getObj(userKey);
        }

        this.getUserid = function () {
            return this.getObj('userid');
        }

        //获取用户名称
        this.getUserName = function () {
            return this.getObj('username');
        }

        this.getTimestamp = function () {
            return this.getObj('timestamp');
        }

        //判断用户状态
        this.initUser = function () {
            if (this.getObj(userKey)) {
                return this.getObj(userKey);
            } else {
                return null;
            }
        }

        //删除token信息
        this.removeUser = function () {
            this.removeObj(userKey);
        }

        //删除所有存储
        this.removeAll = function () {
            this.removeObj("appUser");
            this.removeObj("userid");
            this.removeObj("timestamp");
            this.removeObj("timespan");
            this.removeObj("username");
            localStorageService.clearAll();

        }

        //获取cookie
        this.getTimeCookie = function () {
            return $cookies.get("timespan");
        }

        //设置cookie
        this.setCookie = function (timespan) {
            var today = new Date();
            var expiresValue = new Date(today);
            //Set 'expires' option in 1 minute
            expiresValue.setMinutes(today.getMinutes() + 4);

            /* //Set 'expires' option in 2 hours
             expiresValue.setMinutes(today.getMinutes() + 120);

             //Set 'expires' option in (60 x 60) seconds = 1 hour
             expiresValue.setSeconds(today.getSeconds() + 3600);

             //Set 'expires' option in (12 x 60 x 60) = 43200 seconds = 12 hours
             expiresValue.setSeconds(today.getSeconds() + 43200);*/

            $cookies.put("timespan", timespan, {expires: expiresValue});
        }

        //获取顶级菜单
        this.getTopMenu = function () {
            return localStorageService.get("topmenu") || "";
        }

        //设置顶级菜单
        this.setTopMenu = function (topmenu) {
            localStorageService.remove('topmenu');
            localStorageService.set('topmenu', topmenu);
        }

        //设置顶级菜单
        this.setMenus = function (menus) {
            localStorageService.remove('menus');
            localStorageService.set('menus', menus);
        }

        //获取菜单集合
        this.getMenus = function () {
            return localStorageService.get("menus") || "";
        }


    }])
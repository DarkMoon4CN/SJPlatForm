'use strict';
angular.module("App")
    .service("storage",['$rootScope','localStorageService','utils',function($rootScope,localStorageService,utils) {
        var userKey = "appUser";
        this.getObj = function(name) {
            return localStorageService.get(name);
        };
        this.removeObj=function(name){
            localStorageService.remove(name);
        };
        this.setObj = function(name, obj) {
            localStorageService.set(name, obj);
        };

        //保存用户id
        this.setUserid = function (userid) {
            //utils.md5(userid);
            this.setObj('userid',userid);
        }

        //保存时间轴
        this.setTimestamp = function (timestamp) {
            this.setObj('timestamp',timestamp);
        }

        //生成jm
        this.jm = function (userid,timestamp) {
           var jmxx =  utils.jm([userid,timestamp,5]);
           this.setObj(userKey,jmxx);
           return true;
        }

        //获取token
        this.getjm = function () {
            return this.getObj(userKey);
        }

        this.getUserid = function () {
            return this.getObj('userid');
        }

        this.getTimestamp = function () {
            return this.getObj('timestamp');
        }

        //判断用户状态
        this.initUser = function() {
            if(this.getObj(userKey)) {
                return this.getObj(userKey);
            } else {
                return null;
            }
        }

        //删除用户信息
        this.removeUser = function () {
            this.removeObj(userKey);

        }



    }])
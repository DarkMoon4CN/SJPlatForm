'use strict';
angular.module('App')
    .service('common', ['$rootScope', '$state',
         '$q', '$previousState', 'cookie', '$http', '$document','$uibModal', 'cfpLoadingBar', 'utils', 'appConfig','apiService',
        function ($rootScope, $state, $q, $previousState, cookie, $http, $document,$uibModal,cfpLoadingBar, utils, appConfig,apiService) {

            $rootScope.currentTime = new Date();
            this.domain = "http://" + appConfig.host + ":" + appConfig.port;
            this.serverUrl = this.domain + "/SJPlatFormWebTest";
            //获取主菜单
            $rootScope.getTopMenu = cookie.getTopMenu();
            //获取日期时间
            $rootScope.getTimeDate = utils.timeDate();
            //获取用户名
            $rootScope.UserName = cookie.getUserName();

            //全局成功
            $rootScope.tips = {
                message: [],
                showSuccess: function (message, duration) {
                    this.message.push({
                        text: message || '成功',
                        duration: duration || 2000,
                        type: 'success'
                    });
                },
                showError: function (message, duration) {
                    this.message.push({
                        text: message || '失败',
                        duration: duration || 20000,
                        type: 'danger'
                    });
                },
                close: function (index) {
                    this.message.splice(index, 1);
                }
            };

            //正在加载提示
            $rootScope.showLoading = function () {
                cfpLoadingBar.start();
            };
            $rootScope.hideLoading = function () {
                cfpLoadingBar.complete();
            };


            //模式窗口
            $rootScope.openContentModal = function (modalName, size, data, e) {
                if (e) {
                    e.stopPropagation();
                }

                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'views/component/' + modalName + '.html',
                    controller: modalName + 'Ctrl',
                    controllerAs: '$scope',
                    //appendTo:angular.element('.smsDel').eq(0), //模态框窗口弹出的位置
                    // appendTo:angular.element($document[0].querySelector('.smsDel')), //模态框窗口弹出的位置
                    size: angular.isDefined(size) ? size : 'center',
                    resolve: {
                        modalParams: function () {
                            return data;
                        }
                    }
                });
                return modalInstance;
            };

            //确认框
            $rootScope.showConfirm = function (tips) {
                var deferred = $q.defer();
                var confirmModal = $rootScope.openContentModal("confirmModal", 'sm', tips);
                confirmModal.result.then(function () {
                    deferred.resolve();
                });
                return deferred.promise;
            };



            //退出系统
            $rootScope.logout = function (e) {
                apiService.loginOut(
                    {}
                ).then(function (data) {
                    cookie.removeAll();             //删除本地存储
                    $state.go("login");
                }, function (error) {
                    alert("Fail: " + error);
                })
            }


        }])
    //==============================================
    // BOOTSTRAP GROWL
    //==============================================

    .service('growlService', function () {
        var gs = {};
        gs.growl = function (message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: true,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'center'
                },
                delay: 2500,
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                offset: {
                    x: 0,
                    y: 200
                }
            });
        }

        return gs;
    })



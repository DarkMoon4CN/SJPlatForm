angular.module("App")
    .service("safeService",['apiService',function(apiService) {

        //获取平安报送
        this.getSafetylist = function(params, config) {
            var api = "/actionapi/SafetyInfo/GetSafetyInfoListByDay";
            return apiService.apiPost(api, params, config);
        };

        //获取联系人
        this.getReceiver = function(params, config) {
            var api = "/actionapi/SafetyInfo/GetLeader";
            return apiService.apiPost(api, params, config);
        };

        //获取历史短信
        this.getHistory = function(params, config) {
            var api = "/actionapi/SafetyInfo/GetLastSafetyInfo";
            return apiService.apiPost(api, params, config);
        };

        //获取报送统计
        this.getStatis = function(params, config) {
            var api = "/actionapi/SafetyInfo/GetSafetyInfoStatistics";
            return apiService.apiPost(api, params, config);
        };

        //发送短信
        this.sendSms = function(params, config) {
            var api = "/actionapi/SafetyInfo/CreateSafetyInfo";
            return apiService.apiPost(api, params, config);
        };

    }])
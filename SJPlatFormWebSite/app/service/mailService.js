angular.module("App")
    .service("mailService",['apiService',function(apiService) {

        //接收邮件列表
        this.receiveMailList = function(params, config) {
            var api = "/actionapi/Mail/ReceiveMailGetList";
            return apiService.apiPost(api, params, config);
        };

        //公共邮件列表
        this.publicMailList = function(params, config) {
            var api = "/actionapi/Mail/PublicMailGetList";
            return apiService.apiPost(api, params, config);
        };

        //已发邮件列表
        this.completedMailList = function(params, config) {
            var api = "/actionapi/Mail/MailSendGetList";
            return apiService.apiPost(api, params, config);
        };

        //已发邮件列表
        this.completedMailList = function(params, config) {
            var api = "/actionapi/Mail/MailSendGetList";
            return apiService.apiPost(api, params, config);
        };

        //批量删除
        this.batchDelete = function(params, config) {
            var api = "/actionapi/Mail/InfosCommDeleteSome";
            return apiService.apiPost(api, params, config);
        };

        //删除
        this.delete = function(params, config) {
            var api = "/actionapi/Mail/InfosCommDelete";
            return apiService.apiPost(api, params, config);
        };

        //全选
        this.selectAll  = function (checked,data) {
            var arr = [];
            if (checked) {
                angular.forEach(data, function (i) {
                    i.checked = true;
                    arr.push(i.ID);
                })
            } else {
                angular.forEach(data, function (i) {
                    i.checked = false;
                })
            }
            return arr;
        }

        //查邮件
        this.cha = function(params, config) {
            var api = "/actionapi/Mail/InfosCommGet";
            return apiService.apiPost(api, params, config);
        };

        //邮件状态改变
        this.changeStatus = function(params, config) {
            var api = "/actionapi/Mail/InfosCommUpdate";
            return apiService.apiPost(api, params, config);
        };

        //
        this.sendMail = function(params, config) {
            var api = "/actionapi/Mail/CreateInfosComm";
            return apiService.apiPost(api, params, config);
        };



    }])
angular.module("App")
    .service("smsService",['apiService',function(apiService) {

        //批量删除数据
        this.batchDelete = function(params, config) {
            var api = "/actionapi/Sms/Sms_SendInfoDelete";
            return apiService.apiPost(api, params, config);
        };

        //获取短信信息
        this.getSms = function(params, config) {
            var api = "/actionapi/Sms/GetSms_SendInfo";
            return apiService.apiPost(api, params, config);
        };

        //删除短信
        this.deleteOne = function(params, config) {
            var api = "/actionapi/Sms/SMS_ModelDelete";
            return apiService.apiPost(api, params, config);
        }

        //获取短信模版
        this.getSmsTemplates = function(params, config) {
            var api = "/actionapi/Sms/GetSMS_Model";
            return apiService.apiPost(api, params, config);
        }

        //获取通信录数据
        this.getSmsPhone = function(params, config) {
            var api = "/actionapi/Sms/GetSmsPhone";
            return apiService.apiPost(api, params, config);
        }

        //发送短信
        this.sendInfo = function(params, config) {
            var api = "/actionapi/Sms/CreateSms_SendInfo";
            return apiService.apiPost(api, params, config);
        }

        //获取短信模版
        this.getSmstemplate = function(params, config) {
            var api = "/actionapi/Sms/SearchSMS_Model";
            return apiService.apiPost(api, params, config);
        }


        //添加短信模版
        this.addTemplate= function(params, config) {
            var api = "/actionapi/Sms/CreateOrUpdateSMS_Model";
            return apiService.apiPost(api, params, config);
        }

        //删除模板
        this.delTemplate= function(params, config) {
            var api = "/actionapi/Sms/SMS_ModelDelete";
            return apiService.apiPost(api, params, config);
        }

        //编辑模板内容
        this.editTemplate= function(params, config) {
            var api = "/actionapi/Sms/GetSMS_ModelByID";
            return apiService.apiPost(api, params, config);
        }

    }])
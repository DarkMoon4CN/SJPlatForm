'use strict';
angular.module('App')
    .controller('confirmModalCtrl', ['$uibModalInstance', '$scope', 'apiService',
        'modalParams', '$rootScope',
        function($uibModalInstance, $scope, apiService, modalParams, $rootScope) {

            $scope.tips = modalParams;
            $scope.ok = function() {
                $uibModalInstance.close();
            };

            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };

        }
    ])
    .controller('imgcutModalCtrl', ['$uibModalInstance', '$scope', 'apiService',
        'modalParams', '$rootScope',
        function($uibModalInstance, $scope, apiService, modalParams, $rootScope) {


            $scope.params = angular.copy(modalParams);
            console.log($scope.params);
            $scope.fileName = "";
            var files = [];

            var dataURItoBlob = function(dataurl) {
                var arr = dataurl.split(','),
                    mime = arr[0].match(/:(.*?);/)[1],
                    bstr = atob(arr[1]),
                    n = bstr.length,
                    u8arr = new Uint8Array(n);
                while(n--) {
                    u8arr[n] = bstr.charCodeAt(n);
                }
                var blob = new Blob([u8arr], {
                    type: mime
                });
                return new File([blob], "user-icon.png");
            };
            $scope.ok = function() {
                var blob = dataURItoBlob($scope.croppedImage);
                files.push($scope.croppedImage);
                files.push(blob);
                $uibModalInstance.close(files);

            };

            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.myImage = $scope.params || '';
            $scope.croppedImage = '';

            var handleFileSelect = function(evt) {
                var file = evt.currentTarget.files[0];

                $scope.fileName = file.name;
                var reader = new FileReader();
                reader.onload = function(evt) {
                    $scope.$apply(function($scope) {
                        $scope.myImage = evt.target.result;
                    });
                };
                reader.readAsDataURL(file);
            };
            $uibModalInstance.rendered.then(function() {
                angular.element(document.querySelector('#uploadIconInput')).on('change', handleFileSelect);
            });



        }
    ]).controller('uploadAttachModalCtrl', ['$uibModalInstance', '$scope', 'apiService',
    'modalParams', '$rootScope',
    function($uibModalInstance, $scope, apiService, modalParams, $rootScope) {
        $scope.fileInfo = {};
        $scope.ok = function() {
            $uibModalInstance.close($scope.fileInfo.file);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


    }
])
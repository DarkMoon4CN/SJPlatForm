angular.module("App")
    .controller('loginCtrl', ['$scope', '$rootScope','$location','$timeout','loginService','utils','ngVerify','cookie','auth', function ($scope,$rootScope, $location,$timeout,loginService,utils,ngVerify,cookie,auth) {

        angular.element(document).ready(function() {
            //页面载入后,设置验证码
            $scope.yzm = utils.createCode();
        });



        //刷新验证码
        $scope.changeYzm = function () {
            $scope.yzm = utils.createCode();
        }

        //登录
        $scope.submitForm = function () {
            if($scope.vercode  == $scope.yzm){
                $scope.beginLoading=true;
                loginService.loginIn({
                    UserCode : $scope.loginUser,
                    PassWord : $scope.loginPassword
                })
                    .then(function (data) {

                      // console.log(data);

                        if (!angular.equals(null, data.Token)) {
                            cookie.setUserid(data.Token);                               //cookie 用户id
                            cookie.setUserName(data.UserName);                          //获取username
                            cookie.setCookie(data.TimeStamp);                           //cookie 用户时间戳
                            cookie.setMenus(data.Powers[0].ChildPowerList);             //local 所有菜单
                            auth.topMenu(data.Powers[0].ChildPowerList);                //设置顶级菜单
                            $rootScope.getTopMenu = cookie.getTopMenu();
                            $rootScope.UserName = cookie.getUserName();
                            if(loginService.generateToken(data.Token,data.TimeStamp)){
                                $location.path('/mail/send');
                            }
                        }else{
                            $rootScope.tips.showError("帐号或密码错误");
                        }

                    });

            }else{
                $rootScope.tips.showError("验证码错误");
            }

        }

    }]);
'use strict';
angular.module("App")
    .controller('infoTabCtrl',['$scope','menu',function($scope,menu){
        //设置二级菜单
        $scope.subMenu = menu;

    }])
    .controller('doneInfoCtrl', ['$scope', 'infoService', function ($scope, infoService) {
        //初始化
        $scope.done = [];
        $scope.rows = 10;

        //获取单位
        infoService.getDeptlist()
            .then(function (data) {
                if (!data.errCode) {
                    $scope.deptList = data;
                    console.log(data);

                }
            });

        $scope.Inital = function () {
            infoService.getDoneList({
                stateTime: "",
                endTime: "",
                NewsType: 0,  //新闻类型
                DepId: 0,     //部门ID
                page: 1,
                rows: $scope.rows
            })
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.doneList = data.NewsInfsList;
                        $scope.totalItems = data.totalRows;
                        console.log(data);
                    }
                });
        }();


        //搜索按钮
        $scope.search = function () {
            infoService.getDoneList({
                stateTime: $scope.done.startTime || "",
                endTime: $scope.done.endTime || "",
                NewsType: $scope.done.NewsType || 0,  //新闻类型
                DepId: $scope.done.dept || 0,  //部门ID
                page: 1,
                rows: $scope.rows
            })
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.doneList = data.NewsInfsList;
                        $scope.totalItems = data.totalRows;
                        console.log(data);
                    }
                });
        }

        //页面改变触发
        $scope.pageChanged = function () {
            infoService.getDoneList({
                stateTime: $scope.done.startTime || "",
                endTime: $scope.done.endTime || "",
                NewsType: $scope.done.NewsType || 0,  //新闻类型
                DepId: $scope.done.dept || 0,  //部门ID
                page: $scope.currentPage,
                rows: $scope.rows
            })
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.doneList = data.NewsInfsList;
                        $scope.totalItems = data.totalRows;
                        console.log(data);
                    }
                });
        }

    }])
    .controller('statisInfoCtrl', ['$scope', 'infoService', function ($scope, infoService) {
        $scope.rows = 10;
        $scope.info = {
            initStartDate: "2013-11-01",
            initEndDate: "2017-11-01",
            startDate: "",
            endDate: ""
        }

        $scope.getStaticList = function (info) {
            infoService.getStaticList(info)
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.staticList = data.NewsInfsList;
                        $scope.totalItems = data.totalRows;
                        console.log(data);
                    }
                });
        }


        //初始化
        $scope.Inital = function () {
            $scope.getStaticList({
                stateTime: $scope.info.initStartDate || "",
                endTime: $scope.info.initEndDate || "",
                page: 1,
                rows: $scope.rows
            });
        }();

        //搜索按钮
        $scope.search = function () {
            $scope.getStaticList({
                stateTime: $scope.info.startDate || $scope.info.initStartDate,
                endTime: $scope.info.endDate || $scope.info.initEndDate,
                page: 1,
                rows: $scope.rows
            });
        }


        //页面改变触发
        $scope.pageChanged = function () {
            $scope.getStaticList({
                stateTime: $scope.info.startDate || $scope.info.initStartDate,
                endTime: $scope.info.endDate || $scope.info.initEndDate,
                page: $scope.currentPage,
                rows: $scope.rows
            });
        }


    }])
    .controller('releaseInfoCtrl', ['$scope', 'infoService', function ($scope, infoService) {
        $scope.info = [];
        $scope.infoList = [];
        $scope.deptList = [];
        $scope.rows = 10;

        //获取单位
        infoService.getDeptlist()
            .then(function (data) {
                if (!data.errCode) {
                    $scope.deptList = data;
                    // $scope.dept = $scope.deptList[0];
                    console.log(data);

                }
            });

        //初始化
        //开始时间：stateTime，结束时间：endTime，部门ID：DepId，类型：DocType，是否发布：IsChecked，页数：page，行数：rows
        $scope.Inital = function () {
            infoService.getReleaseList({
                stateTime: "",
                endTime: "",
                DepId: 0,
                DocType: 0,  // 0代表全部 信息类别：1,信息；2，公文
                IsChecked: 2,  //2 代表全部  1 代表 已发布  0 代表 未发布
                page: 1,
                rows: $scope.rows
            })
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.infoList = data.NewsTempList;
                        $scope.totalItems = data.totalRows;
                        console.log(data);
                    }
                });
        }();

        //搜索按钮
        $scope.search = function () {
            console.log($scope.info);
            infoService.getReleaseList({
                stateTime: $scope.info.startTime,
                endTime: $scope.info.endTime,
                DepId: $scope.info.dept || 0,
                DocType: $scope.info.DocType || 0,  // 0代表全部 信息类别：1,信息；2，公文
                IsChecked: $scope.info.IsAchieved || 2,  //2 代表全部  1 代表 已发布  0 代表 未发布
                page: 1,
                rows: $scope.rows
            })
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.infoList = data.NewsTempList;
                        $scope.totalItems = data.totalRows;
                        console.log(data.NewsTempList);
                    }
                });
        }

        //页面改变触发
        $scope.pageChanged = function () {
            infoService.getReleaseList({
                stateTime: $scope.info.startTime || "",
                endTime: $scope.info.endTime || "",
                DepId: $scope.info.dept || 0,
                DocType: $scope.info.DocType || 0,  // 0代表全部 信息类别：1,信息；2，公文
                IsChecked: $scope.info.IsAchieved || 2,  //2 代表全部  1 代表 已发布  0 代表 未发布
                page: $scope.currentPage,
                rows: $scope.rows
            })
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.infoList = data.NewsTempList;
                        $scope.totalItems = data.totalRows;
                        console.log(data.NewsTempList);
                    }
                });
        }


    }])
    .controller('PreviewInfoCtrl', ['$scope', '$state','$rootScope','Upload', 'apiService', 'infoService','common','ngVerify','cookie', function ($scope, $state,$rootScope,Upload, apiService,infoService,common,ngVerify,cookie) {

        angular.element(document).ready(function () {
               //页面加载完毕后执行命令
        });

        $scope.newsStatus = 1;
        $scope.upImg = {};
        $scope.uploadText = "上传图片";
        $scope.fileInfo = {};
        $scope.vm = {};   //进度条
        var newsInStore = cookie.getObj('newsInfo');
        $scope.newsInfo = newsInStore || {};
        $scope.$watch('newsInfo', function () {
            cookie.setObj('newsInfo', $scope.newsInfo);
        }, true);

        //判断图片是否已经上传
        var imgInStore = cookie.getObj('upImg');
        if(imgInStore) {
            $scope.uploadText = "更改图片";
            $scope.upImg.urlImg = apiService.serverUrl + "/" + imgInStore.FilePath;
        }

        $scope.openUploadIconModal = function() {
            var modal = $rootScope.openContentModal('imgcutModal', '', $scope.upImage || $scope.upImg.urlImg);
            modal.result.then(function(files) {
                $scope.upImg.cutimg = files[0];
                $scope.upImg.file = files[1];
                //console.log($scope.upImg);
            });
        };

        //文件改变时触发事件
        $scope.imgSelected = function (files, file, event, rejectedFiles) {
            var file = event.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.upImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);
        }

        //提交预览
        $scope.previewSubmit = function () {

            if (angular.isUndefined($scope.upImg.file)) {
                $state.go('info.prevsend');
            } else {
                $scope.upload($scope.upImg.file, newsInfo);
            }
        }
        //提交发送附件
        $scope.submitForm = function () {
            console.log($scope.fileInfo.file);
            Upload.upload({
                //服务端接收
                url: apiService.uploadUrl,
                //上传的同时带的参数
                data: {"blackbox" : cookie.getjm()},
                //上传的文件
                file: $scope.fileInfo.file
            }).progress(function (evt) {
                //进度条
                $scope.vm.showProgress = true;
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progess:' + $scope.progressPercentage + '%' + evt.config.file.name);

            }).success(function (data, status, headers, config) {
                $scope.vm.showProgress = false;
                //上传成功
                console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                // 新闻内容：Content，附件ID：NewsAttachId，图片地址：PictureAddress，发布人：PublishPerson，发布时间：PublishDate，新闻标题：Title，信息类型：DocType，文件名称：FileName，文件路径：FilePath
                infoService.sendNews({
                    Content: "",
                    PictureAddress: "",
                    PublishPerson: $scope.fileInfo.publisher,
                    PublishDate: $scope.fileInfo.releaseTime,
                    Title: $scope.fileInfo.infoTitle,
                    DocType: $scope.fileInfo.infoType,
                    FileName: config.file.name,
                    FilePath: data.UploadDataModelList[0].FilePath
                })
                    .then(function (data) {
                        if (!data.errCode) {
                            console.log("成功");
                            $state.go('info.release');
                        }
                    });

            }).error(function (data, status, headers, config) {
                //上传失败
                console.log('error status: ' + status);
            });

        }


        //上传服务
        $scope.upload = function (file, newsInfo) {
            Upload.upload({
                //服务端接收
                url: apiService.uploadUrl,
                //上传的同时带的参数
                data: {},
                //上传的文件
                file: file
            }).progress(function (evt) {
                //进度条
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progess:' + progressPercentage + '%' + evt.config.file.name);
            }).success(function (data, status, headers, config) {
                //上传成功
                console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                cookie.setObj('upImg', data.UploadDataModelList[0]);
                $state.go('info.prevsend');
            }).error(function (data, status, headers, config) {
                //上传失败
                console.log('error status: ' + status);
            });
        };


    }])
    .controller('prevSendInfoCtrl', ['$scope', '$state', 'apiService', 'cookie', 'infoService', function ($scope, $state, apiService, cookie, infoService) {

        var newsInStore = cookie.getObj('newsInfo');
        $scope.info = newsInStore || {};
        var upImg = cookie.getObj('upImg');
        $scope.upImg = upImg || {};
        console.log($scope.upImg);
        if (angular.equals({}, $scope.upImg)) {
            $scope.upImg.status = 0;
        } else {
            $scope.upImg.status = 1;
            $scope.upImg.Url = apiService.serverUrl + "/" + upImg.FilePath;
        }

        $scope.submitForm = function () {
            // 新闻内容：Content，附件ID：NewsAttachId，图片地址：PictureAddress，
            // 发布人：PublishPerson，发布时间：PublishDate，新闻标题：Title，
            // 信息类型：DocType，文件名称：FileName，文件路径：FilePath
            if (!angular.equals({}, $scope.info)) {
                infoService.sendNews({
                    Content: $scope.info.content,
                    PictureAddress: $scope.upImg.FilePath || "",
                    PublishPerson: $scope.info.publisher,
                    PublishDate: $scope.info.releaseTime,
                    Title: $scope.info.infoTitle,
                    DocType: $scope.info.infoType,
                    FileName: $scope.upImg.FileName,
                    FilePath: ""
                })
                    .then(function (data) {
                        if (!data.errCode) {
                            cookie.removeObj('newsInfo');
                            cookie.removeObj('upImg');
                            console.log("成功");
                            $state.go('info.release');
                        }
                    });
            }

        }

    }])
    .controller('SendInfoCtrl', ['$scope', '$stateParams','apiService', 'infoService', function ($scope, $stateParams, apiService,infoService) {
        $scope.info = {};
        //获取内容信息
        infoService.getArticleContent({
            ID: $stateParams.ID
        })
            .then(function (data) {
                if (!data.errCode) {
                    $scope.info = data;
                    if($scope.info.PictureAddress){
                        $scope.info.imgStatus = 1;
                        $scope.info.imgUrl =apiService.serverUrl + "/" + $scope.info.PictureAddress;

                    }else{
                        $scope.info.imgStatus = 0;
                    }

                }
            });

    }])

    .controller('contentInfoCtrl', ['$scope', '$stateParams', 'infoService', function ($scope, $stateParams, infoService) {
        console.log($stateParams);
        //获取内容信息
        infoService.getArticleContent({
            ID: $stateParams.ID
        })
            .then(function (data) {
                if (!data.errCode) {
                    $scope.articleContent = data;
                    console.log(data);
                }
            });
    }])
    .controller('detailedInfoCtrl', ['$scope', '$stateParams', '$state', 'infoService', function ($scope, $stateParams, $state, infoService) {
        $scope.rows = 10;

        //获取所有部门统计信息
        infoService.getDeptstaticCount({
            stateTime: "",
            endTime: "",
            DepId: $stateParams.ID
        })
            .then(function (data) {
                if (!data.errCode) {
                    $scope.deptList = data;
                    console.log(data);

                }
            });

        $scope.getDeptstaticList = function (info) {
            infoService.getDeptstaticList(info)
                .then(function (data) {
                    if (!data.errCode) {
                        $scope.DeptstaticList = data.NewsInfsList;
                        $scope.totalItems = data.totalRows;
                        console.log(data);
                    }
                });
        }


        //初始化
        $scope.Inital = function () {
            $scope.getDeptstaticList({
                DepId: $stateParams.ID || "",
                stateTime: "",
                endTime: "",
                NewsType: 0,
                page: 1,
                rows: $scope.rows
            });
        }();

        //页面改变触发
        $scope.pageChanged = function () {
            $scope.getDeptstaticList({
                DepId: $stateParams.ID || "",
                stateTime: "",
                endTime: "",
                NewsType: 0,
                page: $scope.currentPage,
                rows: $scope.rows
            });
        }


    }])



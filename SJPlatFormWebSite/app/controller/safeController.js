'use strict';
angular.module("App")
    .controller('safeTabCtrl',['$scope','menu',function($scope,menu){
        //设置二级菜单
        $scope.subMenu = menu;

    }])
    //平安报送
    .controller('safetyInfoCtrl',['$scope','safeService',function ($scope,safeService) {
                $scope.Inital = function(){
                    safeService.getSafetylist({
                        ReportDate : new Date(),
                        page : 1,
                        rows : 1000
                    })
                        .then(function(data) {
                        if(!data.errCode) {
                            $scope.sendList =  data.SafetyInfoList;
                           // console.log($scope);
                        }
                    });
                }();


                $scope.cha = function () {
                    safeService.getSafetylist({
                        ReportDate : $scope.safety.Date || "",
                        page : 1,
                        rows : 1000
                    })
                        .then(function(data) {
                            if(!data.errCode) {
                                $scope.sendList =  data.SafetyInfoList;
                            }
                        });
                }


    }])
    //短信发送
    .controller('safeSendCtrl',['$scope','safeService',function ($scope,safeService) {
        $scope.safety = {};
        $scope.choseArr = [];
        $scope.safety.content = "北京市森林防火指挥中心汇报：截止到10月16日下午18:00全市森林防火工作全天正常!";
        //初始化
        $scope.Inital = function(){
            safeService.getReceiver()
                .then(function(data) {
                    if(!data.errCode) {
                        $scope.receiver =  data;
                        console.log(data);
                    }
                });
        }();

        //多选发送
        $scope.chk = function (username,phone,selected) {
            if (selected == true) {//选中
                $scope.choseArr.push(username+","+phone);
            } else {
                $scope.choseArr.splice($scope.choseArr.indexOf(username+","+phone), 1);//取消选中
            }
        }

        $scope.search = function () {
            safeService.getHistory({
                stateTime : $scope.safety.startDate,
                endTime : $scope.safety.endDate,
                SmsInfo : $scope.safety.queryVal,
                page : 1,
                rows : 5
            })
                .then(function(data) {
                    if(!data.errCode) {
                        $scope.sendHistory =  data.SafetySms_SendInfoList;
                        $scope.totalRows = data.totalRows;
                    }
                });
        }

        //提交发送
        $scope.submitForm = function (){
            safeService.sendSms({
                ReportInfo : $scope.safety.content,
                ReceiversInfo : $scope.choseArr.join("|")
            })
                .then(function(data) {
                    if(!data.errCode) {
                       // $scope.sendHistory =  data.SafetySms_SendInfoList;
                       // $scope.totalRows = data.totalRows;
                        console.log(data);
                    }
                });

        }
    }])
    //历史短信
    .controller('smsHistoryCtrl',['$scope','safeService',function ($scope,safeService) {
        $scope.safety = {};
        $scope.safety.startDate = "2016-11-01";
        $scope.safety.endDate = "2017-11-01";
        var params = {
            stateTime : $scope.safety.startDate,
            endTime : $scope.safety.endDate,
            SmsInfo : $scope.safety.queryVal || "",
            page : 1,
            rows : 5
        };
        $scope.Inital = function(){
            safeService.getHistory(params)
                .then(function(data) {
                    if(!data.errCode) {
                       $scope.sendHistory =  data.SafetySms_SendInfoList;
                       $scope.totalRows = data.totalRows;
                    }
                });
        }();

        $scope.search = function () {
            safeService.getHistory({
                stateTime : $scope.safety.startDate,
                endTime : $scope.safety.endDate,
                SmsInfo : $scope.safety.queryVal,
                page : 1,
                rows : 5
            })
                .then(function(data) {
                    if(!data.errCode) {
                        $scope.sendHistory =  data.SafetySms_SendInfoList;
                        $scope.totalRows = data.totalRows;
                    }
                });
        }

    }])
    //历史统计
    .controller('safeStatisCtrl',['$scope','safeService',function ($scope,safeService) {
       // 开始时间：stateTime，结束时间：endTime
       $scope.safety = {};
       $scope.safety.initStartDate = "2016-11-01";
       $scope.safety.initEndDate = "2017-11-01";

       $scope.Inital = function(){
            safeService.getStatis({
                stateTime :  $scope.safety.initStartDate,
                endTime :  $scope.safety.initEndDate
            })
                .then(function(data) {
                    if(!data.errCode) {
                        $scope.statisList =  data;
                    }
                });
        }();


        $scope.search = function () {
            safeService.getStatis({
                stateTime :  $scope.safety.startDate || "",
                endTime :  $scope.safety.endDate || ""
            })
                .then(function(data) {
                    if(!data.errCode) {
                        $scope.statisList =  data;
                    }
                });
        }

    }])
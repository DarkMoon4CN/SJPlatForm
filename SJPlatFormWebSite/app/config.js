'use strict';
angular.module('App')
     .service('appConfig',['$rootScope' , function ($rootScope) {
         var appConfig = {
             host : '172.16.2.68',
             //host:'localhost'
             //host:'ceshi.com'
             port :'8018',
             appId : 'ceshi.com'
         };
         $rootScope.appConfig = appConfig;
         return appConfig;
     }])
     .service('settings',['$rootScope',function($rootScope){
         var settings = {
               //
         };
         $rootScope.settings = settings;
         return settings;
     }])
    .config(['$stateProvider','$locationProvider','$urlRouterProvider',function($stateProvider,$locationProvider,$urlRouterProvider){
        //请求拦截器和过滤器
        $locationProvider.html5Mode(true);

        $stateProvider
        //-------------------首页//
            .state('home', {
                abstract: true,
                url: "/sms",
                data: {
                    pageTitle: '短信列表'
                },
                views: {
                    'index': {
                        templateUrl: "views/sms/list.html",
                        controller: "smsListCtrl"
                    }
                },
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'App',
                            files: [
                                "app/controller/smsController.js"
                            ]
                        }

                        ]);
                    }]
                }
            });

            $urlRouterProvider.otherwise('/sms');

    }]);
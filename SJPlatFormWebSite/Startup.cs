﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SJPlatFormWebSite.Startup))]
namespace SJPlatFormWebSite
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 汇总统计请求体
    /// </summary>
    public class SummaryStatisticsReq
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public List<int> Years { get; set; }

        /// <summary>
        /// 选择的年份
        /// </summary>
        public int SelectYear { get; set; }
    }
}
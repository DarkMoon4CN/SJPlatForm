﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.FuDi
{

    /// <summary>
    /// 登录请求体
    /// </summary>
    public class LoginReq
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserCode { get; set; }

        /// <summary>
        /// 密码(MD5)
        /// </summary>
        public string PassWord { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.FuDi
{

    /// <summary>
    /// 上传请求体
    /// </summary>
    public class UpLoadReq
    {
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 火灾当前进度
        /// </summary>
        public int FireState { get; set; }
        /// <summary>
        /// 文件主体byte流 字符串
        /// </summary>
        public string FileByte { get; set; }

        /// <summary>
        ///  后缀名 不需要加点
        /// </summary>
        public string ExtName { get; set; }

        /// <summary>
        /// 1.图片 2.文件
        /// </summary>
        public int FileType { get; set; }

    }
}
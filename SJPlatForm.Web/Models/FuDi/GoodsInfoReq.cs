﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.FuDi
{
    /// <summary>
    /// 物资请求体
    /// </summary>
    public class GoodsInfoReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 物资编号
        /// </summary>
        public string NO { get; set; }
      
        /// <summary>
        /// 物资仓库ID
        /// </summary>
        public Nullable<int> WH_ID { get; set; }

        /// <summary>
        /// 物资仓库名称
        /// </summary>
        public string WH_Name { get; set; }
      
        /// <summary>
        /// 物资数量
        /// </summary>
        public string Count { get; set; }
        /// <summary>
        /// 货位
        /// </summary>
        public string AllLocation { get; set; }

        /// <summary>
        /// 生产日期
        /// </summary>
        public Nullable<System.DateTime> ManufactureDate { get; set; }
    }
}
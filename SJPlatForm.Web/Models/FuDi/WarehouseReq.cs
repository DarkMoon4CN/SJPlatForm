﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.FuDi
{

    /// <summary>
    /// 物资
    /// </summary>
    public class WarehouseReq: GridPager2
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 物资仓库名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 物资仓库简拼
        /// </summary>
        public string Spell { get; set; }

        /// <summary>
        /// 物资仓库地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 物资仓库电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 仓库容量
        /// </summary>
        public string Capacity { get; set; }

        /// <summary>
        /// 仓库照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 所属单位ID
        /// </summary>
        public Nullable<int> Entity_ID { get; set; }

        /// <summary>
        /// 所属单位名称
        /// </summary>
        public string Entity_Name { get; set; }

        /// <summary>
        /// 经度
        /// </summary>
        public Nullable<decimal> Longitude { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public Nullable<decimal> Latitude { get; set; }
    }
}
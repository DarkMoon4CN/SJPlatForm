﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    ///  将消息设置成已读状态
    /// </summary>
    public class AutomaticReplayReq
    {
        /// <summary>
        /// 消息ID 与  FireID IsCityManager互斥
        /// </summary>
        public int AutomaticID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 是否是市局
        /// </summary>
        public int IsCityManager { get; set; }
    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 火灾信息请求体
    /// </summary>
    public class FireInfoReq: GridPager2
    {
        
        /// <summary>
        /// 0=正常火灾、1=结束火灾、2=未开始火灾(区县刚转入还未接收)
        /// </summary>
        public int FireSate { get; set; }

        /// <summary>
        /// 区县地址ID集合
        /// </summary>
        public string[] AddressCountys { get; set; }

    }
}
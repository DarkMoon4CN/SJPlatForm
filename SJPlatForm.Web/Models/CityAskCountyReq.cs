﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
     /// <summary>
     /// 市局询问区县请求体
     /// </summary>
    public class CityAskCountyReq
    {
         /// <summary>
         /// 火灾ID
         /// </summary>
         public int FireID { get; set; }
         
        /// <summary>
        /// 内容
        /// </summary>
         public string Message { get; set; }

        /// <summary>
        ///  询问者ID
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        ///  询问者名称
        /// </summary>
        public string TransUserName { get; set; }
    }


    /// <summary>
    /// 区县回答市局请求体
    /// </summary>
    public class CountyReplayCityReq {

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 询问ID
        /// </summary>
        public int AskID { get; set; }

        /// <summary>
        /// 回复人ID
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 回复人名称
        /// </summary>
        public string TransUserName { get; set; }

    }
}
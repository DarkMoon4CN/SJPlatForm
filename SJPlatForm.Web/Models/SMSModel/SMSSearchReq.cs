﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChuanYe.Models;
namespace SJPlatForm.Web.Models.SMSModel
{
    public class SMSSearchReq : GridPager
    {

        /// <summary>
        /// 
        /// </summary>
        public string Mcont { get; set; }


        /// <summary>
        /// 选填 0.办公 1.处置
        /// </summary>
        public int? MType { get; set; }
    }
}
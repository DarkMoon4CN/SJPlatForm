﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 森林防火应急指挥部请求体
    /// </summary>
    public class HeadquarterReq
    {
        /// <summary>
        /// 增加时忽略，更新时必填
        /// </summary>
        public int UH_ID { get; set; }

        /// <summary>
        ///指挥部名称
        /// </summary>
        public string UH_Name { get; set; }

        /// <summary>
        /// 指挥领导
        /// </summary>
        public string UH_Leader { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string UH_Phone { get; set; }

        /// <summary>
        /// 父级ID    
        /// </summary>
        public int UH_ParentId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int UH_Sort { get; set; }


        /// <summary>
        /// 添加人
        /// </summary>
        public string UH_TransUser { get; set; }


        /// <summary>
        /// 指挥部属于几级菜单
        /// </summary>
        public int UH_Level { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string UH_Remarks { get; set; }

        /// <summary>
        /// 所属大区ID
        /// </summary>
        public int UH_LinchpinID { get; set; }

        /// <summary>
        /// 忽略
        /// </summary>
        public int key { get; set; }


    }
}
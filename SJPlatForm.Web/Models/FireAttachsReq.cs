﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 火灾附件请求体
    /// </summary>
    public class FireAttachsReq
    {
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string FilePath { get; set; }


        /// <summary>
        /// 0.默认 1.鉴定 2.立案
        /// </summary>
        public int Type { get; set; }
    }

    public class RemoveFireAttachsReq 
    {
        /// <summary>
        /// 附件编号
        /// </summary>
        public int ID { get; set; }
    }


}
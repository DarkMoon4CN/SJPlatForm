﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.Other
{
    /// <summary>
    /// 视频记录请求体
    /// </summary>
    public class FireVideoInfoReq
    {
        /// <summary>
        /// 编号,增加(AddFireVideoInfo)时忽略
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 视频名称
        /// </summary>
        public string VideoName { get; set; }

        /// <summary>
        /// 是否是市局操作  1：是 0：否
        /// </summary>
        public int IsCityManager { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.Other
{
    /// <summary>
    /// 轮播查询请求体
    /// </summary>
    public class CarouselFigureSearchReq
    {
          /// <summary>
          /// 0.查询所有，1.查询已启用轮播
          /// </summary>
          public int SearchType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.Other
{

    /// <summary>
    /// 轮播图请求体
    /// </summary>
    public class UpdateCarouselFigureReq 
    {
        /// <summary>
        /// 更改请求体
        /// </summary>
        public List<UpdateCarouselFigureFlag> List { get; set; }

        public int key { get; set; }
    }

    /// <summary>
    /// UpdateCarouselFigureReq 的数据
    /// </summary>
    public class UpdateCarouselFigureFlag
    {
        /// <summary>
        /// 轮播图ID
        /// </summary>
        public int CF_ID { get; set; }
        /// <summary>
        /// 轮播图名称
        /// </summary>
        public string CF_Name { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string CF_ImgPath { get; set; }


        /// <summary>
        /// 是否启用
        /// </summary>
        public bool CF_IsEnable { get; set; }
    }

}
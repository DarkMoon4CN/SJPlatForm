﻿using ChuanYe.Models;
using System.Collections.Generic;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 获取全局未读消息请求体
    /// </summary>
    public class GlobalAutomaticReq
    {
        /// <summary>
        /// 所在区县ID，市局 -1
        /// </summary>
        public int AddressCounty { get; set; }

        // <summary>
        /// 需要提示消息类型
        /// </summary>
        public  List<int> MessageTypes { get; set; }

        /// <summary>
        /// 是否是市局
        /// </summary>
        public int IsCityManager { get; set; }
    }

    /// <summary>
    /// 区县支援火灾请求体
    /// </summary>
    public class SupportFireRequest: GridPager2
    {
        /// <summary>
        /// 所在区县ID，市局 -1
        /// </summary>
        public int AddressCounty { get; set; }
    }
}
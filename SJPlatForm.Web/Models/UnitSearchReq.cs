﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    ///  扑救力量->单位请求体
    /// </summary>
    public class UnitSearchReq: GridPager2
    {
        /// <summary>
        ///  单位类型ID
        /// </summary>
        public int ID { get; set; }


        /// <summary>
        /// 指挥部编号
        /// </summary>
        public int UH_ID { get; set; }


        /// <summary>
        /// 响应等级
        /// </summary>
        public int U_ResponseLevel { get; set; }
    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 火灾倒计时配置模型类
    /// </summary>
    public class FireCountDownModel 
    {
        /// <summary>
        /// 火灾ID
        /// </summary>
       public int FireID { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireTime { get; set; }

        /// <summary>
        /// 21 首报未及时操作，22 续报过去未及时操作 23 终报过期未及时操作
        /// </summary>
        public int CountDownType { get; set; }
    }
}
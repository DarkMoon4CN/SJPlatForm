﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 逐级响应请求体
    /// </summary>
    public class StepResponseReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 启动时间
        /// </summary>
        public System.DateTime? StartDatetime { get; set; }

        /// <summary>
        /// 启动人
        /// </summary>
        public string Starter { get; set; }

        /// <summary>
        /// 批示内容
        /// </summary>
        public string InstructionContent { get; set; }

        /// <summary>
        /// 启动条件  string|string2|strng3
        /// </summary>
        public string StartCondition { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 操作人名称
        /// </summary>
        public string TransUserName { get; set; }
    }

    /// <summary>
    /// 区县4级响应请求体
    /// </summary>
    public class FourStepResponseReq : StepResponseReq
    {
        /// <summary>
        /// 紧急临时追加人员
        /// </summary>
        public List<StepResponseTempPeopleReq> FTPs { get; set; }
    }

    /// <summary>
    /// 简化4级启动
    /// </summary>
    public class FourStepResponseSimpleReq
    {
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 启动人
        /// </summary>
        public string Starter { get; set; }


        /// <summary>
        /// 紧急临时追加人员
        /// </summary>
        public List<StepResponseTempPeopleReq> FTPs { get; set; }
    }

    public class StepResponseTempPeopleReq
    {
        /// <summary>
        /// 主键 增加时忽略
        /// </summary>
        public int FTP_ID { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>

        public string Name { get; set; }

        /// <summary>
        /// 职务名称
        /// </summary>
        public string PositionTitle { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
    }

}
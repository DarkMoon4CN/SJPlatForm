﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    ///  单位类型请求体
    /// </summary>
    public class UnitTypeReq
    {
        /// <summary>
        /// 单位类型ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 单位名
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 单位第一负责人
        /// </summary>
        public string MainLeader { get; set; }

        /// <summary>
        /// 单位第二负责人
        /// </summary>
        public string OtherLeader { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }
    }
}
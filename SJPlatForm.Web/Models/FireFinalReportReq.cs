﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 火灾终报请求体
    /// </summary>
    public class FireFinalReportReq 
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int FFR_ID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 火灾地址  例:门头沟区,龙泉镇,,琉璃渠
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 接警时间或事发时间
        /// </summary>
        public DateTime? Jdate { get; set; }

        /// <summary>
        /// 接警类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        ///  火灾报告
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 专业森林消防队数量（几支队伍）
        /// </summary>
        public int Fh { get; set; }

        /// <summary>
        /// 专业森林消防队人数（每支队伍人数）
        /// </summary>
        public int FhPeople { get; set; }

        /// <summary>
        /// 驻区部队数量
        /// </summary>
        public int Com { get; set; }

        /// <summary>
        /// 驻区部队人数
        /// </summary>
        public int ComPeople { get; set; }

        /// <summary>
        /// 乡镇扑火队伍（单位/人）
        /// </summary>
        public int Ranks { get; set; }

        /// <summary>
        /// 群众队伍(单位/人)
        /// </summary>
        public int Mas { get; set; }

        /// <summary>
        /// 前线指挥
        /// </summary>
        public string Leader { get; set; }


        /// <summary>
        /// 扑火时间
        /// </summary>
        public DateTime? Ptime { get; set; }

        /// <summary>
        /// 留守人员 专业扑火队（单位/支）
        /// </summary>
        public int Zf { get; set; }

        /// <summary>
        ///  留守人员 专业扑火队(单位/人)
        /// </summary>
        public int Zpeople { get; set; }


        /// <summary>
        ///  留守人员 半专业扑火队(单位/支)
        /// </summary>
        public int BZf { get; set; }

        /// <summary>
        ///  留守人员 半专业扑火队(单位/人)
        /// </summary>
        public int LocalP { get; set; }

        /// <summary>
        /// 天气
        /// </summary>
        public string Weather { get; set; }


        /// <summary>
        /// 风力
        /// </summary>
        public string Wind { get; set; }

        /// <summary>
        /// 风向
        /// </summary>
        public string Direc { get; set; }


        /// <summary>
        /// 温度
        /// </summary>
        public string Temp { get; set; }

        /// <summary>
        /// 湿度
        /// </summary>
        public string Damp { get; set; }

        /// <summary>
        /// 过火面积
        /// </summary>
        public string Garea { get; set; }

        /// <summary>
        /// 过火面积单位 （平方米/公顷）
        /// </summary>
        public string Gunit { get; set; }

        /// <summary>
        /// 林地过火面积
        /// </summary>
        public string Larea { get; set; }

        /// <summary>
        /// 林地过火面积单位（平方米/公顷）
        /// </summary>
        public string Lunit { get; set; }

        /// <summary>
        /// 过火树种
        /// </summary>
        public string Trees { get; set; }

        /// <summary>
        /// 过火树种 树龄
        /// </summary>
        public string Age { get; set; }

        /// <summary>
        /// 过火树种 数量
        /// </summary>
        public string Num { get; set; }

        /// <summary>
        /// 过火树种 死亡数量
        /// </summary>
        public string Die { get; set; }

        /// <summary>
        /// 火灾原因ID
        /// </summary>
        public int WhyID { get; set; }
        /// <summary>
        /// 火灾原因
        /// </summary>
        public string Why { get; set; }

        /// <summary>
        /// 人员伤亡情况
        /// </summary>
        public string Loss { get; set; }

        /// <summary>
        /// 其他情况
        /// </summary>
        public string Other { get; set; }

        /// <summary>
        /// 报告人
        /// </summary>
        public string Reporter { get; set; }

        /// <summary>
        /// 报告时间
        /// </summary>
        public DateTime? ReporterTime { get; set; }


        /// <summary>
        ///  记录人ID
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 火灾类型ID
        /// </summary>
        public int FireTypeID { get; set; }


        /// <summary>
        /// 火灾类型名
        /// </summary>
        public string FireType { get; set; }

        /// <summary>
        /// 是否是补录数据 0.非补录  1.补录
        /// </summary>
        public int IsSupplement { get; set; }


        /// <summary>
        /// 0.未立案 1.立案
        /// </summary>
        public int CaseState { get; set; }

        /// <summary>
        /// 火灾附件 
        /// </summary>
        public List<FireAttachsReq> Attachs { get; set; }
    }
}
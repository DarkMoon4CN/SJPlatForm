﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    public class FireContinueReportReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int CID{get;set;}

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 续报内容
        /// </summary>
        public string ContinueReportContent { get; set; }

        /// <summary>
        /// 续报人
        /// </summary>
        public string Applicant { get; set; }

        /// <summary>
        /// 续报时间
        /// </summary>
        public DateTime? ContinueTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 成立前指挥
    /// </summary>
    public class FrontCommandReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }
        /// <summary>
        /// 总指挥姓名
        /// </summary>
        public string CommanderName { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 现场经度
        /// </summary>
        public string LocaleLongitude { get; set; }
        /// <summary>
        /// 现场纬度
        /// </summary>
        public string LocaleLatitude { get; set; }
        /// <summary>
        /// 前指人员
        /// </summary>
        public string Member { get; set; }
   
        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 添加人名称
        /// </summary>
        public string TransUserName { get; set; }

        /// <summary>
        /// 前指地址
        /// </summary>
        public string Address { get; set; }


        /// <summary>
        /// 临时人员集合
        /// </summary>
        public List<FrontCommandTempPeopleReq> FTPs { get; set; }
    }






    public class FrontCommandTempPeopleReq
    {
        /// <summary>
        /// 主键 增加时忽略
        /// </summary>
        public int FTP_ID { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>

        public string Name { get; set; }

        /// <summary>
        /// 职务名称
        /// </summary>
        public string PositionTitle { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
    }

}
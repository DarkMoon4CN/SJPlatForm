﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    public enum CountDownType
    {
         FirstCountDown=10,
         ContinueCountDown=20,
         EndCountDown=120
    }
}
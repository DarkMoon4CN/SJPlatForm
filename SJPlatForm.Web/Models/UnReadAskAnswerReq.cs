﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 未读的询问与回复请求体
    /// </summary>
    public class UnReadAskAnswerReq
    {
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 是否是市局
        /// </summary>
        public int IsCityManager { get; set; }

    }

    /// <summary>
    /// 市局更新回复状态请求体
    /// </summary>
    public class UpdateAnswerReadReq
    {
         /// <summary>
         /// 回复ID
         /// </summary>
         public int Cou_ID { get;set;}
    }

}
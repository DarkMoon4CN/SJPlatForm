﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 单位成员请求体
    /// </summary>
    public class UnitMemberReq
    {
        /// <summary>
        ///  增加是忽略
        /// </summary>
        public int UM_ID { get; set; }

        /// <summary>
        ///  成员名
        /// </summary>
        public string UM_MemberName { get; set; }

        /// <summary>
        ///  性别
        /// </summary>
        public Nullable<int> UM_MemberSex { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        public Nullable<System.DateTime> UM_Birthday { get; set; }

        /// <summary>
        /// ??
        /// </summary>
        public string UM_Nation { get; set; }


        /// <summary>
        ///  单位ID
        /// </summary>
        public string UM_Unit { get; set; }

        /// <summary>
        ///  邮编
        /// </summary>
        public Nullable<int> UM_Post { get; set; }
        /// <summary>
        ///  电话
        /// </summary>
        public string UM_Phone { get; set; }

        /// <summary>
        /// 座机
        /// </summary>
        public string UM_Tel { get; set; }

        /// <summary>
        ///  头像
        /// </summary>
        public string UM_Pic { get; set; }

        /// <summary>
        ///  地址
        /// </summary>
        public string UM_Address { get; set; }

        /// <summary>
        ///  邮箱
        /// </summary>
        public string UM_Email { get; set; }
        /// <summary>
        ///  备注
        /// </summary>
        public string UM_Remark { get; set; }

        /// <summary>
        /// 单位名称
        /// </summary>
        public string pUnitName { get; set; }


        /// <summary>
        /// 添加人
        /// </summary>
        public string UM_TransUser { get; set; }
    }
}
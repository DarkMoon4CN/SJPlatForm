﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 市局防火指挥部
    /// </summary>
    public class HeadquarterSearchReq:GridPager2
    {
        /// <summary>
        ///  顶级ParentID 为 0
        /// </summary>
        public int UH_ParentID { get; set; }

        /// <summary>
        /// 指挥部名称
        /// </summary>
        public string UH_Name { get; set; }

        /// <summary>
        /// 指挥领导名称
        /// </summary>
        public string UH_Leader { get; set; }

        /// <summary>
        /// 指挥部的级别 与 UH_ParentID 查询互斥
        /// </summary>
        public int UH_Level { get; set; }


        /// <summary>
        /// 区县ID
        /// </summary>
        public int UH_LinchpinID { get; set; }
    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{   

    /// <summary>
    /// 2级3级响应请求体
    /// </summary>
    public class OtherGradeTemplateSearchReq: GridPager2
    {
         /// <summary>
         /// 2,3级,4级(20201009调整)
         /// </summary>
         public int SGradeID { get; set; }

        /// <summary>
        /// 区县ID
        /// </summary>
        public long LinchpinIID { get; set; }

    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 区县请求体
    /// </summary>
    public class LinchpinReq 
    {
        /// <summary>
        /// 区县ID
        /// </summary>
        public int LinchpinID { get; set; }
    }
}
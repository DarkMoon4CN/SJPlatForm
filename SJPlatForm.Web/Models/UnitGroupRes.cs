﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// GetUnitGroup  API 的响应体
    /// </summary>
    public class UnitGroupRes: F_UnitType
    {

        /// <summary>
        /// 单位成员集合
        /// </summary>
        public List<F_Unit> Children { get; set; }
    }
}
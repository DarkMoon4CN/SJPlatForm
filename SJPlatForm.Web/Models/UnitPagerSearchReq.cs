﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 火灾单位请求体
    /// </summary>
    public class UnitPagerSearchReq:GridPager2
    {
        /// <summary>
        /// 单位分类ID
        /// </summary>
        public int UnitTypeID { get; set; }
    }
}
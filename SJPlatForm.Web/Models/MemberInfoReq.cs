﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Controllers
{
    /// <summary>
    /// 指挥部用户信息
    /// </summary>
    public class MemberInfoReq
    {
      
    }

    /// <summary>
    /// 指挥部信息
    /// </summary>
    public class OrgInfoReq
    {
        /// <summary>
        /// 指挥部ID
        /// </summary>
        public int OrgID { get; set; }

    }
}
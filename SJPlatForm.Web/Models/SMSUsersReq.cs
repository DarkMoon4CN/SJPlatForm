﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 短信用户请求体
    /// </summary>
    public class SMSUsersReq
    {
        /// <summary>
        /// 查询类型 
        /// </summary>
        public int SelectType { get; set; }
    }
}
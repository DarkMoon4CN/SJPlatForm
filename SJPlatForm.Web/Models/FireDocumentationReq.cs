﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 纪录文档请求体
    /// </summary>
    public class FireDocumentationReq
    {
        /// <summary>
        ///  增加时忽略
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }
        /// <summary>
        /// 消息 内容
        /// </summary>
        public string MessageContent { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        public System.DateTime? MessageTime { get; set; }
        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }
        
        /// <summary>
        /// 添加人名称
        /// </summary>
        public string TransUserName { get; set; }

        /// <summary>
        /// 短信集合
        /// </summary>
        public List<SMS> SMSs { get; set; }

        /// <summary>
        /// 短信内容
        /// </summary>
        public string SMSMessage { get; set; }

    }
}
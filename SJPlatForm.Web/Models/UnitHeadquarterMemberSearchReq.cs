﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 指挥部成员查询请求体
    /// </summary>
    public class UnitHeadquarterMemberSearchReq: GridPager2
    {
            /// <summary>
            /// 指挥部领导(成员)姓名
            /// </summary>
           public string UHM_Leader { get; set; }

           /// <summary>
           /// 指挥部ID
           /// </summary>
           public int? UH_ID { get; set; }

    }

    /// <summary>
    /// 指挥部增加或编辑请求体
    /// </summary>
    public class UnitHeadquarterMemberReq: UserKey
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int UHM_ID { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string UHM_Leader { get; set; }

        /// <summary>
        /// 部门Id
        /// </summary>
        public int UHM_DepID { get; set; }
        
        /// <summary>
        /// 部门名称
        /// </summary>
        public string UHM_DepName { get; set; }

        /// <summary>
        /// 职务
        /// </summary>
        public string UHM_Post { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string UHM_Phone { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string UHM_Tel { get; set; }


        /// <summary>
        /// 联络人名称
        /// </summary>
        public string UHM_LiaisonName { get; set; }

        /// <summary>
        /// 联络人职务
        /// </summary>
        public string UHM_LiaisonPost { get; set; }

        /// <summary>
        /// 联络人手机
        /// </summary>
        public string UHM_LiaisonPhone { get; set; }
        /// <summary>
        /// 联络人电话
        /// </summary>
        public string UHM_LiaisonTel { get; set; }

        /// <summary>
        /// 指挥部ID
        /// </summary>
        public int UH_ID { get; set; }

        /// <summary>
        /// 指挥部名称
        /// </summary>
        public string UH_Name { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string UHM_TransUser { get; set; }
    }


    /// <summary>
    /// 指挥部单位请求体
    /// </summary>
    public class UnitHeadquarterDepartmentSearchReq : GridPager2
    {

        /// <summary>
        ///  单位名称
        /// </summary>
        public string UHD_Name { get; set; }

    }

    /// <summary>
    /// 指挥部单位请求体
    /// </summary>
    public class UnitHeadquarterDepartmentReq : GridPager2
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int UHD_ID { get; set; }

        /// <summary>
        /// 单位名称
        /// </summary>
        public string UHD_Name { get; set; }

        /// <summary>
        /// 排序 选填默认为0
        /// </summary>
        public int UHD_Sort { get; set; }
        /// <summary>
        /// 简短说明
        /// </summary>
        public string UHD_Remarks { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string UHD_TransUser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int key { get; set; }
    }



    public class UserKey
    {
        public int key { get; set; }
    }
}
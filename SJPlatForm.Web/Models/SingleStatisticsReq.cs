﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 线性统计请求体
    /// </summary>
    public class SingleStatisticsReq
    {
        /// <summary>
        /// 统计类型 1.事发地点 2.事发时间 3.起火原因 4.接警途径 5.火灾类别
        /// </summary>
        public int StatisticsType { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public List<int> Years { get; set; }

        /// <summary>
        /// 选择的年份
        /// </summary>
        public int SelectYear { get; set; }
    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 火灾类型及报警方式请求体
    /// </summary>
    public class DirectoryReq: GridPager2
    {
        /// <summary>
        /// 增加时忽略 
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///  -1:所有非1级分类数据  0.一级分类 其他.相应子级数据
        /// </summary>
        public int ParentId { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int key { get; set; }
    }
}
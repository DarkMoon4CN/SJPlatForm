﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 火灾报灭请求体
    /// </summary>
    public class ReportOutFireReq 
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 报灭内容
        /// </summary>
        public string ReportoutContent { get; set; }

        /// <summary>
        /// 申请人
        /// </summary>
        public string Applicant { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime? HandleTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }
    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 申请响应
    /// </summary>
    public class ApplyRespondReq 
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 申请内容
        /// </summary>
        public string ApplyReason { get; set; }

        /// <summary>
        /// 申请人
        /// </summary>
        public string Applicant { get; set; }

        /// <summary>
        /// 申请时间
        /// </summary>
        public DateTime? AppTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }
    }
}
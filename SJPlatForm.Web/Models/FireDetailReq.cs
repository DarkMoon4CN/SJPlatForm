﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 火灾查询
    /// </summary>
    public class FireDetailReq: GridPager2
    {

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

    }


    /// <summary>
    /// 火灾处置绘制查询体
    /// </summary>
    public class FireRecordPlotSearchReq 
    {
         /// <summary>
         /// 火灾编号 
         /// </summary>
         public int F_FireID { get; set; }

        /// <summary>
        /// 处置类型进度编号(处置进度编号)
        /// </summary>
        public int MR_Type { get; set; }

        /// <summary>
        /// 处置编号
        /// </summary>
        public int MR_ID { get; set; }
    }

    public class FireRecordPlotReq : FireRecordPlotSearchReq
    {
        /// <summary>
        /// 主键 增加时忽略
        /// </summary>
        public int FRP_ID { get; set; }
      
        /// <summary>
        /// 添加人
        /// </summary>
        public string F_TransUser { get; set; }

        /// <summary>
        ///  第三方标绘ID
        /// </summary>
        public string PlotID { get; set; }

        /// <summary>
        /// 第三方标绘内容
        /// </summary>
        public string PlotContent { get; set; }

        /// <summary>
        /// 第三方标绘类型
        /// </summary>
        public string PlotType { get; set; }
    }



    public class AddFireAlarmRecordReq : DelFireAlarmRecordReq
    {


        /// <summary>
        /// 告警内容
        /// </summary>
        public string AR_Content { get; set; }

        /// <summary>
        /// 镜头编号
        /// </summary>
        public int UnitID { get; set; }

        /// <summary>
        /// 站点编号
        /// </summary>
        public string LocaID { get; set; }


        /// <summary>
        /// 大区名称
        /// </summary>
        public string AddressName { get; set; }

        /// <summary>
        /// 选填 增加时间
        /// </summary>
        public System.DateTime? AddTime { get; set; }

        /// <summary>
        /// 选填 告警类型
        /// </summary>
        public string AlarmType { get; set; }


        /// <summary>
        /// 选填 水平角度
        /// </summary>
        public Nullable<decimal> HorizontalAngle { get; set; }


        /// <summary>
        /// 选填 俯仰角度
        /// </summary>
        public Nullable<decimal> PitchAngle { get; set; }

        /// <summary>
        /// 选填 处理状态
        /// </summary>
        public Nullable<int> AuditState { get; set; }


        /// <summary>
        /// 选填 告警图片
        /// </summary>
        public Nullable<decimal> LPFileName { get; set; }

        /// <summary>
        /// 选填 镜头名称
        /// </summary>
        public string UnitName { get; set; }

        /// <summary>
        /// 选填 站点名称
        /// </summary>
        public string LocaName { get; set; }

        /// <summary>
        /// 选填 告警类型名称
        /// </summary>
        public string AlarmTypeName { get; set; }



    }

    public class DelFireAlarmRecordReq 
    {
        /// <summary>
        /// 增加(AddFireAlarmRecord)时，忽略
        /// </summary>
        public int AR_ID { get; set; }
    }


    public  class AddAlarmRecordConfigReq
    {

        /// <summary>
        ///  增加(AddFireAlarmRecordConfig)时，忽略
        /// </summary>
        public int ARC_ID { get; set; }

        /// <summary>
        /// 站点ID
        /// </summary>
        public string LocaID { get; set; }

        /// <summary>
        /// UnitID
        /// </summary>
        public Nullable<int> UnitID { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public System.DateTime? StartTime { get; set; }


        /// <summary>
        /// 过期值 单位/分钟
        /// </summary>
        public int Value { get; set; }
    }

    public class ChangeFireAlarmRecordStateReq
    {

        /// <summary>
        /// 火灾编号
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 告警编号
        /// </summary>
        public int AR_ID { get; set; }


        /// <summary>
        /// 0.区县 1.市局
        /// </summary>
        public int IsCityManager { get; set; }
    }


}
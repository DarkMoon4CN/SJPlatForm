﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class FireCatalogReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 火警ID
        /// </summary>
        public int FireID { get; set; }
        /// <summary>
        /// 误报理由
        /// </summary>
        public string FalseReason { get; set; }
        /// <summary>
        /// 申请人
        /// </summary>
        public string Applicant { get; set; }
        /// <summary>
        /// 申请时间
        /// </summary>
        public DateTime? AppTime { get; set; }
        /// <summary>
        /// 处理结果 1.不同意 0.同意(默认为0)
        /// </summary>
        public int Results { get; set; }
        /// <summary>
        /// 处理原因 (处理原因 与  处理 时间为空时,为未处理误报)
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime? ReaTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 添加人名称
        /// </summary>
        public string TranUserName { get; set; }

        /// <summary>
        /// 误报审批时,消息会设置为已读
        /// </summary>
        public int AutomaticID { get; set; }

    }
}
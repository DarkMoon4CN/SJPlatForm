﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 联动响应请求体
    /// </summary>
    public class LinkCorrespondingReq
    {
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        public string DepartMent { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactMen { get; set; }
        /// <summary>
        /// 手机电话
        /// </summary>
        public string MobilePhone { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone { get; set; }
        /// <summary>
        /// 职务
        /// </summary>
        public string Duty { get; set; }
    }
}
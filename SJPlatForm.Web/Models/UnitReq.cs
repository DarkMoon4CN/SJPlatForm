﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 火灾单位请求体
    /// </summary>
    public class UnitReq:UserKey
    {

        /// <summary>
        ///  单位ID
        /// </summary>
        public int U_ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string U_UnitName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string U_UnitType { get; set; }



        /// <summary>
        /// 防火指挥部ID(旧版弃用 2018/08/06)
        /// </summary>
        public string U_OrgID { get; set; }


        /// <summary>
        /// 电话
        /// </summary>
        public string U_Phone { get; set; }


        /// <summary>
        /// 地址
        /// </summary>

        public string U_Address { get; set; }

        /// <summary>
        /// 传真
        /// </summary>
        public string U_Fax { get; set; }


        /// <summary>
        /// 邮箱
        /// </summary>
        public string U_Email { get; set; }

        /// <summary>
        /// 响应等级
        /// </summary>
        public Nullable<int> U_ResponseLevel { get; set; }



        /// <summary>
        /// 经度
        /// </summary>
        public string U_SeatLongitude { get; set; }


        /// <summary>
        /// 纬度
        /// </summary>
        public string U_SeatLatitude { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        public string U_Remark { get; set; }


        /// <summary>
        /// 添加人
        /// </summary>
        public string U_TransUser { get; set; }

        /// <summary>
        /// 指挥部ID
        /// </summary>
        public int UH_ID { get; set; }

        /// <summary>
        ///指挥部名称
        /// </summary>
        public string UH_Name { get; set; }

        /// <summary>
        ///  单位类型名称
        /// </summary>
        public string PuTypeName { get; set; }

        /// <summary>
        /// 人数
        /// </summary>
        public int U_Total { get; set; }

        /// <summary>
        /// 是否是应急小组  true | false
        /// </summary>
        public bool U_Emergency { get; set; }

        /// <summary>
        /// 队长名称
        /// </summary>
        public string U_Captain { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string U_Tel { get; set; }

    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 查看当前处置流程权限
    /// </summary>
    public class DisposalAuthorReq: GridPager2
    {

        /// <summary>
        ///  火灾ID
        /// </summary>
        public int FireID { get; set; }
       
        /// <summary>
        /// 处置流程ID
        /// </summary>
        public int MRID { get; set; }


    }
}
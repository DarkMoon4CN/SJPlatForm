﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    public class BlConsReq
    {
        /// <summary>
        ///  主键  增加时忽略
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        ///  火灾ID
        /// </summary>
        public Nullable<int> F_FireID { get; set; }

        /// <summary>
        /// 接警内容
        /// </summary>
        public string Contens { get; set; }

        /// <summary>
        /// 创建时间（必须由用户指定时间）
        /// </summary>
        public Nullable<System.DateTime> CreateTime { get; set; }
    }

    public class RemoveBlConsReq
    {
        /// <summary>
        ///  删除的ID
        /// </summary>
        public int ID { get; set; }
    }


}
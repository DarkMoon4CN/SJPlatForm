﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 火灾增加请求体
    /// </summary>
    public class AddFireInfoReq
    {
        /// <summary>
        /// 添加时忽略
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 火警名称
        /// </summary>
        public string FireName { get; set; }

        /// <summary>
        /// 来电人姓名
        /// </summary>
        public string ReporterName { get; set; }

        /// <summary>
        /// 来电人单位
        /// </summary>
        public string ReporterUnit { get; set; }

        /// <summary>
        /// 来电人工号
        /// </summary>
        public string ReporterWorknum { get; set; }

        /// <summary>
        /// 来电号码
        /// </summary>
        public string ReporterPhone { get; set; }

        /// <summary>
        /// 事发地点(区县)
        /// </summary>
        public string AddressCounty { get; set; }

        /// <summary>
        /// 事发地点(乡镇)
        /// </summary>
        public string AddressTown { get; set; }

        /// <summary>
        /// 事发地点(村组)
        /// </summary>
        public string AddressVillage { get; set; }

        /// <summary>
        /// 事发地点
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 事发时间
        /// </summary>
        public DateTime? HappenDatetime { get; set; }

        /// <summary>
        /// 接警时间
        /// </summary>
        public DateTime? AlarmTime { get; set; }

        /// <summary>
        /// 接警人
        /// </summary>
        public string Alarm { get; set; }

        /// <summary>
        /// 接警内容
        /// </summary>
        public string AlarmContent { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }


        /// <summary>
        /// 经度： 度|分|秒
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// 纬度： 度|分|秒
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// 补录  0.否 1.是
        /// </summary>
        public int IsHb { get; set; }

        /// <summary>
        /// 火灾状态，0.正常火灾 1.结束火灾 2.火灾转入  在火灾补录时，设置为1
        /// </summary>
        public int IsState { get; set; }


        /// <summary>
        /// 火灾坐标集合
        /// </summary>
        public List<FireInfoSite> Sites { get; set; }

    }

    /// <summary>
    /// 市局火灾请求体
    /// </summary>
    public class CityAddFireInfoReq:AddFireInfoReq
    {
        /// <summary>
        /// 短信集合
        /// </summary>
       public  List<SMS> SMSs { get; set; }

        /// <summary>
        /// 短消息内容
        /// </summary>
        public string SMSMessage { get; set; }
    }

    /// <summary>
    /// 短信体
    /// </summary>
    public class SMS
    {
         
        /// <summary>
        /// 短信人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string UserPhone { get; set; }

    }


    /// <summary>
    /// 坐标点集合
    /// </summary>
    public class FireInfoSite {
        /// <summary>
        /// 经度： 度|分|秒
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// 纬度： 度|分|秒
        /// </summary>
        public string Latitude { get; set; }
    }
}
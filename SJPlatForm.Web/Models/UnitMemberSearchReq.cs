﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 单位成员请求体
    /// </summary>
    public class UnitMemberSearchReq:GridPager2
    {
        /// <summary>
        /// 单位ID
        /// </summary>
        public int UnitID { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 成员名
        /// </summary>
        public string Name { get; set; }


    }
}
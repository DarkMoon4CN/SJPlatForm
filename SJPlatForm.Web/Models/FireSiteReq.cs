﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 定点火位请求体
    /// </summary>
    public class FireSiteReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 火灾ID 
        /// </summary>
        public int FireID { get; set; }


        /// <summary>
        /// 简短说明
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 操作人ID
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 操作人名字
        /// </summary>
        public string TransUserName { get; set; }

        /// <summary>
        /// 多个坐标
        /// </summary>
        public List<FireInfoSite> Sites { get; set; }

    }

    /// <summary>
    /// 更改火点请求体
    /// </summary>
    public class UpdateFireSiteReq: FireSiteReq
    {
        /// <summary>
        /// 经度： 度|分|秒
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// 纬度： 度|分|秒
        /// </summary>
        public string Latitude { get; set; }
    }

}
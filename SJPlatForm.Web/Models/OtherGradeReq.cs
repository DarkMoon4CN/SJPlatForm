﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 2,3级响应
    /// </summary>
    public class OtherGradeTemplateReq
    {

        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 区县ID
        /// </summary>
        public long LinchpinIID { get; set; }

        /// <summary>
        /// 区县名称
        /// </summary>
        public string LinchpinIName { get; set; }

        /// <summary>
        /// 级别  1，2，3
        /// </summary>
        public Nullable<int> sGradeID { get; set; }


        /// <summary>
        /// 海淀区森林公安处| 门头沟区森林公安处| 石景山区森林公安处| 丰台区森林公安处            
        /// </summary>
        public string FireResourceList { get; set; }

        /// <summary>
        /// 1239|1241|1246|1247
        /// </summary>
        public string FireResourceListID { get; set; }

        public int key { get; set; }
    }
}
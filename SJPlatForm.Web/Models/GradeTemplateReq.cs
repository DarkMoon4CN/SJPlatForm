﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 一级响应请求体
    /// </summary>
    public class GradeTemplateReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 扑火单位ID
        /// </summary>
        public int PUnitID { get; set; }
        /// <summary>
        /// 扑火单位名称
        /// </summary>
        public string PUnitName { get; set; }
        /// <summary>
        /// 单位领导
        /// </summary>
        public string Leader { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 单位类型ID
        /// </summary>

        public int PUnitTypeID { get; set; }

        /// <summary>
        /// 单位类型名称
        /// </summary>
        public string PUnitTypeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int key { get; set; }
    }
}
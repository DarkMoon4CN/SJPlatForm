﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 处置列表请求体
    /// </summary>
    public class MRecordReq: GridPager2
    {
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 是否是市局管理者
        /// </summary>
        public int IsCityManager { get; set; }

    }
}
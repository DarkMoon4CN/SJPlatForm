﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 火灾转场请求体
    /// </summary>
    public class FireTransFieldReq 
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int  ID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }
        /// <summary>
        /// 转出区县(区县)
        /// </summary>
        public string TransOutAddressCounty { get; set; }
        /// <summary>
        ///转出区县(乡镇)
        /// </summary>
        public string TransOutAddressTown { get; set; }
        /// <summary>
        /// 转出区县(村组)
        /// </summary>
        public string TransOutAddressVillage { get; set; }
        /// <summary>
        /// 转出区县
        /// </summary>
        public string TransOutAddress { get; set; }


        /// <summary>
        /// 转入区县(区县)
        /// </summary>
        public string TransInAddressCounty { get; set; }

        /// <summary>
        /// 转入区县(乡镇)
        /// </summary>
        public string TransInAddressTown { get; set; }

        /// <summary>
        /// 转入区县(村组)
        /// </summary>
        public string TransInAddressVillage { get; set; }

        /// <summary>
        /// 转入区县
        /// </summary>
        public string TransInAddress { get; set; }

        /// <summary>
        /// 转场原因
        /// </summary>
        public string TransReason { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public System.DateTime? DateTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }
    }
}
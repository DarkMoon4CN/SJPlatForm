﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.Automatic
{
    /// <summary>
    /// 办公消息请求体
    /// </summary>
    public class OfficialAutomaticReq
    {
        /// <summary>
        /// 消息ID,增加时忽略
        /// </summary>
        public int M_ID { get; set; }

        /// <summary>
        /// 发送者ID
        /// </summary>
        public int M_SendUserID { get; set; }

        /// <summary>
        /// 发送者名称
        /// </summary>
        public string M_SendUserName { get; set; }

        /// <summary>
        /// 发送者 所在区县(可选)
        /// </summary>
        public string M_SendArea { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public Nullable<System.DateTime> M_SendTime { get; set; }

        /// <summary>
        /// 发送内容
        /// </summary>
        public string M_SendContent { get; set; }

        /// <summary>
        /// 附带参数
        /// </summary>
        public string M_SendCustomData { get; set; }


        /// <summary>
        /// 接收者ID
        /// </summary>
        public int M_ReceiveUserID { get; set; }


        /// <summary>
        /// 接收者名称
        /// </summary>
        public string M_ReceiveUserName { get; set; }

        /// <summary>
        /// 接收者区县
        /// </summary>
        public string M_ReceiveArea { get; set; }

        /// <summary>
        /// 读取时间
        /// </summary>
        public Nullable<System.DateTime> M_ReplayTime { get; set; }

        /// <summary>
        /// 消息类型
        /// </summary>
        public int M_Type { get; set; }

        /// <summary>
        /// 是否已读（0:未读，1:已读）
        /// </summary>
        public int M_IsReplay { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.Automatic
{
    /// <summary>
    /// 更改消息状态
    /// </summary>
    public class OfficialAutomaticUpdateReq
    {
        /// <summary>
        /// 消息ID
        /// </summary>
        public int M_ID { get; set; }

        /// <summary>
        /// 接收者ID，联合使用 与M_ID互斥,用于清除
        /// </summary>
        public int M_ReceiveUserID { get; set; }


        /// <summary>
        /// 消息发送时间，联合使用，与M_ID互斥
        /// </summary>
        public DateTime? M_SendTime { get; set; }

        /// <summary>
        /// 消息类型，联合使用，与M_ID互斥 （备注：联合使用 设置用户在指定时间之前全部数据设置状态）
        /// </summary>
        public int M_Type { get; set; }

        /// <summary>
        /// 是否已读（0:未读，1:已读）
        /// </summary>
        public int M_IsReplay { get; set; }
    }
}
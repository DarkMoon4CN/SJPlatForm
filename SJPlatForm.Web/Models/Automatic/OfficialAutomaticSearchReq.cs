﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models.Automatic
{
    /// <summary>
    /// 办公消息查询请求体
    /// </summary>
    public class OfficialAutomaticSearchReq 
    {

        /// <summary>
        /// 接收者ID
        /// </summary>
        public int M_ReceiveUserID { get; set; }

        /// <summary>
        /// 接收者名称
        /// </summary>
        public string M_ReceiveUserName { get; set; }

        /// <summary>
        /// 接收者区县
        /// </summary>
        public string M_ReceiveArea { get; set; }


        /// <summary>
        /// 是否已读（0:未读，1:已读）
        /// </summary>
        public int M_IsReplay { get; set; }

    }
}
﻿using ChuanYe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 首报信息请求体
    /// </summary>
    public class FireFirstReportReq 
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int RID { get; set; }
        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 首报内容
        /// </summary>
        public string FirstReportContent { get; set; }

        /// <summary>
        /// 申请人
        /// </summary>
        public string Applicant { get; set; }

        /// <summary>
        /// 首报时间
        /// </summary>
        public DateTime? FirstTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>

        public string TransUser { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{

    /// <summary>
    /// 扑救力量请求体
    /// </summary>
    public class FightingForceReq
    {

        /// <summary>
        /// 扑救力量ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }
        

        /// <summary>
        /// 扑救队伍
        /// </summary>
        public List<FightingRanks> FightingRanks { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        public string Annex { get; set; }

  

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 操作人名称
        /// </summary>
        public string TransUserName { get; set; }
    }

    /// <summary>
    ///  扑救队伍
    /// </summary>
    public class FightingRanks
    {
        /// <summary>
        /// 扑救队伍所在指挥部
        /// </summary>
        public int FF_UH_ID { get; set; }


        /// <summary>
        /// 调集队伍数量
        /// </summary>
        public int RanksNum { get; set; }

    }


    /// <summary>
    /// 区县支援扑火队请求体
    /// </summary>
    public class FightingForceReplyReq
    {
        /// <summary>
        /// 火灾ID 
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 回复者的区县ID
        /// </summary>
        public int ReplyCounty { get; set; }

        /// <summary>
        /// 区县支援的扑火队信息
        /// </summary>
        public List<FightingInfo> FightingInfos { get; set; }


        /// <summary>
        /// 操作人
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 操作人名称
        /// </summary>

        public string TransUserName { get; set; }

        /// <summary>
        /// 回复人
        /// </summary>
        public string ReplyName { get; set; }

    }


    /// <summary>
    /// 区县支援扑火队信息
    /// </summary>
    public class FightingInfo
    {
        /// <summary>
        /// 队伍名称
        /// </summary>
        public string RankName { get; set; }

        /// <summary>
        /// 带队人
        /// </summary>
        public string CaptainName { get; set; }

        /// <summary>
        /// 带队人电话
        /// </summary>
        public string CaptainPhone { get;set;}

        /// <summary>
        /// 支援人数
        /// </summary>
        public int Sum { get; set; }
    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SJPlatForm.Web.Models
{
    /// <summary>
    /// 联动响应请求体
    /// </summary>
    public class ResponseReq
    {
        /// <summary>
        /// 增加时忽略
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 火灾ID
        /// </summary>
        public int FireID { get; set; }

        /// <summary>
        /// 响应单位ID集合 例:1,2,3,4
        /// </summary>
        public string UnitIDs { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        public string TransUser { get; set; }

        /// <summary>
        /// 添加人名称
        /// </summary>
        public string TransUserName { get; set; }
    }
}
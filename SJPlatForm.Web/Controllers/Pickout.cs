﻿using ChuanYe.Models;
using SJPlatForm.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Data;
using Microsoft.AspNet.SignalR;
using ChuanYe.Commom;

namespace SJPlatForm.Web.Controllers
{
    public class Pickout
    {


        private static XmlDocument xmlDoc = new XmlDocument();
        private static object objLock = new object();
        private static IHubContext hub = new Lazy<IHubContext>(() => GlobalHost.ConnectionManager.GetHubContext<FireDisposalChatHub>()).Value;
        public Pickout() { }

        public static GridPager2 GetGridPager2(dynamic entity)
        {
            GridPager2 pager = new GridPager2();
            if (entity != null)
            {
                pager.DataId = entity.DataId;
                pager.Keyword = entity.Keyword;
                pager.Sort = entity.Sort;
                pager.PageIndex = entity.PageIndex == 0 ? 1 : entity.PageIndex;
                pager.PageSize = entity.PageSize == 0 ? 10 : entity.PageSize;
                pager.StartRow = entity.StartRow;
                pager.StartTime = entity.StartTime;
                pager.EndTime = entity.EndTime;
            }
            return pager;
        }
        public static string GetMRecordTypeText(int mrType, bool isCounty = true)
        {
            string msg = string.Empty;
            switch (mrType)
            {
                case 0: msg = isCounty == true ? "首报倒计时" : "火警录入"; break;
                case 1: msg = "区县回复"; break;
                case 2: msg = "火灾首报"; break;
                case 3: msg = "误报申请"; break;
                case 4: msg = "误报审批"; break;
                case 5: msg = "火灾续报"; break;
                case 6: msg = "申请响应"; break;
                case 7: msg = "市局询问"; break;
                case 8: msg = "区县回答"; break;
                case 9: msg = "启动响应"; break;
                case 10: msg = "接收响应"; break;
                case 11: msg = "成立前指"; break;
                case 12: msg = "接收前指"; break;
                case 13: msg = "市局调集扑救力量"; break;
                case 14: msg = "调集提示"; break;
                case 15: msg = "调集回复"; break;
                case 16: msg = "火灾报灭"; break;
                case 17: msg = "火灾终报"; break;
                case 18: msg = "记录文档"; break;
                case 19: msg = "定位火点"; break;
                case 20: msg = "逐级响应"; break;
                case 21: msg = "首报未按时操作"; break;
                case 22: msg = "续报未按时操作"; break;
                case 23: msg = "终报未按时操作"; break;
                case 24: msg = "火灾转场";break;
            }
            return msg;
        }

        /// <summary>
        /// 根据现有处置类型获取消息记录类型
        /// </summary>
        /// <param name="mrType"></param>
        /// <returns></returns>
        public static int GetAutomaticTypeByMType(int mrType=0)
        {
            int tempType = 0;
            switch (mrType)
            {
                case 2:
                    tempType = 5;break;
            }
            return tempType;
        }


        public static string GetFireInfoFormat(int mrType)
        {
            string content = string.Empty;

            switch (mrType)
            {
                case 0:
                    content = "{0}接{1}报警在 {2} 发生火灾，接报案内容：{3}"; break;
                case 3:
                    content = "因 {0} 特申请误报"; break;
                case 4:
                    content = "{0}区县误报申请。原因：{1}"; break;
                case 6:
                    content = "因  {0} 申请启动三级响应";break;
                case 11:
                    content = "{0}成立前指，地址：{1}。经度为：{2} 纬度为：{3}。总指挥：{4}。指挥部成员：{5}。"; break;
                case 13:
                    content = "市局向{0}调集{1}支扑火队伍";break;
                case 17:
                    content = "{0}于{1}完成终报"; break;
                case 19:
                    content = "确定火点位置经度：{0} 纬度：{1}。";break;
                case 20:
                    content = "经 {0} 领导批准, 市局启动{1}级响应。";break;
                case 21:
                    content = "{0}没有在规定时间内进行首报"; break;
                case 22:
                    content = "{0}没有在规定时间内进行续报"; break;
                case 23:
                    content = "{0}没有在规定时间内进行终报"; break;
                case 24:
                    content = "{0}因{1}被转到{2}，此处火灾结束"; break;
            }
            return content;
        }


        /// <summary>
        /// 依据火灾ID 消息发送
        /// </summary>
        /// <param name="fireid"></param>
        /// <param name="automaticid"></param>
        ///  <param name="mrtype"></param>
        /// <param name="message"></param>
        /// <param name="isCityManager"></param>
        public static void SendAutoMaticMessage(int fireid,int mrtype, int automaticid,string message,int  isCityManager=1)
        {
            List<string> sendClients = FireDisposalChatHub.fireConnection.Where(p=>p.FireID==fireid).Select(s => s.ConnectionId).ToList();
            //hub.Clients.Clients(sendClients).sendAutomatic(fireid,mrtype,automaticid,message,isCityManager);
            //Log.Error("Pickout SendAutoMaticMessage", "消息=>"+"FireID:"+fireid+"   处置类型:"+mrtype+ "   消息ID:" + automaticid+"   消息内容:"+message+"   角色状态:"+isCityManager);
            hub.Clients.Clients(sendClients).sendAutomatic(fireid, mrtype, automaticid, message, isCityManager);
        }

        /// <summary>
        /// 依据大区ID 消息发送
        /// </summary>
        /// <param name="address"></param>
        /// <param name="fireid"></param>
        /// <param name="mrtype"></param>
        /// <param name="automaticid"></param>
        /// <param name="message"></param>
        /// <param name="isCityManager"></param>
        public static void SendAreaMessage(int address,int fireid, int mrtype, int automaticid, string message,int isCityManager= 1)
        {
            //Log.Error("Pickout SendAreaMessage", "消息=>" + "FireID:" + fireid + "   处置类型:" + mrtype + "   消息ID:" + automaticid + "   消息内容:" + message + "   角色状态:" + isCityManager);
            hub.Clients.All.sendAreaMessage(address, fireid, mrtype, automaticid, message, isCityManager);
        }



        /// <summary>
        /// 发送告警消息
        /// </summary>
        /// <param name="address">-1是市局,编号是区县</param>
        /// <param name="arid">告警消息编号</param>
        /// <param name="content">内容</param>
        /// <param name="locaID">站点编号</param>
        /// <param name="unitID">镜头编号</param>
        /// <param name="type">0.告警 1.重复火灾</param>
        /// <param name="isCityManager">是否是市局</param>
        public static void SendAlarmMessage(int address, int arid, string content, string locaID, int unitID, int type = 0, int isCityManager = 1)
        {
            hub.Clients.All.sendAlarmMessage(address, arid, content, locaID, unitID, isCityManager);
        }



        public static void AddOrEditCountDownElement(FireCountDownModel cdModel, string filePath = "")
        {
            lock (objLock)
            {
                /*satart->加入倒计时配置文件*/
                xmlDoc.Load(filePath);
                XmlNode fireXml = xmlDoc.SelectSingleNode("Fire");
                XmlNodeList allFireCountDown = xmlDoc.SelectSingleNode("Fire").ChildNodes;
                bool isExist = false;
                foreach (XmlNode item in allFireCountDown)
                {
                    var nodes = item.Attributes;
                    int fireid = nodes["FireID"].Value.ToInt();
                    int cdType = nodes["CountDownType"].Value.ToInt();
                    if (fireid == cdModel.FireID)
                    {
                        nodes["ExpireTime"].Value = cdModel.ExpireTime.ToString();
                        nodes["CountDownType"].Value = cdModel.CountDownType.ToString();
                        isExist = true;
                        break;
                    }
                }
                if (isExist == false)
                {
                    XmlElement countDownXml = xmlDoc.CreateElement("CountDown");
                    countDownXml.SetAttribute("FireID", cdModel.FireID.ToString());
                    countDownXml.SetAttribute("ExpireTime", cdModel.ExpireTime.ToString());
                    countDownXml.SetAttribute("CountDownType", cdModel.CountDownType.ToString());
                    fireXml.AppendChild(countDownXml);
                }
                xmlDoc.Save(filePath);
                /*end->加入倒计时配置文件*/
            }
        }

        public static List<FireCountDownModel> FireCountDownMaintain(string filePath)
        {
            lock (objLock)
            {
                List<FireCountDownModel> removeList = new List<FireCountDownModel>();
                bool isEdit = false;
                xmlDoc.Load(filePath);
                XmlNodeList allFireCountDown = xmlDoc.SelectSingleNode("Fire").ChildNodes;
                foreach (XmlNode item in allFireCountDown)
                {
                    var nodes = item.Attributes;
                    DateTime expireTime = nodes["ExpireTime"].Value.ToDateTime();
                    int fireid = nodes["FireID"].Value.ToInt();
                    int cdType = nodes["CountDownType"].Value.ToInt();
                    if (DateTime.Now > expireTime)
                    {
                        removeList.Add(new FireCountDownModel() { FireID = fireid, ExpireTime = expireTime, CountDownType = cdType });
                        item.ParentNode.RemoveChild(item);
                        isEdit = true;
                    }
                }
                if (isEdit)
                {
                    xmlDoc.Save(filePath);
                }
                return removeList;
            }
        }

        public static void RemoveCountDownElement(FireCountDownModel cdModel, string filePath = "")
        {
            lock (objLock)
            {
                /*satart->移除倒计时配置文件中某一条数据*/
                xmlDoc.Load(filePath);
                //XmlNode fireXml = xmlDoc.SelectSingleNode("Fire");
                XmlNodeList allFireCountDown = xmlDoc.SelectSingleNode("Fire").ChildNodes;
                bool isEdit = false;
                foreach (XmlNode item in allFireCountDown)
                {
                    var nodes = item.Attributes;
                    DateTime expireTime = nodes["ExpireTime"].Value.ToDateTime();
                    int fireid = nodes["FireID"].Value.ToInt();
                    int cdType = nodes["CountDownType"].Value.ToInt();
                    if (fireid == cdModel.FireID)
                    {
                        item.ParentNode.RemoveChild(item);
                        isEdit = true;
                    }
                }
                if (isEdit)
                {
                    xmlDoc.Save(filePath);
                }
                /*end->移除倒计时配置文件中某一条数据*/
            }

        }
    }
}
﻿using ChuanYe.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
namespace SJPlatForm.Web.Controllers
{
    public class HelloWorldController: ApiController
    {

        [HttpGet]
        public string SayHello()
        {
            var data = IndexDAL.GetAutomatic(66, 4).SerializeJSON();
            string message = "hello world ,Test connection successful,";
            message += "   server time: " + DateTime.Now.ToString();
            return message;
        }

        /// <summary>
        ///  测试参数
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public string Test(dynamic obj)
        {
            string stcd = obj.STCD;
            return stcd;
        }

        /// <summary>
        /// 异常页面输出 web.config
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Error404()
        {
            return "404";
        }
        /// <summary>
        /// 异常页面输出 web.config
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Error500()
        {
            return "500";
        }
    }
}

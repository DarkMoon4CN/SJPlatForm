﻿using ChuanYe.Commom;
using ChuanYe.DAL;
using ChuanYe.IBLL;
using ChuanYe.Models.Model;
using Microsoft.Practices.Unity;
using SJPlatForm.Web.Common;
using System;
using System.Web.Http;

namespace SJPlatForm.Web.Controllers
{

    [WebApiExceptionFilter]
    //[ApiSecurityFilter]
    public class BaseApiController : ApiController
    {
        [Dependency]
        public IUsersInfosBLL s_DAL { get; set; }
        /// <summary>
        /// 获取登录用户信息
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
        public static LoginUserModel GetLoginInfo(string UID)
        {
            try
            {
                //LoginUserModel result = (LoginUserModel)System.Web.HttpContext.Current.Cache[UID];
                //if (result.IsLogin == true)
                //{
                //    return result;
                //}
                //return new LoginUserModel();

                //List<UsersOnLion> userList = ChatHub.GetUserOnLion();
                //Log.Error("BaseApiController获取已登录人数。。。", userList.Count().ToString());
                //foreach (UsersOnLion key in userList)
                //{
                //    Log.Error("用户ID。。。" + key.UserID, key.UserName);
                //}

                //int ID = int.Parse(UID);
                //List<UsersOnLion> userList = ChatHub.GetUserOnLion().Where(m => m.UserID == ID).ToList();
                //if (userList.Count() > 0)
                //{
                //    UsersInfosDAL s_DAL = new UsersInfosDAL();
                //    return s_DAL.LoginUserInfos(int.Parse(UID));
                //}
                //else {
                //    return new LoginUserModel();
                //}

                UsersInfosDAL s_DAL = new UsersInfosDAL();
                return s_DAL.LoginUserInfos(int.Parse(UID));
            }
            catch (Exception ex)
            {
                Log.Error("BaseApiController", ex.ToString());
                return new LoginUserModel();
            }
        }





        
    }
}
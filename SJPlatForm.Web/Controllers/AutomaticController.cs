﻿using ChuanYe.Commom;
using ChuanYe.IBLL;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using SJPlatForm.Web.Models;
using SJPlatForm.Web.Models.Automatic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SJPlatForm.Web.Controllers
{
    /// <summary>
    ///  办公专门消息类
    /// </summary>
    [EnableCors("*", "*", "*")]
    public class AutomaticController : ApiController
    {
        /// <summary>
        /// 消息
        /// </summary>
        [Dependency]
        public IAutomaticBLL automaticBLL { get; set; }

        /// <summary>
        /// 增加办公消息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> Add(OfficialAutomaticReq obj)
        {

            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    Automatic info = new Automatic() {
                        M_SendUserID=obj.M_SendUserID,
                        M_SendUserName=obj.M_SendUserName,
                        M_SendArea=obj.M_SendArea,
                        M_SendContent=obj.M_SendContent,
                        M_SendTime=obj.M_SendTime,
                        M_ReceiveUserID=obj.M_ReceiveUserID,
                        M_ReceiveUserName=obj.M_ReceiveUserName,
                        M_Type=obj.M_Type,
                        M_IsReplay=obj.M_IsReplay,
                        M_ReceiveArea=obj.M_ReceiveArea,
                        M_ReplayTime=obj.M_ReplayTime,
                        M_SendCustomData=obj.M_SendCustomData,
                    };
                    int aid=automaticBLL.AddAutomatic(info);
                    result.FirstParam = aid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "AutomaticController Add Server Error";
                Log.Error("AutomaticController Add ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  消息查询
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> Get(OfficialAutomaticSearchReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                   
                    var list = automaticBLL.GetAutomatic(obj.M_ReceiveArea,obj.M_ReceiveUserID,obj.M_ReceiveUserName,obj.M_IsReplay);
                    result.FirstParam = list;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "AutomaticController AddAutomatic Server Error";
                Log.Error("AutomaticController AddAutomatic ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 消息更新
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> Update(OfficialAutomaticUpdateReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                int updateRow = 0;
                if (obj != null)
                {
                    if (obj.M_ID > 0)
                    {
                        updateRow = automaticBLL.UpdateAutomaticReplay(obj.M_ID, obj.M_IsReplay);
                    }
                    else {
                        updateRow = automaticBLL.UpdateAutomaticReplay(obj.M_ReceiveUserID, obj.M_SendTime, obj.M_Type, obj.M_IsReplay);
                    }
                    result.FirstParam = updateRow;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "AutomaticController AddAutomatic Server Error";
                Log.Error("AutomaticController AddAutomatic ", ex.ToString());
            }
            return result;
        }
    }
}

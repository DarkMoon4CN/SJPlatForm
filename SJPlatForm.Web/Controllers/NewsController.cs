﻿using ChuanYe.Commom;
using ChuanYe.IBLL;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Cors;

namespace SJPlatForm.Web.Controllers
{
    /// <summary>
    /// 信息管理
    /// </summary>
    [EnableCors("*", "*", "*")]
    public class NewsController : BaseApiController
    {

        [Dependency]
        public INewsBLL n_bll { get; set; }

        [Dependency]
        public IDocumentInfosBLL doc_bll { get; set; }

        /// <summary>
        /// 信息上传，发布附件新闻
        /// </summary>
        /// <param name="obj">新闻内容：Content，图片地址：PictureAddress，发布人：PublishPerson，发布时间：PublishDate，新闻标题：Title，信息类型：DocType，文件名称：FileName，文件路径：FilePath</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public bool NewsTempCreate(dynamic obj)
        {
            try
            {
                NewsTemp model = new NewsTemp();
                model.Content = obj.Content;
                model.IsChecked = false;
                model.PictureAddress = obj.PictureAddress;
                model.DepartmentId = GetLoginInfo(obj.key.ToString()).DepID;
                model.DepartmentName = GetLoginInfo(obj.key.ToString()).DepName;
                model.PublishCode = GetLoginInfo(obj.key.ToString()).UserCode;
                model.PublishPerson = obj.PublishPerson;
                model.PublishDate = Convert.ToDateTime(obj.PublishDate);
                model.Title = obj.Title;
                model.DocType = obj.DocType;
                model.UpLoadType = obj.UpLoadType;
                NewsAttachs AttachModel = new NewsAttachs();
                AttachModel.FileName = obj.FileName;
                AttachModel.FilePath = obj.FilePath;
                AttachModel.FileSize= obj.FileSize;
                AttachModel.FromType = 2;
                bool RT = n_bll.NewsTempCreate(model, AttachModel);
                if (RT)
                {
                    Log.Info(obj.key.ToString(), "信息上传", "信息上传成功！");
                }
                return RT;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 修改临时新闻信息
        /// </summary>
        /// <param name="obj">临时新闻ID：ID，新闻内容：Content，附件ID：NewsAttachId，图片地址：PictureAddress，发布人：PublishPerson，发布时间：PublishDate，新闻标题：Title，文件类型：DocType，原稿ID：CopyId</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public bool NewsTempUpdate(dynamic obj)
        {
            try
            {
                NewsTemp model = new NewsTemp();
                NewsTempOneResult exist = n_bll.NewsTempGetByID(Convert.ToInt32(obj.ID));
                model =exist.NewsTemp;
                model.Content = obj.Content;
                model.IsChecked = false;
                model.NewsAttachId = obj.NewsAttachId;
                model.PictureAddress = obj.PictureAddress;
                //model.DepartmentId = GetLoginInfo(obj.key.ToString()).DepID;
                //model.DepartmentName = GetLoginInfo(obj.key.ToString()).DepName;
                //model.PublishCode = GetLoginInfo(obj.key.ToString()).UserCode;
                model.PublishPerson = obj.PublishPerson;
                model.PublishDate = Convert.ToDateTime(obj.PublishDate);
                model.Title = obj.Title;
                model.DocType = obj.DocType;
                model.CopyId = obj.CopyId;
                model.UpLoadType = obj.UpLoadType;
                bool RT= n_bll.NewsTempUpdate(model);
                Log.Info(obj.key.ToString(), "发布新闻", "临时新闻修改成功！");
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return false;
            }
        }
        /// <summary>
        /// 删除单个临时新闻
        /// </summary>
        /// <param name="obj">临时新闻信息ID：ID</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public int NewsTempDelete(dynamic obj)
        {
            try
            {
                int RT= n_bll.NewsTempDelete(Convert.ToInt32(obj.ID));
                if (RT > 0)
                {
                    Log.Info(obj.key.ToString(), "发布新闻", "临时新闻修改成功！");
                }
                return RT;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return -1;
            }
        }
        /// <summary>
        /// 批量删除临时新闻
        /// </summary>
        /// <param name="obj">新闻信息ID：IDS：格式：ID|ID|ID</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public int NewsTempDeleteAll(dynamic obj)
        {
            try
            {
                int result = 0;
                string IDSStr = obj.IDS;
                string[] ids = IDSStr.Split('|');
                foreach (var id in ids)
                {
                    result = n_bll.NewsTempDelete(Convert.ToInt32(id));
                }
                if (result > 0)
                {
                    Log.Info(obj.key.ToString(), "发布新闻", "临时新闻修改成功！");
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return -1;
            }
        }
        /// <summary>
        /// 发布新闻
        /// </summary>
        /// <param name="obj">新闻ID：ID，新闻类型：NewsType 格式：Type|Type|Type</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public int NewsInfsPublic(dynamic obj)
        {
            try
            {
                int result = 0;
                NewsInfs model = new NewsInfs();
                model.ID = obj.ID;
                string NewsType = obj.NewsType;
                string[] Types = NewsType.Split('|');
                foreach(string Type in Types)
                {
                    model.NewsType = Convert.ToInt32(Type);
                    result+= n_bll.NewsInfsPublic(model);
                }
                if (result > 0)
                {
                    Log.Info(obj.key.ToString(), "发布新闻", "新闻发布成功！");
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return -1;
            }
        }

        /// <summary>
        /// 临时新闻详情
        /// </summary>
        /// <param name="obj">新闻信息ID：ID</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsTempOneResult NewsTempGetByID(dynamic obj)
        {
            try
            {
                //FromType: 1.NewsInfs 2.NewsTemp 
                return n_bll.NewsTempGetByID(Convert.ToInt32(obj.ID));
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsTempOneResult();
            }
        }
        /// <summary>
        /// 发布新闻数据详情
        /// </summary>
        /// <param name="obj">新闻信息ID：ID</param>
        /// <returns></returns>
        public NewsInfsOneResult NewsInfsGetByID(dynamic obj)
        {
            try
            {
                NewsInfsOneResult RT = new NewsInfsOneResult();
                NewsInfs model = n_bll.NewsInfsGetByID(Convert.ToInt32(obj.ID));
                NewsInfsModel result = new NewsInfsModel();
                result.Content = model.Content;
                result.DepartmentId = model.DepartmentId;
                result.DepartmentName = model.DepartmentName;
                result.ID = model.ID;
                result.IsChecked = model.IsChecked;
                result.NewsAttachId = model.NewsAttachId;
                result.NewsType = model.NewsType;
                result.ParentID = model.ParentID;
                result.PictureAddress = model.PictureAddress;
                result.PublishCode = model.PublishCode;
                result.PublishDate = model.PublishDate;
                result.PublishPerson = model.PublishPerson;
                result.Title = model.Title;
                result.UpLoadType = model.UpLoadType;
                switch (result.NewsType)
                {
                    case 1:
                        result.NewsTypeName = "经验交流";
                        break;
                    case 2:
                        result.NewsTypeName = "防火信息";
                        break;
                    case 3:
                        result.NewsTypeName = "通知通报";
                        break;
                    case 4:
                        result.NewsTypeName = "专项行动";
                        break;
                    case 5:
                        result.NewsTypeName = "队伍建设";
                        break;
                    case 6:
                        result.NewsTypeName = "保险";
                        break;
                    case 7:
                        result.NewsTypeName = "领导讲话";
                        break;
                }
                RT.NewsInfs = result;
                RT.NewsAttachsList= n_bll.NewsAttachsGetByNewsID(Convert.ToInt32(obj.ID));
                return RT;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsInfsOneResult();
            }
        }
        /// <summary>
        /// 撤销单个新闻
        /// </summary>
        /// <param name="obj">新闻信息ID：ID</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public int NewsInfsDelete(dynamic obj)
        {
            try
            {
                int result= n_bll.NewsInfsDelete(Convert.ToInt32(obj.ID));
                if (result > 0)
                {
                    Log.Info(obj.key.ToString(), "已发布新闻", "新闻撤销成功！");
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return -1;
            }
        }
        /// <summary>
        /// 批量撤销新闻
        /// </summary>
        /// <param name="obj">新闻信息ID：IDS：格式ID|ID|ID</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public int NewsInfsAllDelete(dynamic obj)
        {
            try
            {
                int result = 0;
                string IDSStr = obj.IDS;
                string[] ids = IDSStr.Split('|');
                foreach (var id in ids)
                {
                    result = n_bll.NewsInfsDelete(Convert.ToInt32(id));
                }
                if (result > 0)
                {
                    Log.Info(obj.key.ToString(), "已发布新闻", "新闻批量撤销成功！");
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return -1;
            }
        }

        ///// <summary>
        ///// 保存新闻附件
        ///// </summary>
        ///// <param name="obj">附件名称：FileName，附件路径：FilePath，附件标题：FileTitle</param>
        ///// <returns></returns>
        //[System.Web.Http.HttpPost]
        //public int NewsAttachsCreate(dynamic obj)
        //{
        //    try
        //    {
        //        NewsAttachs model = new NewsAttachs();
        //        model.FileName = obj.FileName;
        //        model.FilePath = obj.FilePath;
        //        model.FileTitle = obj.FileTitle;
        //        //model.NewsId = obj.NewsId;
        //        return n_bll.NewsAttachsCreate(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error("NewsController", ex.ToString());
        //        return -1;
        //    }
        //}

        /// <summary>
        /// 保存新闻阅读状态表
        /// </summary>
        /// <param name="obj">新闻ID：NewsId,新闻类型：NewsType</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public bool NewsReadStateCreate(dynamic obj)
        {
            try
            {
                NewsReadState model = new NewsReadState();
                model.NewsId = obj.NewsId;
                model.Access = GetLoginInfo(obj.key.ToString()).UserCode;
                model.NewsType = obj.NewsType;
                model.CreateDate = DateTime.Now;
                bool result= n_bll.NewsReadStateCreate(model);
                if (result)
                {
                    Log.Info(obj.key.ToString(), "信息管理", "新闻阅读状态修改成功！");
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return false;
            }
        }
        /// <summary>
        /// 信息管理，分页查询临时新闻列表
        /// </summary>
        /// <param name="obj">开始时间：stateTime，结束时间：endTime，部门ID：DepId，类型：DocType，是否发布：IsChecked，页数：page，行数：rows</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsTempListResult NewsTempGetList(dynamic obj)
        {
            try
            {
                NewsTempListSearch search = new NewsTempListSearch();

                search.DepId = Convert.ToInt32(obj.DepId);

                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = Convert.ToDateTime("2013-10-31 00:00:00");
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }

                int str = obj.IsChecked;
                switch (str)
                {
                    case 1:
                        search.IsChecked = true;
                        break;
                    case 0:
                        search.IsChecked = false;
                        break;
                    case 2:
                        search.IsChecked = null;
                        break;
                }

                search.DocType = obj.DocType;
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                return n_bll.NewsTempGetList(search, Pager);
            } catch (Exception ex) {
                Log.Error("NewsController", ex.ToString());
                return new NewsTempListResult();
            }
        }

        /// <summary>
        /// 信息管理，分页查询临时新闻列表
        /// </summary>
        /// <param name="obj">开始时间：stateTime，结束时间：endTime，部门ID：DepId，类型：DocType，是否发布：IsChecked，页数：page，行数：rows</param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public NewsTempListResult NewsTempGetList2(dynamic obj)
        {
            try
            {
                NewsTempListSearch search = new NewsTempListSearch();

                search.DepId = Convert.ToInt32(obj.DepId);

                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = Convert.ToDateTime("2013-10-31");
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }

                int str = obj.IsChecked;
                switch (str)
                {
                    case 1:
                        search.IsChecked = true;
                        break;
                    case 0:
                        search.IsChecked = false;
                        break;
                    case 2:
                        search.IsChecked = null;
                        break;
                }

                search.DocType = obj.DocType;
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                return n_bll.NewsTempGetList(search, Pager);
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsTempListResult();
            }
        }

        /// <summary>
        /// 市局已发布新闻列表
        /// </summary>
        /// <param name="obj">部门ID：DepId，开始时间：stateTime，结束时间：endTime，新闻类型：NewsType，页数：page，行数：rows</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsInfsResult NewsInfsPublicGetList(dynamic obj)
        {
            try
            {
                NewsInfsPublicSearch search = new NewsInfsPublicSearch();
                search.DepId = Convert.ToInt32(obj.DepId);
                //search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                //search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = SJPlatForm.Web.Common.ConfigHelper.StateTime;
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.NewsType = obj.NewsType;
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                return n_bll.NewsInfsPublicGetList2(search, Pager);
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsInfsResult();
            }
        }
        /// <summary>
        /// 区县已发布新闻列表
        /// </summary>
        /// <param name="obj">开始时间：stateTime，结束时间：endTime，新闻类型：NewsType，页数：page，行数：rows</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsInfsResult NewsInfsPublicGetListForOne(dynamic obj)
        {
            try
            {
                NewsInfsPublicSearch search = new NewsInfsPublicSearch();
                search.DepId = GetLoginInfo(obj.key.ToString()).DepID;
                //search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                //search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = SJPlatForm.Web.Common.ConfigHelper.StateTime;
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.NewsType = obj.NewsType;
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                return n_bll.NewsInfsPublicGetList(search, Pager);
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsInfsResult();
            }
        }

        /// <summary>
        /// 根据TypeID 进行分组查询
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public List<dynamic> NewsInfsPublicGetListForGroup(dynamic obj)
        {
            try
            {
                NewsInfsPublicGroupSearch search = new NewsInfsPublicGroupSearch();
                search.DepId = GetLoginInfo(obj.key.ToString()).DepID;
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                    search.stateTime = search.stateTime.AddSeconds(1);
                }
                else
                {
                    search.stateTime = SJPlatForm.Web.Common.ConfigHelper.StateTime;
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.NewsTypes = new List<int>();
                if (obj.NewsTypes != null)
                {
                    search.NewsTypes = ((JArray)obj.NewsTypes).ToObject<List<int>>();
                }
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                var result=n_bll.NewsInfsPublicGetListForGroup(search, Pager);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// 市局新闻信息统计
        /// </summary>
        /// <param name="obj">开始时间：stateTime，结束时间：endTime，页数：page，行数：rows</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsInfsStatisticsResult NewsInfsStatisticsGet(dynamic obj)
        {
            try
            {
                NewsInfsStatisticsSearch search = new NewsInfsStatisticsSearch();
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = Convert.ToDateTime("2013-10-31"); 
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.DepID = 0;
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                var temp = n_bll.NewsInfsStatisticsGet(search, Pager);
                var allDep = doc_bll.DepartmentsManagerCode();
                NewsInfsStatisticsResult result = new NewsInfsStatisticsResult();
                result.NewsInfsList = temp.NewsInfsList !=null ? temp.NewsInfsList: new List<SP_NewsInfsStatistics_Result>();

                //allDep = allDep.OrderBy(p => p.Sort).ToList();
                var sortData = new List<SP_NewsInfsStatistics_Result>();
                foreach (var item in allDep)
                {
                    var t = temp.NewsInfsList.Where(p => item.ID == p.ID).FirstOrDefault();
                    if (t != null)
                    {
                        SP_NewsInfsStatistics_Result r = new SP_NewsInfsStatistics_Result();
                        r.ID = item.ID;
                        r.DepName = item.DepName;
                        r.AllCount = t != null ? t.AllCount : 0;
                        r.PublicCount = t != null ? t.PublicCount : 0;
                        r.PublicPercent = t != null ? t.PublicPercent : 0;
                        sortData.Add(r);
                    }
                    else
                    {
                        SP_NewsInfsStatistics_Result r = new SP_NewsInfsStatistics_Result();
                        r.ID = item.ID;
                        r.DepName = item.DepName;
                        r.AllCount =  0;
                        r.PublicCount = 0;
                        r.PublicPercent = 0;
                        sortData.Add(r);
                    }
                }

                result.NewsInfsList = sortData;
                result.totalRows = sortData.Count;
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsInfsStatisticsResult();
            }
        }
        /// <summary>
        /// 区县新闻信息统计
        /// </summary>
        /// <param name="obj">开始时间：stateTime，结束时间：endTime，页数：page，行数：rows</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsInfsStatisticsResult NewsInfsStatisticsGetOne(dynamic obj)
        {
            try
            {
                NewsInfsStatisticsSearch search = new NewsInfsStatisticsSearch();
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = SJPlatForm.Web.Common.ConfigHelper.StateTime;
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.DepID = GetLoginInfo(obj.key.ToString()).DepID;
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                var temp = n_bll.NewsInfsStatisticsGet(search, Pager);
                var allDep = doc_bll.DepartmentsManagerCode();
                NewsInfsStatisticsResult result = new NewsInfsStatisticsResult();
                result.NewsInfsList = temp.NewsInfsList != null ? temp.NewsInfsList : new List<SP_NewsInfsStatistics_Result>();
                foreach (var item in allDep)
                {
                    var t = temp.NewsInfsList.Where(p => item.ID == p.ID).FirstOrDefault();
                    if (t == null)
                    {
                        SP_NewsInfsStatistics_Result r = new SP_NewsInfsStatistics_Result();
                        r.ID = item.ID;
                        r.DepName = item.DepName;
                        r.AllCount = t != null ? t.AllCount : 0;
                        r.PublicCount = t != null ? t.PublicCount : 0;
                        r.PublicPercent = t != null ? t.PublicPercent : 0;
                        result.NewsInfsList.Add(r);
                    }

                }
                result.NewsInfsList = result.NewsInfsList.OrderByDescending(o => o.PublicPercent).ToList();
                result.totalRows = result.NewsInfsList.Count;
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsInfsStatisticsResult();
            }
        }
        /// <summary>
        /// 获取部门各信息类型数量统计
        /// </summary>
        /// <param name="obj">开始时间：stateTime，结束时间：endTime，部门ID：DepId</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public SP_NewsInfsStatisticsByDepId_Result NewsInfsStatisticsByDepIdGet(dynamic obj)
        {
            try
            {
                NewsInfsStatisticsByDepIdSearch search = new NewsInfsStatisticsByDepIdSearch();
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = SJPlatForm.Web.Common.ConfigHelper.StateTime;
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.DepId = obj.DepId;
                return n_bll.NewsInfsStatisticsByDepIdGet(search);
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new SP_NewsInfsStatisticsByDepId_Result();
            }
        }
        /// <summary>
        /// 市局获取部门各信息类型信息列表
        /// </summary>
        /// <param name="obj">部门ID：DepId，开始时间：stateTime，结束时间：endTime，新闻类型：NewsType，页数：page，行数：rows</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsInfsGetListByDepIdResult NewsInfsGetListByDepId(dynamic obj)
        {
            try
            {
                NewsInfsGetListByDepIdSearch search = new NewsInfsGetListByDepIdSearch();
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = Convert.ToDateTime("2013-10-31");
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.DepId = Convert.ToInt32(obj.DepId);
                search.NewsType = obj.NewsType == null ? 0 : obj.NewsType;
                GridPager Pager = new GridPager();
                Pager.page = obj.page;
                Pager.rows = obj.rows;
                return n_bll.NewsInfsGetListByDepId(search, Pager);
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsInfsGetListByDepIdResult();
            }
        }

        /// <summary>
        /// 区县获取新闻信息统计
        /// </summary>
        /// <param name="obj">开始时间：stateTime 默认""，结束时间：endTime 默认""</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsInfsStatisticsResult NewsInfsCountyStatisticsGet(dynamic obj)
        {
            try
            {
                NewsInfsStatisticsResult result = new NewsInfsStatisticsResult();
                NewsInfsCountyStatisticsSearch search = new NewsInfsCountyStatisticsSearch();
                if (obj.stateTime != null && obj.stateTime != "")
                {
                    search.stateTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    search.stateTime = Convert.ToDateTime("2013-10-31");
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    search.endTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    search.endTime = DateTime.Now;
                }
                search.DepId = GetLoginInfo(obj.key.ToString()).DepID;
                SP_NewsInfsStatistics_Result model = new SP_NewsInfsStatistics_Result();
                SP_NewsInfsCountyStatistics_Result RT= n_bll.NewsInfsCountyStatisticsGet(search);
                model.AllCount = RT==null?0:RT.AllCount;
                model.DepName =  RT==null? GetLoginInfo(obj.key.ToString()).DepName : RT.DepName;
                model.ID = RT==null?0:RT.ID;
                model.PublicCount = RT == null ? 0 : RT.PublicCount;
                model.PublicPercent = RT == null ? 0 : RT.PublicPercent;
                result.NewsInfsList = new List<SP_NewsInfsStatistics_Result>();
                result.NewsInfsList.Add(model);
                result.totalRows = 1;
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsInfsStatisticsResult();
            }
        }

        /// <summary>
        /// 获取新闻已发布新闻 调整时间:2018/12/22
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public NewsTempListResult NewsTempGetList3(dynamic obj)
        {
            try
            {
                NewsTempListResult  result= new NewsTempListResult();
                GridPager2 pager = new GridPager2();
                pager.PageIndex = obj.page;
                pager.PageSize = obj.rows;

                if (obj.stateTime != null && obj.stateTime != "")
                {
                    pager.StartTime = ValueConvert.StateTimeConvert(obj.stateTime);
                }
                else
                {
                    pager.StartTime = Convert.ToDateTime("2013-10-31");
                }
                if (obj.endTime != null && obj.endTime != "")
                {
                    pager.EndTime = ValueConvert.EndTimeConvert(obj.endTime);
                }
                else
                {
                    pager.EndTime = DateTime.Now;
                }
                View_NewsTemp info = new View_NewsTemp() {
                     DepartmentId= Convert.ToInt32(obj.DepId),
                     IsChecked= obj.IsChecked,
                     DocType=obj.DocType,
                };
                var temp=n_bll.NewTempGetList(info, pager);

                //保证与原来的接口(NewsTempGetList)返回数据一样
                List<SP_NewsTempList_Result> list = new List<SP_NewsTempList_Result>();
                foreach (var item in temp)
                {
                    list.Add(new SP_NewsTempList_Result() {
                         ID=item.ID,
                         Title=item.Title,
                         NewsAttachId=item.NewsAttachId,
                         PublishCode=item.PublishCode,
                         PublishDate=item.PublishDate,
                         PublishPerson=item.PublishPerson,
                         CopyId=item.CopyId,
                         DepartmentName=item.DepartmentName,
                         DocType=item.DocType,
                         FilePath=item.FilePath,
                         IsChecked=item.IsChecked,
                    });
                }
                result.NewsTempList = list;
                result.totalRows = pager.TotalPages;
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("NewsController", ex.ToString());
                return new NewsTempListResult();
            }
        } 


        public string Options()
        {
            return null; // HTTP 200 response with empty body
        }
    }
}
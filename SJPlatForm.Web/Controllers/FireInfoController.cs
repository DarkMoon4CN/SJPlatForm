﻿using ChuanYe.Commom;
using ChuanYe.IBLL;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using SJPlatForm.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data;
using ChuanYe.Utils;
using SJPlatForm.Web.Common;
using System.Web.Http.Cors;
using System.Threading.Tasks;
using System.Xml;
using System.Threading;
using ChuanYe.Models.Model;
using SJPlatForm.Web.Commom;
using Microsoft.AspNet.SignalR;
using System.Net.Http;
using System.Net;
using System.Web;
using System.IO;
using iTextSharp.text;
using System.Net.Http.Headers;
using iTextSharp.text.pdf;
namespace SJPlatForm.Web.Controllers
{
    
    /// <summary>
    /// 火灾处置
    /// </summary>
  
    [EnableCors("*", "*", "*")]
    public class FireInfoController : ApiController
    {
        #region 依赖注入及配置文件
        /// <summary>
        ///  倒计时配置文件路径位置
        /// </summary>
        private readonly string FCD_FILEPATH = AppDomain.CurrentDomain.BaseDirectory +"FireCountDown.xml";
        /// <summary>
        /// PDF 存放位置
        /// </summary>
        public readonly  string PDF_PATH = AppDomain.CurrentDomain.BaseDirectory+"PDF";
        /// <summary>
        /// 视频文件路径位置
        /// </summary>
        public readonly string VIDEO_PATH = Settings.Instance.GetSetting("VideoPath");
        /// <summary>
        ///  倒计时的XML
        /// </summary>
        private XmlDocument xmlDoc = new XmlDocument();
        /// <summary>
        /// 火灾处置主业务块
        /// </summary>
        [Dependency]
        public IFireInfoBLL bll { get; set; }

        /// <summary>
        /// 市局区县地区业务块
        /// </summary>
        [Dependency]
        public ILinchpinBLL linchpinBll { get; set; }

        /// <summary>
        /// 用户信息依赖块
        /// </summary>
        [Dependency]
        public IUsersInfosBLL userInfoBll { get; set; }

        /// <summary>
        /// 用户信息依赖块
        /// </summary>
        [Dependency]
        public IMemberInfoBLL memberInfoBll { get; set; }

        /// <summary>
        /// 火灾统计块
        /// </summary>
        [Dependency]
        public IFireStatisticsBLL fireStatisticsBLL { get; set; }

        /// <summary>
        /// 短信块
        /// </summary>
        [Dependency]
        public ISms_SendInfoBLL m_bll { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Dependency]
        public ISafetyInfoBLL s_bll { get; set; }
        #endregion

        #region 火灾列表
        /// <summary>
        /// (市局+区县)--火灾列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, int> GetFireInfoByPager(FireInfoReq obj)
        {
            ResultDto<dynamic, int> result = new ResultDto<dynamic, int>();
            List<F_FireInfo> list = null;
            try
            {
                GridPager2 pager = Pickout.GetGridPager2(obj);
                if (pager.StartTime != null)
                {
                    pager.StartTime = pager.StartTime.StateTimeConvert();
                }
                if (pager.EndTime != null)
                {
                    pager.EndTime = pager.EndTime.EndTimeConvert();
                }
                list = bll.ListByPager(obj.AddressCountys, obj.FireSate, pager);

                List<int> fireIDs = list.Select(s => s.F_FireID).ToList();

                //2018-12-04 增加逻辑，区县不显示文档
                //如果 obj.AddressCountys 如果不为空，即设定为区县调用
                int searchLevel = obj.AddressCountys != null && obj.AddressCountys.Length >0? 1 : 0;
                var mrList = bll.MRListByFireIDs(fireIDs, searchLevel);
                var finalList = bll.GetFireFinalReport(fireIDs);
                var fireAttachs =  bll.GetFireAttachs(fireIDs);

                
                List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(2);
                var data = from info in list
                           join mr in mrList
                           on info.F_FireID equals mr.F_FireID
                           into a
                           from b in a.DefaultIfEmpty()
                           join f in finalList on info.F_FireID equals f.F_FireID
                           into e
                           from g in e.DefaultIfEmpty()
                           select new
                           {
                               info.F_FireID,
                               info.F_Address,
                               info.F_FireName,
                               info.F_ReporterPhone,
                               info.F_ReporterName,
                               info.F_ReporterUnit,
                               info.F_HappenDatetime,
                               info.F_AlarmTime,
                               info.F_AddressCounty,
                               MR_Type = b==null ? null: b.MR_Type,
                               MR_Message = b==null? string.Empty:Pickout.GetMRecordTypeText(b.MR_Type.Value),
                               FFR_Why = g==null?null:g.FFR_Why,
                               FFR_Type= g==null?null:g.FFR_Type,
                               FFR_TypeName= g==null?null:(dtbList.Where(p => p.id == g.FFR_Type).FirstOrDefault()==null?string.Empty:
                                                           dtbList.Where(p => p.id == g.FFR_Type).FirstOrDefault().DName),
                               IsExistVideo = fireAttachs.Where(p => p.F_FireID == info.F_FireID && p.Type == 0).Count()>0 ?1 :0,
                               VideoAttachs= fireAttachs.Where(p => p.F_FireID == info.F_FireID && p.Type==0).ToList(),//0.视频 1.鉴定 2.立案
                               IdentifyAttachs = fireAttachs.Where(p => p.F_FireID == info.F_FireID && p.Type == 1).ToList(),//0.视频 1.鉴定 2.立案
                               CaseAttachs = fireAttachs.Where(p => p.F_FireID == info.F_FireID && p.Type == 2).ToList(),//0.视频 1.鉴定 2.立案
                               info.F_IsHb,
                           };
                var temp = data.ToList();
                //获取续报终报消息是否得到回复
                var maList = bll.GetFireAutomatic(fireIDs, new List<int>() { });
                List<dynamic> res = new List<dynamic>();
                foreach (var item in data)
                {
                    string color = "red";
                    //判定状态字体颜色
                    if (item.MR_Type!=null && item.MR_Type == 2)
                    {
                        //首报属于过期状态
                        var tempData = maList.Where(p => p.F_FireID == item.F_FireID && p.M_Type == 17).FirstOrDefault();
                        color = tempData == null ? "green" : "red";
                    }
                    else if (item.MR_Type != null && item.MR_Type == 5)
                    {
                        //续报属于过期状态
                        var tempData = maList.Where(p => p.F_FireID == item.F_FireID && p.M_Type == 18).FirstOrDefault();
                        color = tempData == null ? "green" : "red";
                    }
                    else if (item.MR_Type != null && item.MR_Type == 17)
                    {
                        //终报属于过期状态
                        var tempData = maList.Where(p => p.F_FireID == item.F_FireID && p.M_Type == 19).FirstOrDefault();
                        color = tempData == null ? "green" : "red";
                    }
                    else
                    {
                        color = "red";
                    }
                    res.Add(new
                    {
                        F_FireID = item.F_FireID,
                        F_Address = item.F_Address,
                        F_FireName = item.F_FireName,
                        F_ReporterPhone = item.F_ReporterPhone,
                        F_ReporterName = item.F_ReporterName,
                        F_ReporterUnit = item.F_ReporterUnit,
                        F_HappenDatetime = item.F_HappenDatetime,
                        F_AlarmTime = item.F_AlarmTime,
                        F_AddressCounty = item.F_AddressCounty,
                        MR_Type = item.MR_Type,
                        MR_Message = item.MR_Message,
                        Color = color,
                        FFR_Why=item.FFR_Why,
                        IsExistVideo = item.IsExistVideo,
                        VideoAttachs=item.VideoAttachs,
                        IdentifyAttachs=item.IdentifyAttachs,
                        CaseAttachs=item.CaseAttachs,
                        F_IsHb = item.F_IsHb == null ? 0 : item.F_IsHb,
                        FFR_Type=item.FFR_Type,
                        FFR_TypeName=item.FFR_TypeName
                    });
                }

                result.FirstParam = res;
                result.SecondParam = pager.TotalRows;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireInfoByPager Server Error";
                Log.Error("FireInfoController GetFireInfoByPager", ex.ToString());
            }
            return result;
        }
        #endregion

        #region 区县

        #region 火灾录入

        /// <summary>
        ///  区县--增加火灾(火灾录入)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireInfo(AddFireInfoReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null
                    && CheckRequest.IsNumber(obj.AddressCounty)
                    && CheckRequest.IsNumber(obj.AddressTown)
                    && CheckRequest.IsNumber(obj.AddressVillage)
                    && (!string.IsNullOrEmpty(obj.FireName)
                    && !string.IsNullOrEmpty(obj.AlarmContent)
                    && obj.HappenDatetime != null
                    && obj.AlarmTime != null))
                {
                    F_FireInfo info = new F_FireInfo();
                    info.F_FireName = obj.FireName;

                    info.F_HappenDatetime = obj.HappenDatetime;
                    info.F_TransDatetime = DateTime.Now;

                    info.F_AlarmTime = obj.AlarmTime;
                    info.F_AlarmContent = obj.AlarmContent;
                    info.F_Alarm = obj.Alarm;

                    info.F_AddressCounty = obj.AddressCounty;
                    info.F_AddressTown = obj.AddressTown;
                    info.F_AddressVillage = obj.AddressVillage;

                    List<Linchpin> temp= linchpinBll.GetLinchpin(new List<long>() {obj.AddressCounty.ToLong(),obj.AddressTown.ToLong(),obj.AddressVillage.ToLong() });
                    string address = string.Empty;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressCounty.ToLong()).FirstOrDefault().LinchpinName;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressTown.ToLong()).FirstOrDefault().LinchpinName;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressVillage.ToLong()).FirstOrDefault().LinchpinName;
                    info.F_Address = address;

                    info.F_ReporterName = obj.ReporterName;
                    info.F_ReporterUnit = obj.ReporterUnit;
                    info.F_ReporterWorknum = obj.ReporterWorknum;
                    info.F_ReporterPhone = obj.ReporterPhone;

                    info.F_TransUser = obj.TransUser;
                    info.F_TransIP = CheckRequest.GetWebClientIp();
                    info.F_IsChange = 0;
                    info.F_IsState = obj.IsState;
                    info.F_IsHb = obj.IsHb;

                    int fireID = bll.Add(info);
                    if (fireID != 0)
                    {
                        //添加火灾消息
                        var aRes=bll.AddFireAutomaticForInit(info);

                        //添加火灾处置信息
                        M_Record mr = new M_Record();
                        mr.F_FireID = fireID;
                        mr.MR_HandleTime = info.F_HappenDatetime;
                        mr.MR_Type = (int)RecordType.火警录入;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        string happenDatetime = info.F_HappenDatetime.Value.ToString("yyyy年MM月dd日HH时mm分");
                        string mrContent = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), happenDatetime, info.F_Alarm, info.F_Address, info.F_AlarmContent);
                        mr.MR_Content = mrContent;
                        mr.MR_AddTime = info.F_TransDatetime;
                        mr.MR_AddIP = info.F_TransIP;
                        mr.MR_Applicant = info.F_Alarm;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);

                        ////加入定位火点
                        //F_FireSite fireSiteInfo = new F_FireSite();
                        //fireSiteInfo.F_FireID = obj.FireID;
                        //fireSiteInfo.FS_Latitude = obj.Latitude;
                        //fireSiteInfo.FS_Longitude = obj.Longitude;
                        //fireSiteInfo.FS_Cont =string.Empty;
                        //fireSiteInfo.FS_TransDatetime = DateTime.Now;
                        //fireSiteInfo.FS_HandleTime = info.F_TransDatetime;
                        //fireSiteInfo.FS_TransUser = obj.TransUser;
                        //fireSiteInfo.FS_TransIP = CheckRequest.GetWebClientIp();
                        //bll.AddFireSite(fireSiteInfo);
                        //增加多个定位火点
                        foreach (var item in obj.Sites)
                        {
                            F_FireSite  tempFireSiteInfo = new F_FireSite();
                            tempFireSiteInfo.F_FireID = fireID;
                            tempFireSiteInfo.FS_Latitude = item.Latitude;
                            tempFireSiteInfo.FS_Longitude = item.Longitude;
                            tempFireSiteInfo.FS_Cont = string.Empty;
                            tempFireSiteInfo.FS_TransDatetime = DateTime.Now;
                            tempFireSiteInfo.FS_HandleTime = info.F_TransDatetime;
                            tempFireSiteInfo.FS_TransUser = obj.TransUser;
                            tempFireSiteInfo.FS_TransIP = CheckRequest.GetWebClientIp();
                            bll.AddFireSite(tempFireSiteInfo);
                        }


                        int mid = aRes.Key;
                        string mcontent = aRes.Value;
                        //(全局发送慎用)向市局进行发送
                        Pickout.SendAreaMessage(-1,fireID,mr.MR_Type.Value, mid, mcontent);

                    }

                    //倒计时配置文件
                    Pickout.AddOrEditCountDownElement(new FireCountDownModel() {
                                                 FireID =fireID,
                                                 ExpireTime = info.F_TransDatetime.Value.AddMinutes(5),
                                                 CountDownType =(int)RecordType.首报未按时操作},FCD_FILEPATH);
                    result.FirstParam = fireID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireInfo Server Error";
                Log.Error("FireInfoController AddFireInfo", ex.ToString());
            }

            return result;
        }


        /// <summary>
        /// 区县--根据火灾ID 获取火灾详情页中的 火灾数据，处置数据
        /// </summary>
        /// <param name="obj">obj.FireID</param>
        /// <returns>Data:FireInfo  Second:M_Record Value:M_Record->RowCount</returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic, dynamic, dynamic> GetFireInfoByFireID(FireDetailReq obj)
        {
            ResultDto<dynamic,dynamic, dynamic, dynamic> result = new ResultDto<dynamic, dynamic, dynamic, dynamic>();
            try
            {
                if (obj != null && obj.FireID>0)
                {
                    F_FireInfo fireDetail = bll.FireInfoDetail(obj.FireID);
                    if (fireDetail != null)
                    {
                        //火灾记录处置分页
                        GridPager2 pager = Pickout.GetGridPager2(obj);
                        List<M_Record> mrList = bll.MRListByFireID(fireDetail.F_FireID, pager);

                        //获取页面权限
                        List<int> selectedType = new List<int>() {0,3,10};
                        var temp=bll.CKRecordTypeByTypes(obj.FireID, selectedType);
                        var typeResult = selectedType.Select(s => new { MR_Type = s,TypeText=Pickout.GetMRecordTypeText(s),
                                          Auth = (temp.Where(p=>p.MR_Type==s).FirstOrDefault()!=null ? 1:0 )});
                        result.FirstParam =fireDetail;
                        result.SecondParam =mrList;
                        result.ThirdParam =pager.TotalRows;
                        result.FourthParam = typeResult;
                        result.Status = 1;
                        result.Message = "Request Is Success";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireInfoByFireID Server Error";
                Log.Error("FireInfoController GetFireInfoByFireID", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 区县--简化区县启动4级响应  版本时间:20200916
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddFourStepResponseSimple(FourStepResponseSimpleReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    int responseGrade = 4;
                    F_StepResponse fsInfo = bll.StepResponseDetail(obj.FireID);
                    if (fsInfo != null)
                    {
                        result.FirstParam = 0;
                        result.Status = 0;
                        result.Message = string.Format("StepResponse Started(Level:{0}", fsInfo.SR_ResponseGrade);
                    }

                    //获取响应条件列表
                    List<F_StartCondition> scList = bll.GetStartCondition(responseGrade);

                    F_StepResponse info = new F_StepResponse();
                    info.F_FireID = obj.FireID;
                    info.SR_ResponseGrade = responseGrade;
                    info.SR_StartDatetime = DateTime.Now;
                    info.SR_Starter = obj.Starter;
                    info.SR_InstructionContent = string.Format("{0}火灾，已启动{1}级响应",fireInfo.F_FireName ,CheckRequest.GetLoumaNumber(responseGrade));
                    info.SR_StartCondition = scList==null?string.Empty: scList.FirstOrDefault().SC_Content;
                    info.SR_TransDatetime = DateTime.Now;
                    info.SR_HandleTime = info.SR_TransDatetime;
                    info.SR_TransUser = fireInfo.F_TransUser;
                    info.SR_TransIP = CheckRequest.GetWebClientIp();
                    int srID = bll.AddStepResponse(info);

                    //临时添加人员
                    string stContent = string.Empty;
                    if (obj.FTPs != null && obj.FTPs.Count > 0)
                    {
                        stContent += "临时调动:";
                        List<F_StepResponseTempPeople> tpList = new List<F_StepResponseTempPeople>();
                        List<string> joinList = new List<string>();
                        for (int i = 0; i < obj.FTPs.Count; i++)
                        {
                            if (string.IsNullOrEmpty(obj.FTPs[i].Name)
                                 || string.IsNullOrEmpty(obj.FTPs[i].PositionTitle)
                                 || string.IsNullOrEmpty(obj.FTPs[i].Phone))
                            {
                                continue;
                            }
                            F_StepResponseTempPeople tp = new F_StepResponseTempPeople();
                            tp.F_FireID = obj.FireID;
                            tp.SR_ID = srID;
                            tp.Name = obj.FTPs[i].Name;
                            tp.PositionTitle = obj.FTPs[i].PositionTitle;
                            tp.Phone = obj.FTPs[i].Phone;
                            tpList.Add(tp);
                            if ((i + 1) % 3 == 0)
                            {
                                joinList.Add(string.Format("{0}{1}({2})", tp.Name, tp.PositionTitle, tp.Phone));
                            }
                            else
                            {
                                joinList.Add(string.Format("{0}{1}({2})", tp.Name, tp.PositionTitle, tp.Phone));
                            }
                        }
                        bll.AddStepResponseTempPeople(tpList);
                        if (tpList.Count > 0)
                        {
                            stContent += string.Join(",", joinList);
                        }
                    }

                    //将处置中添加 MType=20 处置消息
                    M_Record mr = new M_Record();
                    mr.F_FireID = info.F_FireID;
                    mr.MR_HandleTime = info.SR_TransDatetime;
                    mr.MR_Type = (int)RecordType.逐级响应;
                    mr.MR_UserID = mr.MR_AddUser = fireInfo.F_TransUser.ToInt();
                    string mrContent = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), info.SR_Starter, CheckRequest.GetLoumaNumber(responseGrade)) + stContent;
                    mr.MR_Content = mrContent;
                    mr.MR_AddTime = info.SR_TransDatetime;
                    mr.MR_AddIP = info.SR_TransIP;
                    mr.MR_UserID = fireInfo.F_TransUser.ToInt();
                    mr.MR_Applicant = obj.Starter;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);

                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = string.Format("{0}已启动{1}级响应。", fireInfo.F_FireName, CheckRequest.GetLoumaNumber(responseGrade)) + stContent;
                    ma.M_DistrictID = 0;
                    ma.M_AssembleID = fireInfo.F_AddressCounty.ToInt();
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.逐级响应;
                    ma.F_FireID = info.F_FireID;
                    ma.M_Receive = fireInfo.F_TransUser;
                    ma.M_UserID = fireInfo.F_TransUser.ToInt();
                    ma.M_HandleTime = info.SR_TransDatetime;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = info.SR_TransIP;
                    int mID = bll.AddFireAutomatic(ma);
                    string mContent = ma.M_Content;

                    //3级为市局向区县发送，4级为区县向市局汇报
                    Pickout.SendAreaMessage(-1, info.F_FireID.Value, mr.MR_Type.Value, mID, mContent, 0);
                    result.FirstParam = srID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFourStepResponseSimple Server Error";
                Log.Error("FireInfoController AddFourStepResponseSimple ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region  火灾首报

        /// <summary>
        ///  区县--增加火灾首报 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireFirstReport(FireFirstReportReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null && obj.FirstTime != null 
                    && !string.IsNullOrEmpty(obj.FirstReportContent)
                    && !string.IsNullOrEmpty(obj.Applicant))
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    if (fireInfo==null)
                    {
                        result.Status = 0;
                        result.Message = "Request Params :FireID Not Exist!!";
                        return result;
                    }
                    F_FireFirstReport firstInfo = new F_FireFirstReport();
                    firstInfo.F_FireID = obj.FireID;
                    firstInfo.R_FirstReportContent = obj.FirstReportContent;
                    firstInfo.R_Applicant = obj.Applicant;
                    firstInfo.R_FirstTime = obj.FirstTime;
                    firstInfo.R_TransUser = obj.TransUser;
                    firstInfo.R_TransIP = CheckRequest.GetWebClientIp();
                    firstInfo.R_TransDatetime = DateTime.Now;
                    int frID = bll.AddFireFirstReport(firstInfo);
                    var mrList = bll.CKRecordTypeByTypes(obj.FireID, new List<int>() { (int)RecordType.火灾首报 });
                    if (frID != 0 && mrList.Count==0)
                    {
                        //将首报倒计时提示转为已读
                        bll.UpdateFireAutoMatic(firstInfo.F_FireID.Value,new List<int>(){ (int)AutomaticType.首报倒计时提示});
                      
                        //增加首报消息
                        KeyValuePair<int, string> aRes =bll.AddFireAutomaticForFirst(firstInfo, fireInfo);
                        //处置数据
                        M_Record mr = new M_Record();
                        mr.F_FireID = firstInfo.F_FireID;
                        mr.MR_HandleTime = firstInfo.R_TransDatetime;
                        mr.MR_Type = (int)RecordType.火灾首报;// Pickout.GetMRecordTypeText(2);//处置类型描述
                        mr.MR_AddIP = firstInfo.R_TransIP;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        mr.MR_Content = firstInfo.R_FirstReportContent;
                        mr.MR_AddTime = mr.MR_HandleTime;
                        mr.MR_Applicant = firstInfo.R_Applicant;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);
                        int mid = aRes.Key;
                        string mcontent =aRes.Value;
                        Pickout.SendAreaMessage(-1, firstInfo.F_FireID.Value, mr.MR_Type.Value, mid, mcontent);
                    }

                    

                    //倒计时配置文件
                    Pickout.AddOrEditCountDownElement(new FireCountDownModel()
                    {
                        FireID = firstInfo.F_FireID.Value,
                        ExpireTime = firstInfo.R_TransDatetime.Value.AddMinutes(30),
                        CountDownType = (int)RecordType.续报未按时操作
                    }, FCD_FILEPATH);

                    result.FirstParam = frID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireFirstReport Server Error";
                Log.Error("FireInfoController AddFireFirstReport", ex.ToString());
            }
           
            return result;
        }


        /// <summary>
        /// 区县--查看火灾首报信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic, dynamic, dynamic> GetFireFirstReport(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic, dynamic, dynamic> result = new ResultDto<dynamic, dynamic, dynamic, dynamic>();
            try
            {
                //火灾信息
                F_FireInfo fireDetail = bll.FireInfoDetail(obj.FireID);
                if (fireDetail != null)
                {
                    //首报信息
                    F_FireFirstReport fireFirstReportDetail = bll.FireFirstReportDetail(fireDetail.F_FireID);
                    if (fireFirstReportDetail != null)
                    {

                        //火灾记录处置分页
                        GridPager2 pager = Pickout.GetGridPager2(obj);
                        List<M_Record> mrList = bll.MRListByFireID(fireDetail.F_FireID, pager);

                        //判定在写入时，是否否是首报未按时操作
                        bool isExpireTime = mrList.Where(p => new int?[] { (int)RecordType.首报未按时操作 }.Contains(p.MR_Type)).FirstOrDefault() == null ? false : true;
                        //首报信息返回数据体
                        var data = new
                        {
                            FireID = fireDetail.F_FireID,
                            FireName = fireDetail.F_FireName,
                            TransDatetime = fireDetail.F_TransDatetime,
                            isExpireTime = isExpireTime,
                            Address = fireDetail.F_Address,
                            FirstReportContent = fireFirstReportDetail.R_FirstReportContent,
                            FirstTime = fireFirstReportDetail.R_FirstTime,
                            Applicant = fireFirstReportDetail.R_Applicant
                        };

                        //获取页面权限
                        List<int> selectedType = new List<int>() { (int)RecordType.火警录入, (int)RecordType.误报申请, (int)RecordType.接收响应 };
                        var temp = bll.CKRecordTypeByTypes(obj.FireID, selectedType);
                        var typeResult = selectedType.Select(s => new
                        {
                            MR_Type = s,
                            TypeText = Pickout.GetMRecordTypeText(s),
                            Auth = (temp.Where(p => p.MR_Type == s).FirstOrDefault() != null ? 1 : 0)
                        });

                        result.FirstParam = data;
                        result.SecondParam = mrList;
                        result.ThirdParam = pager.TotalRows;
                        result.FourthParam = typeResult;
                        result.Status = 1;
                        result.Message = "Request Is Success";
                    }
                    else
                    {
                        result.Status = 0;
                        result.Message = "No Matching Data";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireFirstReport Server Error";
                Log.Error("FireInfoController GetFireFirstReport", ex.ToString());
            }
            return result;
        }
        #endregion

        #region 火灾续报

        /// <summary>
        /// 区县--增加火灾续报信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireContinueReport(FireContinueReportReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null && obj.ContinueTime != null && !string.IsNullOrEmpty(obj.ContinueReportContent))
                {
                    F_FireContinueReport info = new F_FireContinueReport();
                    info.F_FireID = obj.FireID;
                    info.C_ContinueReportContent = obj.ContinueReportContent;
                    info.C_Applicant = obj.Applicant;
                    info.C_ContinueTime = obj.ContinueTime;
                    info.C_TransDatetime = DateTime.Now;
                    info.C_TransUser = obj.TransUser;
                    info.C_TransIP = CheckRequest.GetWebClientIp();
                    int cid = bll.AddFireContinueReport(info);
                    var mrList = bll.CKRecordTypeByTypes(obj.FireID, new List<int>() { (int)RecordType.火灾续报 });
                    KeyValuePair<int, string> aRes = new KeyValuePair<int, string>();
                    if (cid != 0)
                    {
                        if (mrList.Count == 0)
                        {
                            aRes = bll.AddFireAutomaticForContinue(info,false);
                        }else
                        {
                            aRes = bll.AddFireAutomaticForContinue(info, true);
                        }

                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.C_TransDatetime;
                        mr.MR_Type = (int)RecordType.火灾续报;// Pickout.GetMRecordTypeText(5);//处置类型描述
                        mr.MR_AddIP = info.C_TransIP;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        mr.MR_Content = info.C_ContinueReportContent;
                        mr.MR_AddTime = mr.MR_HandleTime;
                        mr.MR_Applicant = info.C_Applicant;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);

                        //移除未读续报 倒计时配置文件
                        Pickout.RemoveCountDownElement(new FireCountDownModel()
                        {
                            FireID = info.F_FireID.Value,
                        }, FCD_FILEPATH);

                        int mid = aRes.Key;
                        string mcontent = aRes.Value;
                        //Pickout.SendAutoMaticMessage(obj.FireID,mr.MR_Type.Value, mid, mcontent);
                        Pickout.SendAreaMessage(-1, info.F_FireID.Value, mr.MR_Type.Value, mid, mcontent);


                    }
                    result.FirstParam = cid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireContinueReport Server Error";
                Log.Error("FireInfoController AddFireContinueReport ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 区县--获取火灾续报信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetFireContinueReport(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                //火灾信息
                F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                if (fireInfo == null)
                {
                    result.Status = 0;
                    result.Message = "Request Params :FireID Not Exist!!";
                    return result;
                }

                //首报信息
                var fireFirstReportDetail =  bll.FireFirstReportDetail(fireInfo.F_FireID);

                //续报信息
                F_FireContinueReport fireContinueReportDetail = bll.FireContinueReportDetail(fireInfo.F_FireID);

                //查询火灾处置记录是否存在此记录
                var mr=bll.DetailMRecordByFireID(fireInfo.F_FireID, (int)RecordType.续报未按时操作, 2);
              
                //续报信息返回数据体
                var data = new
                {
                    ID= fireContinueReportDetail == null ? 0 : fireContinueReportDetail.C_ID,
                    FireID = fireInfo.F_FireID,
                    FireName = fireInfo.F_FireName,
                    FireFirstReportTransDateTime= fireFirstReportDetail == null ? null : fireFirstReportDetail.R_TransDatetime,
                    TransDatetime = fireContinueReportDetail == null ? null : fireContinueReportDetail.C_TransDatetime,
                    IsExpireTime = mr == null ? false : true,
                    Address = fireInfo.F_Address,
                    ContinueReportContent = fireContinueReportDetail == null ? string.Empty : fireContinueReportDetail.C_ContinueReportContent,
                    ContinueTime = fireContinueReportDetail == null ? null : fireContinueReportDetail.C_ContinueTime,
                    Applicant = fireContinueReportDetail == null ? string.Empty : fireContinueReportDetail.C_Applicant
                };

                //获取页面权限
                List<int> selectedType = new List<int>() { (int)RecordType.火警录入, (int)RecordType.误报申请, (int)RecordType.接收响应 };
                var temp = bll.CKRecordTypeByTypes(obj.FireID, selectedType);
                var typeResult = selectedType.Select(s => new
                {
                    MR_Type = s,
                    TypeText = Pickout.GetMRecordTypeText(s),
                    Auth = (temp.Where(p => p.MR_Type == s).FirstOrDefault() != null ? 1 : 0)
                });
                result.FirstParam = data;
                result.SecondParam = typeResult;
                result.Status = 1;
                result.Message = "Request Is Success";
                
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireContinueReport Server Error";
                Log.Error("FireInfoController GetFireContinueReport ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 误报
        /// <summary>
        /// 区县--增加误报
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddCatalog(FireCatalogReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null && obj.AppTime != null && !string.IsNullOrEmpty(obj.FalseReason))
                {
                    Catalog info = new Catalog();
                    info.F_FireID = obj.FireID;
                    info.A_FalseReason = obj.FalseReason;
                    info.A_Applicant = obj.Applicant;
                    info.A_AppTime = obj.AppTime;
                    info.A_Results = obj.Results;
                    info.A_Reason = obj.Reason;
                    info.A_ReaTime = obj.ReaTime;
                    info.A_TransUser = obj.TransUser;
                    info.A_TransDatetime = DateTime.Now;
                    info.A_TransIP = CheckRequest.GetWebClientIp();
                    int aid = bll.AddCatalog(info);

                    if (aid != 0)
                    {

                        //写入消息
                        KeyValuePair<int, string> aRes= bll.AddFireAutomaticForCatalog(info);

                        //写入处置记录
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.A_TransDatetime;
                        mr.MR_Type = (int)RecordType.误报申请;// Pickout.GetMRecordTypeText(10);//处置类型描述
                        mr.MR_AddIP = info.A_TransIP;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        mr.MR_Content =string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), info.A_FalseReason);
                        mr.MR_AddTime = mr.MR_HandleTime;
                        mr.MR_Applicant = info.A_Applicant;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);


                        int mid = aRes.Key;
                        string mcontent = aRes.Value;
                        //Pickout.SendAutoMaticMessage(obj.FireID,mr.MR_Type.Value, mid, mcontent);
                        Pickout.SendAreaMessage(-1, info.F_FireID.Value, mr.MR_Type.Value, mid, mcontent);

                    }
                    result.FirstParam = aid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddCatalog Server Error";
                Log.Error("FireInfoController AddCatalog ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--获取误报信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetCatalog(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                //火灾信息
                F_FireInfo fireDetail = bll.FireInfoDetail(obj.FireID);
                if (fireDetail != null)
                {
                    //正在误报中数据
                    var catalogDetail = bll.GetFireCatalog(fireDetail.F_FireID)
                              .Where(p=>p.A_ReaTime!=null && p.A_Reason !=null)
                              .OrderByDescending(o=>o.A_ReaTime).FirstOrDefault();
                    //误报信息返回数据体
                    var data = new
                    {
                        FireID =  fireDetail.F_FireID,
                        FireName = fireDetail.F_FireName,
                        Address = fireDetail.F_Address,
                        TransDatetime = catalogDetail == null ? null : catalogDetail.A_TransDatetime,
                        FalseReason = catalogDetail == null ? null : catalogDetail.A_FalseReason,
                        ContinueTime = catalogDetail == null ? null : catalogDetail.A_AppTime,
                        Applicant = catalogDetail == null ? null : catalogDetail.A_Applicant,
                        Results = catalogDetail == null ? null : catalogDetail.A_Results,
                        Reason = catalogDetail == null ? null : catalogDetail.A_Reason,
                        ReaTime = catalogDetail == null ? null : catalogDetail.A_ReaTime,
                    };

                    //获取页面权限
                    List<int> selectedType = new List<int>() { (int)RecordType.火警录入, (int)RecordType.误报申请, (int)RecordType.接收响应 };
                    var temp = bll.CKRecordTypeByTypes(obj.FireID, selectedType);
                    var typeResult = selectedType.Select(s => new
                    {
                        MR_Type = s,
                        TypeText = Pickout.GetMRecordTypeText(s),
                        Auth = (temp.Where(p => p.MR_Type == s).FirstOrDefault() != null ? 1 : 0)
                    });
                    result.FirstParam = data;
                    result.SecondParam = typeResult;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetCatalog Server Error";
                Log.Error("FireInfoController GetCatalog ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 申请响应

        /// <summary>
        /// 区县--增加申请响应
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddApplyRespond(ApplyRespondReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null && obj.AppTime != null && !string.IsNullOrEmpty(obj.ApplyReason))
                {
                    F_ApplyRespond info = new F_ApplyRespond();
                    info.F_FireID = obj.FireID;
                    info.AR_ApplyReason = obj.ApplyReason;
                    info.AR_Applicant = obj.Applicant;
                    info.AR_AppTime = obj.AppTime;
                    info.AR_TransDatetime = DateTime.Now;
                    info.AR_TransUser = obj.TransUser;
                    info.AR_TransIP = CheckRequest.GetWebClientIp();
                    int aid = bll.AddApplyRespond(info);

                    if (aid != 0)
                    {
                        //写入消息
                        KeyValuePair<int, string> aRes=bll.AddFireAutomaticForApplyRespond(info);

                        //写入处置记录
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.AR_TransDatetime;
                        mr.MR_Type = (int)RecordType.申请响应;// Pickout.GetMRecordTypeText(6);//处置类型描述
                        mr.MR_AddIP = info.AR_TransIP;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), info.AR_ApplyReason);
                        mr.MR_AddTime = mr.MR_HandleTime;
                        mr.MR_Applicant = info.AR_Applicant;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);

                        int mid = aRes.Key;
                        string mcontent = aRes.Value;
                        //Pickout.SendAutoMaticMessage(obj.FireID,mr.MR_Type.Value, mid, mcontent);
                        Pickout.SendAreaMessage(-1, info.F_FireID.Value, mr.MR_Type.Value, mid, mcontent);
                    }


                    //倒计时配置文件中续报未按时操作更改终报未及时操作
                    Pickout.AddOrEditCountDownElement(new FireCountDownModel()
                    {
                        FireID = info.F_FireID.Value,
                        ExpireTime = info.AR_TransDatetime.Value.AddMinutes(120),
                        CountDownType = (int)RecordType.终报未按时操作
                    }, FCD_FILEPATH);

      


                    result.FirstParam = aid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddApplyRespond Server Error";
                Log.Error("FireInfoController AddApplyRespond ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 区县--获取申请响应信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetApplyRespond(FireDetailReq obj)
        {

            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                //火灾信息
                F_FireInfo fireDetail = bll.FireInfoDetail(obj.FireID);
                if (fireDetail != null)
                {
                    //申请响应信息
                    F_ApplyRespond ApplyRespondDetail = bll.FireApplyRespondDetail(fireDetail.F_FireID);

                    //申请响应信息返回数据体
                    var data = new
                    {
                        FireID = fireDetail.F_FireID,
                        FireName = fireDetail.F_FireName,
                        Address = fireDetail.F_Address,
                        TransDatetime = ApplyRespondDetail==null?null: ApplyRespondDetail.AR_TransDatetime,
                        ApplyReason = ApplyRespondDetail == null ? null : ApplyRespondDetail.AR_ApplyReason,
                        AppTime = ApplyRespondDetail == null ? null : ApplyRespondDetail.AR_AppTime,
                        Applicant = ApplyRespondDetail == null ? null : ApplyRespondDetail.AR_Applicant,
                        ID= ApplyRespondDetail == null ? 0 : ApplyRespondDetail.AR_ID
                    };

                    //获取页面权限
                    List<int> selectedType = new List<int>() { (int)RecordType.火警录入, (int)RecordType.误报申请, (int)RecordType.接收响应 };
                    var temp = bll.CKRecordTypeByTypes(obj.FireID, selectedType);
                    var typeResult = selectedType.Select(s => new
                    {
                        MR_Type = s,
                        TypeText = Pickout.GetMRecordTypeText(s),
                        Auth = (temp.Where(p => p.MR_Type == s).FirstOrDefault() != null ? 1 : 0)
                    });
                    result.FirstParam = data;
                    result.SecondParam = typeResult;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetApplyRespond Server Error";
                Log.Error("FireInfoController GetApplyRespond ", ex.ToString());
            }

            return result;
        }

        #endregion

        #region 火灾报灭

        /// <summary>
        /// 区县--增加报灭信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddReportOutFire(ReportOutFireReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null && obj.HandleTime != null && !string.IsNullOrEmpty(obj.ReportoutContent))
                {
                    F_ReportOutFire info = new F_ReportOutFire();
                    info.F_FireID = obj.FireID;
                    info.FR_ReportoutContent = obj.ReportoutContent;
                    info.FR_Applicant = obj.Applicant;
                    info.FR_HandleTime = obj.HandleTime;
                    info.FR_TransDatetime = DateTime.Now;
                    info.FR_TransUser = obj.TransUser;
                    info.FR_TransIP = CheckRequest.GetWebClientIp();
                    int aid = bll.AddReportOutFire(info);

                    if (aid != 0)
                    {
                        //写入消息
                        KeyValuePair<int, string> aRes= bll.AddFireAutomaticForReportOutFire(info);

                        //写入处置记录
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.FR_TransDatetime;
                        mr.MR_Type = (int)RecordType.火灾报灭;// Pickout.GetMRecordTypeText(16);//处置类型描述
                        mr.MR_AddIP = info.FR_TransIP;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        mr.MR_Content = obj.ReportoutContent;
                        mr.MR_AddTime = mr.MR_HandleTime;
                        mr.MR_Applicant = info.FR_Applicant;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);


                        int mid = aRes.Key;
                        string mcontent = aRes.Value;
                        //Pickout.SendAutoMaticMessage(obj.FireID,mr.MR_Type.Value, mid, mcontent);
                        Pickout.SendAreaMessage(-1, info.F_FireID.Value, mr.MR_Type.Value, mid, mcontent);

                    }

                    //倒计时配置文件
                    Pickout.AddOrEditCountDownElement(new FireCountDownModel()
                    {
                        FireID = info.F_FireID.Value,
                        ExpireTime = info.FR_TransDatetime.Value.AddMinutes(120),
                        CountDownType = (int)RecordType.终报未按时操作
                    }, FCD_FILEPATH);


                    result.FirstParam = aid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddReportOutFire Server Error";
                Log.Error("FireInfoController AddReportOutFire ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--获取报灭信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetReportOutFire(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                //火灾信息
                F_FireInfo fireDetail = bll.FireInfoDetail(obj.FireID);
                if (fireDetail != null)
                {
                    //报灭
                    F_ReportOutFire fireReportOutDetail = bll.FireReportOutDetail(fireDetail.F_FireID);

                    //报灭返回数据体
                    var data = new
                    {
                        FireID = fireDetail.F_FireID,
                        FireName = fireDetail.F_FireName,
                        Address = fireReportOutDetail == null ? null : fireDetail.F_Address,
                        ReportoutContent = fireReportOutDetail == null ? null : fireReportOutDetail.FR_ReportoutContent,
                        Applicant = fireReportOutDetail == null ? null : fireReportOutDetail.FR_Applicant,
                        HandleTime = fireReportOutDetail == null ? null:fireReportOutDetail.FR_HandleTime,
                        ID = fireReportOutDetail == null ? 0 : fireReportOutDetail.Res_ID,
                        TransDatetime = fireReportOutDetail == null ? null : fireReportOutDetail.FR_TransDatetime,
                    };

                    //获取页面权限
                    List<int> selectedType = new List<int>() { (int)RecordType.火警录入, (int)RecordType.误报申请, (int)RecordType.接收响应 };
                    var temp = bll.CKRecordTypeByTypes(obj.FireID, selectedType);
                    var typeResult = selectedType.Select(s => new
                    {
                        MR_Type = s,
                        TypeText = Pickout.GetMRecordTypeText(s),
                        Auth = (temp.Where(p => p.MR_Type == s).FirstOrDefault() != null ? 1 : 0)
                    });
                    result.FirstParam = data;
                    result.SecondParam = typeResult;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetApplyRespond Server Error";
                Log.Error("FireInfoController GetApplyRespond ", ex.ToString());
            }

            return result;
        }
        #endregion

        #region 火灾终报
        /// <summary>
        ///  区县--增加火灾终报信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireFinalReport(FireFinalReportReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                //更改火灾信息表状态
                int closeFireId=bll.UpdateFireInfoState(fireInfo.F_FireID, 1);
                var existFFR = bll.FireFinalReportDetail(fireInfo.F_FireID);
                if (existFFR != null)
                {
                    result.Status = 0;
                    result.Message = "Request Params:FireID  FinalReport Is Exist!!!";
                    return result;
                }

                if (fireInfo != null &&obj != null && obj.Ptime != null && obj.ReporterTime != null)
                {
                    F_FireFinalReport info = new F_FireFinalReport();
                    info.FFR_ID = obj.FFR_ID;
                    info.F_FireID = obj.FireID;
                    info.FFR_Address = obj.Address;
                    info.FFR_Jdate = obj.Jdate;
                    info.FFR_Type = obj.Type;
                    info.FFR_Cont = obj.Content;
                    info.FFR_Fh = obj.Fh;
                    info.FFR_FhPeople = obj.FhPeople;
                    info.FFR_Com = obj.Com;
                    info.FFR_ComPeople = obj.ComPeople;
                    info.FFR_Ranks = obj.Ranks;
                    info.FFR_Mas = obj.Mas;
                    info.FFR_Leader = obj.Leader;
                    info.FFR_Ptime = obj.Ptime.Value;
                    info.FFR_Zf = obj.Zf;
                    info.FFR_Zpeople = obj.Zpeople;
                    info.FFR_BZf = obj.BZf;
                    info.FFR_LocalP = obj.LocalP;
                    info.FFR_Weather = obj.Weather;
                    info.FFR_Wind = obj.Wind;
                    info.FFR_Direc = obj.Direc;
                    info.FFR_Temp = obj.Temp;
                    info.FFR_Damp = obj.Damp;
                    info.FFR_Garea = obj.Garea;
                    info.FFR_Gunit = obj.Gunit;
                    info.FFR_Larea = obj.Larea;
                    info.FFR_Lunit = obj.Lunit;
                    info.FFR_Trees = obj.Trees;
                    info.FFR_Age = obj.Age;
                    info.FFR_Num = obj.Num;
                    info.FFR_Die = obj.Die;
                    info.FFR_Why = obj.Why;
                    info.FFR_WhyID=obj.WhyID;
                    info.FFR_Loss = obj.Loss;
                    info.FFR_Other = obj.Other;
                    info.FFR_Reporter = obj.Reporter;
                    info.FFR_ReporterTime = obj.ReporterTime.Value;
                    info.FFR_FireTypeID = obj.FireTypeID;
                    info.FFR_FireType = obj.FireType;
                    info.FFR_TransUser = obj.TransUser;
                    info.FFR_TransDatetime = DateTime.Now;
                    info.FFR_CaseState = obj.CaseState;
                    info.FFR_TransIP = CheckRequest.GetWebClientIp();
                    int fid =bll.AddFireFinalReport(info);


                    //增加 鉴定与立案
                    List<F_FireAttachs> attachs = new List<F_FireAttachs>();
                    if (obj.Attachs != null && obj.Attachs.Count > 0)
                    {
                        foreach (var item in obj.Attachs)
                        {
                            F_FireAttachs attach = new F_FireAttachs()
                            {
                                F_FireID = item.FireID,
                                FileName = item.FileName,
                                FilePath = item.FilePath,
                                AddTime = DateTime.Now,
                                Type = item.Type,
                            };
                            attachs.Add(attach);
                        }
                        bll.AddFireAttachs(attachs);
                    }



                    if (fid != 0 && obj.IsSupplement == 0)
                    {
                        //写入消息
                        KeyValuePair<int, string> aRes=bll.AddFireAutomaticForFinalReport(info,fireInfo);

                        //写入处置记录
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.FFR_TransDatetime;
                        mr.MR_Type = (int)RecordType.火灾终报;// Pickout.GetMRecordTypeText(17);//处置类型描述
                        mr.MR_AddIP = info.FFR_TransIP;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), fireInfo.F_FireName,info.FFR_TransDatetime.ToString("yyyy-MM-dd HH:mm:ss"));
                        mr.MR_AddTime = mr.MR_HandleTime;
                        mr.MR_Applicant = info.FFR_Reporter;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);
                        

                        //移除符合条件的倒计时配置文件项
                        Pickout.RemoveCountDownElement(new FireCountDownModel()
                        {
                            FireID = info.F_FireID,
                            ExpireTime = info.FFR_TransDatetime,
                            CountDownType = (int)RecordType.终报未按时操作
                        }, FCD_FILEPATH);

                        int mid = aRes.Key;
                        string mcontent = aRes.Value;
                        //Pickout.SendAutoMaticMessage(obj.FireID,mr.MR_Type.Value, mid, mcontent);
                        Pickout.SendAreaMessage(-1, info.F_FireID,mr.MR_Type.Value, mid, mcontent);

                    }
                    result.FirstParam =fid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireFinalReport Server Error";
                Log.Error("FireInfoController AddFireFinalReport ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 区县--获取火灾终报信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetFireFinalReport(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    //获取火灾信息
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);

                    List<Linchpin> linchpins = linchpinBll.GetLinchpin(new List<long>()
                                        { fireInfo.F_AddressCounty.ToLong(), fireInfo.F_AddressTown.ToLong(),fireInfo.F_AddressVillage.ToLong()});

                    List<string> linchpinNames = linchpins.Select(s => s.LinchpinName).ToList();

                    List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(0);//0.所有 1.起火原因 2.报警方式 1023.火灾类型
                    var fireWhy=dtbList.Where(p => p.DParentID == 1).ToList();
                    var alarmType = dtbList.Where(p => p.DParentID == 2).ToList();
                    var fireType=dtbList.Where(p => p.DParentID == 1023).ToList();


                    //区县地址和接警时间
                    string fireAddress = string.Join(",", linchpinNames);
                    DateTime alarmTime = fireInfo.F_AlarmTime.Value;


                    //获取前场总指挥
                    var frontCommand = bll.GetFrontCommand(fireInfo.F_FireID).OrderByDescending(o => o.FC_TransDatetime).ToList();
                    string leader = frontCommand != null || frontCommand.Count==0?string.Empty:frontCommand.FirstOrDefault().FC_CommanderName;

                    //报灭时间
                    F_ReportOutFire fireReportOutDetail = bll.FireReportOutDetail(fireInfo.F_FireID);
                    DateTime? reportOutTime = fireReportOutDetail==null ? null : fireReportOutDetail.FR_HandleTime;


                    //火灾附件
                    List<F_FireAttachs> attachs = bll.GetFireAttachs(obj.FireID);

                    attachs = attachs.Where(p => p.Type == (int)FireAttachType.Identification || p.Type == (int)FireAttachType.Case).ToList();

                    var data = new { FireAddress = fireAddress,AlarmTime= alarmTime, AlarmType = alarmType,FireWhy=fireWhy,Leader =leader,ReportOutTime= reportOutTime, FireType= fireType , Attachs = attachs };    
                                
                    result.FirstParam = data;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetApplyRespond Server Error";
                Log.Error("FireInfoController GetApplyRespond ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--火灾终报详情
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetFireFinalReportDetail(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    //火灾终报
                    F_FireFinalReport finalReportInfo = bll.FireFinalReportDetail(obj.FireID);
                    result.FirstParam = finalReportInfo;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch(Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController FireDetail Server Error";
                Log.Error("FireInfoController GetFireFinalReportDetail ", ex.ToString());
            }
            return result;
        }
        #endregion

        #region 火灾处置

        /// <summary>
        /// 区县--获取火灾处置信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetMRecord(MRecordReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null && obj.FireID > 0)
                {
                    //火灾记录处置
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    pager.PageSize = int.MaxValue;
                    // 2018-06-29 调整 
                    //int searchLevel = 0;
                    //searchLevel = obj.IsCityManager == 1 ? 0 : 1;
                    //List<M_Record> mrList = bll.MRListByFireID(obj.FireID, pager, 0);
                    //List<M_Record> mrList = obj.IsCityManager ==1 ? bll.MRListByFireID(obj.FireID, pager): bll.MRListByFireID(obj.FireID, pager,1);

                    //2018-10-16 调整
                    int searchLevel = obj.IsCityManager == 0 ? 1 : 0;
                    List<M_Record> mrList =  bll.MRListByFireID(obj.FireID, pager, searchLevel) ;

                    //2020-09-28 调整  增加绘制信息
                    List<F_FireRecordPlot> frpList = bll.GetFireRecordPlot(0, obj.FireID, 0);

                    var mrList2 = from a in mrList
                                  join b in frpList on a.MR_ID equals b.MR_ID
                                  into c
                                  let d = c.FirstOrDefault()
                                  select new
                                  {
                                      a.MR_ID,
                                      a.MR_Type,
                                      a.MR_UserID,
                                      a.MR_Content,
                                      a.MR_AddTime,
                                      a.MR_AddIP,
                                      a.MR_AddUser,
                                      a.MR_Applicant,
                                      a.MR_HandleTime,
                                      a.F_FireID,
                                      a.CFA_ID,
                                      FireRecordPlot=d
                                  };
                    result.FirstParam = mrList2.ToList();
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetMRecord Server Error";
                Log.Error("FireInfoController GetMRecord", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--获取用户访问权限及存在的倒计时
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic,dynamic,dynamic> GetDisposalAuthor(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic, dynamic, dynamic> result = new ResultDto<dynamic, dynamic, dynamic, dynamic>();
            try
            {
                IEnumerable<string> ids;
                Request.Headers.TryGetValues("ID", out ids);
                if (obj != null &&(ids != null && ids.Count() > 0))
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    if (fireInfo == null)
                    {
                        result.Status = 0;
                        result.Message = "Request Params :FireID Not Exist!!";
                        return result;
                    }
                    int userid = ids.FirstOrDefault().ToInt();



                    /*判定火灾进度，处于RecordType=0,2,16 时进行返回倒计时
                     *只计算当前处置记录中MR_Type最大的那个
                     */
                    obj.PageSize = int.MaxValue;
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    List<M_Record> mrList = bll.MRListByFireID(fireInfo.F_FireID, pager);


                    //获取页面权限
                    List<int> selectedType = new List<int>() { (int)RecordType.火警录入,(int)RecordType.火灾续报, (int)RecordType.误报申请, (int)RecordType.接收响应,(int)RecordType.火灾报灭, (int)RecordType.逐级响应  };
                    var temp = bll.CKRecordTypeByTypes(obj.FireID, selectedType);

                    
                    List<dynamic> typeResult = new List<dynamic>();
                    foreach (var item in selectedType)
                    {
                        var auth = 0;
                        //处理4级响应判定
                        if (item == (int)RecordType.逐级响应)
                        {
                           var existStepResponse= bll.StepResponseDetail(fireInfo.F_FireID);
                            if (existStepResponse != null)
                            {
                                auth = 1;
                            }
                        }
                        else 
                        {
                            auth = (temp.Where(p => p.MR_Type == item).FirstOrDefault() != null ? 1 : 0);
                        }
                        typeResult.Add(new {
                            MR_Type = item,
                            TypeText = Pickout.GetMRecordTypeText(item),
                            Auth=auth
                        });
                    }
                    
                    List<int> expireTypeList = new List<int>() { (int)RecordType.火警录入, (int)RecordType.火灾首报, (int)RecordType.火灾报灭 };
                    var waitMecordList = mrList.Where(p=>expireTypeList.Contains(p.MR_Type.Value)).ToList();
                    int mrType = waitMecordList.Max(m => m.MR_Type.Value);
                    List<dynamic> expireTimes = new List<dynamic>();

                    //火灾正在处置的流程
                    var disping = bll.MRListByFireIDs(new List<int> { obj.FireID}).FirstOrDefault();
                    foreach (var item in waitMecordList)
                    {
                        DateTime? expireTime = null;
                        if (item.MR_Type == mrType)
                        {
                            switch (mrType)
                            {
                                case 0:
                                    expireTime =mrList.Where(p=>p.MR_Type==(int)RecordType.火灾首报).FirstOrDefault()!=null? null: bll.FireInfoDetail(fireInfo.F_FireID).F_TransDatetime;break;
                                case 2:
                                    expireTime = mrList.Where(p => p.MR_Type == (int)RecordType.火灾续报).FirstOrDefault() != null ? null : bll.FireFirstReportDetail(fireInfo.F_FireID).R_TransDatetime;break;
                                case 16:
                                    expireTime = mrList.Where(p => p.MR_Type == (int)RecordType.火灾终报).FirstOrDefault() != null ? null:bll.FireReportOutDetail(fireInfo.F_FireID).FR_TransDatetime;break;
                            }
                        }
                       expireTimes.Add(new { MR_Type = item.MR_Type, ExpireTime = expireTime });
                    }
                    result.FirstParam = "用户可使用的权限，代码预留";
                    result.SecondParam = typeResult;
                    result.ThirdParam = expireTimes;
                    result.FourthParam =new { MR_Type= disping.MR_Type, TypeText = Pickout.GetMRecordTypeText(disping.MR_Type.Value) };
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetDisposalAuthor Server Error";
                Log.Error("FireInfoController GetDisposalAuthor ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///  处置 PDF导出
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult MRecordByPDF(MRecordReq obj)
        {
            PDFHelper pdf = new PDFHelper();
            try
            {
                if (obj != null && obj.FireID > 0)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    pager.PageSize = int.MaxValue;
                    int searchLevel = obj.IsCityManager == 0 ? 1 : 0;
                    List<M_Record> mrList = bll.MRListByFireID(obj.FireID, pager, searchLevel);
                    #region PDF
                    //1.创建位置
                    string historyFirePath = PDF_PATH + "/MRecord";
                    if (!Directory.Exists(historyFirePath))
                    {
                        Directory.CreateDirectory(historyFirePath);
                    }
                    historyFirePath = historyFirePath + "/" + fireInfo.F_FireName + "_处置记录" + ".pdf";

                    //2.设置字体
                    pdf.Open(new FileStream(historyFirePath, FileMode.Create));
                    pdf.SetBaseFont(@"C:\Windows\Fonts\simkai.ttf");

                    Font title_font = new Font(pdf.basefont, 23.5f, Font.UNDEFINED);
                    Font content_font = new Font(pdf.basefont, 10.0f, Font.UNDEFINED);

                    Paragraph par = new Paragraph();
                    PdfPTable table = new PdfPTable(1);

                    if (mrList != null && mrList.Count > 0)
                    {
                        #region  处置记录
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "处置记录"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(4, 90);
                        table.SetWidths(new int[] { 20, 20, 25, 35 });

                        PdfPCell cell = pdf.CreateCell("操作时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("处置类型", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);


                        cell = pdf.CreateCell("值守人员", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        #endregion

                        foreach (var item in mrList)
                        {
                            cell = pdf.CreateCell(item.MR_HandleTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(Pickout.GetMRecordTypeText(item.MR_Type.Value, false), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.MR_Applicant, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.MR_Content, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                        pdf.AddTable(table);
                        #endregion
                    }
                    pdf.Close();
                    #endregion

                    var browser = String.Empty;
                    if (HttpContext.Current.Request.UserAgent != null)
                    {
                        browser = HttpContext.Current.Request.UserAgent.ToUpper();
                    }
                    HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
                    FileStream fileStream = File.OpenRead(historyFirePath); 
                    httpResponseMessage.Content = new StreamContent(fileStream);
                    httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName =
                        browser.Contains("FIREFOX")
                            ? Path.GetFileName(historyFirePath)
                            : HttpUtility.UrlEncode(Path.GetFileName(historyFirePath))
                    };
                    return ResponseMessage(httpResponseMessage);
                }
                else
                {
                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
                }
            }
            catch (Exception ex)
            {
                pdf.Close();
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                return ResponseMessage(httpResponseMessage);
            }
          
        }

        #endregion

        #region 扑救力量支援火灾
        /// <summary>
        /// 区县--支援火灾信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetSupportFire(SupportFireRequest obj)
        {
            ResultDto<dynamic,dynamic> result = new ResultDto<dynamic,dynamic>();
            try
            {
                if (obj != null)
                {
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    List<dynamic> ffList = bll.GetFireForceSupportByPager(obj.AddressCounty,pager);
                    result.FirstParam = ffList;
                    result.SecondParam = pager.TotalRows;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetSupportFire Server Error";
                Log.Error("FireInfoController GetSupportFire ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 区县--启动4级响应  版本时间:20200907
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFourStepResponse(FourStepResponseReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    int responseGrade = 4;
                    F_StepResponse fsInfo = bll.StepResponseDetail(obj.FireID);
                    if (fsInfo != null)
                    {
                        result.FirstParam = 0;
                        result.Status = 0;
                        result.Message = string.Format("StepResponse Started(Level:{0}",fsInfo.SR_ResponseGrade);
                    }

                    F_StepResponse info = new F_StepResponse();
                    info.F_FireID = obj.FireID;
                    info.SR_ResponseGrade = responseGrade;
                    info.SR_StartDatetime = obj.StartDatetime;
                    info.SR_Starter = obj.Starter;
                    info.SR_InstructionContent = obj.InstructionContent;
                    info.SR_StartCondition = obj.StartCondition;
                    info.SR_TransDatetime = DateTime.Now;
                    info.SR_HandleTime = info.SR_TransDatetime;
                    info.SR_TransUser = obj.TransUser;
                    info.SR_TransIP = CheckRequest.GetWebClientIp();
                    int srID = bll.AddStepResponse(info);

                    //临时添加人员
                    string stContent = string.Empty;
                    if (obj.FTPs != null && obj.FTPs.Count > 0)
                    {
                        stContent += "临时调动:";
                        List<F_StepResponseTempPeople> tpList = new List<F_StepResponseTempPeople>();
                        List<string> joinList = new List<string>();
                        for (int i = 0; i < obj.FTPs.Count; i++)
                        {
                            if (string.IsNullOrEmpty(obj.FTPs[i].Name)
                                        || string.IsNullOrEmpty(obj.FTPs[i].PositionTitle)
                                        || string.IsNullOrEmpty(obj.FTPs[i].Phone))
                            {
                                continue;
                            }

                            F_StepResponseTempPeople tp = new F_StepResponseTempPeople();
                            tp.F_FireID = obj.FireID;
                            tp.SR_ID = srID;
                            tp.Name = obj.FTPs[i].Name;
                            tp.PositionTitle = obj.FTPs[i].PositionTitle;
                            tp.Phone = obj.FTPs[i].Phone;
                            tpList.Add(tp);
                            joinList.Add(string.Format("{0}{1}({2})", tp.Name, tp.PositionTitle, tp.Phone));

                            #region 发送短信
                            Sms_SendInfo model = new Sms_SendInfo();
                            model.IsAchieved = false;
                            model.IsSafety = false;
                            model.SendDate = DateTime.Now;
                            model.SmsInfo = fireInfo.F_FireName + "现已启动" + CheckRequest.GetLoumaNumber(responseGrade) + "级响应，需要你部支援，请速与市局联系！！";
                            model.Sender = obj.TransUser;
                            model.SenderCode = obj.TransUser;
                            model.SafetyCode = Guid.NewGuid().ToString();
                            model.MobileCode = obj.FTPs[i].Phone;
                            model.Receiver = obj.FTPs[i].Name;
                            m_bll.CreateSms_SendInfo(model);
                            #endregion



                        }
                        bll.AddStepResponseTempPeople(tpList);
                        if (tpList.Count > 0)
                        {
                            stContent += string.Join(",", joinList);
                        }
                    }
                     
                    //将处置中添加 MType=20 处置消息
                    M_Record mr = new M_Record();
                    mr.F_FireID = info.F_FireID;
                    mr.MR_HandleTime = info.SR_TransDatetime;
                    mr.MR_Type = (int)RecordType.逐级响应;
                    mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                    string mrContent = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), info.SR_Starter, CheckRequest.GetLoumaNumber(responseGrade)) + stContent;
                    mr.MR_Content = mrContent;
                    mr.MR_AddTime = info.SR_TransDatetime;
                    mr.MR_AddIP = info.SR_TransIP;
                    mr.MR_Applicant = obj.TransUserName;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);

                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = string.Format("{0}已启动{1}级响应。", fireInfo.F_FireName, CheckRequest.GetLoumaNumber(responseGrade)) + stContent;
                    ma.M_DistrictID = 0;
                    ma.M_AssembleID = fireInfo.F_AddressCounty.ToInt();
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.逐级响应;
                    ma.F_FireID = info.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = info.SR_TransDatetime;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = info.SR_TransIP;
                    int mID = bll.AddFireAutomatic(ma);
                    string mContent = ma.M_Content;

                    //3级为市局向区县发送，4级为区县向市局汇报
                    Pickout.SendAreaMessage(-1, info.F_FireID.Value, mr.MR_Type.Value, mID, mContent, 0);
                    result.FirstParam = srID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFourStepResponse Server Error";
                Log.Error("FireInfoController AddFourStepResponse ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--获取4级响应 版本时间:20200907
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetFourStepResponse(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    int responseGrade = 4;
                    F_StepResponse fsInfo = bll.StepResponseDetail(obj.FireID);
                    responseGrade = fsInfo != null ? responseGrade = fsInfo.SR_ResponseGrade.Value - 1 : responseGrade;
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    //级别超过现有级别设置
                    if (responseGrade <= 0)
                    {
                        responseGrade = 1;
                    }

                    //FireTempPeople
                    List<F_StepResponseTempPeople> ftps = new List<F_StepResponseTempPeople>();
                    if (fsInfo != null)
                    {
                       ftps= bll.GetStepResponseTempPeople(fsInfo.F_FireID.Value, fsInfo.SR_ID);
                    }
                    //获取响应条件列表
                    List<F_StartCondition> scList = bll.GetStartCondition(responseGrade);
                    result.FirstParam = new { F_AddressCounty = fireInfo.F_AddressCounty, ResponseGrade = responseGrade, LoumaNumber = CheckRequest.GetLoumaNumber(responseGrade) ,FTPs =ftps};
                    result.SecondParam = scList;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFourStepResponse Server Error";
                Log.Error("FireInfoController GetFourStepResponse ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--增加前指  版本时间:20200916
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFourFrontCommand(FrontCommandReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    F_FrontCommand info = new F_FrontCommand();
                    info.FC_ID = obj.ID;
                    info.F_FireID = obj.FireID;
                    info.FC_CommanderName = obj.CommanderName;
                    info.FC_Tel = obj.Tel;
                    info.FC_LocaleLatitude = obj.LocaleLatitude;
                    info.FC_LocaleLongitude = obj.LocaleLongitude;
                    info.FC_Member = obj.Member;
                    info.FC_TransDatetime = DateTime.Now;
                    info.FC_HandleTime = info.FC_TransDatetime;
                    info.FC_TransUser = obj.TransUser;
                    info.FC_TransIP = CheckRequest.GetWebClientIp();
                    info.FC_Address = obj.Address;
                    int fcID = 0;
                    if (info.FC_ID > 0)//更新
                    {
                        fcID = bll.UpdateFrontCommand(info);
                    }
                    else //新增
                    {
                        fcID = bll.AddFrontCommand(info);
                    }
                    //坐标地址字符串转换
                    string[] longitudeSplit = info.FC_LocaleLongitude.Split('|');
                    string longitudeString = longitudeSplit[0] + "度" + longitudeSplit[1] + "分" + longitudeSplit[2] + "秒";
                    string[] latitudeSplit = info.FC_LocaleLatitude.Split('|');
                    string latitudeString = latitudeSplit[0] + "度" + latitudeSplit[1] + "分" + latitudeSplit[2] + "秒";

                    //获取前指人员集合
                    string[] mArray = info.FC_Member.Split(',');
                    List<int> mIDs = mArray.Select(s => s.ToInt()).ToList();

                    //2018-08-15 新调整指挥部成员
                    List<F_UnitHeadquarterMember> mInfo = memberInfoBll.GetUnitHeadquarterMember(mIDs);
                    string mNames = string.Join(",", mInfo.Select(s => s.UHM_Leader).ToList());

                    //老版本指挥部成员
                    //List<F_MemberInfo> mInfo = memberInfoBll.GetMemberInfo(mIDs);
                    //string mNames = string.Join(",", mInfo.Select(s => s.M_MemberName).ToList());


                    //临时添加人员
                    string fcContent = string.Empty;
                    if (obj.FTPs != null && obj.FTPs.Count > 0)
                    {



                        fcContent += "临时调动:";
                        List<F_FrontCommandTempPeople> tpList = new List<F_FrontCommandTempPeople>();
                        List<string> joinList = new List<string>();
                        for (int i = 0; i < obj.FTPs.Count; i++)
                        {

                            if (string.IsNullOrEmpty(obj.FTPs[i].Name)
                                 || string.IsNullOrEmpty(obj.FTPs[i].PositionTitle)
                                 || string.IsNullOrEmpty(obj.FTPs[i].Phone))
                            {
                                continue;
                            }
                            F_FrontCommandTempPeople tp = new F_FrontCommandTempPeople();
                            tp.F_FireID = obj.FireID;
                            tp.FC_ID = info.FC_ID;
                            tp.Name = obj.FTPs[i].Name;
                            tp.PositionTitle = obj.FTPs[i].PositionTitle;
                            tp.Phone = obj.FTPs[i].Phone;
                            tpList.Add(tp);
                            joinList.Add(string.Format("{0}{1}({2})", tp.Name, tp.PositionTitle, tp.Phone));

                        }
                        bll.AddFrontCommandTempPeople(tpList);
                        if (tpList.Count > 0)
                        {
                            fcContent += string.Join(",", joinList);
                        }
                    }

                    //添加火灾处置信息
                    M_Record mr = new M_Record();
                    mr.F_FireID = info.F_FireID;
                    mr.MR_HandleTime = info.FC_TransDatetime;
                    mr.MR_Type = (int)RecordType.成立前指;
                    mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                    mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value)
                                                      , fireInfo.F_FireName, info.FC_Address, longitudeString, latitudeString, info.FC_CommanderName, mNames) + fcContent;
                    mr.MR_AddTime = info.FC_HandleTime;
                    mr.MR_AddIP = info.FC_TransIP;
                    mr.MR_Applicant = obj.TransUserName;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);


                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value)
                                                      , fireInfo.F_FireName, info.FC_Address, longitudeString, latitudeString, info.FC_CommanderName, mNames) + fcContent;
                    ma.M_DistrictID = 0;
                    ma.M_AssembleID = fireInfo.F_AddressCounty.ToInt();
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.成立前指;
                    ma.F_FireID = info.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = DateTime.Now;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = info.FC_TransIP;
                    int mID = bll.AddFireAutomatic(ma);
                    string mContent = ma.M_Content;
                    Pickout.SendAreaMessage(-1, info.F_FireID.Value, mr.MR_Type.Value, mID, mContent, 0);

                    result.FirstParam = fcID;
                    result.Status = 1;
                    result.Message = "Request Is Success";

                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required  Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFourFrontCommand Server Error";
                Log.Error("FireInfoController AddFourFrontCommand ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--获取前指 版本时间:20200916
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic,dynamic> GetFourFrontCommand(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic,dynamic> result = new ResultDto<dynamic, dynamic,dynamic>();
            try
            {
                if (obj != null && obj.FireID != 0)
                {
                    //start->排除重复FC_Member
                    List<F_FrontCommand> list = bll.GetFrontCommand(obj.FireID);
                    List<string> tempList = list.Select(s => s.FC_Member).ToList();
                    List<int> mIdList = new List<int>();
                    for (int i = 0; i < tempList.Count; i++)
                    {
                        string mIDsStr = tempList[i];
                        if (mIDsStr.IndexOf(",") != -1)
                        {
                            string[] mids = mIDsStr.Split(',');
                            for (int j = 0; j < mids.Length; j++)
                            {
                                mIdList.Add(mids[j].ToInt());
                            }
                        }
                        else
                        {
                            mIdList.Add(mIDsStr.ToInt());
                        }
                    }
                    mIdList = mIdList.Distinct().ToList();
                    List<F_MemberInfo> mList = memberInfoBll.GetMemberInfo(mIdList);
                    //end->排除重复FC_Member



                    F_FrontCommand fcInfo = list.LastOrDefault();
                    List<F_FrontCommandTempPeople> ftps = new List<F_FrontCommandTempPeople>();
                    if (fcInfo != null)
                    {
                        ftps = bll.GetFrontCommandTempPeople(fcInfo.F_FireID.Value, fcInfo.FC_ID);
                    }

                    fcInfo.FC_Member = string.Join(",", mList.Select(s => s.M_MemberName).ToList());
                    result.FirstParam = fcInfo;
                    result.SecondParam = mList.Select(s => new { MemberID = s.M_ID, MemberName = s.M_MemberName }).ToList();
                    result.ThirdParam = ftps;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFourFrontCommand Server Error";
                Log.Error("FireInfoController GetFourFrontCommand ", ex.ToString());
            }
            return result;
        }

        #endregion

        #endregion

        #region 市局

        #region 火灾录入

        /// <summary>
        /// 市局--火灾录入
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ResultDto<int> CityAddFireInfo(CityAddFireInfoReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null
                    && CheckRequest.IsNumber(obj.AddressCounty)
                    && CheckRequest.IsNumber(obj.AddressTown)
                    && CheckRequest.IsNumber(obj.AddressVillage)
                    && (!string.IsNullOrEmpty(obj.FireName)
                    && !string.IsNullOrEmpty(obj.AlarmContent)
                    && obj.HappenDatetime != null
                    && obj.AlarmTime != null))
                {
                    F_FireInfo info = new F_FireInfo();
                    info.F_FireName = obj.FireName;

                    info.F_HappenDatetime = obj.HappenDatetime;
                    info.F_TransDatetime = DateTime.Now;

                    info.F_AlarmTime = obj.AlarmTime;
                    info.F_AlarmContent = obj.AlarmContent;
                    info.F_Alarm = obj.Alarm;

                    info.F_AddressCounty = obj.AddressCounty;
                    info.F_AddressTown = obj.AddressTown;
                    info.F_AddressVillage = obj.AddressVillage;


                    //短消息记录功能只在市局有
                    info.F_SMSContent = obj.SMSMessage;

                    List<Linchpin> temp = linchpinBll.GetLinchpin(new List<long>() { obj.AddressCounty.ToLong(), obj.AddressTown.ToLong(), obj.AddressVillage.ToLong() });
                    string address = string.Empty;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressCounty.ToLong()).FirstOrDefault().LinchpinName;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressTown.ToLong()).FirstOrDefault().LinchpinName;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressVillage.ToLong()).FirstOrDefault().LinchpinName;
                    info.F_Address = address;


                    info.F_ReporterName = obj.ReporterName;
                    info.F_ReporterUnit = obj.ReporterUnit;
                    info.F_ReporterWorknum = obj.ReporterWorknum;
                    info.F_ReporterPhone = obj.ReporterPhone;

                    info.F_TransUser = obj.TransUser;
                    info.F_TransIP = CheckRequest.GetWebClientIp();
                    info.F_IsChange = 0;
                    info.F_IsState = obj.IsState; 
                    info.F_IsHb = obj.IsHb;

                    int fireID = bll.Add(info);
                    if (fireID != 0)
                    {
                        //添加火灾消息
                        var aRes = bll.AddFireAutomaticForInit(info);


                        //添加火灾处置信息
                        M_Record mr = new M_Record();
                        mr.F_FireID = fireID;
                        mr.MR_HandleTime = info.F_HappenDatetime;
                        mr.MR_Type = (int)RecordType.火警录入;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        string happenDatetime = info.F_HappenDatetime.Value.ToString("yyyy年MM月dd日HH时mm分");
                        string mrContent = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), happenDatetime, info.F_Alarm, info.F_Address, info.F_AlarmContent);
                        mr.MR_Content = mrContent;
                        mr.MR_AddTime = info.F_TransDatetime;
                        mr.MR_AddIP = info.F_TransIP;
                        mr.MR_Applicant = info.F_Alarm;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);

                        //加入定位火点
                        //F_FireSite fireSiteInfo = new F_FireSite();
                        //fireSiteInfo.F_FireID = obj.FireID;
                        //fireSiteInfo.FS_Latitude = obj.Latitude;
                        //fireSiteInfo.FS_Longitude = obj.Longitude;
                        //fireSiteInfo.FS_Cont = string.Empty;
                        //fireSiteInfo.FS_TransDatetime = DateTime.Now;
                        //fireSiteInfo.FS_HandleTime = info.F_TransDatetime;
                        //fireSiteInfo.FS_TransUser = obj.TransUser;
                        //fireSiteInfo.FS_TransIP = CheckRequest.GetWebClientIp();
                        //int fsID = bll.AddFireSite(fireSiteInfo);

                        foreach (var item in obj.Sites)
                        {
                            F_FireSite tempFireSiteInfo = new F_FireSite();
                            tempFireSiteInfo.F_FireID = fireID;
                            tempFireSiteInfo.FS_Latitude = item.Latitude;
                            tempFireSiteInfo.FS_Longitude = item.Longitude;
                            tempFireSiteInfo.FS_Cont = string.Empty;
                            tempFireSiteInfo.FS_TransDatetime = DateTime.Now;
                            tempFireSiteInfo.FS_HandleTime = info.F_TransDatetime;
                            tempFireSiteInfo.FS_TransUser = obj.TransUser;
                            tempFireSiteInfo.FS_TransIP = CheckRequest.GetWebClientIp();
                            bll.AddFireSite(tempFireSiteInfo);
                        }

                        //发送短信
                        if (obj.SMSs != null && obj.SMSs.Count > 0)
                        {
                            foreach (var item in obj.SMSs)
                            {
                                Sms_SendInfo model = new Sms_SendInfo();
                                model.IsAchieved = false;
                                model.IsSafety = false;
                                model.SendDate = DateTime.Now;
                                model.SmsInfo = obj.SMSMessage;
                                model.Sender =obj.TransUser;
                                model.SenderCode = obj.TransUser;
                                model.SafetyCode = Guid.NewGuid().ToString();
                                model.MobileCode = item.UserPhone;
                                model.Receiver = item.UserName;
                                m_bll.CreateSms_SendInfo(model);
                            }
                        }
                        //市局向区县下发消息
                        int mid = aRes.Key;
                        string mcontent = aRes.Value;
                        Pickout.SendAreaMessage(obj.AddressCounty.ToInt(),fireID, mr.MR_Type.Value, mid, mrContent,0);
                        Pickout.SendAreaMessage(-1, fireID, mr.MR_Type.Value, mid, mrContent, 1);
                    }

                    //倒计时配置文件
                    Pickout.AddOrEditCountDownElement(new FireCountDownModel()
                    {
                        FireID = fireID,
                        ExpireTime = info.F_TransDatetime.Value.AddMinutes(5),
                        CountDownType = (int)RecordType.首报未按时操作
                    }, FCD_FILEPATH);
                    result.FirstParam = fireID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireInfo Server Error";
                Log.Error("FireInfoController AddFireInfo", ex.ToString());
            }

            return result;
        }

        #endregion

        #region 火灾转场


        /// <summary>
        /// 市局--增加火灾转场信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireTransField(FireTransFieldReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    //改变当前火灾信息  isState=1,M_Record,Automatic
                    F_FireInfo info = bll.FireInfoDetail(obj.FireID);

                    //将当前火灾状态变更为 火灾结束
                    bll.UpdateFireInfoState(info.F_FireID,1);

                    //获取城市名
                    string countyName = linchpinBll.GetLinchpin(obj.TransInAddressCounty.ToInt()).LinchpinName;
                    string happenDatetime = info.F_HappenDatetime.Value.ToString("yyyy年MM月dd日HH时mm分");
                    //将处置中添加 MType=24 处置消息
                    M_Record mr = new M_Record();
                    mr.F_FireID = info.F_FireID;
                    mr.MR_HandleTime = info.F_HappenDatetime;
                    mr.MR_Type = (int)RecordType.火灾转场;
                    mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                    string mrContent = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), info.F_FireName,obj.TransReason, countyName);
                    mr.MR_Content = mrContent;
                    mr.MR_AddTime = info.F_TransDatetime;
                    mr.MR_AddIP = info.F_TransIP;
                    mr.MR_Applicant = info.F_Alarm;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);

                    //将消息表添加M_Type=20 的消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = mrContent;
                    ma.M_DistrictID = Convert.ToInt32(info.F_AddressCounty);
                    ma.M_AssembleID = 0;
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.火灾转场出;
                    ma.F_FireID = info.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = DateTime.Now;
                    ma.M_IsFull = 1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = info.F_TransIP;
                    //火灾转出消息
                    int outMid = bll.AddFireAutomatic(ma);
                    string outContent = mrContent;



                    //设置新的火灾地址
                    List<long> linchpinIDs = new List<long>() {
                        obj.TransInAddressCounty.ToLong(),
                        obj.TransInAddressTown.ToLong(),
                        obj.TransInAddressVillage.ToLong(),
                    };
                    var linchpinList=linchpinBll.GetLinchpin(linchpinIDs);


                    //写入新的火灾信息
                    F_FireInfo newInfo = info.SerializeJSON().DeserializeJSON<F_FireInfo>();//深度拷贝
                    newInfo.F_TransDatetime = DateTime.Now;
                    newInfo.F_FireName = linchpinList[0].LinchpinName + obj.DateTime.Value.ToString("yyyy年MM月dd日HH时mm分");
                    newInfo.F_ReporterName = string.Empty;
                    newInfo.F_ReporterUnit = string.Empty;
                    newInfo.F_ReporterWorknum = string.Empty;
                    newInfo.F_ReporterPhone = string.Empty;
                    newInfo.F_Address = linchpinList[0].LinchpinName+linchpinList[1].LinchpinName+linchpinList[2].LinchpinName;
                    newInfo.F_AddressCounty = obj.TransInAddressCounty;
                    newInfo.F_AddressTown = obj.TransInAddressTown;
                    newInfo.F_AddressVillage = obj.TransInAddressVillage;
                    
                    newInfo.F_AlarmContent =string.Format("{0}({1})经调查，已经转到{2}，请尽快赶往现场扑救。",info.F_FireName,info.F_Address,newInfo.F_Address);
                    newInfo.F_IsChange = 1;
                    newInfo.F_ChangeFireID = info.F_FireID;
                    newInfo.F_TransUser = obj.TransUser;
                    int newFID=bll.Add(newInfo);


                    //获取用户信息
                    var userinfo=userInfoBll.UsersInfosInfo(obj.TransUser.ToInt());

                    //将处置中添加 MType=0 处置消息
                    M_Record mr2 = new M_Record();
                    mr2.F_FireID = newInfo.F_FireID;
                    mr2.MR_HandleTime = newInfo.F_HappenDatetime;
                    mr2.MR_Type = (int)RecordType.火警录入;
                    mr2.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                    string username = userinfo.UserName;
                    string newHappenDatetime = obj.DateTime.Value.ToString("yyyy年MM月dd日HH时mm分");
                    string mrContent2 = string.Format("{0}接{1}报警在 {2} 发生火灾，接报案内容：{3}({4})经调查，已经转到{5}，请尽快赶往现场扑救。"
                                                     , newHappenDatetime, username, newInfo.F_Address,info.F_FireName,info.F_Address,newInfo.F_Address);
                    mr2.MR_Content = mrContent2;
                    mr2.MR_AddTime = newInfo.F_TransDatetime;
                    mr2.MR_AddIP = newInfo.F_TransIP;
                    mr2.MR_Applicant = username;
                    mr2.CFA_ID = 0;
                    bll.AddRecord(mr2);

                    //将消息中添加 0，13，17类型消息
                    KeyValuePair<int, string> aRes=bll.AddFireAutomaticForTransField(newInfo);


                    //写入倒计时配置文件
                    Pickout.AddOrEditCountDownElement(new FireCountDownModel()
                    {
                        FireID = newFID,
                        ExpireTime = DateTime.Now.AddMinutes(5),
                        CountDownType = (int)RecordType.首报未按时操作
                    }, FCD_FILEPATH);


                    //关闭老火灾的超时记录
                    Pickout.RemoveCountDownElement(new FireCountDownModel() {
                        FireID= info.F_FireID,
                    },FCD_FILEPATH);


                    //写入转场信息
                    F_FireTransField ftInfo = new F_FireTransField();
                    ftInfo.F_FireID = obj.FireID;
                    ftInfo.FT_TransOutAddressCounty = obj.TransOutAddressCounty;
                    ftInfo.FT_TransOutAddressTown = obj.TransOutAddressTown;
                    ftInfo.FT_TransOutAddressVillage = obj.TransOutAddressVillage;
                    ftInfo.FT_TransOutAddress = obj.TransOutAddress;
                    ftInfo.FT_TransInAddressCounty = obj.TransInAddressCounty;
                    ftInfo.FT_TransInAddressTown = obj.TransInAddressTown;
                    ftInfo.FT_TransInAddressVillage = obj.TransInAddressVillage;
                    ftInfo.FT_TransInAddress = obj.TransInAddress;
                    ftInfo.FT_TransReason = obj.TransReason;
                    ftInfo.FT_DateTime = obj.DateTime;
                    ftInfo.FT_TransDatetime = DateTime.Now;
                    ftInfo.FT_TransUser = obj.TransUser;
                    ftInfo.FT_TransIP = CheckRequest.GetWebClientIp();
                    bll.AddFireTransField(ftInfo);




                    #region 市局向2个区县下发消息

                    //向转出火灾所在地发送信息
                    Pickout.SendAreaMessage(info.F_AddressCounty.ToInt(), info.F_FireID, mr.MR_Type.Value, outMid, outContent, 0);

                    //向转入火灾所在地发送消息
                    int inMid = aRes.Key;
                    string inContent = aRes.Value;
                    Pickout.SendAreaMessage(obj.TransInAddressCounty.ToInt(), newFID, mr2.MR_Type.Value, inMid, inContent, 0);

                    #endregion

                    //返回的转场的火灾ID，并不是转场信息ID
                    result.FirstParam = newFID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireTransField Server Error";
                Log.Error("FireInfoController AddFireTransField ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///  市局--获取火灾转场信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>、
        [HttpPost]
        public ResultDto<dynamic> GetFireTransField(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                  
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);

                    List<long> linchpinIDs = new List<long>() {
                        fireInfo.F_AddressCounty.ToLong(),
                        fireInfo.F_AddressVillage.ToLong(),
                        fireInfo.F_AddressTown.ToLong(),
                    };

                    //F_FireTransField info = bll.FireTransFieldDetail(obj.FireID);
                    //List<long> linchpinIDs = new List<long>() {
                    //    info.FT_TransInAddressCounty.ToLong(),
                    //    info.FT_TransInAddressVillage.ToLong(),
                    //    info.FT_TransInAddressTown.ToLong(),
                    //    info.FT_TransOutAddressCounty.ToLong(),
                    //    info.FT_TransOutAddressVillage.ToLong(),
                    //    info.FT_TransOutAddressTown.ToLong()
                    //};

                    List<Linchpin> linchpins= linchpinBll.GetLinchpin(linchpinIDs);
                    result.FirstParam = linchpins;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireTransField Server Error";
                Log.Error("FireInfoController GetFireTransField ", ex.ToString());
            }
            return result;
        }
        #endregion

        #region 定位火点
        /// <summary>
        /// 市局--增加定位火点
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireSite(FireSiteReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {

                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    string lastLongitudeString = string.Empty;
                    string lastLatitudeString = string.Empty;
                    foreach (var item in obj.Sites)
                    {
                        F_FireSite info = new F_FireSite();
                        info.F_FireID = obj.FireID;
                        info.FS_Latitude = item.Latitude;
                        info.FS_Longitude = item.Longitude;
                        info.FS_Cont = string.Empty;
                        info.FS_TransDatetime = DateTime.Now;
                        info.FS_HandleTime = info.FS_TransDatetime;
                        info.FS_TransUser = obj.TransUser;
                        info.FS_TransIP = CheckRequest.GetWebClientIp();
                        int fsID=bll.AddFireSite(info);
                        //定位火点处置
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.FS_TransDatetime;
                        mr.MR_Type = (int)RecordType.定位火点;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        string[] longitudeSplit = info.FS_Longitude.Split('|');
                        string longitudeString = longitudeSplit[0] + "度" + longitudeSplit[1] + "分" + longitudeSplit[2] + "秒";
                        string[] latitudeSplit = info.FS_Latitude.Split('|');
                        string latitudeString = latitudeSplit[0] + "度" + latitudeSplit[1] + "分" + latitudeSplit[2] + "秒";
                        mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), longitudeString, latitudeString);
                        mr.MR_AddTime = info.FS_HandleTime;
                        mr.MR_AddIP = info.FS_TransIP;
                        mr.MR_Applicant = obj.TransUserName;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);
                        lastLongitudeString = longitudeString;
                        lastLatitudeString = latitudeString;
                        result.FirstParam = fsID;
                    }

                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = string.Format("{0}具体火点位置经度：{1} 纬度：{2}。", fireInfo.F_FireName, lastLongitudeString, lastLatitudeString);
                    ma.M_DistrictID = 0;
                    ma.M_AssembleID = 0;
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.定位火点;
                    ma.F_FireID = fireInfo.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = DateTime.Now;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = CheckRequest.GetWebClientIp();
                    int mID = bll.AddFireAutomatic(ma);
                    string mContent = ma.M_Content;
                    Pickout.SendAreaMessage(fireInfo.F_AddressCounty.ToInt(), fireInfo.F_FireID, (int)RecordType.定位火点, mID, mContent, 0);
                    
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireSite Server Error";
                Log.Error("FireInfoController AddFireSite ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 市局--获取定位火点
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetFireSite(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null && obj.FireID != 0)
                {
                    F_FireInfo info = bll.FireInfoDetail(obj.FireID);
                    if (info != null)
                    {
                        F_FireSite fsInfo = bll.FireSiteDetail(obj.FireID);
                        var data = new
                        {
                            FireID = fsInfo == null ? info.F_FireID : fsInfo.F_FireID,
                            FireName=info.F_FireName,
                            ID = fsInfo == null ? 0 : fsInfo.FS_ID,
                            Latitude = fsInfo == null ? null : fsInfo.FS_Latitude,
                            Longitude = fsInfo == null ? null : fsInfo.FS_Longitude,
                            Content = fsInfo == null ? null : fsInfo.FS_Cont
                        };
                        result.FirstParam = data;
                        result.Status = 1;
                        result.Message = "Request Is Success";
                    }
                    else
                    {
                        result.Status = 0;
                        result.Message = "Missing Required Parameters";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireSite Server Error";
                Log.Error("FireInfoController GetFireSite ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 市局--获取多个定位火点
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetFireSiteByFireID(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null && obj.FireID != 0)
                {
                    F_FireInfo info = bll.FireInfoDetail(obj.FireID);
                    if (info != null)
                    {
                        var  fsInfo = bll.GetFireSiteInfoByFireID(obj.FireID);
                        List<dynamic> list = new List<dynamic>();

                        foreach (var item in fsInfo)
                        {
                            var data = new
                            {
                                FireID = fsInfo == null ? info.F_FireID : item.F_FireID,
                                FireName = info.F_FireName,
                                ID = fsInfo == null ? 0 : item.FS_ID,
                                Latitude = fsInfo == null ? null : item.FS_Latitude,
                                Longitude = fsInfo == null ? null : item.FS_Longitude,
                                Content = fsInfo == null ? null : item.FS_Cont
                            };
                            list.Add(data);
                        }
                     
                        result.FirstParam = list;
                        result.Status = 1;
                        result.Message = "Request Is Success";
                    }
                    else
                    {
                        result.Status = 0;
                        result.Message = "Missing Required Parameters";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireSiteByFireID Server Error";
                Log.Error("FireInfoController GetFireSiteByFireID ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 市局--修改定位火点
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> UpdateFireSite(UpdateFireSiteReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    F_FireSite info = new F_FireSite();
                    info.FS_ID = obj.ID;
                    info.F_FireID = obj.FireID;
                    info.FS_Longitude = obj.Sites!=null && obj.Sites.Count>0?  obj.Sites[0].Longitude:string.Empty;
                    info.FS_Latitude = obj.Sites != null && obj.Sites.Count > 0 ? obj.Sites[0].Latitude:string.Empty;
                    info.FS_Cont = obj.Content;
                    info.FS_TransDatetime = DateTime.Now;
                    info.FS_HandleTime = info.FS_TransDatetime;
                    info.FS_TransUser = obj.TransUser;
                    info.FS_TransIP = CheckRequest.GetWebClientIp();
                    int fsID = bll.UpdateFireSite(info);
                    if (fsID != 0)
                    {
                        //更新定位火点
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.FS_TransDatetime;
                        mr.MR_Type = (int)RecordType.定位火点;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        string[] longitudeSplit = info.FS_Longitude.Split('|');
                        string longitudeString = longitudeSplit[0] + "度" + longitudeSplit[1] + "分" + longitudeSplit[2] + "秒";
                        string[] latitudeSplit = info.FS_Latitude.Split('|');
                        string latitudeString = latitudeSplit[0] + "度" + latitudeSplit[1] + "分" + latitudeSplit[2] + "秒";
                        mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), longitudeString, latitudeString);
                        mr.MR_AddTime = info.FS_HandleTime;
                        mr.MR_AddIP = info.FS_TransIP;
                        mr.MR_Applicant = obj.TransUserName;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);

                        //写入消息
                        M_Automatic ma = new M_Automatic();
                        ma.M_Content = string.Format("{0}具体火点位置经度：{1} 纬度：{2}。", fireInfo.F_FireName, longitudeString, latitudeString);
                        ma.M_DistrictID = 0;
                        ma.M_AssembleID = 0;
                        ma.M_From = 1;
                        ma.M_To = 0;
                        ma.M_Type = (int)AutomaticType.定位火点;
                        ma.F_FireID = info.F_FireID;
                        ma.M_Receive = obj.TransUser;
                        ma.M_UserID = obj.TransUser.ToInt();
                        ma.M_HandleTime = DateTime.Now;
                        ma.M_IsFull = -1;
                        ma.M_Account = "-1";
                        ma.M_NUserID = -1;
                        ma.M_NHandleTime = ma.M_HandleTime;
                        ma.M_NIsReplay = -1;
                        ma.M_IsReplay = 1;
                        ma.M_Intime = ma.M_HandleTime;
                        ma.M_DateTime = ma.M_HandleTime;
                        ma.M_User_ID = ma.M_UserID;
                        ma.M_IP = info.FS_TransIP;
                        bll.AddFireAutomatic(ma);
                    }
                    result.FirstParam = fsID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController UpdateFireSite Server Error";
                Log.Error("FireInfoController UpdateFireSite ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 纪录文档
        /// <summary>
        /// 市局--增加纪录文档
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFireDocumentation(FireDocumentationReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null  &&  CheckRequest.IsNumber(obj.TransUser)==true && !string.IsNullOrEmpty(obj.TransUserName) )
                {
                    F_FireDocumentation info = new F_FireDocumentation();
                    info.F_FireID = obj.FireID;
                    info.FD_MessageContent = obj.MessageContent;
                    info.FD_MessageTime = obj.MessageTime;     
                    info.FD_TransDatetime = DateTime.Now;
                    info.FD_TransUser = obj.TransUser;
                    info.FD_TransIP = CheckRequest.GetWebClientIp();
                    int fdID = bll.AddFireDocumentation(info);
                    List<string> receivers = new List<string>();
                    //发送短信
                    if (obj.SMSs != null && obj.SMSs.Count > 0)
                    {
                        foreach (var item in obj.SMSs)
                        {
                            Sms_SendInfo model = new Sms_SendInfo();
                            model.IsAchieved = false;
                            model.IsSafety = false;
                            model.SendDate = DateTime.Now;
                            model.SmsInfo = obj.SMSMessage;
                            model.Sender = obj.TransUser;
                            model.SenderCode = obj.TransUser;
                            model.SafetyCode = Guid.NewGuid().ToString();
                            model.MobileCode = item.UserPhone;
                            model.Receiver = item.UserName;
                            m_bll.CreateSms_SendInfo(model);
                            receivers.Add(item.UserName);
                        }
                    }
                    if (fdID != 0)
                    {
                        //增加火灾文档处置信息
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.FD_TransDatetime;
                        mr.MR_Type = (int)RecordType.记录文档;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        string mrContent = info.FD_MessageContent;
                        mr.MR_Content = mrContent;
                        mr.MR_AddTime = info.FD_TransDatetime;
                        mr.MR_AddIP = info.FD_TransIP;
                        mr.MR_Applicant = obj.TransUserName;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);
                    }

                    result.FirstParam = fdID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireDocumentation Server Error";
                Log.Error("FireInfoController AddFireDocumentation ", ex.ToString());
            }
            return result;
        }

    
        /// <summary>
        /// 市局--修改记录文档
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> UpdateFireDocumentation(FireDocumentationReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null && CheckRequest.IsNumber(obj.TransUser) == true)
                {
                    F_FireDocumentation info = new F_FireDocumentation();
                    info.FD_ID = obj.ID;
                    info.FD_MessageContent = obj.MessageContent;
                    info.FD_MessageTime = obj.MessageTime;
                    info.FD_TransDatetime = DateTime.Now;
                    info.FD_TransUser = obj.TransUser;
                    info.FD_TransIP = CheckRequest.GetWebClientIp();
                    int fdID = bll.UpdateFireDocumentation(info);
                    if (fdID != 0)
                    {
                        //增加火灾文档处置信息
                        M_Record mr = new M_Record();
                        mr.F_FireID = info.F_FireID;
                        mr.MR_HandleTime = info.FD_TransDatetime;
                        mr.MR_Type = (int)RecordType.记录文档;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        string mrContent = info.FD_MessageContent;
                        mr.MR_Content = mrContent;
                        mr.MR_AddTime = info.FD_TransDatetime;
                        mr.MR_AddIP = info.FD_TransIP;
                        mr.MR_Applicant = obj.TransUserName;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);
                    }
                    result.FirstParam = fdID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireDocumentation Server Error";
                Log.Error("FireInfoController AddFireDocumentation ", ex.ToString());
            }
            return result;
        }



        /// <summary>
        /// 市局--获取记录文档列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetFireDocumentation(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null && obj.FireID != 0)
                {
                    F_FireInfo info = bll.FireInfoDetail(obj.FireID);
                    if (info != null)
                    {
                        List<F_FireDocumentation> fdList = bll.GetFireDocumentation(obj.FireID);
                        var list = fdList.Select(s => new { ID = s.FD_ID, MessageTime = s.FD_MessageTime, MessageContent = s.FD_MessageContent })
                                       .OrderByDescending(o => o.MessageTime).ToInt();
                        result.FirstParam = list;
                        result.SecondParam = info.F_FireName;
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireDocumentation Server Error";
                Log.Error("FireInfoController GetFireDocumentation ", ex.ToString());
            }
            return result;
        }
        #endregion

        #region 成立前指

        /// <summary>
        /// 市局--获取指挥部列表 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetOrgInfo()
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {

                var list = memberInfoBll.GetOrgInfo();
                result.FirstParam = list;
                result.Status = 1;
                result.Message = "Request Is Success";
               
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetOrgInfo Server Error";
                Log.Error("FireInfoController GetOrgInfo ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 市局--获取指挥部下的用户
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetMemberInfo(OrgInfoReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                var list = memberInfoBll.GetMemberInfo(obj.OrgID);
                result.FirstParam = list;
                result.Status = 1;
                result.Message = "Request Is Success";

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetMemberInfo Server Error";
                Log.Error("FireInfoController GetMemberInfo ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 市局--增加前指
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFrontCommand(FrontCommandReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    F_FrontCommand info = new F_FrontCommand();
                    info.FC_ID = obj.ID;
                    info.F_FireID = obj.FireID;
                    info.FC_CommanderName = obj.CommanderName;
                    info.FC_Tel = obj.Tel;
                    info.FC_LocaleLatitude = obj.LocaleLatitude;
                    info.FC_LocaleLongitude = obj.LocaleLongitude;
                    info.FC_Member = obj.Member;
                    info.FC_TransDatetime = DateTime.Now;
                    info.FC_HandleTime = info.FC_TransDatetime;
                    info.FC_TransUser = obj.TransUser;
                    info.FC_TransIP = CheckRequest.GetWebClientIp();
                    info.FC_Address = obj.Address;
                    int fcID = 0;
                    if (info.FC_ID > 0)//更新
                    {
                        fcID= bll.UpdateFrontCommand(info);
                    }
                    else //新增
                    {
                        fcID= bll.AddFrontCommand(info);
                    }
                    //坐标地址字符串转换
                    string[] longitudeSplit = info.FC_LocaleLongitude.Split('|');
                    string longitudeString = longitudeSplit[0] + "度" + longitudeSplit[1] + "分" + longitudeSplit[2] + "秒";
                    string[] latitudeSplit = info.FC_LocaleLatitude.Split('|');
                    string latitudeString = latitudeSplit[0] + "度" + latitudeSplit[1] + "分" + latitudeSplit[2] + "秒";

                    //获取前指人员集合
                    string[] mArray = info.FC_Member.Split(',');
                    List<int> mIDs = mArray.Select(s => s.ToInt()).ToList();

                    //2018-08-15 新调整指挥部成员
                    List<F_UnitHeadquarterMember> mInfo = memberInfoBll.GetUnitHeadquarterMember(mIDs);
                    string mNames = string.Join(",", mInfo.Select(s => s.UHM_Leader).ToList());

                    //老版本指挥部成员
                    //List<F_MemberInfo> mInfo = memberInfoBll.GetMemberInfo(mIDs);
                    //string mNames = string.Join(",", mInfo.Select(s => s.M_MemberName).ToList());


                    //临时添加人员
                    string fcContent = string.Empty;
                    if (obj.FTPs != null && obj.FTPs.Count > 0)
                    {
                        fcContent += "临时调动:";
                        List<F_FrontCommandTempPeople> tpList = new List<F_FrontCommandTempPeople>();
                        List<string> joinList = new List<string>();
                        for (int i = 0; i < obj.FTPs.Count; i++)
                        {

                            if (string.IsNullOrEmpty(obj.FTPs[i].Name)
                                 || string.IsNullOrEmpty(obj.FTPs[i].PositionTitle)
                                 || string.IsNullOrEmpty(obj.FTPs[i].Phone))
                            {
                                continue;
                            }

                            F_FrontCommandTempPeople tp = new F_FrontCommandTempPeople();
                            tp.F_FireID = obj.FireID;
                            tp.FC_ID =info.FC_ID ;
                            tp.Name = obj.FTPs[i].Name;
                            tp.PositionTitle = obj.FTPs[i].PositionTitle;
                            tp.Phone = obj.FTPs[i].Phone;
                            tpList.Add(tp);
                            joinList.Add(string.Format("{0}{1}({2})", tp.Name, tp.PositionTitle, tp.Phone));
                        }
                        bll.AddFrontCommandTempPeople(tpList);
                        if (tpList.Count > 0)
                        {
                            fcContent += string.Join(",", joinList);
                        }
                    }

                    //添加火灾处置信息
                    M_Record mr = new M_Record();
                    mr.F_FireID = info.F_FireID;
                    mr.MR_HandleTime = info.FC_TransDatetime;
                    mr.MR_Type = (int)RecordType.成立前指;
                    mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                    mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value)
                                                      , fireInfo.F_FireName,info.FC_Address,longitudeString, latitudeString,info.FC_CommanderName, mNames) +fcContent;
                    mr.MR_AddTime = info.FC_HandleTime;
                    mr.MR_AddIP = info.FC_TransIP;
                    mr.MR_Applicant = obj.TransUserName;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);


                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value)
                                                      , fireInfo.F_FireName, info.FC_Address, longitudeString, latitudeString, info.FC_CommanderName, mNames)+ fcContent;
                    ma.M_DistrictID = 0;
                    ma.M_AssembleID = fireInfo.F_AddressCounty.ToInt();
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.成立前指;
                    ma.F_FireID = info.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = DateTime.Now;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = info.FC_TransIP;
                    int mID= bll.AddFireAutomatic(ma);
                    string mContent = ma.M_Content;
                    Pickout.SendAreaMessage(fireInfo.F_AddressCounty.ToInt(),info.F_FireID.Value, mr.MR_Type.Value, mID, mContent, 0);


                    result.FirstParam = fcID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                   
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required  Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFrontCommand Server Error";
                Log.Error("FireInfoController AddFrontCommand ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 市局--获取前指
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic, dynamic> GetFrontCommand(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic, dynamic> result = new ResultDto<dynamic, dynamic, dynamic>();
            try
            {
                if (obj != null && obj.FireID!=0)
                {

                    //start->排除重复FC_Member
                    List<F_FrontCommand> list= bll.GetFrontCommand(obj.FireID);
                    List<string>  tempList=list.Select(s => s.FC_Member).ToList();
                    List<int> mIdList = new List<int>();
                    for (int i = 0; i < tempList.Count; i++)
                    {
                        string mIDsStr = tempList[i];
                        if (mIDsStr.IndexOf(",") != -1)
                        {
                            string[] mids = mIDsStr.Split(',');
                            for (int j = 0; j < mids.Length; j++)
                            {
                                mIdList.Add(mids[j].ToInt());
                            }
                        }
                        else
                        {
                            mIdList.Add(mIDsStr.ToInt());
                        }
                    }
                    mIdList = mIdList.Distinct().ToList();
                    List<F_MemberInfo> mList = memberInfoBll.GetMemberInfo(mIdList);
                    //end->排除重复FC_Member

                    F_FrontCommand fcInfo = list.LastOrDefault();
                    List<F_FrontCommandTempPeople> ftps = new List<F_FrontCommandTempPeople>();
                    if (fcInfo != null)
                    {
                        ftps = bll.GetFrontCommandTempPeople(fcInfo.F_FireID.Value, fcInfo.FC_ID);
                    }

                    fcInfo.FC_Member = string.Join(",", mList.Select(s => s.M_MemberName).ToList());
                    result.FirstParam = fcInfo;
                    result.SecondParam = mList.Select(s => new { MemberID=s.M_ID, MemberName=s.M_MemberName }).ToList();
                    result.ThirdParam = ftps;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFrontCommand Server Error";
                Log.Error("FireInfoController GetFrontCommand ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 逐级响应

        /// <summary>
        /// 市局--增加逐级响应
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddStepResponse(FourStepResponseReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    int responseGrade = 3;//默认级别
                    F_StepResponse fsInfo=bll.StepResponseDetail(obj.FireID);
                    responseGrade= fsInfo!=null ? responseGrade = fsInfo.SR_ResponseGrade.Value - 1 :  responseGrade ;

                    //级别超过现有级别设置
                    if (responseGrade<=0)
                    {
                        responseGrade = 1;
                    }
                    F_StepResponse info = new F_StepResponse();
                    info.F_FireID = obj.FireID;
                    info.SR_ResponseGrade = responseGrade;
                    info.SR_StartDatetime = obj.StartDatetime;
                    info.SR_Starter = obj.Starter;
                    info.SR_InstructionContent = obj.InstructionContent;
                    info.SR_StartCondition = obj.StartCondition;
                    info.SR_TransDatetime = DateTime.Now;
                    info.SR_HandleTime = info.SR_TransDatetime;
                    info.SR_TransUser = obj.TransUser;
                    info.SR_TransIP = CheckRequest.GetWebClientIp();
                    int srID=bll.AddStepResponse(info);

                    //临时添加人员
                    string stContent = string.Empty;
                    if (obj.FTPs != null && obj.FTPs.Count > 0)
                    {
                        stContent += "紧急派遣信息:";
                        List<F_StepResponseTempPeople> tpList = new List<F_StepResponseTempPeople>();
                        List<string> joinList = new List<string>();
                        for (int i = 0; i < obj.FTPs.Count; i++)
                        {
                            if (string.IsNullOrEmpty(obj.FTPs[i].Name)
                                 || string.IsNullOrEmpty(obj.FTPs[i].PositionTitle)
                                 || string.IsNullOrEmpty(obj.FTPs[i].Phone))
                            {
                                continue;
                            }
                            F_StepResponseTempPeople tp = new F_StepResponseTempPeople();
                            tp.F_FireID = obj.FireID;
                            tp.SR_ID = srID;
                            tp.Name = obj.FTPs[i].Name;
                            tp.PositionTitle = obj.FTPs[i].PositionTitle;
                            tp.Phone = obj.FTPs[i].Phone;
                            tpList.Add(tp);
                            joinList.Add(string.Format("{0}{1}({2})", tp.Name, tp.PositionTitle, tp.Phone));

                            #region 发送短信
                            Sms_SendInfo model = new Sms_SendInfo();
                            model.IsAchieved = false;
                            model.IsSafety = false;
                            model.SendDate = DateTime.Now;
                            model.SmsInfo = fireInfo.F_FireName + " 现已启动"+ CheckRequest.GetLoumaNumber(responseGrade) + "级响应，需要你部支援，请速与市局联系！！";
                            model.Sender = obj.TransUser;
                            model.SenderCode = obj.TransUser;
                            model.SafetyCode = Guid.NewGuid().ToString();
                            model.MobileCode = obj.FTPs[i].Phone;
                            model.Receiver = obj.FTPs[i].Name;
                            m_bll.CreateSms_SendInfo(model);
                            #endregion


                        }
                        bll.AddStepResponseTempPeople(tpList);
                        if (tpList.Count > 0)
                        {
                            stContent += string.Join(",", joinList);
                        }
                    }

                    //将处置中添加 MType=20 处置消息
                    M_Record mr = new M_Record();
                    mr.F_FireID = info.F_FireID;
                    mr.MR_HandleTime = info.SR_TransDatetime;
                    mr.MR_Type = (int)RecordType.逐级响应;
                    mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                    string mrContent = string.Format("{0}已启动{1}级响应。", fireInfo.F_FireName, CheckRequest.GetLoumaNumber(responseGrade)) + stContent;
                    mr.MR_Content = mrContent;
                    mr.MR_AddTime = info.SR_TransDatetime;
                    mr.MR_AddIP = info.SR_TransIP;
                    mr.MR_Applicant = obj.TransUserName;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);

                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = string.Format("{0}已启动{1}级响应。",fireInfo.F_FireName, CheckRequest.GetLoumaNumber(responseGrade))+ stContent;
                    ma.M_DistrictID = 0;
                    ma.M_AssembleID = fireInfo.F_AddressCounty.ToInt();
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.逐级响应;
                    ma.F_FireID = info.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = info.SR_TransDatetime;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = info.SR_TransIP;
                    int mID=bll.AddFireAutomatic(ma);
                    string mContent = ma.M_Content;
                    Pickout.SendAreaMessage(fireInfo.F_AddressCounty.ToInt(),info.F_FireID.Value, mr.MR_Type.Value, mID, mContent, 0);

                    result.FirstParam = srID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddStepResponse Server Error";
                Log.Error("FireInfoController AddStepResponse ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 市局--获取逐级响应
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetStepResponse(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    int responseGrade = 3;
                    F_StepResponse fsInfo = bll.StepResponseDetail(obj.FireID);
                    responseGrade = fsInfo != null ? responseGrade = fsInfo.SR_ResponseGrade.Value - 1 : responseGrade;
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    //级别超过现有级别设置
                    if (responseGrade <= 0)
                    {
                        responseGrade = 1;
                    }
                    //获取响应条件列表
                    List<F_StartCondition> scList= bll.GetStartCondition(responseGrade);

                    //FireTempPeople
                    List<F_StepResponseTempPeople> ftps = new List<F_StepResponseTempPeople>();
                    if (fsInfo != null)
                    {
                       ftps= bll.GetStepResponseTempPeople(fsInfo.F_FireID.Value, fsInfo.SR_ID);
                    }
                    
                    result.FirstParam =new  { F_AddressCounty = fireInfo.F_AddressCounty, ResponseGrade=responseGrade, LoumaNumber=CheckRequest.GetLoumaNumber(responseGrade),FTPs=ftps};
                    result.SecondParam = scList;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetStepResponse Server Error";
                Log.Error("FireInfoController GetStepResponse ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 扑救力量

        /// <summary>
        /// 市局--增加扑救力量
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFightingForce(FightingForceReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null && obj.FightingRanks!=null && obj.FightingRanks.Count>0)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);

                    foreach (var item in obj.FightingRanks)
                    {
                        var unEntity = memberInfoBll.GetUnitHeadquarter(item.FF_UH_ID).FirstOrDefault();
                        F_FightingForce ffInfo = new F_FightingForce()
                        {
                            F_FireID = obj.FireID,
                            FF_RanksNum = item.RanksNum,
                            FF_Annex = obj.Annex,
                            FF_HandleTime = DateTime.Now,
                            FF_TransDatetime = DateTime.Now,
                            FF_TransIP = CheckRequest.GetWebClientIp(),
                            FF_TransUser = obj.TransUser,
                            FF_UH_ID=item.FF_UH_ID,
                            FF_UH_LinchpinID = unEntity != null ? unEntity.UH_LinchpinID : new Nullable<int>(),
                        };

                        //找出响应级别
                        int responseGrade = 3;
                        F_StepResponse fsInfo = bll.StepResponseDetail(obj.FireID);
                        responseGrade = fsInfo != null ? responseGrade = fsInfo.SR_ResponseGrade.Value - 1 : responseGrade;

                        //扑救
                        int ffid=bll.AddFightingForce(ffInfo);

                        //处置记录
                        M_Record mr = new M_Record();
                        mr.F_FireID = ffInfo.F_FireID;
                        mr.MR_HandleTime = ffInfo.FF_HandleTime;
                        mr.MR_Type = (int)RecordType.市局调集扑救力量;// Pickout.GetMRecordTypeText(13);//处置类型描述
                        mr.MR_AddIP = ffInfo.FF_TransIP;
                        mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                        mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value), unEntity != null ? unEntity.UH_Name : string.Empty, ffInfo.FF_RanksNum);
                        mr.MR_AddTime = mr.MR_HandleTime;
                        mr.MR_Applicant = obj.TransUserName;
                        mr.CFA_ID = 0;
                        bll.AddRecord(mr);


                        string starter = fsInfo != null ? fsInfo.SR_Starter : "市局";
                        //消息
                        M_Automatic ma = new M_Automatic();
                        ma.M_Content = string.Format("{0}，经{1}领导同意，现已启动{2}级响应；请你区派出({3})支扑火队赶赴现场支援，收到消息请立即回复，说明具体情况。"
                                                   , fireInfo.F_FireName, starter, CheckRequest.GetLoumaNumber(responseGrade),ffInfo.FF_RanksNum);
                        ma.M_DistrictID = ffInfo.FF_UH_LinchpinID;  
                        ma.M_AssembleID = fireInfo.F_AddressCounty.ToInt();
                        ma.M_From = 1;
                        ma.M_To = 0;
                        ma.M_Type = (int)AutomaticType.调集其他区县扑火队;
                        ma.F_FireID = fireInfo.F_FireID;
                        ma.M_Receive = ffid.ToString();
                        ma.M_UserID = obj.TransUser.ToInt();
                        ma.M_HandleTime = ffInfo.FF_HandleTime;
                        ma.M_IsFull = -1;
                        ma.M_Account = "-1";
                        ma.M_NUserID = -1;
                        ma.M_NHandleTime = ma.M_HandleTime;
                        ma.M_NIsReplay = -1;
                        ma.M_IsReplay = 1;
                        ma.M_Intime = ma.M_HandleTime;
                        ma.M_DateTime = ma.M_HandleTime;
                        ma.M_User_ID = ma.M_UserID;
                        ma.M_IP = ffInfo.FF_TransIP;
                        int mID = bll.AddFireAutomatic(ma);
                        string mContent = ma.M_Content;
                        Pickout.SendAreaMessage(ffInfo.FF_UH_LinchpinID.Value, fireInfo.F_FireID, mr.MR_Type.Value, mID, mContent, 0);
                        result.FirstParam = ffid;
                    }
                    
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFightingForce Server Error";
                Log.Error("FireInfoController AddFightingForce ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 区县  特殊--增加回复市局扑救力量(队伍)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddFightingForceReply(FightingForceReplyReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    string countyName = linchpinBll.GetLinchpin(obj.ReplyCounty).LinchpinName;
                    string content = countyName+"对";
                    content += string.Format("{0}已派出 {1} 支扑火队伍赶往现场，分别为：",fireInfo.F_FireName,obj.FightingInfos.Count) ;
                    int total = 0;
                    for (int i = 0; i < obj.FightingInfos.Count; i++)
                    {
                        content += i + 1 + "：" + obj.FightingInfos[i].CaptainName ;
                        content += "(电话：" + obj.FightingInfos[i].CaptainPhone + ")带领的";
                        content += obj.FightingInfos[i].RankName + " ";
                        content += "人数： "+obj.FightingInfos[i].Sum+" ";
                        total += obj.FightingInfos[i].Sum;
                    }
                    content +=" 共计："+total+ "人，";
                    content += " 回复人："+obj.ReplyName+" ";

                    //写入支援表
                    F_FireForceSupport ffs = new F_FireForceSupport();
                    ffs.F_FireID = obj.FireID;
                    ffs.FFS_FromCounty = obj.ReplyCounty;
                    ffs.FFS_ToCounty = fireInfo.F_AddressCounty.ToInt();
                    ffs.FFS_Content = content;
                    ffs.FFS_TransDatetime = DateTime.Now;
                    ffs.FFS_TransIP = CheckRequest.GetWebClientIp();
                    ffs.FFS_TransUser = obj.TransUser.ToInt();
                    ffs.FFS_TransUserName = obj.TransUserName;
                    var ffsId=bll.AddFireForceSupport(ffs);



                    ////写入市局询问
                    //F_CityFireAsk ask = new F_CityFireAsk();
                    //ask.CFA_TransUser = obj.TransUser;
                    //ask.CFA_TransDatetime = DateTime.Now;
                    //ask.CFA_TransIP = CheckRequest.GetWebClientIp();
                    //ask.CFA_TransIP = ask.CFA_TransIP == null ? "0.0.0.0" : ask.CFA_TransIP;
                    //ask.CFA_HandleTime = ask.CFA_TransDatetime;
                    //ask.CFA_AskContent = content;
                    //ask.F_FireID = obj.FireID;
                    //int askId = bll.AddCityFireAsk(ask);

                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = content;
                    ma.M_DistrictID = obj.ReplyCounty;
                    ma.M_AssembleID =  0;
                    ma.M_From = 0;
                    ma.M_To = 1;
                    ma.M_Type = (int)AutomaticType.调集其他区县扑火队;
                    ma.F_FireID = fireInfo.F_FireID;
                    ma.M_Receive =string.Empty ;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = ffs.FFS_TransDatetime;
                    ma.M_IsFull = ffsId;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = CheckRequest.GetWebClientIp();
                    int mID = bll.AddFireAutomatic(ma);

                    //写入处置记录
                    M_Record mr = new M_Record();
                    mr.F_FireID = obj.FireID;
                    mr.MR_HandleTime = ffs.FFS_TransDatetime;
                    mr.MR_Type = (int)RecordType.市局调集扑救力量;
                    mr.MR_AddIP = CheckRequest.GetWebClientIp();
                    mr.MR_UserID = mr.MR_AddUser = obj.TransUser.ToInt();
                    mr.MR_Content = content;
                    mr.MR_AddTime = mr.MR_HandleTime;
                    mr.MR_Applicant = obj.TransUserName;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);

                    string mContent = ma.M_Content;
                    Pickout.SendAreaMessage(-1, fireInfo.F_FireID, mr.MR_Type.Value, mID, mContent, 1);

                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFightingForceReply Server Error";
                Log.Error("FireInfoController AddFightingForceReply ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 联动响应
        /// <summary>
        /// 市局--增加联动响应
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddResponse(ResponseReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    F_Response rInfo = new F_Response();
                    rInfo.F_FireID = obj.FireID;
                    rInfo.Res_UnitID = obj.UnitIDs;
                    rInfo.Res_TransDatetime = DateTime.Now;
                    rInfo.Res_HandleTime = rInfo.Res_TransDatetime;
                    rInfo.Res_TransUser = obj.TransUser;
                    rInfo.Res_TransIP = CheckRequest.GetWebClientIp();

                    if (!string.IsNullOrEmpty(obj.UnitIDs) && obj.UnitIDs.IndexOf(",") > -1)
                    {
                        //获取选中的成员单位下的所有发送短信的人员信息
                        List<int> uhm_depIDs = obj.UnitIDs.Split(',').ToArray()
                                          .Select<string, int>(s => Convert.ToInt32(s)).ToList();
                        var members = memberInfoBll.GetUnitHeadquarterMemberByDepIDs(uhm_depIDs);
                        List<string> receivers = new List<string>();
                        foreach (var item in members)
                        {
                            if (!string.IsNullOrEmpty(item.UHM_Tel))
                            {
                                Sms_SendInfo model = new Sms_SendInfo();
                                model.IsAchieved = false;
                                model.IsSafety = false;
                                model.SendDate = DateTime.Now;
                                model.SmsInfo = fireInfo.F_FireName + " 现已启动联动响应，需要你部支援，请速与市局联系！！";
                                model.Sender = obj.TransUser;
                                model.SenderCode = obj.TransUser;
                                model.SafetyCode = Guid.NewGuid().ToString();
                                model.MobileCode = item.UHM_Phone;
                                model.Receiver = item.UHM_Leader;
                                m_bll.CreateSms_SendInfo(model);
                                receivers.Add(item.UHM_Leader);
                            }
                            if (!string.IsNullOrEmpty(item.UHM_LiaisonPhone))
                            {
                                Sms_SendInfo model2 = new Sms_SendInfo();
                                model2.IsAchieved = false;
                                model2.IsSafety = false;
                                model2.SendDate = DateTime.Now;
                                model2.SmsInfo = fireInfo.F_FireName + " 现已启动联动响应，需要你部支援，请速与市局联系！！";
                                model2.Sender = obj.TransUser;
                                model2.SenderCode = obj.TransUser;
                                model2.SafetyCode = Guid.NewGuid().ToString();
                                model2.MobileCode = item.UHM_LiaisonPhone;
                                model2.Receiver = item.UHM_LiaisonName;
                                m_bll.CreateSms_SendInfo(model2);
                                receivers.Add(item.UHM_LiaisonName);
                            }
                        }
                    }

                    int rId = bll.AddResponse(rInfo);

                    result.FirstParam = rId;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddResponse Server Error";
                Log.Error("FireInfoController AddResponse ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 误报审批
        /// <summary>
        ///  市局--误报审批(更新 Catalog)FireID Results  Reason TransUser TransUserName
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ResultDto<int> UpdateCatalog(FireCatalogReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    Catalog info = new Catalog() {
                         F_FireID=obj.FireID,
                         A_Results=obj.Results,
                         A_Reason=obj.Reason,
                         A_ReaTime=DateTime.Now,
                    };

                    int updateRow=bll.UpdateCatalogToApproval(info);
                    string isAgreeStr = info.A_Results == null || info.A_Results.Value == 1 ? "不同意" : "同意";
                    //关闭火灾
                    if (info.A_Results == 0 && !string.IsNullOrEmpty(info.A_Reason) && info.A_ReaTime!=null &&updateRow >0)
                    {
                        //更改火灾信息表状态
                        int closeFireId = bll.UpdateFireInfoState(fireInfo.F_FireID, 1);
                        //移除未读续报等 倒计时配置文件
                        Pickout.RemoveCountDownElement(new FireCountDownModel()
                        {
                            FireID = info.F_FireID.Value,
                        }, FCD_FILEPATH);
                    }
                    //将区县误报申请时的消息ID设置为已读
                    bll.UpdateFireAutoMaticToReplay(obj.AutomaticID, obj.FireID, null);

                    //处置
                    M_Record mr = new M_Record();
                    mr.F_FireID = info.F_FireID;
                    mr.MR_HandleTime = info.A_ReaTime;
                    mr.MR_Type = (int)RecordType.误报审批;
                    mr.MR_Content = string.Format(Pickout.GetFireInfoFormat(mr.MR_Type.Value),isAgreeStr,info.A_Reason);
                    mr.MR_AddTime = info.A_ReaTime;
                    mr.MR_AddIP = CheckRequest.GetWebClientIp();
                    mr.MR_Applicant = obj.TranUserName;
                    mr.CFA_ID = 0;
                    bll.AddRecord(mr);

                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = mr.MR_Content;
                    ma.M_DistrictID = 0;
                    ma.M_AssembleID = 0;
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.误报申请;
                    ma.F_FireID = info.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = DateTime.Now;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = ma.M_IP;
                    

                    int mID = bll.AddFireAutomatic(ma);
                    string mContent = ma.M_Content;
                    Pickout.SendAreaMessage(fireInfo.F_AddressCounty.ToInt(), info.F_FireID.Value, mr.MR_Type.Value, mID, obj.Results+"_"+ mContent, 0);
                    result.FirstParam = updateRow;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController UpdateCatalog Server Error";
                Log.Error("FireInfoController UpdateCatalog ", ex.ToString());
            }
            return result;
        }
        #endregion

        #region 市局询问
        /// <summary>
        /// 市局--市局询问区县
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> CityAskCounty(CityAskCountyReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    //写入市局询问库
                    F_CityFireAsk ask = new F_CityFireAsk();
                    ask.CFA_TransUser = obj.TransUser;
                    ask.CFA_TransDatetime = DateTime.Now;
                    ask.CFA_TransIP = CheckRequest.GetWebClientIp();
                    ask.CFA_TransIP = ask.CFA_TransIP == null ? "0.0.0.0" : ask.CFA_TransIP;
                    ask.CFA_HandleTime = ask.CFA_TransDatetime;
                    ask.CFA_AskContent = obj.Message;
                    ask.F_FireID = obj.FireID;
                    int askId = bll.AddCityFireAsk(ask);
                    //写入火灾信息表
                    M_Record mr = new M_Record();
                    mr.F_FireID = obj.FireID;
                    mr.MR_HandleTime = ask.CFA_TransDatetime;
                    mr.MR_Type = (int)RecordType.市局询问;//市局询问 类型
                    mr.MR_UserID = 0;
                    mr.MR_Content = ask.CFA_AskContent;
                    mr.CFA_ID = askId;
                    mr.MR_AddTime = mr.MR_HandleTime;
                    mr.MR_AddUser = obj.TransUser.ToInt();
                    mr.MR_AddIP = ask.CFA_TransIP;
                    mr.MR_Applicant = obj.TransUserName;
                    int recordId = bll.AddRecord(mr);


                    string specialMsg = askId + "_" + obj.Message;
                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = specialMsg;
                    ma.M_DistrictID = fireInfo.F_AddressCounty.ToInt();
                    ma.M_AssembleID = 0;
                    ma.M_From = 1;
                    ma.M_To = 0;
                    ma.M_Type = (int)AutomaticType.火情询问;
                    ma.F_FireID = fireInfo.F_FireID;
                    ma.M_Receive = obj.TransUser;
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = DateTime.Now;
                    ma.M_IsFull = -1;
                    ma.M_Account = "-1";
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = -1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = ma.M_IP;
                    int mID = bll.AddFireAutomatic(ma);

                    //向区县发送询问 
                    Pickout.SendAreaMessage(fireInfo.F_AddressCounty.ToInt(), fireInfo.F_FireID, mr.MR_Type.Value, mID, specialMsg);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController CityAskCounty Server Error";
                Log.Error("FireInfoController CityAskCounty ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 区县--区县回答市局
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> CountyReplayCity(CountyReplayCityReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    string userName = string.Empty;
                    string askMessage = string.Empty;
                    string answerMessage = obj.Message;
                    int answerId = 0;
                    //校验市局询问
                    var askInfo = bll.CityFireAskDetail(obj.AskID);
                    askMessage = askInfo.CFA_AskContent;
                    userName =obj.TransUserName;
                    if (askInfo == null)
                    {
                        result.Status = 0;
                        result.Message = "Request Is Success";
                        return result;
                    }
                    //判定是否回复过
                    var existList = bll.ExistCountyFireAnswer(obj.AskID);

                    //写入区县回复库
                    F_CountyFireAnswer answer = new F_CountyFireAnswer();
                    answer.F_FireID = obj.FireID;
                    answer.CFA_ID = obj.AskID;
                    answer.Cou_Answerer = obj.TransUserName;
                    answer.Cou_AnswerContent = obj.Message;
                    answer.Cou_TransDatetime = DateTime.Now;
                    answer.Cou_HandleTime = answer.Cou_TransDatetime;
                    answer.Cou_TransUser = obj.TransUser;
                    answer.Cou_TransIP = CheckRequest.GetWebClientIp();
                    if (existList.Count == 0)
                    {
                        answerId = bll.AddCountyFireAnswer(answer);

                        //写入火灾处置表
                        M_Record record = new M_Record();
                        record.F_FireID = obj.FireID.ToInt();
                        record.MR_HandleTime = answer.Cou_TransDatetime;
                        record.MR_Type = (int)RecordType.区县回答;//区县回复 类型
                        record.MR_UserID = 0;
                        record.MR_Content = answer.Cou_AnswerContent;
                        record.CFA_ID = answer.CFA_ID;
                        record.MR_AddTime = askInfo.CFA_TransDatetime;
                        record.MR_AddUser = obj.TransUser.ToInt();
                        record.MR_AddIP = answer.Cou_TransIP;
                        record.MR_Applicant = obj.TransUserName;
                        int recordId = bll.AddRecord(record);
                    }
                    else
                    {
                        var temp = existList.FirstOrDefault();
                        temp.Cou_AnswerContent = answer.Cou_AnswerContent;
                        temp.Cou_Answerer = obj.TransUserName;
                        temp.Cou_TransDatetime = answer.Cou_TransDatetime;
                        temp.Cou_HandleTime = answer.Cou_HandleTime;
                        temp.Cou_TransUser = answer.Cou_TransUser;
                        answerId = bll.UpdateCountyFireAnswer(temp);
                    }

                    string specialMsg = askInfo.CFA_ID + "_" + askMessage + "_" +obj.AskID + "_" + answer.Cou_Answerer + "_" + answerMessage;
                    //写入消息
                    M_Automatic ma = new M_Automatic();
                    ma.M_Content = specialMsg;
                    ma.M_DistrictID = fireInfo.F_AddressCounty.ToInt();
                    ma.M_AssembleID = 0;
                    ma.M_From = 0;
                    ma.M_To = 1;
                    ma.M_Type = (int)AutomaticType.区县回复;
                    ma.F_FireID = fireInfo.F_FireID;
                    ma.M_Receive = "接收人";
                    ma.M_UserID = obj.TransUser.ToInt();
                    ma.M_HandleTime = DateTime.Now;
                    ma.M_IsFull = 1;
                    ma.M_Account = null;
                    ma.M_NUserID = -1;
                    ma.M_NHandleTime = ma.M_HandleTime;
                    ma.M_NIsReplay = 1;
                    ma.M_IsReplay = 1;
                    ma.M_Intime = ma.M_HandleTime;
                    ma.M_DateTime = ma.M_HandleTime;
                    ma.M_User_ID = ma.M_UserID;
                    ma.M_IP = CheckRequest.GetWebClientIp();
                    int maID = bll.AddFireAutomatic(ma);
                   
                    //向市局回复
                    Pickout.SendAreaMessage(-1, obj.FireID, (int)RecordType.区县回答, maID, specialMsg);
                    result.FirstParam = answerId;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController CountyReplayCity Server Error";
                Log.Error("FireInfoController CountyReplayCity ", ex.ToString());
            }
            return result;
        }

        #endregion

        #endregion

        #region 结束火灾所有相关信息
        /// <summary>
        /// 完成终报以后关闭所有火灾相关状态
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> SetFinishFire(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    //1.关闭消息
                    bll.UpdateFireAutoMatic(obj.FireID, null);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController SetFinishFire Server Error";
                Log.Error("FireInfoController SetFinishFire ", ex.ToString());
            }
            return result;
        }
        #endregion

        #region 火灾补录
        /// <summary>
        ///  补录--火灾补录接口
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddSupplementFireInfo(AddFireInfoReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null
                     && CheckRequest.IsNumber(obj.AddressCounty)
                     && CheckRequest.IsNumber(obj.AddressTown)
                     && CheckRequest.IsNumber(obj.AddressVillage)
                     && (!string.IsNullOrEmpty(obj.FireName)
                     && !string.IsNullOrEmpty(obj.AlarmContent)
                     && obj.HappenDatetime != null
                     && obj.AlarmTime != null))
                {
                    F_FireInfo info = new F_FireInfo();
                    info.F_FireName = obj.FireName;

                    info.F_HappenDatetime = obj.HappenDatetime;
                    info.F_TransDatetime = DateTime.Now;

                    info.F_AlarmTime = obj.AlarmTime;
                    info.F_AlarmContent = obj.AlarmContent;
                    info.F_Alarm = obj.Alarm;

                    info.F_AddressCounty = obj.AddressCounty;
                    info.F_AddressTown = obj.AddressTown;
                    info.F_AddressVillage = obj.AddressVillage;

                    List<Linchpin> temp = linchpinBll.GetLinchpin(new List<long>() { obj.AddressCounty.ToLong(), obj.AddressTown.ToLong(), obj.AddressVillage.ToLong() });
                    string address = string.Empty;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressCounty.ToLong()).FirstOrDefault().LinchpinName;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressTown.ToLong()).FirstOrDefault().LinchpinName;
                    address += temp.Where(p => p.LinchpinIID == obj.AddressVillage.ToLong()).FirstOrDefault().LinchpinName;
                    info.F_Address = address;

                    info.F_ReporterName = obj.ReporterName;
                    info.F_ReporterUnit = obj.ReporterUnit;
                    info.F_ReporterWorknum = obj.ReporterWorknum;
                    info.F_ReporterPhone = obj.ReporterPhone;

                    info.F_TransUser = obj.TransUser;
                    info.F_TransIP = CheckRequest.GetWebClientIp();
                    info.F_IsChange = 0;

                    //Stat 和 IsHb 都为1时,表示火灾已关闭及属于补录
                    info.F_IsState = 1;info.F_IsHb = 1;
                    int fireID = bll.Add(info);
                    result.FirstParam = fireID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddSupplement Server Error";
                Log.Error("FireInfoController AddSupplement ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 补录--获取补录火灾详细
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic, dynamic, dynamic> GetSupplementFireDetail(FireDetailReq obj)
        {
            ResultDto<dynamic, dynamic, dynamic, dynamic> result = new ResultDto<dynamic, dynamic, dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    F_FireInfo info = bll.FireInfoDetail(obj.FireID);
                    List<F_BlCons> tempBLCList = bll.GetFireBlCons(obj.FireID);

                    var blcList = tempBLCList.Select(s => new
                    {
                        ID = s.id,
                        F_FireID = s.F_fireid,
                        Contens = s.Contens,
                        CreateTime = s.CreateTime,
                    });

                    F_FireFinalReport finalReportInfo = bll.GetFireFinalReport(new List<int>() { obj.FireID }).FirstOrDefault();

                    List<F_FireAttachs> attachsList = bll.GetFireAttachs(obj.FireID);
                    foreach (var item in attachsList)
                    {
                        item.FilePath = VIDEO_PATH + item.FilePath;
                    }

                    result.FirstParam = info;
                    result.SecondParam = blcList;
                    result.ThirdParam = finalReportInfo;
                    result.FourthParam = attachsList;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetSupplementFireDetail Server Error";
                Log.Error("FireInfoController GetSupplementFireDetail ", ex.ToString());
            }

            return result;
        }

        /// <summary>
        /// 补录--添加火灾接警内容
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateSupplementFireBlCons(BlConsReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_BlCons info = new F_BlCons()
                    {
                        id = obj.ID,
                        F_fireid = obj.F_FireID,
                        Contens = obj.Contens,
                        CreateTime = obj.CreateTime,
                    };
                    int addOrUpdateID = 0;

                    //更新
                    if (info.id > 0)
                    {
                        addOrUpdateID = bll.UpdateFireBlCons(info);
                    }
                    //增加
                    else
                    {
                        addOrUpdateID = bll.AddFireBlCons(info);
                    }
                    result.FirstParam = addOrUpdateID;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateSupplementFireBlCons Server Error";
                Log.Error("FireInfoController AddOrUpdateSupplementFireBlCons ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  补录--删除火灾接警内容
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveSupplementFireBlCons(RemoveBlConsReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = bll.DeleteFireBlCons(obj.ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveSupplementFireBlCons Server Error";
                Log.Error("FireInfoController RemoveSupplementFireBlCons ", ex.ToString());
            }
            return result;

        }
        /// <summary>
        /// 补录--更新火灾终报
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> UpdateSupplementFireFinalReport(FireFinalReportReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                //更改火灾信息表状态
                int closeFireId = bll.UpdateFireInfoState(fireInfo.F_FireID, 1);
                if (fireInfo != null && obj != null && obj.Ptime != null && obj.ReporterTime != null)
                {
                    F_FireFinalReport info = new F_FireFinalReport();
                    info.FFR_ID = obj.FFR_ID;
                    info.F_FireID = obj.FireID;
                    info.FFR_Address = obj.Address;
                    info.FFR_Jdate = obj.Jdate;
                    info.FFR_Type = obj.Type;
                    info.FFR_Cont = obj.Content;
                    info.FFR_Fh = obj.Fh;
                    info.FFR_FhPeople = obj.FhPeople;
                    info.FFR_Com = obj.Com;
                    info.FFR_ComPeople = obj.ComPeople;
                    info.FFR_Ranks = obj.Ranks;
                    info.FFR_Mas = obj.Mas;
                    info.FFR_Leader = obj.Leader;
                    info.FFR_Ptime = obj.Ptime.Value;
                    info.FFR_Zf = obj.Zf;
                    info.FFR_Zpeople = obj.Zpeople;
                    info.FFR_BZf = obj.BZf;
                    info.FFR_LocalP = obj.LocalP;
                    info.FFR_Weather = obj.Weather;
                    info.FFR_Wind = obj.Wind;
                    info.FFR_Direc = obj.Direc;
                    info.FFR_Temp = obj.Temp;
                    info.FFR_Damp = obj.Damp;
                    info.FFR_Garea = obj.Garea;
                    info.FFR_Gunit = obj.Gunit;
                    info.FFR_Larea = obj.Larea;
                    info.FFR_Lunit = obj.Lunit;
                    info.FFR_Trees = obj.Trees;
                    info.FFR_Age = obj.Age;
                    info.FFR_Num = obj.Num;
                    info.FFR_Die = obj.Die;
                    info.FFR_Why = obj.Why;
                    info.FFR_WhyID = obj.WhyID;
                    info.FFR_Loss = obj.Loss;
                    info.FFR_Other = obj.Other;
                    info.FFR_Reporter = obj.Reporter;
                    info.FFR_ReporterTime = obj.ReporterTime.Value;
                    info.FFR_FireTypeID = obj.FireTypeID;
                    info.FFR_FireType = obj.FireType;
                    info.FFR_TransUser = obj.TransUser;
                    info.FFR_TransDatetime = DateTime.Now;
                    info.FFR_TransIP = CheckRequest.GetWebClientIp();
                    int fid = bll.UpdateFireFinalReport(info);
                    result.FirstParam = fid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController UpdateSupplementFireFinalReport Server Error";
                Log.Error("FireInfoController UpdateSupplementFireFinalReport  ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 补录--历史火灾PDF
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult SupplementHistoryFireDetailByPDF(FireDetailReq obj)
        {
            PDFHelper pdf = new PDFHelper();
            try
            {
                if (obj != null)
                {
                    //火灾详情
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    //接警内容
                    List<F_BlCons> tempBLCList = bll.GetFireBlCons(obj.FireID);
                    var blcList = tempBLCList.Select(s => new
                    {
                        ID = s.id,
                        F_FireID = s.F_fireid,
                        Contens = s.Contens,
                        CreateTime = s.CreateTime,
                    });
                    //火灾终报
                    F_FireFinalReport finalReportInfo = bll.FireFinalReportDetail(obj.FireID);
                    //火灾类型 起火原因等集合
                    List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(0);



                    #region PDF
                    string historyFirePath = PDF_PATH + "/HistoryFire";
                    if (!Directory.Exists(historyFirePath))
                    {
                        Directory.CreateDirectory(historyFirePath);
                    }
                    historyFirePath = historyFirePath + "/" + fireInfo.F_FireName + "_历史详情" + ".pdf";

                    //2.设置字体

                    pdf.Open(new FileStream(historyFirePath, FileMode.Create));
                    pdf.SetBaseFont(@"C:\Windows\Fonts\simkai.ttf");

                    Font title_font = new Font(pdf.basefont, 23.5f, Font.UNDEFINED);
                    Font content_font = new Font(pdf.basefont, 10.0f, Font.UNDEFINED);

                    Paragraph par = new Paragraph();
                    PdfPTable table = new PdfPTable(1);


                    if (fireInfo != null)
                    {
                        #region 火灾录入
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾录入"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 1, 1);

                        #region 留空位置

                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 第一行
                        table = pdf.CreateTable(6, 90);
                        PdfPCell cell = pdf.CreateCell("来电人电话:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_ReporterPhone, content_font, 3, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell("姓名/工号:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_ReporterName + "/" + fireInfo.F_ReporterWorknum, content_font, 0, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第二行
                        cell = pdf.CreateCell("事发地点:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_Address, content_font, 2, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell("事发时间：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_HappenDatetime.Value.ToString(), content_font, 2, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第三行
                        cell = pdf.CreateCell("来电人单位：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_ReporterUnit, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第四行
                        cell = pdf.CreateCell("接警人:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_Alarm, content_font, 2, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell("接警时间：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_AlarmTime.Value.ToString(), content_font, 2, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第五行
                        cell = pdf.CreateCell("接警内容：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_AlarmContent, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        pdf.AddTable(table);
                        #endregion
                    }

                    if (blcList != null && blcList.Count()> 0)
                    {
                        #region  接警内容
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "接警内容"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);

                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(2, 90);
                        table.SetWidths(new int[] { 25, 75 });

                        PdfPCell cell = pdf.CreateCell("操作时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("接警内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        #endregion

                        foreach (var item in blcList)
                        {
                            cell = pdf.CreateCell(item.CreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss"), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.Contens, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                        pdf.AddTable(table);

                        #endregion
                    }

                    if (finalReportInfo != null)
                    {
                        #region  火灾终报
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾终报"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 事发地点
                        table = pdf.CreateTable(4, 90);
                        PdfPCell cell = pdf.CreateCell("事发地点:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(fireInfo.F_Address, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 接警时间与方式
                        cell = pdf.CreateCell("接警时间:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(fireInfo.F_AlarmTime.Value.ToString(), content_font, 0, 30.0f);
                        table.AddCell(cell);


                        cell = pdf.CreateCell("报警方式:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        string ffrTypeName = dtbList.Where(p => p.id == finalReportInfo.FFR_Type).FirstOrDefault().DName;
                        cell = pdf.CreateCell(ffrTypeName, content_font, 0, 30.0f);
                        table.AddCell(cell);

                        #endregion

                        #region 扑救力量
                        cell = pdf.CreateCell("扑救力量:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(string.Format("专业扑火队 {0} 支 {1} 人，半专业扑火队 {2} 人，当地干部群众 {3} 人"
                                                              , finalReportInfo.FFR_Fh, finalReportInfo.FFR_FhPeople
                                                              , finalReportInfo.FFR_Zpeople, finalReportInfo.FFR_LocalP), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 扑灭时间
                        cell = pdf.CreateCell("扑灭时间:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Ptime.ToString(), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 火场留有人员
                        cell = pdf.CreateCell("火场留有人员:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(string.Format("专业森林消防队：{0} 支 共 {1} 人调动驻区部队：{2} 支 共 {3} 人乡镇扑火队伍： {4} 人 群众队伍：{5}人"
                                                         , finalReportInfo.FFR_Fh, finalReportInfo.FFR_FhPeople
                                                         , finalReportInfo.FFR_Com, finalReportInfo.FFR_ComPeople
                                                         , finalReportInfo.FFR_Ranks, finalReportInfo.FFR_Mas), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 天气情况
                        cell = pdf.CreateCell("天气情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Weather, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 初步调查情况
                        cell = pdf.CreateCell("初步调查情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(string.Format("过火面积 {0}{1}，其中有林地面积 {2}{3}，过火树种有 {4} 树龄 {5} 年生，过火树木 {6} 株，死亡树木 {7} 株"
                                              , finalReportInfo.FFR_Garea, finalReportInfo.FFR_Gunit
                                              , finalReportInfo.FFR_Larea, finalReportInfo.FFR_Lunit
                                              , finalReportInfo.FFR_Trees, finalReportInfo.FFR_Age
                                              , finalReportInfo.FFR_Num, finalReportInfo.FFR_Die), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 起火原因
                        cell = pdf.CreateCell("起火原因:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Why, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 人员伤亡情况
                        cell = pdf.CreateCell("人员伤亡情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Loss, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 其他情况
                        cell = pdf.CreateCell("其他情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Other, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 报告人
                        cell = pdf.CreateCell("报告人:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Reporter, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 报告时间
                        cell = pdf.CreateCell("报告人:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_ReporterTime.ToString(), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        pdf.AddTable(table);

                        #endregion
                    }

                    #endregion

                    pdf.Close();

                    var browser = String.Empty;
                    if (HttpContext.Current.Request.UserAgent != null)
                    {
                        browser = HttpContext.Current.Request.UserAgent.ToUpper();
                    }
                    HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
                    FileStream fileStream = File.OpenRead(historyFirePath);
                    httpResponseMessage.Content = new StreamContent(fileStream);
                    httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName =
                        browser.Contains("FIREFOX")
                            ? Path.GetFileName(historyFirePath)
                            : HttpUtility.UrlEncode(Path.GetFileName(historyFirePath))
                    };
                    return ResponseMessage(httpResponseMessage);
                }
                else
                {
                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
                }
            }
            catch (Exception ex)
            {
                pdf.Close();
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                return ResponseMessage(httpResponseMessage);
            }
        }
            
        #endregion

        #region 历史火灾详情
        /// <summary>
        /// 历史火灾详情
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic GetHistoryFireDetail(UnReadAskAnswerReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    //火灾信息
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);
                    //首报
                    F_FireFirstReport ffrInfo = bll.FireFirstReportDetail(obj.FireID);
                    //续报
                    List<F_FireContinueReport> continueReportInfo = bll.GetFireContinueReport(obj.FireID);

                    //火灾转场信息
                    F_FireTransField fftInfo = bll.FireTransFieldDetail(obj.FireID);
                    //定位火点
                    List<F_FireSite> fsList = bll.GetFireSiteInfoByFireID(obj.FireID);
                    //记录文档
                    List<F_FireDocumentation> fdList = bll.GetFireDocumentation(obj.FireID);

                    List<int> fdTransUserIds = fdList.Where(p=>!string.IsNullOrEmpty(p.FD_TransUser))
                                                     .Select(s => s.FD_TransUser).ToList().Select<string, int>(s =>Convert.ToInt32(s)).ToList();
                    List<UsersInfos>  fdUserInfosList=userInfoBll.GetUserInfos(fdTransUserIds);

                    List<dynamic> fdresList = new List<dynamic>();
                    for (int i = 0; i < fdList.Count; i++)
                    {
                       var temp=fdUserInfosList.Where(p => p.ID == Convert.ToInt32(fdList[i].FD_TransUser)).FirstOrDefault();
                        fdresList.Add(new {
                            FD_ID = fdList[i].FD_ID,
                            FD_MessageTime = fdList[i].FD_MessageTime,
                            FD_MessageContent = fdList[i].FD_MessageContent,
                            FD_TransUser = temp != null ? temp.UserName : string.Empty
                        });
                    }

                    //成立前指
                    #region 成立前指 
                    List<F_FrontCommand> list = bll.GetFrontCommand(obj.FireID);
                    List<string> tempList = list.Select(s => s.FC_Member).ToList();
                    List<int> mIdList = new List<int>();
                    for (int i = 0; i < tempList.Count; i++)
                    {
                        string mIDsStr = tempList[i];
                        if (!string.IsNullOrEmpty(mIDsStr))
                        {
                            if (mIDsStr.IndexOf(",") != -1)
                            {
                                string[] mids = mIDsStr.Split(',');
                                for (int j = 0; j < mids.Length; j++)
                                {
                                    mIdList.Add(mids[j].ToInt());
                                }
                            }
                            else
                            {
                                mIdList.Add(mIDsStr.ToInt());
                            }
                        }
                    }
                    mIdList = mIdList.Distinct().ToList();
                    List<F_UnitHeadquarterMember> mList = memberInfoBll.GetUnitHeadquarterMember(mIdList);
                    F_FrontCommand fcInfo = list.FirstOrDefault();
                    if (fcInfo != null)
                    {
                        fcInfo.FC_Member = string.Join(",", mList.Select(s => s.UHM_Leader).ToList());
                    }
                    #endregion
                    //逐级响应
                    F_StepResponse stepResponse = bll.StepResponseDetail(obj.FireID);
                    //扑救力量
                    List<F_FightingForce> ffList = bll.GetFightingForce(obj.FireID);

                    //联动响应
                    #region 联动响应
                    List<F_Response> responseList = bll.GetResponse(obj.FireID);
                    List<dynamic> respnseDepList = new List<dynamic>();

                    foreach (var item in responseList)
                    {
                        if (item != null && !string.IsNullOrEmpty(item.Res_UnitID) && item.Res_UnitID.IndexOf(',') > -1)
                        {
                            List<int> uhd_depIDs = item.Res_UnitID.Split(',').Where(p=>!string.IsNullOrEmpty(p)).Select(s=> Convert.ToInt32(s)).ToList();
                            var members = memberInfoBll.GetUnitHeadquarterDepartment(uhd_depIDs);
                            List<string> deps = members.Select(s => s.UHD_Name).ToList();
                            string deps_Str = string.Join(",", deps);
                            respnseDepList.Add(new { ID = item.Res_ID, DepartMent = deps_Str, HandleTime = item.Res_HandleTime });
                        }
                    }
                    #endregion

                    //火灾误报
                    List<Catalog> catalogInfoList = bll.GetFireCatalog(obj.FireID);
                    //火灾报灭
                    F_ReportOutFire fireReportOutInfo = bll.FireReportOutDetail(obj.FireID);
                    //处置记录
                    var pager = new GridPager2();
                    pager.PageSize = int.MaxValue;
                    pager.PageIndex = 1;
                    int searchLevel = obj.IsCityManager == 0 ? 1 : 0;
                    List<M_Record> rinfoList = bll.MRListByFireID(obj.FireID, pager, searchLevel);
                    //火灾询问与回答
                    var askInfoList = bll.FireAskAndAnswer(obj.FireID);
                    //扑救处置记录
                    List<M_Record> fffInfoList = rinfoList.Where(p => p.MR_Type == (int)RecordType.市局调集扑救力量).OrderBy(o => o.CFA_ID).ToList();

                    //火灾终报
                    F_FireFinalReport finalReportInfo = bll.FireFinalReportDetail(obj.FireID);

                    //火灾类型 起火原因等集合
                    List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(0);

                    //火灾附件
                    List<F_FireAttachs> attachsList = bll.GetFireAttachs(obj.FireID);

                    var data = new
                    {
                        FireInfo = fireInfo,
                        FireFirstReport = ffrInfo,
                        ContinueReport = continueReportInfo,
                        FireTransField = fftInfo,
                        FireSite = fsList,
                        FireDocumentation =obj.IsCityManager==1?fdresList:null,
                        FrontCommand = fcInfo,
                        StepResponse = stepResponse,
                        FightingForce = ffList,
                        FightingForce2 = fffInfoList,
                        Response = respnseDepList,
                        Catalog = catalogInfoList,
                        ReportOutFire = fireReportOutInfo,
                        Ask = askInfoList,
                        FireFinalReport = finalReportInfo,
                        MRecord = rinfoList,
                        DirectorysTb = dtbList,
                        FireAttachs=attachsList,
                    };
                    result.FirstParam = data;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController FireDetail Server Error";
                Log.Error("FireInfoController FireDetail ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 终报 PDF导出
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult FinalReportByPDF(FireDetailReq obj)
        {
            try
            {
                if (obj != null)
                {
                    F_FireInfo info=bll.FireInfoDetail(obj.FireID);
                    F_FireFinalReport finalReportInfo = bll.FireFinalReportDetail(obj.FireID);
                    if (info != null && finalReportInfo != null)
                    {
                        //1.创建位置
                        string finalReportPath = PDF_PATH + "/FinalReport";
                        if (!Directory.Exists(finalReportPath))
                        {
                            Directory.CreateDirectory(finalReportPath);
                        }
                        finalReportPath = finalReportPath + "/" + info.F_FireName + "_终报.pdf";

                        PDFHelper pdf = new PDFHelper();
                        pdf.Open(new FileStream(finalReportPath, FileMode.Create));
                        //2.设置字体
                        pdf.SetBaseFont(@"C:\Windows\Fonts\simkai.ttf");

                        //准备数据
                        string[] ffrAddress_split = finalReportInfo.FFR_Address.Split(',');
                        
                        #region 标题
                        Paragraph par1 = new Paragraph();
                        Font titleLine_font = new Font(pdf.basefont, 21.5f, Font.UNDERLINE);
                        Font title_font = new Font(pdf.basefont, 21.5f, Font.UNDEFINED);
                        //标题动态内容
                        pdf.AddChunkByParagraph(par1, string.Format(" {0} ", ffrAddress_split[0]), titleLine_font);
                        pdf.AddChunkByParagraph(par1, "森林火情报告", title_font);
                        pdf.AddParagraph(par1, 21.5f, 1, 1, 1, 6);
                        #endregion

                        #region 称谓(抬头)
                        pdf.AddParagraph("市森林防火办：", 15.0f, 0, 1, 1, 2);//par2
                        #endregion

                        #region 正文
                        Font contetLine_font = new Font(pdf.basefont, 15.0f, Font.UNDERLINE);
                        Font contet_font = new Font(pdf.basefont, 15.0f, Font.UNDEFINED);

                        Paragraph par3 = new Paragraph();
                        pdf.AddChunkByParagraph(par3, "     ", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", ffrAddress_split[0]), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "森林防火指挥办公室于", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ",finalReportInfo.FFR_Jdate.Value.Year), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "年", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Jdate.Value.Month), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "月", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Jdate.Value.Day), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "日", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Jdate.Value.Hour), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "时", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Jdate.Value.Minute), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "分", contet_font);
                        pdf.AddChunkByParagraph(par3, "，接到", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", info.F_Alarm), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "火情报告", contet_font);
                        pdf.AddChunkByParagraph(par3, "，在", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", ffrAddress_split[1]), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "镇", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", ffrAddress_split[2]), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "村", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", ffrAddress_split[3]), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "(小地名)发生森林火情。区（县）防火办迅速调动专业森林消防队", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_FhPeople), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "人，驻区部队", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_ComPeople), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "人参加扑救救，赶赴现场指挥部扑救的领导有：", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Leader), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "。山火于", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Ptime.Day), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "日", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Ptime.Hour), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "时", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Ptime.Minute), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "分", contet_font);
                        pdf.AddChunkByParagraph(par3, " 被扑灭。火场留有专业扑火队", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Zpeople), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "人、当地群众", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_LocalP), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "人清理余火，看守火场。当地天气情况：", contet_font);
                        pdf.AddChunkByParagraph(par3, string.Format(" {0} ", finalReportInfo.FFR_Weather), contetLine_font);
                        pdf.AddChunkByParagraph(par3, "。", contet_font);
                        pdf.AddParagraph(par3, 15.0f, 0, 1, 1, 2.5f);



                        Paragraph par4 = new Paragraph();
                        pdf.AddChunkByParagraph(par4, "     ", contet_font);
                        pdf.AddChunkByParagraph(par4, "火场初步调查情况：过火面积", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Garea), contetLine_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Gunit), contet_font);
                        pdf.AddChunkByParagraph(par4, "，其中有林地面积", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Larea), contetLine_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Lunit), contet_font);
                        pdf.AddChunkByParagraph(par4, "，过火树种有", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Trees), contetLine_font);
                        pdf.AddChunkByParagraph(par4, " ，树龄", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Age), contetLine_font);
                        pdf.AddChunkByParagraph(par4, "年生，过火树木", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Num), contetLine_font);
                        pdf.AddChunkByParagraph(par4, "株，死亡树木", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Die), contetLine_font);
                        pdf.AddChunkByParagraph(par4, "株；起火原因为", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Why), contetLine_font);
                        pdf.AddChunkByParagraph(par4, "。 人员伤亡情况：", contet_font);
                        pdf.AddChunkByParagraph(par4, string.Format(" {0} ", finalReportInfo.FFR_Loss), contetLine_font);
                        pdf.AddChunkByParagraph(par4, "。", contet_font);

                        pdf.AddParagraph(par4, 15.0f, 0, 1, 1, 2.5f);
                        #endregion

                        #region 署名
                        Paragraph par5 = new Paragraph();
                        string par5string=SundryHelper.StringMiddleAlgorithm(30, string.Format("{0}森林防火指挥部", ffrAddress_split[0]));
                        pdf.AddChunkByParagraph(par5, par5string, contetLine_font);
                        pdf.AddParagraph(par5, 15.0f, 2, 1, 1, 6f);

                        Paragraph par6 = new Paragraph();
                        pdf.AddChunkByParagraph(par6, "报 告 人：", contet_font);

                        string par6string = SundryHelper.StringMiddleAlgorithm(20, finalReportInfo.FFR_Reporter);
                        pdf.AddChunkByParagraph(par6,par6string, contetLine_font);
                        pdf.AddParagraph(par6, 15.0f, 2, 1, 1, 2.5f);

                        Paragraph par7 = new Paragraph();
                        pdf.AddChunkByParagraph(par7, "报告时间：", contet_font);

                        pdf.AddChunkByParagraph(par7, string.Format(" {0} ", finalReportInfo.FFR_ReporterTime.Year), contetLine_font);
                        pdf.AddChunkByParagraph(par7, "年", contet_font);


                        int ffr_pmonth = finalReportInfo.FFR_Ptime.Month;
                        string month = ffr_pmonth > 9 ? ffr_pmonth.ToString() : "0" + ffr_pmonth;

                        int ffr_pday = finalReportInfo.FFR_Ptime.Day;
                        string day = ffr_pday > 9 ? ffr_pday.ToString() : "0" + ffr_pday;


                        pdf.AddChunkByParagraph(par7, string.Format(" {0} ", month), contetLine_font);
                        pdf.AddChunkByParagraph(par7, "月", contet_font);
                        pdf.AddChunkByParagraph(par7, string.Format(" {0} ", day), contetLine_font);
                        pdf.AddChunkByParagraph(par7, "日", contet_font);
                        pdf.AddParagraph(par7, 15.0f, 2, 1, 1, 2.5f);

                        #endregion

                        pdf.Close();


                        var browser = String.Empty;
                        if (HttpContext.Current.Request.UserAgent != null)
                        {
                            browser = HttpContext.Current.Request.UserAgent.ToUpper();
                        }
                        HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
                        FileStream fileStream = File.OpenRead(finalReportPath);
                        httpResponseMessage.Content = new StreamContent(fileStream);
                        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                        {
                            FileName =
                            browser.Contains("FIREFOX")
                                ? Path.GetFileName(finalReportPath)
                                : HttpUtility.UrlEncode(Path.GetFileName(finalReportPath))
                        };
                        return ResponseMessage(httpResponseMessage);
                    }
                    else
                    {
                        return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
                    }

                }
                else
                {
                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                return ResponseMessage(httpResponseMessage);
            }

        }

        /// <summary>
        /// 历史火灾详情 PDF导出
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult HistoryFireDetailByPDF(UnReadAskAnswerReq obj)
        {
            PDFHelper pdf = new PDFHelper();
            try
            {
                if (obj != null)
                {
                    #region 历史火灾所有数据
                    //火灾信息
                    F_FireInfo fireInfo = bll.FireInfoDetail(obj.FireID);

                    //首报
                    F_FireFirstReport ffrInfo = bll.FireFirstReportDetail(obj.FireID);
                    //续报
                    List<F_FireContinueReport> continueReportInfo = bll.GetFireContinueReport(obj.FireID);

                    //火灾转场信息
                    F_FireTransField fftInfo = bll.FireTransFieldDetail(obj.FireID);
                    //定位火点
                    List<F_FireSite> fsList = bll.GetFireSiteInfoByFireID(obj.FireID);
                    //记录文档
                    List<F_FireDocumentation> fdList = bll.GetFireDocumentation(obj.FireID);

                    List<int> fdTransUserIds = fdList.Where(p => !string.IsNullOrEmpty(p.FD_TransUser))
                                                     .Select(s => s.FD_TransUser).ToList().Select<string, int>(s => Convert.ToInt32(s)).ToList();
                    List<UsersInfos> fdUserInfosList = userInfoBll.GetUserInfos(fdTransUserIds);

                    List<F_FireDocumentation> fdresList = new List<F_FireDocumentation>();
                    for (int i = 0; i < fdList.Count; i++)
                    {
                        var temp = fdUserInfosList.Where(p => p.ID == Convert.ToInt32(fdList[i].FD_TransUser)).FirstOrDefault();
                        fdresList.Add(new F_FireDocumentation
                        {
                            FD_ID = fdList[i].FD_ID,
                            FD_MessageTime = fdList[i].FD_MessageTime,
                            FD_MessageContent = fdList[i].FD_MessageContent,
                            FD_TransUser = temp != null ? temp.UserName : string.Empty
                        });
                    }

                    //成立前指
                    #region 成立前指 
                    List<F_FrontCommand> list = bll.GetFrontCommand(obj.FireID);
                    List<string> tempList = list.Select(s => s.FC_Member).ToList();
                    List<int> mIdList = new List<int>();
                    for (int i = 0; i < tempList.Count; i++)
                    {
                        string mIDsStr = tempList[i];
                        if (!string.IsNullOrEmpty(mIDsStr))
                        {
                            if (mIDsStr.IndexOf(",") != -1)
                            {
                                string[] mids = mIDsStr.Split(',');
                                for (int j = 0; j < mids.Length; j++)
                                {
                                    mIdList.Add(mids[j].ToInt());
                                }
                            }
                            else
                            {
                                mIdList.Add(mIDsStr.ToInt());
                            }
                        }
                    }
                    mIdList = mIdList.Distinct().ToList();
                    List<F_UnitHeadquarterMember> mList = memberInfoBll.GetUnitHeadquarterMember(mIdList);
                    F_FrontCommand fcInfo = list.FirstOrDefault();
                    if (fcInfo != null)
                    {
                        fcInfo.FC_Member = string.Join(",", mList.Select(s => s.UHM_Leader).ToList());
                    }
                    #endregion
                    //逐级响应
                    F_StepResponse stepResponse = bll.StepResponseDetail(obj.FireID);
                    //扑救力量
                    List<F_FightingForce> ffList = bll.GetFightingForce(obj.FireID);


                    //联动响应
                    #region 联动响应
                    List<F_Response> responseList = bll.GetResponse(obj.FireID);
                    List<dynamic> respnseDepList = new List<dynamic>();

                    foreach (var item in responseList)
                    {
                        if (item != null && !string.IsNullOrEmpty(item.Res_UnitID) && item.Res_UnitID.IndexOf(',') > -1)
                        {
                            List<int> uhd_depIDs = item.Res_UnitID.Split(',').Where(p => !string.IsNullOrEmpty(p)).Select(s => Convert.ToInt32(s)).ToList();
                            var members = memberInfoBll.GetUnitHeadquarterDepartment(uhd_depIDs);
                            List<string> deps = members.Select(s => s.UHD_Name).ToList();
                            string deps_Str = string.Join(",", deps);
                            respnseDepList.Add(new { ID = item.Res_ID, DepartMent = deps_Str, HandleTime = item.Res_HandleTime });
                        }
                    }
                    #endregion

                    //火灾误报
                    List<Catalog> catalogInfoList = bll.GetFireCatalog(obj.FireID);
                    //火灾报灭
                    F_ReportOutFire fireReportOutInfo = bll.FireReportOutDetail(obj.FireID);
                    //处置记录
                    var pager = new GridPager2();
                    pager.PageSize = int.MaxValue;
                    pager.PageIndex = 1;
                    int searchLevel=obj.IsCityManager==0? 1 : 0;
                    List<M_Record> rinfoList = bll.MRListByFireID(obj.FireID, pager, searchLevel);
                    //火灾询问与回答
                    var askInfoList = bll.FireAskAndAnswer(obj.FireID);
                    //扑救处置记录
                    List<M_Record> fffInfoList = rinfoList.Where(p => p.MR_Type == (int)RecordType.市局调集扑救力量).OrderBy(o => o.CFA_ID).ToList();

                    //火灾终报
                    F_FireFinalReport finalReportInfo = bll.FireFinalReportDetail(obj.FireID);

                    //火灾类型 起火原因等集合
                    List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(0);

                    #endregion

                    #region PDF
                    //1.创建位置
                    string historyFirePath = PDF_PATH + "/HistoryFire";
                    if (!Directory.Exists(historyFirePath))
                    {
                        Directory.CreateDirectory(historyFirePath);
                    }
                    historyFirePath = historyFirePath + "/" + fireInfo.F_FireName +"_历史详情"+ ".pdf";
                   
                    //2.设置字体
                   
                    pdf.Open(new FileStream(historyFirePath, FileMode.Create));
                    pdf.SetBaseFont(@"C:\Windows\Fonts\simkai.ttf");

                    Font title_font = new Font(pdf.basefont, 23.5f, Font.UNDEFINED);
                    Font content_font = new Font(pdf.basefont, 10.0f, Font.UNDEFINED);

                    Paragraph par = new Paragraph();
                    PdfPTable table = new PdfPTable(1);
                    if (fireInfo != null)
                    {
                        #region 火灾录入
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾录入"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 1, 1);

                        #region 留空位置
                       
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 第一行
                        table = pdf.CreateTable(6, 90);
                        PdfPCell cell = pdf.CreateCell("来电人电话:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_ReporterPhone  , content_font, 3, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell("姓名/工号:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_ReporterName+"/"+fireInfo.F_ReporterWorknum, content_font, 0, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第二行
                        cell = pdf.CreateCell("事发地点:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_Address, content_font, 2, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell("事发时间：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_HappenDatetime.Value.ToString(), content_font, 2, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第三行
                        cell = pdf.CreateCell("来电人单位：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_ReporterUnit, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第四行
                        cell = pdf.CreateCell("接警人:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_Alarm, content_font, 2, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell("接警时间：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_AlarmTime.Value.ToString(), content_font, 2, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第五行
                        cell = pdf.CreateCell("接警内容：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_AlarmContent, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第六行
                        cell = pdf.CreateCell("短信：", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_SMSContent, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        pdf.AddTable(table);
                        #endregion
                    }
                    if (ffrInfo != null)
                    {
                        #region 火灾首报
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾首报"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);

                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 第一行
                        table = pdf.CreateTable(6, 90);
                        PdfPCell cell = pdf.CreateCell("事发地点:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireInfo.F_Address, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第二行
                        cell = pdf.CreateCell("首报内容:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(ffrInfo.R_FirstReportContent, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第三行
                        cell = pdf.CreateCell("报告人:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(ffrInfo.R_Applicant, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第四行
                        cell = pdf.CreateCell("报送时间:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(ffrInfo.R_TransDatetime.Value.ToString(), content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        pdf.AddTable(table);
                        #endregion
                    }

                    if (continueReportInfo != null && continueReportInfo.Count > 0)
                    {
                        #region  火灾续报
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾续报"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);

                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5,0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(3, 90);
                        table.SetWidths(new int[] { 35, 35, 30 });

                        PdfPCell cell = pdf.CreateCell("报送内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("报告人", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("报送时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        #endregion

                        foreach (var item in continueReportInfo)
                        {
                            cell = pdf.CreateCell(item.C_ContinueReportContent, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.C_Applicant, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.C_ContinueTime.Value.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                        }

                        pdf.AddTable(table);

                        #endregion

                    }
                    if (fftInfo != null)
                    {
                        #region 火灾转场
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾转场"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);

                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        table = pdf.CreateTable(6, 90);
                        PdfPCell cell = pdf.CreateCell("转出地址:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fftInfo.FT_TransOutAddress, content_font, 2, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell("转入地址:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fftInfo.FT_TransInAddress, content_font, 2, 30.0f);
                        table.AddCell(cell);

                        pdf.AddTable(table);
                        #endregion
                    }
                    if (fsList != null && fsList.Count>0)
                    {
                        #region 定点火位
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "定点火位"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        foreach (var fsInfo in fsList)
                        {
                            table = pdf.CreateTable(6, 90);
                            table.SetWidths(new int[] { 15, 25, 15, 25, 15, 35 });
                            PdfPCell cell = pdf.CreateCell("经度:", content_font, 0, 30.0f);
                            table.AddCell(cell);
                            cell = pdf.CreateCell(SundryHelper.SplitCoordinateName(fsInfo.FS_Longitude), content_font, 0, 30.0f);
                            table.AddCell(cell);
                            cell = pdf.CreateCell("纬度:", content_font, 0, 30.0f);
                            table.AddCell(cell);
                            cell = pdf.CreateCell(SundryHelper.SplitCoordinateName(fsInfo.FS_Latitude), content_font, 0, 30.0f);
                            table.AddCell(cell);
                            cell = pdf.CreateCell("时间:", content_font, 0, 30.0f);
                            table.AddCell(cell);
                            cell = pdf.CreateCell(fsInfo.FS_TransDatetime.ToString(), content_font, 0, 30.0f);
                            table.AddCell(cell);
                            pdf.AddTable(table);
                        }

                        #endregion
                    }

                    if (fdresList != null && fdresList.Count > 0 && obj.IsCityManager ==1)
                    {
                        #region 记录文档
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "记录文档"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);

                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(3, 90);
                        table.SetWidths(new int[] { 40,40,30 });
                        PdfPCell cell = pdf.CreateCell("操作时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = pdf.CreateCell("内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);
                        cell = pdf.CreateCell("记录人", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);
                        #endregion

                        foreach (var item in fdresList)
                        {
                            cell = pdf.CreateCell(item.FD_MessageTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.FD_MessageContent, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.FD_TransUser, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }     
                        pdf.AddTable(table);

                        #endregion
                    }

                    if (fcInfo != null)
                    {
                        #region 成立前指
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "成立前指"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        table = pdf.CreateTable(4, 90);

                        PdfPCell cell = pdf.CreateCell("总指挥:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fcInfo.FC_CommanderName, content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell("电话:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fcInfo.FC_Tel, content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell("现场经度:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(SundryHelper.SplitCoordinateName(fcInfo.FC_LocaleLongitude), content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell("现场纬度:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(SundryHelper.SplitCoordinateName(fcInfo.FC_LocaleLatitude), content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell("前指人员:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fcInfo.FC_Member, content_font, 3, 30.0f);
                        table.AddCell(cell);

                        pdf.AddTable(table);
                        #endregion
                    }


                    if (stepResponse != null)
                    {
                        #region  逐级响应
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "逐级响应"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(5, 90);
                        table.SetWidths(new int[] { 10, 20, 20,20,30 });

                        PdfPCell cell = pdf.CreateCell("响应级别", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("启动时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("启动人", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("批示内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("启动条件", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);


                        #endregion

                        cell = pdf.CreateCell(CheckRequest.GetLoumaNumber(stepResponse.SR_ResponseGrade.Value)+"级", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell(stepResponse.SR_StartDatetime.ToString(), content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell(stepResponse.SR_Starter, content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell(stepResponse.SR_InstructionContent, content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);


                        cell = pdf.CreateCell(stepResponse.SR_StartCondition, content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        pdf.AddTable(table);

                        #endregion
                    }

                    if (fffInfoList != null && fffInfoList.Count > 0)
                    {
                        #region  扑救力量
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "扑救力量"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(3, 90);
                        table.SetWidths(new int[] { 40,30,30 });

                        PdfPCell cell = pdf.CreateCell("扑火单位及队伍", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("调动时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("操作时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        #endregion

                        foreach (var item in fffInfoList)
                        {
                            cell = pdf.CreateCell(item.MR_Content, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.MR_HandleTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.MR_AddTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                        pdf.AddTable(table);

                        #endregion
                    }

                    if (respnseDepList != null && respnseDepList.Count > 0)
                    {
                        #region  联动响应
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "联动响应"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(2, 90);
                        table.SetWidths(new int[] { 70,30 });

                        PdfPCell cell = pdf.CreateCell("调动单位", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("调动时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        #endregion

                        foreach (var item in respnseDepList)
                        {
                            cell = pdf.CreateCell(item.DepartMent, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.HandleTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                        pdf.AddTable(table);

                        #endregion
                    }

                    if (catalogInfoList != null && catalogInfoList.Count > 0)
                    {
                        #region  火灾误报
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾误报"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(4, 90);
                        table.SetWidths(new int[] { 25, 25,25,25 });

                        PdfPCell cell = pdf.CreateCell("报送内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("报送时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);


                        cell = pdf.CreateCell("回复内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("处理结果", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        #endregion

                        foreach (var item in catalogInfoList)
                        {
                            cell = pdf.CreateCell(item.A_FalseReason, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.A_AppTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.A_Reason, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            string results = item.A_Results > 1 ? "同意" : "不同意";
                            cell = pdf.CreateCell(results, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                        pdf.AddTable(table);

                        #endregion
                    }

                    if (fireReportOutInfo != null)
                    {
                        #region 火灾报灭
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾报灭"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);

                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 第一行
                        table = pdf.CreateTable(6, 90);
                        PdfPCell cell = pdf.CreateCell("报灭内容:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireReportOutInfo.FR_ReportoutContent, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第二行
                        cell = pdf.CreateCell("报灭时间:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireReportOutInfo.FR_TransDatetime.Value.ToString(), content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 第三行
                        cell = pdf.CreateCell("报灭人:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(fireReportOutInfo.FR_Applicant, content_font, 5, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        pdf.AddTable(table);
                        #endregion
                    }

                    if (askInfoList != null && askInfoList.Count > 0)
                    {
                        #region  火灾询问
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾询问"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(5, 90);
                       
                        PdfPCell cell = pdf.CreateCell("询问时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("询问内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);


                        cell = pdf.CreateCell("回答内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("回答人", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("回答时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);
                        #endregion

                        foreach (var item in askInfoList)
                        {
                            cell = pdf.CreateCell(item.CFA_HandleTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.CFA_AskContent, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.Cou_AnswerContent, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.Cou_Answerer, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.Cou_HandleTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                        pdf.AddTable(table);

                        #endregion
                    }

                    if (finalReportInfo != null)
                    {
                        #region  火灾终报
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "火灾终报"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 事发地点
                        table = pdf.CreateTable(4, 90);
                        PdfPCell cell = pdf.CreateCell("事发地点:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(fireInfo.F_Address, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 接警时间与方式
                        cell = pdf.CreateCell("接警时间:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(fireInfo.F_AlarmTime.Value.ToString(), content_font, 0, 30.0f);
                        table.AddCell(cell);

                        
                        cell = pdf.CreateCell("报警方式:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        string ffrTypeName = dtbList.Where(p => p.id == finalReportInfo.FFR_Type).FirstOrDefault().DName;
                        cell = pdf.CreateCell(ffrTypeName, content_font, 0, 30.0f);
                        table.AddCell(cell);

                        #endregion

                        #region 火灾报告
                        cell = pdf.CreateCell("火灾报告:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Cont, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 扑救力量
                        cell = pdf.CreateCell("扑救力量:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(string.Format("专业森林消防队：{0} 支 共 {1} 人调动驻区部队：{2} 支 共 {3} 人乡镇扑火队伍： {4} 人 群众队伍：{5}人"
                                                         , finalReportInfo.FFR_Fh, finalReportInfo.FFR_FhPeople
                                                         , finalReportInfo.FFR_Com, finalReportInfo.FFR_ComPeople
                                                         , finalReportInfo.FFR_Ranks, finalReportInfo.FFR_Mas), content_font, 3, 30.0f);

                        table.AddCell(cell);
                        #endregion

                        #region 扑灭时间
                        cell = pdf.CreateCell("扑灭时间:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Ptime.ToString(), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 火场留有人员
                        cell = pdf.CreateCell("火场留有人员:", content_font, 0, 30.0f);
                        table.AddCell(cell);
                        cell = pdf.CreateCell(string.Format("专业扑火队 {0} 支 {1} 人，半专业扑火队 {2} 人，当地干部群众 {3} 人"
                                                              , finalReportInfo.FFR_Zf, finalReportInfo.FFR_Zpeople
                                                              , finalReportInfo.FFR_BZf, finalReportInfo.FFR_LocalP), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 天气情况
                        cell = pdf.CreateCell("天气情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);


                        string weatherStr = finalReportInfo.FFR_Weather+",";
                        weatherStr += "风力:" + finalReportInfo.FFR_Wind + "级,";
                        weatherStr += "风向:"+ finalReportInfo.FFR_Die+",";
                        weatherStr += "温度:" + finalReportInfo.FFR_Temp + "°C,";
                        weatherStr += "湿度:" + finalReportInfo.FFR_Damp + "%";

                        cell = pdf.CreateCell(weatherStr, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 初步调查情况
                        cell = pdf.CreateCell("初步调查情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(string.Format("过火面积 {0}{1}，其中有林地面积 {2}{3}，过火树种有 {4} 树龄 {5} 年生，过火树木 {6} 株，死亡树木 {7} 株"
                                              ,finalReportInfo.FFR_Garea,finalReportInfo.FFR_Gunit
                                              ,finalReportInfo.FFR_Larea,finalReportInfo.FFR_Lunit
                                              ,finalReportInfo.FFR_Trees,finalReportInfo.FFR_Age
                                              ,finalReportInfo.FFR_Num,finalReportInfo.FFR_Die), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 起火原因
                        cell = pdf.CreateCell("起火原因:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Why, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        
                        #region 人员伤亡情况
                        cell = pdf.CreateCell("人员伤亡情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Loss, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 其他情况
                        cell = pdf.CreateCell("其他情况:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Other, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 报告人
                        cell = pdf.CreateCell("报告人:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_Reporter, content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        #region 报告时间
                        cell = pdf.CreateCell("报告人:", content_font, 0, 30.0f);
                        table.AddCell(cell);

                        cell = pdf.CreateCell(finalReportInfo.FFR_ReporterTime.ToString(), content_font, 3, 30.0f);
                        table.AddCell(cell);
                        #endregion

                        pdf.AddTable(table);

                        #endregion
                    }

                    if (rinfoList != null && rinfoList.Count > 0)
                    {
                        #region  处置记录
                        par = new Paragraph();
                        pdf.AddChunkByParagraph(par, string.Format(" {0} ", "处置记录"), title_font);
                        pdf.AddParagraph(par, 23.5f, 1, 1, 30, 1);
                        #region 留空位置
                        table = new PdfPTable(1);
                        pdf.AddCellByTable(table, string.Empty, title_font, 1, 1, 5, 0);
                        pdf.AddTable(table);
                        #endregion

                        #region 标题
                        table = pdf.CreateTable(4, 90);
                        table.SetWidths(new int[] { 20, 20, 25, 35 });

                        PdfPCell cell = pdf.CreateCell("操作时间", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("处置类型", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);


                        cell = pdf.CreateCell("值守人员", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        cell = pdf.CreateCell("内容", content_font, 0, 30.0f);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.AddCell(cell);

                        #endregion

                        foreach (var item in rinfoList)
                        {
                            cell = pdf.CreateCell(item.MR_HandleTime.ToString(), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(Pickout.GetMRecordTypeText(item.MR_Type.Value,false), content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.MR_Applicant, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);

                            cell = pdf.CreateCell(item.MR_Content, content_font, 0, 30.0f);
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                        pdf.AddTable(table);

                        #endregion
                    }


                    #endregion

                    pdf.Close();

                    var browser = String.Empty;
                    if (HttpContext.Current.Request.UserAgent != null)
                    {
                        browser = HttpContext.Current.Request.UserAgent.ToUpper();
                    }
                    HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
                    FileStream fileStream = File.OpenRead(historyFirePath);
                    httpResponseMessage.Content = new StreamContent(fileStream);
                    httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName =
                        browser.Contains("FIREFOX")
                            ? Path.GetFileName(historyFirePath)
                            : HttpUtility.UrlEncode(Path.GetFileName(historyFirePath))
                    };
                    return ResponseMessage(httpResponseMessage);
                }
                else
                {
                    return ResponseMessage(new HttpResponseMessage(HttpStatusCode.Forbidden));
                }
            }
            catch (Exception ex)
            {
                pdf.Close();
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                return ResponseMessage(httpResponseMessage);
            }
           
        }
        #endregion

        #region 统计

        /// <summary>
        /// 统计--单项统计
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public  ResultDto<dynamic,dynamic> SingleStatistics(SingleStatisticsReq obj)
        {
            ResultDto<dynamic,dynamic> result = new ResultDto<dynamic,dynamic>();
            try
            {
                if (obj != null)
                {

                    if (obj.SelectYear > 0)
                    {
                        obj.Years.Add(obj.SelectYear);
                    }
                    obj.Years=obj.Years.Distinct().OrderByDescending(o => o).ToList();

                    string tempStartTimeSplit = "/01/01 00:00:00";
                    string tempEndTimeSplit = "/12/31 23:59:59";
                    if (obj.StartTime == null)
                    {
                        obj.StartTime = (DateTime.Now.Year + tempStartTimeSplit).ToDateTime();
                    }
                    else
                    {
                        obj.StartTime = obj.StartTime.Value.StateTimeConvert();
                    }

                    if (obj.EndTime == null)
                    {
                        obj.EndTime = (DateTime.Now.Year + tempEndTimeSplit).ToDateTime();
                    }
                    else
                    {
                        obj.EndTime = obj.EndTime.Value.EndTimeConvert();
                    }

                    string exclusiveYearStartTime =obj.StartTime.Value.ToString("/MM/dd HH:mm:ss");
                    string exclusiveYearEndime = obj.EndTime.Value.ToString("/MM/dd HH:mm:ss");

                    //事发地点统计
                    if (obj.StatisticsType == 1)
                    {
                        List<dynamic> csList = new List<dynamic>();
                        if (obj.Years == null || obj.Years.Count == 0)
                        {
                            csList.Add(new { Year = DateTime.Now.Year, Data = fireStatisticsBLL.CountyStatistics(obj.StartTime.Value, obj.EndTime.Value) });
                        }
                        else
                        {
                            for (int i = 0; i < obj.Years.Count; i++)
                            { 
                                DateTime tempStartTime = (obj.Years[i] + exclusiveYearStartTime).ToDateTime();
                                DateTime tempEndTime = (obj.Years[i] + exclusiveYearEndime).ToDateTime();
                                csList.Add(new { Year = obj.Years[i], Data = fireStatisticsBLL.CountyStatistics(tempStartTime, tempEndTime) });
                            }
                        }
                        //设置xAxis->data
                        var linList=linchpinBll.GetSubLinchpin(101);//北京
                        string[] linNames = new string[linList==null?0:linList.Count];
                        for (int i = 0; i < linList.Count; i++)
                        {
                            linNames[i] = linList[i].LinchpinName;
                        }

                        result.FirstParam = csList;
                        result.SecondParam = linNames;
                    }
                    //事发时间统计
                    else if (obj.StatisticsType == 2)
                    {
                        List<dynamic> tsList = new List<dynamic>();
                        if (obj.Years == null || obj.Years.Count == 0)
                        {
                            tsList.Add(new { Year = DateTime.Now.Year, Data = fireStatisticsBLL.TimeStatistics(obj.StartTime.Value, obj.EndTime.Value) });
                        }
                        else
                        {
                            for (int i = 0; i < obj.Years.Count; i++)
                            {
                                DateTime tempStartTime = (obj.Years[i] + exclusiveYearStartTime).ToDateTime();
                                DateTime tempEndTime = (obj.Years[i] + exclusiveYearEndime).ToDateTime();
                                tsList.Add(new { Year = obj.Years[i], Data = fireStatisticsBLL.TimeStatistics(tempStartTime, tempEndTime) });
                            }
                        }
                        //设置xAxis->data
                        int[] xAxisTime = new int[24];
                        for (int i = 0; i < xAxisTime.Length; i++)
                        {
                            xAxisTime[i] = i;
                        }
                        result.FirstParam = tsList;
                        result.SecondParam = xAxisTime;
                        
                    }
                    //起火原因
                    else if (obj.StatisticsType == 3)
                    {
                        List<dynamic> fwList = new List<dynamic>();
                        if (obj.Years == null || obj.Years.Count == 0)
                        {
                            fwList.Add(new { Year = DateTime.Now.Year, Data = fireStatisticsBLL.FireWhyStatistics(obj.StartTime.Value, obj.EndTime.Value) });
                        }
                        else
                        {
                            for (int i = 0; i < obj.Years.Count; i++)
                            {
                                DateTime tempStartTime = (obj.Years[i] + exclusiveYearStartTime).ToDateTime();
                                DateTime tempEndTime = (obj.Years[i] + exclusiveYearEndime).ToDateTime();
                                fwList.Add(new { Year = obj.Years[i], Data = fireStatisticsBLL.FireWhyStatistics(tempStartTime, tempEndTime) });
                            }
                        }
                        List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(1);//起火原因

                        result.FirstParam = fwList;
                        result.SecondParam = dtbList;
                    }
                    //报警类型
                    else if (obj.StatisticsType == 4)
                    {
                        List<dynamic> atList = new List<dynamic>();
                        if (obj.Years == null || obj.Years.Count == 0)
                        {
                            atList.Add(new { Year = DateTime.Now.Year, Data = fireStatisticsBLL.AlarmTypeStatistics(obj.StartTime.Value, obj.EndTime.Value) });
                        }
                        else
                        {
                            for (int i = 0; i < obj.Years.Count; i++)
                            {
                                DateTime tempStartTime = (obj.Years[i] + exclusiveYearStartTime).ToDateTime();
                                DateTime tempEndTime = (obj.Years[i] + exclusiveYearEndime).ToDateTime();
                                atList.Add(new { Year = obj.Years[i], Data = fireStatisticsBLL.AlarmTypeStatistics(tempStartTime, tempEndTime) });
                            }
                        }
                        List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(2);//报警方式
                        result.FirstParam = atList;
                        result.SecondParam = dtbList;
                    }
                    //火灾类别
                    else if (obj.StatisticsType == 5)
                    {
                        List<dynamic> fireType = new List<dynamic>();
                        if (obj.Years == null || obj.Years.Count == 0)
                        {
                            fireType.Add(new { Year = DateTime.Now.Year, Data = fireStatisticsBLL.FireTypeStatistics(obj.StartTime.Value, obj.EndTime.Value) });
                        }
                        else
                        {
                            for (int i = 0; i < obj.Years.Count; i++)
                            {
                                DateTime tempStartTime = (obj.Years[i] + exclusiveYearStartTime).ToDateTime();
                                DateTime tempEndTime = (obj.Years[i] + exclusiveYearEndime).ToDateTime();
                                fireType.Add(new { Year = obj.Years[i], Data = fireStatisticsBLL.FireTypeStatistics(tempStartTime, tempEndTime) });
                            }
                        }
                        List<DirectorysTb> dtbList = memberInfoBll.GetDirectorysTb(1023);//火灾类别
                        result.FirstParam = fireType;
                        result.SecondParam = dtbList;
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController SingleStatistics Server Error";
                Log.Error("FireInfoController SingleStatistics ", ex.ToString());
            }
            return result;

        }

        /// <summary>
        /// 统计--汇总统计
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> SummaryStatistics(SummaryStatisticsReq obj)
        {
            ResultDto<dynamic,dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    if (obj.SelectYear > 0)
                    {
                        obj.Years.Add(obj.SelectYear);
                    }
                    obj.Years = obj.Years.Distinct().OrderByDescending(o => o).ToList();

                    string tempStartTimeSplit = "/01/01 00:00:00";
                    string tempEndTimeSplit = "/12/31 23:59:59";
                    if (obj.StartTime == null)
                    {
                        obj.StartTime = (DateTime.Now.Year + tempStartTimeSplit).ToDateTime();
                    }
                    else
                    {
                        obj.StartTime = obj.StartTime.Value.StateTimeConvert();
                    }

                    if (obj.EndTime == null)
                    {
                        obj.EndTime = (DateTime.Now.Year + tempEndTimeSplit).ToDateTime();
                    }
                    else
                    {
                        obj.EndTime = obj.EndTime.Value.EndTimeConvert();
                    }

                    string exclusiveYearStartTime = obj.StartTime.Value.ToString("/MM/dd HH:mm:ss");
                    string exclusiveYearEndime = obj.EndTime.Value.ToString("/MM/dd HH:mm:ss");

                    List<dynamic> summaryList = new List<dynamic>();
                    if (obj.Years == null || obj.Years.Count == 0)
                    {
                        summaryList.Add(new { Year = DateTime.Now.Year, Data = fireStatisticsBLL.SummaryStatistics(obj.StartTime.Value, obj.EndTime.Value) });
                    }
                    else
                    {
                        for (int i = 0; i < obj.Years.Count; i++)
                        {
                            DateTime tempStartTime = (obj.Years[i] + exclusiveYearStartTime).ToDateTime();
                            DateTime tempEndTime = (obj.Years[i] + exclusiveYearEndime).ToDateTime();
                            summaryList.Add(new { Year = obj.Years[i], Data = fireStatisticsBLL.SummaryStatistics(tempStartTime, tempEndTime) });
                        }
                    }

                    //设置xAxis->data
                    var linList = linchpinBll.GetSubLinchpin(101);//北京
                    string[] linNames = new string[linList == null ? 0 : linList.Count];
                    for (int i = 0; i < linList.Count; i++)
                    {
                        linNames[i] = linList[i].LinchpinName;
                    }

                    result.FirstParam =summaryList;
                    result.SecondParam = linNames;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController SingleStatistics Server Error";
                Log.Error("FireInfoController SingleStatistics ", ex.ToString());
            }
            return result;
        }

        #endregion

        #region 其他

        /// <summary>
        /// 其他--根据ID 获取城市信息
        /// </summary>
        /// <param name="obj">obj.LinchpinID</param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetLinchpin(LinchpinReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                int linchpinID = 101;
                if (obj != null && obj.LinchpinID != 0)
                {
                    linchpinID = obj.LinchpinID;
                }

                result.FirstParam = linchpinBll.GetLinchpin(linchpinID);
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetLinchpin Server Error";
                Log.Error("FireInfoController GetLinchpin", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 更改消息状态
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> UpdateAutomaticReplay(AutomaticReplayReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {

                    if (obj.AutomaticID != 0)
                    {
                        if (obj.IsCityManager == 1)
                        {
                            result.FirstParam = bll.UpdateFireAutoMaticToNReplay(obj.AutomaticID, obj.FireID, null);
                        }
                        else
                        {
                            result.FirstParam = bll.UpdateFireAutoMaticToReplay(obj.AutomaticID, obj.FireID, null);
                        }
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController UpdateAutomaticReplay Server Error";
                Log.Error("FireInfoController UpdateAutomaticReplay ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 获取消息列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetAutomatic(AutomaticReplayReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    if (obj.AutomaticID > 0)
                    {
                        result.FirstParam = bll.AutoMaticDetail(obj.AutomaticID);
                    }
                    else
                    {
                        result.FirstParam = bll.GetAutomatic(obj.FireID,obj.IsCityManager);
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetAutomatic Server Error";
                Log.Error("FireInfoController GetAutomatic ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 获取全局消息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetGlobalAutomatic(GlobalAutomaticReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    //由市局权限转变为 市局获取区县数据
                    obj.IsCityManager = obj.IsCityManager == 1 ? 0 : 1;
                    var  mList = bll.GetAutomatic(obj.AddressCounty, obj.MessageTypes, obj.IsCityManager);
                    var fireIds = mList.Select(s => s.F_FireID.Value).ToList();
                    var mrList = bll.MRListByFireIDs(fireIds,0);
                    List<dynamic> list = new List<dynamic>();
                    foreach (var item in mList)
                    {
                        //var mr = mrList.Where(p => p.F_FireID == item.F_FireID).FirstOrDefault();
                        int recordType = 0;
                        switch (item.M_Type.Value)
                        {
                            case (int)AutomaticType.接收火灾:recordType =(int)RecordType.火警录入;break;
                            case (int)AutomaticType.火灾首报: recordType = (int)RecordType.火灾首报; break;
                            case (int)AutomaticType.火灾续报: recordType = (int)RecordType.火灾续报; break;
                            case (int)AutomaticType.申请响应: recordType = (int)RecordType.申请响应; break;
                            case (int)AutomaticType.接收响应: recordType = (int)RecordType.接收响应; break;
                            case (int)AutomaticType.成立前指: recordType = (int)RecordType.成立前指; break;
                            case (int)AutomaticType.调集其他区县扑火队: recordType = (int)RecordType.市局调集扑救力量; break;
                            case (int)AutomaticType.区县报灭: recordType = (int)RecordType.火灾报灭; break;
                            case (int)AutomaticType.区县终报: recordType = (int)RecordType.火灾终报; break;
                            case (int)AutomaticType.误报申请: recordType = (int)RecordType.误报申请; break;
                            case (int)AutomaticType.首报倒计时提示: recordType = (int)RecordType.火灾首报; break;
                            case (int)AutomaticType.续报倒计时提示: recordType = (int)RecordType.火灾续报; break;
                            case (int)AutomaticType.终报倒计时提示: recordType = (int)RecordType.火灾终报; break;
                            case (int)AutomaticType.首报未作提示: recordType = (int)RecordType.首报未按时操作; break;
                            case (int)AutomaticType.续报未作提示: recordType = (int)RecordType.续报未按时操作; break;
                            case (int)AutomaticType.终报未作提示: recordType = (int)RecordType.终报未按时操作; break;
                            case (int)AutomaticType.火灾转场出: recordType = (int)RecordType.火灾转场; break;
                            case (int)AutomaticType.火灾转场入: recordType = (int)RecordType.火灾转场; break;
                            case (int)AutomaticType.定位火点:recordType = (int)RecordType.定位火点; break;
                            case (int)AutomaticType.逐级响应: recordType = (int)RecordType.逐级响应; break;
                            case (int)AutomaticType.火情询问: recordType = (int)RecordType.市局询问; break;
                            case (int)AutomaticType.区县回复: recordType = (int)RecordType.区县回复; break;
                            default:recordType = 0;break;
                        }

                        list.Add(new {
                            item.M_ID,
                            item.M_Content,
                            item.M_DistrictID,
                            item.M_AssembleID,
                            item.M_From,
                            item.M_To,
                            item.M_Type,
                            item.F_FireID,
                            item.M_Receive,
                            item.M_UserID,
                            item.M_HandleTime,
                            item.M_IsFull,
                            item.M_Account,
                            item.M_NUserID,
                            item.M_NHandleTime,
                            item.M_NIsReplay,
                            item.M_IsReplay,
                            item.M_Intime,
                            item.M_DateTime,
                            item.M_User_ID,
                            item.M_IP,
                            MR_Type= recordType,
                        });      
                    }
                    result.FirstParam = list;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetAutomatic Server Error";
                Log.Error("FireInfoController GetAutomatic ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 办公--获取当前大区未完成火灾列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetUnFinishFire(GlobalAutomaticReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    string addressCounty = null;
                    if (obj.AddressCounty >0)
                    {
                        //市局用户获取所有未处理完成的火灾
                        addressCounty = obj.AddressCounty.ToString();
                    }
                    var tempUnFinishFireList = bll.GetFireInfo(addressCounty,0);
                    List<F_FireInfo> ffList = bll.GetFireInfoFromForce(addressCounty, 0);

                    var unFinishFireList = tempUnFinishFireList.GroupBy(g => new { g.F_FireID, g.F_FireName })
                        .Select(s=>new {s.Key.F_FireID,s.Key.F_FireName}).ToList();

                    //本区未完成火灾结果
                    List<dynamic> data = new List<dynamic>();

                    //支援火灾结果
                    List<dynamic> data2 = new List<dynamic>();

                    foreach (var item in unFinishFireList)
                    {
                        data.Add(new { F_FireID = item.F_FireID, F_FireName = item.F_FireName + "流程未完成" });
                    }

                    foreach (var item in ffList)
                    {
                        data2.Add(new { F_FireID = item.F_FireID, F_FireName = item.F_FireName });
                    }
                    result.FirstParam = data;
                    result.SecondParam = data2;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUnFinishFire Server Error";
                Log.Error("FireInfoController GetUnFinishFire ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///  获取未读询问与回复
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetUnReadAskAnswer(UnReadAskAnswerReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    List<AskAnswerModel> aaList = bll.FireAskAndAnswer(obj.FireID);
                    if (obj.IsCityManager == 1)
                    {
                        result.FirstParam = aaList.Where(p => p.Cou_ID > 0 && p.Cou_CityRead == false).ToList();
                    }
                    else
                    {
                        result.FirstParam = aaList.Where(p => p.Cou_ID == 0).ToList();
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUnReadAskAnswer Server Error";
                Log.Error("FireInfoController GetUnReadAskAnswer ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 市局更新回复已读状态
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> UpdateAnswerRead(UpdateAnswerReadReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    int updateRow=bll.UpdateAnswerRead(obj.Cou_ID);
                    result.FirstParam = updateRow;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUnReadAskAnswer Server Error";
                Log.Error("FireInfoController GetUnReadAskAnswer ", ex.ToString());
            }
            return result;
        }



        /// <summary>
        ///  获取最新一条未读消息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetNewAutomatic(AutomaticReplayReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = bll.GetAutomatic(obj.FireID, obj.IsCityManager).OrderBy(o=>o.M_HandleTime).FirstOrDefault();
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetAutomatic Server Error";
                Log.Error("FireInfoController GetAutomatic ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--根据ID 获取下级城市信息集合
        /// </summary>
        /// <param name="obj">obj.LinchpinID</param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetSubLinchpin(LinchpinReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                int linchpinID = 101;//默认北京市
                if (obj != null && obj.LinchpinID != 0)
                {
                    linchpinID = obj.LinchpinID;
                }
                result.FirstParam = linchpinBll.GetSubLinchpin(linchpinID);
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetSubLinchpin Server Error";
                Log.Error("FireInfoController GetSubLinchpin", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--获取单位类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetUnitType()
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                List<F_UnitType> unitTypeList = memberInfoBll.GetUnitType();
                result.FirstParam = unitTypeList;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUnitType Server Error";
                Log.Error("FireInfoController GetUnitType ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--增加或者编辑 单位类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateUnitType(UnitTypeReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {

                F_UnitType unitType = new F_UnitType();
                unitType.UT_ID = obj.ID;
                unitType.UT_TypeName = obj.TypeName;
                unitType.UT_MainLeader = obj.MainLeader;
                unitType.UT_OtherLeader = obj.OtherLeader;
                unitType.UT_Remark = obj.Remark;
                unitType.UT_TransIP = CheckRequest.GetWebClientIp();
                unitType.UT_TransUser = obj.TransUser;
                int addOrUpdateID = 0;

                //更新
                if (unitType.UT_ID > 0)
                {
                    var temp = memberInfoBll.GetUnitType(unitType.UT_ID);
                    if (temp.Count == 0)
                    {
                        result.Status = 0;
                        result.Message = "Request Params :ID Not Exist!!";
                        return result;
                    }
                    addOrUpdateID = memberInfoBll.UpdateUnitType(unitType);
                }
                //增加
                else
                {
                    unitType.UT_TransDatetime = DateTime.Now;
                    addOrUpdateID = memberInfoBll.AddUnitType(unitType);
                }
                result.FirstParam = addOrUpdateID;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateUnitType Server Error";
                Log.Error("FireInfoController AddOrUpdateUnitType ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///  其他--获取单位
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetUnit(UnitSearchReq obj)
        {
            ResultDto<dynamic,dynamic> result = new ResultDto<dynamic,dynamic>();
            try
            {
                GridPager2 pager = Pickout.GetGridPager2(obj);
                List<F_Unit> unitList = memberInfoBll.GetUnitByUnitTypeID(obj.ID,obj.UH_ID,obj.U_ResponseLevel,pager);
                result.FirstParam = unitList;
                result.SecondParam = pager.TotalRows;
                result.Status = 1;
                result.Message = "Request Is Success";

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUnit Server Error";
                Log.Error("FireInfoController GetUnit ", ex.ToString());
            }
            return result;
        }
        
        /// <summary>
        ///  其他--获取单位 并依据单位类型进行分组
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetUnitGroup()
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                GridPager2 pager = Pickout.GetGridPager2(new  GridPager2());
                pager.PageSize = int.MaxValue;
                List<F_UnitType> unitTypeList = memberInfoBll.GetUnitType();
                List<UnitGroupRes> list = new List<UnitGroupRes>();
                for (int i = 0; i < unitTypeList.Count; i++)
                {
                    List<F_Unit> unitList = memberInfoBll.GetUnitByUnitTypeID(unitTypeList[i].UT_ID,0,0, pager);
                    UnitGroupRes res = new UnitGroupRes();
                    res.UT_ID = unitTypeList[i].UT_ID;
                    res.UT_TypeName = unitTypeList[i].UT_TypeName;
                    res.UT_MainLeader= unitTypeList[i].UT_MainLeader;
                    res.UT_OtherLeader = unitTypeList[i].UT_OtherLeader;
                    res.Children = unitList;
                    list.Add(res);
                }
                result.FirstParam = list;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUnitGroup Server Error";
                Log.Error("FireInfoController GetUnitGroup ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--增加或编辑单位
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateUnit(UnitReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                F_Unit unit = new F_Unit() {
                    U_ID = obj.U_ID,
                    U_UnitName = obj.U_UnitName,
                    U_UnitType=obj.U_UnitType,
                    U_OrgID=obj.U_OrgID,
                    U_Phone=obj.U_Phone,
                    U_Address=obj.U_Address,
                    U_Fax=obj.U_Fax,
                    U_Email=obj.U_Email,
                    U_ResponseLevel=obj.U_ResponseLevel,
                    U_SeatLongitude=obj.U_SeatLongitude,
                    U_SeatLatitude=obj.U_SeatLatitude,
                    U_Remark=obj.U_Remark,
                    U_TransIP=CheckRequest.GetWebClientIp(),
                    U_TransUser=obj.U_TransUser,
                    U_TransDatetime=DateTime.Now,
                    U_Total=obj.U_Total,
                    UH_ID =obj.UH_ID,
                    UH_Name=obj.UH_Name,
                    puTypeName=obj.PuTypeName,
                    U_Emergency=obj.U_Emergency,
                    U_Captain=obj.U_Captain,
                    U_Tel = obj.U_Tel,
                };
                int addOrUpdateID = 0;

                //更新
                if (unit.U_ID > 0)
                {
                    GridPager2 pager = Pickout.GetGridPager2(new GridPager2());
                    var temp = memberInfoBll.GetUnit(unit.U_ID, pager);
                    if (temp.Count == 0)
                    {
                        result.Status = 0;
                        result.Message = "Request Params :ID Not Exist!!";
                        return result;
                    }
                    addOrUpdateID = memberInfoBll.UpdateUnit(unit);
                    Log.Info(obj.key.ToString(), "扑火指挥部", "添加扑火队成功！");
                }
                //增加
                else
                {
                    GridPager2 pager = Pickout.GetGridPager2(new GridPager2());
                    var temp = memberInfoBll.GetUnitByName(unit.U_UnitName);
                    if (temp.Count == 0)
                    {
                        unit.U_TransDatetime = DateTime.Now;
                        addOrUpdateID = memberInfoBll.AddUnit(unit);
                        Log.Info(obj.key.ToString(), "扑火指挥部", "更新扑火队成功！");
                    }
                    else
                    {
                        result.Status = 0;
                        result.Message = "Request Params :U_UnitName Is Exist!!";
                        return result;
                    }
                    
                }
                result.FirstParam = addOrUpdateID;
                result.Status = 1;
                result.Message = "Request Is Success";

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateUnit Server Error";
                Log.Error("FireInfoController AddOrUpdateUnit ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--查询联动响应(分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetLinkCorresponding(GridPager2 obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                GridPager2 pager = Pickout.GetGridPager2(obj);
                result.FirstParam=memberInfoBll.LinkCorrespondingByPager(null,pager);
                result.SecondParam = pager.TotalRows;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetLinkCorresponding Server Error";
                Log.Error("FireInfoController GetLinkCorresponding ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--增加或编辑联动响应
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddOrUpdateLinkCorresponding(LinkCorrespondingReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    LinkCorresponding info = new LinkCorresponding()
                    {
                        ID=obj.ID,
                        ContactMen = obj.ContactMen,
                        DepartMent = obj.DepartMent,
                        ContactPhone = obj.ContactPhone,
                        Duty = obj.Duty,
                        MobilePhone = obj.MobilePhone,
                    };

                    if (info.ID > 0)
                    {
                        result.FirstParam = memberInfoBll.UpDateLinkCorresponding(info);
                    }
                    else
                    {
                        result.FirstParam = memberInfoBll.AddLinkCorresponding(info);
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateLinkCorresponding Server Error";
                Log.Error("FireInfoController AddOrUpdateLinkCorresponding ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--移除联动响应（移除依据:ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveLinkCorresponding(LinkCorrespondingReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteLinkCorresponding(obj.ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveLinkCorresponding Server Error";
                Log.Error("FireInfoController RemoveLinkCorresponding ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--查询起火原因 火灾类型 及报警方式 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetDirectory(DirectoryReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    //1.起火原因 2.报警方式 1023.火灾类型
                    List<DirectorysTb> dtbList = memberInfoBll.DirectorysTbByPager(obj.ParentId,obj.Name,pager);
                    result.FirstParam = dtbList;
                    result.SecondParam = pager.TotalRows;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetDirectory Server Error";
                Log.Error("FireInfoController GetDirectory ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他-增加或编辑起火原因 火灾类型 及报警方式 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateDirectory(DirectoryReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {

                    var existEntity = memberInfoBll.GetDirectorysTb(obj.ParentId, obj.Name);
                    if (obj.ID > 0)
                    {
                        if (existEntity != null && existEntity.Where(p=>p.id!=obj.ID).Count() >0)
                        {
                            result.Status = 0;
                            result.Message = "Request Params:Name Is Exist!!!";
                            return result;
                        }
                        int updateRows = memberInfoBll.UpdateDirectorysTb(new DirectorysTb()
                        {
                            DName = obj.Name,
                            DOrder = 0,
                            DParentID = obj.ParentId,
                            id = obj.ID
                        });
                        result.FirstParam = updateRows;
                       
                    }
                    else
                    {
                        if (existEntity != null && existEntity.Count > 0)
                        {
                            //重复
                            result.Status = 2;
                            result.Message = "Request Params:Name Is Exist!!!";
                            return result;
                        }
                        int id = memberInfoBll.AddDirectorysTb(new DirectorysTb
                        {
                            DName = obj.Name,
                            DOrder = 0,
                            DParentID = obj.ParentId,
                        });
                        result.FirstParam = id;
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                    Log.Info(obj.key.ToString(), "数据维护", "数据字典添加成功！");
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddDirectory Server Error";
                Log.Error("FireInfoController AddDirectory ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--移除起火原因 火灾类型 及报警方式 （移除依据:ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveDirectory(DirectoryReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam= memberInfoBll.DeleteDirectorysTb(obj.ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                    Log.Info(obj.key.ToString(), "数据维护", "数据字典移除成功！");
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveDirectory Server Error";
                Log.Error("FireInfoController RemoveDirectory ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  其他--起火原因详细 （查询依据:ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetDirectoryDetail(DirectoryReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam=memberInfoBll.DirectorysTbDetail(obj.ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController DirectoryDetail Server Error";
                Log.Error("FireInfoController DirectoryDetail ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--数据防火指挥部列表（分页）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetHeadquarter(HeadquarterSearchReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    List<F_UnitHeadquarter> uhTemp =memberInfoBll.UintHeadquarterByPager(obj.UH_ParentID,obj.UH_Name, obj.UH_Level,obj.UH_Leader,obj.UH_LinchpinID,pager);

                    List<dynamic> uhList = new List<dynamic>();

                    //获取正在进行中的火灾
                    var fireInfos= bll.GetFireInfo(null, 0);

                    var fireIDs = fireInfos.Select(s => new int?(s.F_FireID)).ToList();

                    var  ffInfos= bll.GetFightingForce(fireIDs);


                    foreach (var item in uhTemp)
                    {
                        string levelName = item.UH_Level != null && item.UH_Level.Value > 0 ? CheckRequest.GetNumToChinese(item.UH_Level.Value) : string.Empty;
                        if (!string.IsNullOrEmpty(levelName))
                        {
                            levelName += "级分类";
                        }
                        bool isMobilize = false;//是否被正在其他火灾被调集中，0:无调集 1:正在被调集
                        int? mobilizeFireID = 0;
                        string mobilizeFireName = string.Empty;
                        var ffInfo = ffInfos.Where(p => p.FF_UH_ID == item.UH_ID).FirstOrDefault();
                        if (ffInfo != null)
                        {
                            isMobilize = true;
                            mobilizeFireID = ffInfo.F_FireID;
                            mobilizeFireName = fireInfos.Where(p => p.F_FireID == mobilizeFireID).FirstOrDefault()?.F_FireName;
                        }
                        uhList.Add(new {
                            UH_ID = item.UH_ID,
                            UH_Leader = item.UH_Leader,
                            UH_Phone = item.UH_Phone,
                            UH_Name = item.UH_Name,
                            UH_Sort = item.UH_Sort,
                            UH_Remarks = item.UH_Remarks,
                            UH_ParentId = item.UH_ParentId,
                            UH_TransDatetime = DateTime.Now,
                            UH_TransUser = item.UH_TransUser,
                            UH_Level = item.UH_Level,
                            UH_LevelName = levelName,
                            UH_LinchpinID = item.UH_LinchpinID,
                            IsMobilizing= isMobilize, 
                            MobilizeFireID= mobilizeFireID,
                            MobilizeFireName= mobilizeFireName,
                        });  
                    }
                    result.FirstParam = uhList;
                    result.Status = 1;
                    result.SecondParam = pager.TotalRows;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetHeadquarter Server Error";
                Log.Error("FireInfoController GetHeadquarter ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--增加或编辑 防火指挥部相关
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateHeadquarter(HeadquarterReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_UnitHeadquarter info = new F_UnitHeadquarter() {
                        UH_ID = obj.UH_ID,
                        UH_Leader = obj.UH_Leader,
                        UH_Phone = obj.UH_Phone,
                        UH_Name = obj.UH_Name,
                        UH_Sort = obj.UH_Sort,
                        UH_Remarks = obj.UH_Remarks,
                        UH_ParentId = obj.UH_ParentId,
                        UH_TransIP = CheckRequest.GetWebClientIp(),
                        UH_TransDatetime=DateTime.Now,
                        UH_TransUser=obj.UH_TransUser,
                        UH_Level=obj.UH_Level,
                        UH_LinchpinID=obj.UH_LinchpinID
                    };
                    var  exist=memberInfoBll.GetUnitHeadquarter(info.UH_Name);
                    if (info.UH_ID > 0)
                    {
                        if (exist.Count >= 2)
                        {
                            return new ResultDto<int>()
                            {
                                Status = 2,
                                Message = "UH_Name Is Exist"
                            };
                        }
                        result.FirstParam = memberInfoBll.UpDateUintHeadquarter(info);
                        Log.Info(obj.key.ToString(), "扑火指挥部", "更新指挥部成功！");
                    }
                    else {
                        if (exist.Count >= 1)
                        {
                            return new ResultDto<int>()
                            {
                                Status = 2,
                                Message = "UH_Name Is Exist"
                            };
                        }
                        result.FirstParam = memberInfoBll.AddUintHeadquarter(info);
                        Log.Info(obj.key.ToString(), "扑火指挥部", "添加指挥部成功！");
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateHeadquarter Server Error";
                Log.Error("FireInfoController AddOrUpdateHeadquarter ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--移除防火指挥部相关（移除依据:UH_ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveHeadquarter(HeadquarterReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteUintHeadquarter(obj.UH_ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                    Log.Info(obj.key.ToString(), "扑火指挥部", "删除指挥部成功！");
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveHeadquarter Server Error";
                Log.Error("FireInfoController RemoveHeadquarter ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  其他--1级响应列表（分页）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetGradeTemplate(GradeTemplateSearchReq obj)
        {
            ResultDto<dynamic,dynamic> result = new ResultDto<dynamic,dynamic>();
            try
            {
                if (obj != null)
                {
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    result.FirstParam = memberInfoBll.GradeTemplateByPager(null, pager);
                    result.Status = 1;
                    result.SecondParam = pager.TotalRows;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetGradeTemplate Server Error";
                Log.Error("FireInfoController GetGradeTemplate ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--增加或修改 一级响应
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateGradeTemplate(GradeTemplateReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    OneGradeTemplate info = new OneGradeTemplate()
                    {
                        ID = obj.ID,
                        pUnitID = obj.PUnitID,
                        pUnitName = obj.PUnitName,
                        Leader = obj.Leader,
                        MobilePhone = obj.MobilePhone,
                        Phone = obj.Phone,
                        pUnitTypeID=obj.PUnitTypeID,
                        pUnitTypeName=obj.PUnitTypeName,
                    };

                    if (info.ID > 0)
                    {
                        result.FirstParam = memberInfoBll.UpDateGradeTemplate(info);
                        Log.Info(obj.key.ToString(), "响应管理", "更新响应成功！");
                    }
                    else
                    {
                        result.FirstParam = memberInfoBll.AddGradeTemplate(info);
                        Log.Info(obj.key.ToString(), "响应管理", "添加响应成功！");
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateGradeTemplate Server Error";
                Log.Error("FireInfoController AddOrUpdateGradeTemplate ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--移除一级响应（移除依据:ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveGradeTemplate(GradeTemplateReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteGradeTemplate(obj.ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveHeadquarter Server Error";
                Log.Error("FireInfoController RemoveHeadquarter ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--2,3级响应列表（分页）
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetOtherGradeTemplate(OtherGradeTemplateSearchReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                GridPager2 pager = Pickout.GetGridPager2(obj);

                result.FirstParam = memberInfoBll.OtherGradeTemplateByPager(
                                        new ThreeAndTwoGradeTemplate()
                                         { sGradeID=obj.SGradeID, LinchpinIID=obj.LinchpinIID}, pager);
                result.Status = 1;
                result.SecondParam = pager.TotalRows;
                result.Message = "Request Is Success";

            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetOtherGrade Server Error";
                Log.Error("FireInfoController GetOtherGrade ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--增加或编辑2,3级响应
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddOrUpdateOtherGradeTemplate(OtherGradeTemplateReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    ThreeAndTwoGradeTemplate info = new ThreeAndTwoGradeTemplate()
                    {
                          ID=obj.ID,
                          FireResourceList=obj.FireResourceList,
                          LinchpinIID=obj.LinchpinIID,
                          LinchpinIName=obj.LinchpinIName,
                          FireResourceListID=obj.FireResourceListID,
                          sGradeID=obj.sGradeID,
                    };
                    if (info.ID > 0)
                    {
                        //判定大区内是否已有响应扑火队
                        var exist = memberInfoBll.GetOtherGradeTemplate(info.sGradeID.Value, obj.LinchpinIID);
                        if (exist == null || exist.Count == 0)
                        {
                            result.Status = -1;
                            result.Message = "Request Params:LinchpinIID Is Empty!!! ";
                            return result;
                        }
                        result.FirstParam = memberInfoBll.UpDateOtherGradeTemplate(info);
                        Log.Info(obj.key.ToString(), "响应管理", "更新响应成功！");
                    }
                    else
                    {
                        result.FirstParam = memberInfoBll.AddOtherGradeTemplate(info);
                        Log.Info(obj.key.ToString(), "响应管理", "添加响应成功！");
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOtherGrade Server Error";
                Log.Error("FireInfoController AddOtherGrade ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--移除2,3级响应（移除依据:ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveOtherGradeTemplate(OtherGradeTemplateReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteOtherGradeTemplate(obj.ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                    Log.Info(obj.key.ToString(), "响应管理", "删除响应成功！");
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveHeadquarter Server Error";
                Log.Error("FireInfoController RemoveHeadquarter ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--单位管理(分页)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> UnitByPager(UnitPagerSearchReq obj)
        {
            ResultDto<dynamic,dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    result.FirstParam = memberInfoBll.UnitByPager(new F_Unit() { U_UnitType=obj.UnitTypeID.ToString() },pager);
                    result.SecondParam = pager.TotalRows;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController UnitByPager Server Error";
                Log.Error("FireInfoController UnitByPager ", ex.ToString());
            }
            return result;

        }

        /// <summary>
        ///其他--移除单位管理
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveUnit(UnitReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteUnit(obj.U_ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                    Log.Info(obj.key.ToString(), "扑火指挥部", "删除扑火队成功！");
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveUnit Server Error";
                Log.Error("FireInfoController RemoveUnit ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  其他--单位成员（分页）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetUnitMember(UnitMemberSearchReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                if (obj != null)
                {
                    GridPager2 pager = Pickout.GetGridPager2(obj);
                    result.FirstParam=memberInfoBll.UnitMemberByPager(obj.UnitID,obj.Phone,obj.Name, pager);
                    result.SecondParam = pager.TotalRows;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUnitMember Server Error";
                Log.Error("FireInfoController GetUnitMember ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--增加或编辑单位成员
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddOrUpdateUnitMember(UnitMemberReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    F_UnitMember info = new F_UnitMember()
                    {
                        UM_ID = obj.UM_ID,
                        UM_MemberName = obj.UM_MemberName,
                        UM_MemberSex = obj.UM_MemberSex,
                        UM_Birthday = obj.UM_Birthday,
                        UM_Nation = obj.UM_Nation,
                        UM_Tel=obj.UM_Tel,
                        UM_Unit = obj.UM_Unit,
                        pUnitName = obj.pUnitName,
                        UM_Post = obj.UM_Post,
                        UM_Phone = obj.UM_Phone,
                        UM_Pic = obj.UM_Pic,
                        UM_Address = obj.UM_Address,
                        UM_Email = obj.UM_Email,
                        UM_Remark = obj.UM_Remark,
                        UM_TransDatetime = DateTime.Now,
                        UM_TransIP = CheckRequest.GetWebClientIp(),
                        UM_TransUser = obj.UM_TransUser
                    };

                    if (info.UM_ID > 0)
                    {
                        result.FirstParam = memberInfoBll.UpdateUnitMember(info);
                    }
                    else
                    {
                        result.FirstParam = memberInfoBll.AddUnitMember(info);
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOtherGrade Server Error";
                Log.Error("FireInfoController AddOtherGrade ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///其他--移除单位成员 （移除依据:UM_ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveUnitMember(UnitMemberReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteUnitMember(obj.UM_ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveUnitMember Server Error";
                Log.Error("FireInfoController RemoveUnitMember ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--获取所有通讯录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetSmsPhone()
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                //办公系统内部短信用户
                List<ChuanYe.Models.Model.View_SmsPhone> SmsPhones = m_bll.GetSmsPhone();
                List<Departments> AllDepartments = m_bll.GetAllDepartments();
                var first = from p in SmsPhones
                           select new
                           {
                               ID = p.UserID,
                               UserName = p.UserName,
                               UserPhone = p.Phone,
                               DepID = p.DepID,
                               DepName = AllDepartments.Where(d => d.ID == p.DepID).FirstOrDefault().DepName
                           };
                //外部短信用户
                OutsideUserInfoSearch Search = new OutsideUserInfoSearch();
                GridPager Pager = new GridPager();
                Pager.page =1;
                Pager.rows =int.MaxValue;
                int totalRows = 0;
                var second = userInfoBll.OutsideUserInfosSelect(Search, Pager, ref totalRows);

                result.FirstParam = first.ToList();
                result.SecondParam = second;
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveUnitMember Server Error";
                Log.Error("FireInfoController RemoveUnitMember ", ex.ToString());
            }

            return result;
        }


        /// <summary>
        /// 其他--获取指挥部成员列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic,dynamic> GetHeadquarterMember(UnitHeadquarterMemberSearchReq obj)
        {
            ResultDto<dynamic,dynamic> result = new ResultDto<dynamic,dynamic>();
            try
            {
                GridPager2 pager = Pickout.GetGridPager2(obj);
                var temp = new F_UnitHeadquarterMember()
                {
                    UH_ID =obj.UH_ID==null?0:obj.UH_ID.Value,
                    UHM_Leader = obj.UHM_Leader
                };
                result.FirstParam = memberInfoBll.UintHeadquarterMemberByPager(temp, pager);
                result.SecondParam = pager.TotalRows;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUintHeadquarterMember Server Error";
                Log.Error("FireInfoController GetUintHeadquarterMember ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--增加或修改 指挥部成员
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateHeadquarterMember(UnitHeadquarterMemberReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_UnitHeadquarterMember info = new F_UnitHeadquarterMember
                    {
                        UHM_ID=obj.UHM_ID,
                        UHM_Leader = obj.UHM_Leader,
                        UHM_DepID = obj.UHM_DepID,
                        UHM_DepName = obj.UHM_DepName,
                        UHM_Post=obj.UHM_Post,
                        UHM_Phone = obj.UHM_Phone,
                        UHM_Tel = obj.UHM_Tel,
                        UHM_LiaisonName = obj.UHM_LiaisonName,
                        UHM_LiaisonPhone = obj.UHM_LiaisonPhone,
                        UHM_LiaisonTel = obj.UHM_LiaisonTel,
                        UHM_LiaisonPost=obj.UHM_LiaisonPost,
                        UH_ID = obj.UH_ID,
                        UH_Name = obj.UH_Name,
                        UHM_TransTime = DateTime.Now,
                        UHM_TransUser = obj.UHM_TransUser,
                        UHM_TransIP =CheckRequest.GetWebClientIp(),
                    };
                    var exist = memberInfoBll.GetUnitHeadquarterMember(info.UHM_Leader);
                    if (info.UHM_ID > 0)
                    {
                        if (exist.Count >= 2)
                        {
                            return new ResultDto<int>()
                            {
                                Status = 2,
                                Message = "UH_Name Is Exist"
                            };
                        }

                        result.FirstParam = memberInfoBll.UpdateUintHeadquarterMember(info);
                    }
                    else
                    {
                        if (exist.Count >= 1)
                        {
                            return new ResultDto<int>()
                            {
                                Status = 2,
                                Message = "UH_Name Is Exist"
                            };
                        }
                        result.FirstParam = memberInfoBll.AddUintHeadquarterMember(info);
                    }
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateUintHeadquarterMember Server Error";
                Log.Error("FireInfoController AddOrUpdateUintHeadquarterMember ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///其他--移除指挥部成员（移除依据:UHM_ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveHeadquarterMember(UnitHeadquarterMemberReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteUintHeadquarterMember(obj.UHM_ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveUintHeadquarterMember Server Error";
                Log.Error("FireInfoController RemoveUintHeadquarterMember ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--指挥部单位
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> AddOrUpdateHeadquarterDepartment(UnitHeadquarterDepartmentReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {
                    F_UnitHeadquarterDepartment info = new F_UnitHeadquarterDepartment()
                    {
                        UHD_ID=obj.UHD_ID,
                        UHD_Name=obj.UHD_Name,
                        UHD_Remarks=obj.UHD_Remarks,
                        UHD_Sort=obj.UHD_Sort,
                        UHD_TransDatetime=DateTime.Now,
                        UHD_TransIP=CheckRequest.GetWebClientIp(),
                        UHD_TransUser=obj.UHD_TransUser,
                    };
                    var exist = memberInfoBll.GetUnitHeadquarterDepartment(info.UHD_Name);
                    if (info.UHD_ID > 0)
                    {
                        if (exist.Count >= 2)
                        {
                            return new ResultDto<int>() {
                                Status = 2,
                                Message = "UHD_Name Is Exist"
                            };
                        }
                        result.FirstParam = memberInfoBll.UpdateUnitHeadquarterDepartment(info);
                        Log.Info(obj.key.ToString(), "扑火指挥部", "更新人员信息成功！");
                    }
                    else
                    {
                        if (exist.Count >= 1)
                        {
                            return new ResultDto<int>()
                            {
                                Status = 2,
                                Message = "UHD_Name Is Exist"
                            };
                        }
                        result.FirstParam = memberInfoBll.AddUnitHeadquarterDepartment(info);
                        Log.Info(obj.key.ToString(), "扑火指挥部", "添加人员信息成功！");
                    }

                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddOrUpdateHeadquarterDepartment Server Error";
                Log.Error("FireInfoController AddOrUpdateHeadquarterDepartment ", ex.ToString());
            }
            return result;

        }


        /// <summary>
        /// 其他--获取指挥部单位列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic, dynamic> GetHeadquarterDepartment(UnitHeadquarterDepartmentSearchReq obj)
        {
            ResultDto<dynamic, dynamic> result = new ResultDto<dynamic, dynamic>();
            try
            {
                GridPager2 pager = Pickout.GetGridPager2(obj);
                var temp = new F_UnitHeadquarterDepartment()
                {
                     UHD_Name=obj.UHD_Name
                };

                
                result.FirstParam = memberInfoBll.UnitHeadquarterDepartmentByPager(temp, pager);
                result.SecondParam = pager.TotalRows;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetHeadquarterDepartment Server Error";
                Log.Error("FireInfoController GetHeadquarterDepartment ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///其他--移除指挥部单位（移除依据:UHD_ID）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> RemoveHeadquarterDepartment(UnitHeadquarterDepartmentReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.FirstParam = memberInfoBll.DeleteUnitHeadquarterDepartment(obj.UHD_ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                    Log.Info(obj.key.ToString(), "扑火指挥部", "删除成员单位成功！");
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveHeadquarterDepartment Server Error";
                Log.Error("FireInfoController RemoveHeadquarterDepartment ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 根据手机号获取用户名
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetUserNameByPhone(UerNamePhoneReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null && !string.IsNullOrEmpty(obj.Phone))
                {
                    var list = bll.GetFireReporterInfo(obj.Phone);
                    result.FirstParam = list;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetUserNameByPhone Server Error";
                Log.Error("FireInfoController GetUserNameByPhone ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--增加火灾附件
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddFireAttachs(FireAttachsReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    F_FireAttachs info = new F_FireAttachs()
                    {
                        F_FireID = obj.FireID,
                        FileName = obj.FileName,
                        FilePath = obj.FilePath,
                        AddTime = DateTime.Now,
                        Type=obj.Type
                    };
                    int aid = bll.AddFireAttachs(info);
                    result.FirstParam = aid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireAttachs Server Error";
                Log.Error("FireInfoController AddFireAttachs ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///  其他--获取火灾附件
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetFireAttachs(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {

                if (obj != null)
                {
                    List<F_FireAttachs> list = bll.GetFireAttachs(obj.FireID);
                    foreach (var item in list)
                    {
                        item.FilePath = VIDEO_PATH + item.FilePath;
                    }
                    result.FirstParam = list;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireAttachs Server Error";
                Log.Error("FireInfoController GetFireAttachs ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///  其他--获取火灾附件
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDtoBase RemoveFireAttachs(RemoveFireAttachsReq obj)
        {
            ResultDtoBase result = new ResultDtoBase();
            try
            {

                if (obj != null)
                {
                    bll.RemoveFireAttachs(obj.ID);
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController RemoveFireAttachs Server Error";
                Log.Error("FireInfoController RemoveFireAttachs ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--增加火灾视频
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddFireVideo(FireAttachsReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    F_FireAttachs info = new F_FireAttachs() {
                         F_FireID=obj.FireID,
                         FileName=obj.FileName,
                         FilePath=obj.FilePath,
                         AddTime=DateTime.Now
                    };
                    int aid=bll.AddFireAttachs(info);
                    result.FirstParam = aid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireAttachs Server Error";
                Log.Error("FireInfoController AddFireAttachs ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  其他--获取火灾视频
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetFireVideo(FireDetailReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                
                if (obj != null)
                {
                    List<F_FireAttachs> list = bll.GetFireAttachs(obj.FireID);
                    foreach (var item in list)
                    {
                        item.FilePath = VIDEO_PATH + item.FilePath;
                    }
                    result.FirstParam = list;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireAttachs Server Error";
                Log.Error("FireInfoController GetFireAttachs ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 其他--获取短信用户列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetSMSUser(SMSUsersReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                List<View_SmsPhone> data = new List<View_SmsPhone>();
                int selectType = obj == null || Convert.ToInt32(obj.SelectType) == 0 ? 1 : Convert.ToInt32(obj.SelectType);
                List<U_OutsideUserInfos> outList = userInfoBll.OutsideUserInfosSelect(selectType);
                data = s_bll.GetLeader();
                for (int i = 0; i < outList.Count; i++)
                {
                    data.Add(new View_SmsPhone()
                    {
                        Phone = outList[i].UserPhone,
                        UserName = outList[i].UserName,
                        UserID = 1000000 + outList[i].ID
                    });
                }
                result.FirstParam = result;
                result.Status = 1;
                result.Message = "Request Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetSMSUsers Server Error";
                Log.Error("FireInfoController GetSMSUsers ", ex.ToString());
            }
            return result;
        }
        #endregion


        /// <summary>
        /// 其他--增加火灾处置进度标绘
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddFireRecordPlot(FireRecordPlotReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    var fireInfo = bll.FireInfoDetail(obj.F_FireID);
                    var recordTypeName = Pickout.GetMRecordTypeText(obj.MR_Type);


                    F_FireRecordPlot info = new F_FireRecordPlot()
                    {
                        F_FireID = obj.F_FireID,
                        F_FireName = fireInfo.F_FireName,
                        MR_ID = obj.MR_ID,
                        MR_Type = obj.MR_Type,
                        F_TransUser = obj.F_TransUser,
                        MR_TypeName = recordTypeName,
                        PlotContent = obj.PlotContent,
                        PlotDateTime = DateTime.Now,
                        PlotID = obj.PlotID,
                        PlotType = obj.PlotType, 
                    };
                    int fid = bll.AddFireRecordPlot(info);
                    result.FirstParam = fid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireAttachs Server Error";
                Log.Error("FireInfoController AddFireAttachs ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        ///  其他--获取火灾处置进度标绘
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetFireRecordPlot(FireRecordPlotSearchReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    var list = bll.GetFireRecordPlot(obj.MR_ID,obj.F_FireID,obj.MR_Type);
                    result.FirstParam = list;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController GetFireRecordPlot Server Error";
                Log.Error("FireInfoController GetFireRecordPlot ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--增加火灾告警记录
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddFireAlarmRecord(AddFireAlarmRecordReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    var linchpins= linchpinBll.GetSubLinchpin(101);
                    var linchpinID = 0L;
                    //推算区县
                    foreach (var item in linchpins)
                    {
                        var truncateName=item.LinchpinName.TrimEnd('区');
                        if (obj.AddressName.Contains(truncateName))
                        {
                            linchpinID = item.LinchpinIID;
                            break;
                        }
                    }
                    F_AlarmRecord info = new F_AlarmRecord()
                    {
                        AlarmType = obj.AlarmType,
                        AR_Content = obj.AR_Content,
                        AuditState = obj.AuditState==null?0:obj.AuditState,
                        HorizontalAngle = obj.HorizontalAngle,
                        LocaID = obj.LocaID,
                        PitchAngle = obj.PitchAngle,
                        UnitID = obj.UnitID,
                        UnitName = obj.UnitName,
                        LocaName = obj.LocaName,
                        LPFileName = obj.LPFileName,
                        AlarmTypeName = obj.AlarmTypeName,
                        AddTime = obj.AddTime.HasValue ? obj.AddTime.Value : DateTime.Now,
                        AddressCounty = linchpinID.ToString()
                    };
                    int fid = bll.AddAlarmRecord(info);
                    if (fid > 0)
                    {
                        //定时忽略规则
                        var showRule= bll.GetAlarmRecordConfig(DateTime.Now,info.LocaID,info.UnitID);
                        if (showRule.Count == 0)
                        {
                            if (linchpinID > 0)
                            {
                                //给所在 区县发送
                                Pickout.SendAlarmMessage((int)linchpinID, info.AR_ID, info.AR_Content, info.LocaID, info.UnitID, 0);
                            }
                            // 给市局发送
                            Pickout.SendAlarmMessage(-1, info.AR_ID, info.AR_Content, info.LocaID, info.UnitID, 1);
                        }
                    }

                    result.FirstParam = fid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireAlarmRecord Server Error";
                Log.Error("FireInfoController AddFireAlarmRecord ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--编辑火灾告警
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDtoBase ChangeFireAlarmRecordState(ChangeFireAlarmRecordStateReq obj)
        {
            ResultDtoBase result = new ResultDtoBase();
            try
            {
                if (obj != null)
                {
                    var info = bll.GetAlarmRecord(obj.AR_ID);
                    if (info != null)
                    {
                        bll.ChangeFireAlarmRecordState(obj.FireID, obj.AR_ID);
                        var fireInfo=bll.FireInfoDetail(obj.FireID);
                        string message = "此告警已由{0}{1}生成火灾:{2}";
                        if (obj.IsCityManager == 0)
                        {
                            //发送给市局
                            message = string.Format(message,"市局领导:",fireInfo.F_Alarm, fireInfo.F_FireName);
                            Pickout.SendAlarmMessage(-1, info.AR_ID, message, info.LocaID, info.UnitID, 1, 1);
                        }
                        else 
                        {
                            //发送给区县
                            if (SundryHelper.IsNumeric(info.AddressCounty))
                            {
                                var linchpin = linchpinBll.GetLinchpin(info.AddressCounty.ToInt());
                                message = string.Format(message, linchpin.LinchpinName+":", fireInfo.F_Alarm, fireInfo.F_FireName);
                            }
                            else 
                            {
                                message = string.Format(message, "区县:", fireInfo.F_Alarm, fireInfo.F_FireName);
                            }
                            Pickout.SendAlarmMessage(info.AddressCounty.ToInt(), info.AR_ID, message, info.LocaID, info.UnitID, 1,0);
                        }
                    }

                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController ChangeFireAlarmRecordState Server Error";
                Log.Error("FireInfoController ChangeFireAlarmRecordState ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 其他--增加火灾告警记录配置项
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> AddFireAlarmRecordConfig(AddAlarmRecordConfigReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    F_AlarmRecordConfig info = new F_AlarmRecordConfig()
                    {
                        LocaID = obj.LocaID,
                        UnitID = obj.UnitID,
                        StartTime = obj.StartTime==null?DateTime.Now:obj.StartTime.Value,
                        Value = obj.Value,
                    };
                    int fid = bll.AddAlarmRecordConfig(info);
                    result.FirstParam = fid;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController AddFireAlarmRecordConfig Server Error";
                Log.Error("FireInfoController AddFireAlarmRecordConfig ", ex.ToString());
            }
            return result;
        }


        #region  Demo
        /// <summary>
        /// 接口模板
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> Demo(dynamic obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "FireInfoController Demo Server Error";
                Log.Error("FireInfoController Demo ", ex.ToString());
            }
            return result;
        }

        #endregion
    }
}
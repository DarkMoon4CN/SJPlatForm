﻿using ChuanYe.Commom;
using ChuanYe.IBLL;
using ChuanYe.Models;
using Microsoft.Practices.Unity;
using SJPlatForm.Web.Models;
using SJPlatForm.Web.Models.Other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data;
namespace SJPlatForm.Web.Controllers
{

    /// <summary>
    /// 其他辅助性接口
    /// </summary>
    [EnableCors("*", "*", "*")]
    public class OtherController : ApiController
    {
        /// <summary>
        /// 消息
        /// </summary>
        [Dependency]
        public ICarouselFigureBLL carouselFigureBLL { get; set; }


        /// <summary>
        /// 火灾处置主业务块
        /// </summary>
        [Dependency]
        public IFireInfoBLL fireInfoBll { get; set; }


        /// <summary>
        /// 批量修改轮播图
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<int> UpdateCarouselFigure(UpdateCarouselFigureReq obj)
        {
            ResultDto<int> result = new ResultDto<int>();
            try
            {
                if (obj != null)
                {

                    if (obj.List == null)
                    {
                        result.Status = 0;
                        result.Message = "Missing Required Parameters";
                        return result;
                    }
                    int updateRow = 0;
                    foreach (var item in obj.List)
                    {
                        CarouselFigure info = new CarouselFigure()
                        {
                            CF_ID = item.CF_ID,
                            CF_Name = item.CF_Name,
                            CF_CreateTime = DateTime.Now,
                            CF_ImgPath = item.CF_ImgPath,
                            CF_IsEnable = item.CF_IsEnable,
                        };

                        updateRow += carouselFigureBLL.UpdateCarouselFigure(info);
                    }
                    result.FirstParam = updateRow;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                    Log.Info(obj.key.ToString(), "轮播管理", "更新轮播成功！");
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "OtherController UpdateCarouselFigure Server Error";
                Log.Error("OtherController AddCarouselFigure ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        ///  获取轮播列表
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDto<dynamic> GetCarouselFigure(CarouselFigureSearchReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                if (obj != null)
                {
                    var list=carouselFigureBLL.GetCarouselFigureAll(obj.SearchType);
                    result.FirstParam = list;
                    result.Status = 1;
                    result.Message = "Request Is Success";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "OtherController GetCarouselFigure Server Error";
                Log.Error("OtherController GetCarouselFigure ", ex.ToString());
            }
            return result;
        }

        /// <summary>
        /// 增加视频记录 Post
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultDtoBase AddFireVideoInfo(FireVideoInfoReq obj)
        {
            ResultDtoBase result = new ResultDtoBase();
            try
            {
                if (obj != null)
                {

                    result.Status = 1;
                    result.Message = "Request Is Success";
                    Log.Debug("OtherController", obj.SerializeJSON());
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "OtherController AddFireVideoInfo Server Error";
                Log.Error("OtherController AddFireVideoInfo ", ex.ToString());
            }
            return result;
        }


        /// <summary>
        /// 增加视频记录Get  
        /// </summary>
        /// <param name="param">火灾ID_是否是市局状态(1或0)_文件名</param>
        /// <returns></returns>
        [HttpGet]
        public ResultDtoBase TempAddFireVideoInfo(string param)
        {
            ResultDtoBase result = new ResultDtoBase();
            try
            {
                if (!string.IsNullOrEmpty(param))
                {
                    if (param.IndexOf("_") > -1)
                    {
                        string[] strSplit = param.Split('_');
                        string fireID = strSplit[0];
                        string isCityManager = strSplit[1];

                        string videoName = string.Empty;
                        if (strSplit.Length > 3)
                        {
                            for (int i = 2; i < strSplit.Length; i++)
                            {
                                videoName += strSplit[i]+"_";
                            }

                            videoName = videoName.Substring(0, videoName.Length - 1);
                        }
                        else
                        {
                            videoName = strSplit[2];
                        }
                       
                        FireVideoInfoReq obj = new FireVideoInfoReq();
                        obj.FireID = fireID.ToInt();
                        obj.IsCityManager = isCityManager.ToInt();
                        obj.VideoName = videoName;
                        F_FireAttachs fa = new F_FireAttachs()
                        {
                             F_FireID=obj.FireID,
                             FileName=videoName,
                             FilePath= videoName,
                             AddTime=DateTime.Now
                        };
                        int faId= fireInfoBll.AddFireAttachs(fa);
                        result.Status = 1;
                        result.Message = "Request Is Success";
                    }
                }
                else
                {
                    result.Status = 0;
                    result.Message = "Missing Required Parameters";
                }
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "OtherController TempAddFireVideoInfo Server Error";
                Log.Error("OtherController TempAddFireVideoInfo ", ex.ToString());
            }
            return result;
        }



    }
}

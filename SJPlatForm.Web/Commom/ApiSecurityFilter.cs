﻿using System.Web.Http.Filters;
using Microsoft.Practices.Unity;
using ChuanYe.IBLL;
using System;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using ChuanYe.Models;
using ChuanYe.Commom;
using System.Collections.Generic;
using ChuanYe.DAL;
using ChuanYe.Models.Model;
using System.Net.Http;
using System.Net;
using System.Linq;
using ChuanYe.Utils;
using ChuanYe.Commom.DEncrypt;
using System.Data;
namespace SJPlatForm.Web.Common
{
    public class ApiSecurityFilter: ActionFilterAttribute
    {
        [Dependency]
        public IUsersInfosBLL m_bll { get; set; }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            try
            {
                var request = actionContext.Request;
                string method = request.Method.Method;
                string staffid = string.Empty;
                string timestamp = string.Empty;
                string nonce = string.Empty;
                string signature = string.Empty;
                string blackbox = string.Empty;
                string key = string.Empty;
                //根据请求类型拼接参数
                NameValueCollection form = HttpContext.Current.Request.QueryString;

                Stream stream = HttpContext.Current.Request.InputStream;
                string responseJson = string.Empty;
                StreamReader streamReader = new StreamReader(stream);
                string data = streamReader.ReadToEnd();
                blackboxModel t = new blackboxModel();
                if (string.IsNullOrEmpty(data))
                {
                    Log.Attack("异常代码：401.6");
                    Log.Attack("请求接口地址："+ HttpContext.Current.Request.RawUrl);
                    Log.Attack("请求参数：" + data);
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, "401.6");
                    return;
                }
                t = JsonHelper.DeserializeJsonToObject<blackboxModel>(data);
                blackbox = t.blackbox;
                key = t.key;
                LoginUserModel loginInfo = (LoginUserModel)SundryHelper.GetCache<LoginUserModel>(key);
                if (loginInfo == null)
                {
                       Log.Attack("用户未登录！");
                       actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, "401.1");
                       return;
                }
                if (string.IsNullOrEmpty(blackbox))
                {
                    Log.Attack("缺少blackbox信息！");
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, "403.2");
                    return;
                }
                string TokenInfo = DESEncrypt.DecryptByAES(blackbox, loginInfo.AES_KEY, ConfigHelper.AES_IV);
                if (TokenInfo.IndexOf(",") == -1)
                {
                    Log.Attack("验证戳核对错误："+ TokenInfo);
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, "403.4");
                    return;
                }
                string[] TokenInfoArr = TokenInfo.Split(',');
                int LoginID = Convert.ToInt32(TokenInfoArr[0]);
                UsersInfosDAL ub = new UsersInfosDAL();
                if (!ub.IsIDRight(LoginID))
                {
                    Log.Attack("错误的Token：" + TokenInfoArr[0]);
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, "403.3");
                    return;
                }
            }
            catch (Exception ex)
            {
                Log.Attack("请求验证出错：" + ex.ToString());
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, "403.7");
                return;
            }
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
        }

        public class blackboxModel
        {
            public string blackbox { set; get; }
            public string key { set; get; }
        }
    }
}
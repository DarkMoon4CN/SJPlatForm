﻿using ChuanYe.Commom.DEncrypt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace SJPlatForm.Web.Common
{
    public class ConfigHelper
    {
        //解密串
        public static string AES_IV = "9999988888123456";
        public static string AES_KEY = ConfigHelper.GetAES_KEY();

        //网站地址
        public static string Host = EncryptForConfig.DecryptByAES(ConfigurationManager.AppSettings["Host"].ToString());

        //公文超时时间
        public static int DocumentInfosTime = Convert.ToInt32(EncryptForConfig.DecryptByAES(ConfigurationManager.AppSettings["DocumentInfosTime"].ToString()));

        //开始时间
        public static DateTime StateTime = Convert.ToDateTime("1970-1-1");

        public static string GetAES_KEY()
        {
            Random ran = new Random();
            StringBuilder AES_KEY = new StringBuilder();
            for (int i = 0; i < 4; i++)
            {
                AES_KEY.Append(ran.Next(10000000, 99999999).ToString());
            }
            return AES_KEY.ToString();
        }
    }
}